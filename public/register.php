<?php
?>
<!DOCTYPE html>
<html>
<head>
        <title>AppointSync - Register</title>


    <?php

    require ("objectHeader.php");

    ?>




</head>
<body data-spy="scroll" data-target="#mainmenu">

  <style>
  button {
        margin-top: 20px;
        background: -webkit-linear-gradient(left, #000000 0%, #1193d4 0%, #000000 100%);
        border: none;
        font-size: 1.6em;
        font-weight: 300;
        padding: 5px 0;
        width: 35%;
        border-radius: 3px;

        &:hover {
          -webkit-animation: hop 2s;
          animation: hop 2s;
        }
      }

      .float {
  display: inline-block;
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
  -webkit-transition-property: transform;
  transition-property: transform;
  -webkit-transform: translateZ(0);
  transform: translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
}

.float:hover, .float:focus, .float:active {
  -webkit-transform: translateY(-3px);
  transform: translateY(-3px);
}

.price-table{

 box-shadow: 5px 10px rgba(0,0,0,0.6);
 border:1px solid;

}

  </style>

  <header class="blue-bg relative fix" id="home">
      <div class="section-bg overlay-bg angle-bg ripple">
          <video autoplay muted id="video-background" loop>
              <source src="http://intimissibd.com/video/video-2.webm" type="video/webm">
          </video>
      </div>
      <!--Mainmenu-->
      <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
          <div class="container">
              <div class="navbar-header">
                  <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a href="#" class="navbar-brand">
                      <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                      <!-- AppointSyncLogo.png <h2 class="text-white logo-text">AppointSync</h2>-->
                  </a>
              </div>
              <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                  <ul class="nav navbar-nav">
                      <li><a href="/appointsync.com/public/home">Home</a></li>
                      <li><a href="/appointsync.com/public/#work">Work</a></li>
                      <li><a href="/appointsync.com/public/#feature">Features</a></li>
                    <!--  <li><a href="#team">Team</a></li> -->
                    <!--  <li><a href="#client">Client</a></li> -->
                      <li><a href="/appointsync.com/public/#price">Pricing</a></li>
                    <!--  <li><a href="#blog">Blog</a></li> -->
                      <li><a href="/appointsync.com/public/#contact">Contact</a></li>
                      <li><a href="/appointsync.com/public/login.php">Login</a></li>
                      <li class="active"><a href="/appointsync.com/public/register">Register</a></li>
                  </ul>
              </div>
          </div>
      </nav>
      <!--Mainmenu/-->
      <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
      <!--Header-Text-->

      <!--Header-Text/-->
  </header>



<div style="width:90%;margin:15% auto;text-align: center;">


  <div class="row">
      <div class="col-xs-12 col-sm-4 wow fadeInLeft">
          <div class="panel price-table text-center">
              <h3 class="text-uppercase price-title">AppointSync</h3>
              <hr>
              <div class="space-30"></div>
              <ul class="list-unstyled">
                  <li><strong class="amount"><span class="big">Free</span></strong></li>
                  <li>Add Hosts</li>
                  <li>Request Appointments</li>
                  <li>Sync your Appointments</li>
                  <li>Add notes and images to appointments</li>
              </ul>
              <div class="space-30"></div>
              <hr>
              <a href="/appointsync.com/public/clientregister.php" class="btn btn-link text-uppercase">Register</a>
          </div>
      </div>
      <div class="col-xs-12 col-sm-4 wow flipInY">
          <div class="panel price-table text-center">
              <h3 class="text-uppercase price-title">AppointSync Pro</h3>
              <hr>
              <div class="space-30"></div>
              <ul class="list-unstyled">
                  <li><strong class="amount">&#36; <span class="big">9</span></strong>.99/Month</li>
                  <li>Add Clients</li>
                  <li>Give Appointments</li>
                  <li>Sync your Appointments</li>
                  <li>keep all your clients in sync</li>
                  <li>Add notes and images to appointments</li>
                  <li>manage and adjust all appointments in one click</li>
              </ul>
              <div class="space-30"></div>
              <hr>
              <a href="/appointsync.com/public/hostregister.php" class="btn btn-link text-uppercase">Register</a>
          </div>
      </div>
      <div class="col-xs-12 col-sm-4 wow fadeInRight">
          <div class="panel price-table text-center">
              <h3 class="text-uppercase price-title">AppointSync Pro Premium</h3>
              <hr>
              <div class="space-30"></div>
              <ul class="list-unstyled">
                  <li><strong class="amount">&#36; <span class="big">14</span></strong>.99/Month</li>
                  <li>Add Clients</li>
                  <li>offline Clients</li>
                  <li>Give Appointments</li>
                  <li>Sync your Appointments</li>
                  <li>keep all your clients in sync</li>
                  <li>Add notes and images to appointments</li>
                  <li>manage and adjust all appointments in one click</li>
              </ul>
              <div class="space-30"></div>
              <hr>
              <a href="/appointsync.com/public/hostregisterpremium.php" class="btn btn-link text-uppercase">Register</a>
          </div>
      </div>
  </div>




</div>

  <?php

  require ("objectFooter.php");

  ?>


</body>
</html>
