<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$getURL = $_SERVER["PATH_INFO"];
$sendUserWebID = explode("/", $getURL)[1];
$webID = $sendUserWebID;

$submittedPin = $_POST['pin'];
$submitPinHashed = hash('sha512', $submittedPin);
$_SESSION['hiddenFilePin'] = $submitPinHashed;

header("Location: /appointsync.com/public/clientfile.php/".$webID);