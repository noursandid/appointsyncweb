<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedPage.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedElement.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedElementKeyValue.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (isset($_SESSION['user'])) {
} else {
    header("Location: /appointsync.com/");
}


$_SESSION['pageName'] = 'filePages';

$getURL = $_SERVER["PATH_INFO"];
$sendUserWebID = explode("/", $getURL)[1];
$webID = $sendUserWebID;

$clientResponse = APIController::getClient($sendUserWebID);
if($clientResponse->success){
    $client = $clientResponse->data;
}else{
//    header("Location: /appointsync.com/public/clients.php"); exit();
}

$webID = $sendUserWebID;
$isFound = false;
if ($client->webID == $webID) {
    $isFound = true;

}
if (!$isFound) {
//    header("Location: /appointsync.com/public/clients.php");
    exit();
}


$hostResponse = APIController::getProfile();
if ($hostResponse->success) {
    $host = $hostResponse->data;
} else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}

if($host->hiddenFiles == '0') {
    $getFileTemplate = APIController::getFileTemplate($webID, null);
    if ($getFileTemplate->success) {
        $fileTemplate = $getFileTemplate->data;
    } elseif ($getFileTemplate->errorCode == 353700) {
        $getFileTemplate = [];
    } else {
//        header("Location: /appointsync.com/public/clients.php");
//        exit();
    }

}elseif($host->hiddenFiles == '1'){
    if(!isset($_SESSION['hiddenFilePin'])){   header("Location: /appointsync.com/public/clients.php"); exit();}
    $getFileTemplate = APIController::getFileTemplate($webID, $_SESSION['hiddenFilePin']);
    if ($getFileTemplate->success) {
        $fileTemplate = $getFileTemplate->data;
    } elseif ($getFileTemplate->errorCode == 353700) {
        $getFileTemplate = [];
    } else {
//        header("Location: /appointsync.com/public/clients.php");
//        exit();
    }

}



?>

<!DOCTYPE html>
<html>
<head>

    <title>AppointSync - Pages</title>

    <?php

    require ("objectHeader.php");

    ?>

</head>

<style>
    #userProfile {
        width: 80%;
    }

    .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus,
    .navbar-default .navbar-nav > .open > a:hover {
        color: #555;
        background-color: red !important;
    }

    .profileClass {
        width: 100%;
        margin: 0px auto;
    }

    #profilePicture {
        width: 50%;
        float: left;
        min-width: 300px;
        margin: 0px auto;
    }

    #profileInfo {
        width: 50%;
        float: left;
        padding-top: 2%;
        min-width: 300px;
    }

    .profileClass .button1 {
        width: 50% !important;
        max-width: 50% !important;
    }

    .profileClass .button3 {
        width: 50% !important;
        max-width: 50% !important;
    }



    @media only screen and (max-width: 1050px) {
        .clientAppointmentObject {
            margin: 1% auto;
            float: none;
        }

    }
    @media only screen and (max-width: 650px) {
        #profilePicture {
            float: none;
        }

        #profileInfo {
            margin: 0 auto;
            float: none;
        }

        .modal-body {
            width: 100%;
            margin: 0 auto;
        }

        .clientAppointmentObject {
            margin: 1% auto;
            float: none;
        }
    }

    #btnRequestAppointment {
        min-width: 200px !important;
    }

    .clientAppointmentObject a {
        cursor: pointer;
        font-style: normal;
    }

    .clientAppointmentObject {
        cursor: pointer;
    }
</style>

<body style='width: 100%;'>

<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">

    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                        src="/appointsync.com/public/img/newlogo.png"
                        style="min-width: 150px; width: 150px; margin-top: -5%;"
                        alt="Logo">
                </a>
            </div>

            <?php

            require ("objectMenu.php");

            ?>
        </div>
    </nav>
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
</header>

<div style="margin-top:7%;"></div>
<?php
        if(isset($fileTemplate)) {
            echo '<div class="pull-right form-inline" style="width: 25%;">',
            '<button class="buttonAddClient buttonAddClient2" id="back">Back</button>',
            '</div>';
        }
?>
    <br/><div class="clientFilePagesHeader">

        <?php
        $pageCounter = 0;
        if(isset($fileTemplate)){
        foreach ($fileTemplate as $templateData){
            $pageCounter += 1;
            echo'<a href="/appointsync.com/public/clientfile.php/'.$webID.'?'.$templateData->webID.'"><div class="pagesView">',
            '<h3>File <br/><br/>'.$pageCounter.'</h3>',
            '</div></a>';
        }
        }

        ?>

    <a href="#" onclick="addFilePage();"> <div class="newPageView">

        <img src="../img/addFile.png">
        <h3>New File</h3>

        </div></a>

    </div>



<?php

require ("objectFooter.php");

?>

<script>

    var back = document.getElementById("back");

    back.onclick = function () {
        location.href = "/appointsync.com/public/clientfile.php/<?php echo $webID;?>";
    };

    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");

    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;


    window.onclick = function(event) {
        if (event.target == modalViewNotificationPopUpgg) {
            modalViewNotificationPopUpgg.style.display = "none";
            $('body').css('overflow','auto');
        }
    }

    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });

    function addFilePage(){
        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'addFilePage', 'clientWebID':'<?php echo $webID;?>'},
            async: false,
            success: function($data){
                location.href = "/appointsync.com/public/clientfile.php/<?php echo $webID;?>";
                // console.log($data);
            }
        });

    // });

    }

</script>

</body>
</html>
