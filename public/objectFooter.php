
<div class="space-50"></div>

<footer class="black-bg" style=" bottom: 0; left: 0; right: 0;">
    <div class="container">


        <div class="space-80"></div>
        <div class="row text-white wow fadeIn">
            <div class="col-xs-12 text-center">
                <div class="social-menu">
                    <a href="#"><span class="ti-facebook"></span></a> <a href="#"><span
                                class="ti-twitter-alt"></span></a> <a href="#"><span
                                class="ti-linkedin"></span></a> <a href="#"><span
                                class="ti-pinterest-alt"></span></a>
                </div>
                <div class="space-20"></div>
                <p>
                    @ 2018 <a href="http://AppointSync.com/">AppointSync</a> all right
                    reserved.
                </p>
            </div>
        </div>
        <div class="space-20"></div>
    </div>
</footer>

<script src="/appointsync.com/public/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/appointsync.com/public/moments.js"></script>
<!--  -->

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>-->


<!--    <script type="text/javascript" src="/bower_components/jquery/jquery.min.js"></script>-->
<script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">


<script src="/appointsync.com/public/js/vendor/bootstrap.min.js"></script>
<!--Plugin JS-->
<script src="/appointsync.com/public/js/owl.carousel.min.js"></script>
<script src="/appointsync.com/public/js/scrollUp.min.js"></script>
<script src="/appointsync.com/public/js/magnific-popup.min.js"></script>
<script src="/appointsync.com/public/js/ripples-min.js"></script>
<script src="/appointsync.com/public/js/contact-form.js"></script>
<script src="/appointsync.com/public/js/spectragram.min.js"></script>
<script src="/appointsync.com/public/js/ajaxchimp.js"></script>
<script src="/appointsync.com/public/js/wow.min.js"></script>
<script src="/appointsync.com/public/js/plugins.js"></script>
<!--Active JS-->
<script src="/appointsync.com/public/js/main.js"></script>