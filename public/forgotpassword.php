<?php

$getURL = $_SERVER["PATH_INFO"];
$PasswordToken = explode("/", $getURL)[1];

include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';

$fileController = new MappedRouterController();
?>

<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Reset Password</title>

    <?php

    require ("objectHeader.php");

    echo $PasswordToken;

    ?>

</head>

<style>

    input{
        min-width: 300px;
    }

    .contentinputs{

        border-radius: 3px;
        min-height:3em;
        max-height: 3em;
        width:35%;
    }


</style>

<body style='width: 100%;'>


<header class="blue-bg relative fix" id="home">

    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                        src="/appointsync.com/public/img/newlogo.png"
                        style="min-width: 150px; width: 150px; margin-top: -5%;"
                        alt="Logo">
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">

                </ul>
            </div>

        </div>
    </nav>
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
</header>

<section style="margin-top:10%;">


       <div style="width:80%;margin:auto;text-align: center;">

                <h3>
                    Please fill the data below.
                </h3><br/>

                    <input class="contentinputs" type="text" id="email" placeholder="Email"><br/><br/>
                    <input class="contentinputs" type="password" id="newPassword" placeholder="New Password"><br/><br/>
                    <input class="contentinputs" type="password" id="confirmPassword" placeholder="Confirm Password">
                    <br/><br/><br/>
           <button class="buttonAddClient buttonAddClient2" id="btnSubmitForgotPassword" style="min-width: 300px;">Submit</button>

        <br/> <br/>

    </div>


</section>

<?php

require ("objectFooter.php");

?>



<!--Maps JS-->
<script>

    $('#btnSubmitForgotPassword').click(function() {

        console.log('emak');

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->setResetPassword;?>';
        var resetPasswordTokenValue = '<?php echo $fileController->resetPasswordToken;?>';
        var resetPasswordTokenData = '<?php echo $PasswordToken;?>';
        var resetPasswordEmailValue = '<?php echo $fileController->rEmail;?>';
        var resetPasswordEmailData = document.getElementById('email').value;
        var resetPasswordPasswordValue = '<?php echo $fileController->password;?>';
        var resetPasswordPasswordData = document.getElementById('newPassword').value;
        var resetPasswordConfirmData = document.getElementById('confirmPassword').value;


    if(resetPasswordConfirmData != resetPasswordPasswordData || resetPasswordConfirmData == '') {

        alert('Passwords do not match.');

    }else {
        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {
                [submitValue]: submitData,
                [resetPasswordTokenValue]: resetPasswordTokenData,
                [resetPasswordEmailValue]: resetPasswordEmailData,
                [resetPasswordPasswordValue]: resetPasswordPasswordData
            },
            async: false,
            success: function (data) {
                if (!JSON.parse(data).success) {
                    location.href = "/appointsync.com/public/dashboard.php";
                } else if (JSON.parse(data).data.webID) {
                    location.href = "/appointsync.com/public/appointment.php/" + JSON.parse(data).data.webID;
                }
            }
        });

    }

    });


</script>
</body>
</html>
