<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedPage.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedElement.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedElementKeyValue.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/APIRouter.php';
//error_reporting(0);
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (isset($_SESSION['user'])) {
} else {
    header("Location: /appointsync.com/");
}
$loggedInUser = unserialize($_SESSION['user']);
if($loggedInUser->type != 2){
    header("Location: /appointsync.com/");
}

$_SESSION['pageName'] = 'clientFile';

$hostResponse = APIController::getProfile();
if ($hostResponse->success) {
    $host = $hostResponse->data;
} else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}
$getURL = $_SERVER["PATH_INFO"];

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (isset(explode("?", $actual_link)[1])) {
    $pageWebID = explode("?", $actual_link)[1];
}
$sendUserWebID = explode("/", $getURL)[1];

$clientResponse = APIController::getClient($_SESSION['selectedClientWebID']);
if ($clientResponse->success) {
    $client = $clientResponse->data;
} else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}

$webID = $sendUserWebID;
$isFound = false;
if ($client->webID == $_SESSION['selectedClientWebID']) {
    $isFound = true;

}
if (!$isFound) {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}


if($host->hiddenFiles == '0'){
if(isset($pageWebID) && $pageWebID != ''){
        $getFileTemplate = APIController::getFilePage($_SESSION['selectedClientWebID'], $pageWebID, null);
        $fileTemplate = $getFileTemplate->data;
    }elseif(!isset($pageWebID)){
        $getFileTemplate = APIController::getFilePage($_SESSION['selectedClientWebID'], null, null);
        $fileTemplate = $getFileTemplate->data;

    }
}elseif($host->hiddenFiles == '1'){
    if(!isset($_SESSION['hiddenFilePin'])){   header("Location: /appointsync.com/public/clients.php"); exit();}
    if(isset($pageWebID) && $pageWebID != ''){
        $getFileTemplate = APIController::getFilePage($_SESSION['selectedClientWebID'], $pageWebID, $_SESSION['hiddenFilePin']);
        $fileTemplate = $getFileTemplate->data;
    }elseif(!isset($pageWebID)){
        $getFileTemplate = APIController::getFilePage($_SESSION['selectedClientWebID'], null, $_SESSION['hiddenFilePin']);
        $fileTemplate = $getFileTemplate->data;

    }
}

if ($getFileTemplate->success) {
    $fileTemplate = $getFileTemplate->data;
}elseif ($getFileTemplate->errorCode == 343900) {
    $_SESSION["AuthFail"] = '4';
    header("Location: /appointsync.com/public/clientprofile.php/".$webID);
    exit();

}elseif ($getFileTemplate->errorCode == 343700) {
    header("Location: /appointsync.com/public/filepages.php/".$webID);
    exit();
}else {

    header("Location: /appointsync.com/public/clients.php");
    exit();
}

include_once dirname(__FILE__) . '/../resources/Controller/File/ElementFactory.php';
ElementFactory::createFactory($webID,$fileTemplate->webID);

?>

<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Client File</title>


    <?php

    require ("objectHeader.php");

    ?>

</head>

<style>
    .clientFileFooter{

        width: 100%;
        margin: 0 auto;
        text-align: center;

    }

    textarea
    {
        background: url(http://i.stack.imgur.com/ynxjD.png) repeat-y;
        width: 87%;
        max-width: 87%;
        min-height: 650px;
        margin:0px auto 5%;
        font: normal 14px verdana;
        line-height: 25px;
        padding: 2px 10px;
        border: solid 1px #ddd;
        background-size: contain;
    }


</style>

<body style='width: 100%;'>


<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">

    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                            src="/appointsync.com/public/img/newlogo.png"
                            style="min-width: 150px; width: 150px; margin-top: -5%;"
                            alt="Logo">
                </a>
            </div>

            <?php

            require ("objectMenu.php");

            ?>
        </div>
    </nav>
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
</header>

<section id="clientFileSection" style="margin-top:5%; ">
    <br/><br/>
    <div class="pull-right form-inline" style="width: 100%;margin-right: 2%;">
        <button class="buttonAddClient buttonAddClient2" id="balance" style="float:right;max-width: 150px;">Balance</button>
        <button class="buttonAddClient buttonAddClient2" id="pages" style="float:right;max-width: 150px;">Pages</button>
    </div><br/>
    <br/><br/>
    <div class="clientFileDataContainer">

        <div class="clientFileHeader" style="width:90%">
            <div class="fileHeaderWithAttachment" style="overflow: hidden;width:100%;">
                <?php

                echo'<div id="fileHeaderWithAttachmentText" style="font-size: 100%;"><em><p><strong>Name: </strong><a href="/appointsync.com/public/clientprofile.php/'.$sendUserWebID.'" style="color:#1193d4"> ' . $fileTemplate->name . '</a></p>',
                    '<p><strong>Date of Birth: </strong> <script> document.write(moment.unix(' . $fileTemplate->dob . ').format("LL"));</script></p>';
                if($fileTemplate->lastAppointmentDate == ''){
                    echo '<p><strong>Last Appointment Date: </strong> No previous appointments.</p>';
                }else {
                    echo '<p><strong>Last Appointment Date: </strong> <script> document.write(moment.unix(' . $fileTemplate->lastAppointmentDate . ').format("LLLL"));</script></p>';
                }
                    echo '<p><strong>File Date: </strong> <script> document.write(moment.unix(' . $fileTemplate->date . ').format("LLLL"));</script></p>',
                    '<p><strong>Gender: </strong>' . $fileTemplate->gender . '</p>',
                    '<p><strong>Phone Number: </strong>' . $fileTemplate->phoneNumber . '</p></em></div> ',
                    '<div id="fileHeaderWithAttachmentButton"><span style="float:right;">Page Number: '.$fileTemplate->number.'</span><br/><span style="float:right;">Client Number: '.$client->clientNumber.'</span><br/><button class="buttonAddClient buttonAddClient2" id="addAttachment">Add Attachment</button></div>';

                ?>
            </div>
        </div><br/><br/>


        <div class="clientFileBody" style="width:90%; margin: 0 auto;">

            <?php
            if (isset($fileTemplate->elements)) {

                foreach ($fileTemplate->elements as $element) {

                    echo '<form>';

                    echo ElementFactory::getElement($element);

                    echo '</form>';
                }
            }
            ?>
        </div>
        <div class="clientFileFooter" style="overflow: auto;">
            <br/><br/><br/>  <textarea id="clientFileNotes"><?php echo  $fileTemplate->note?></textarea>
        </div>
        <div class="templateFileAttachments">
            <?php
            if (isset($fileTemplate->attachments)) {

                foreach ($fileTemplate->attachments as $attachment) {
                    $ext = pathinfo($attachment->attachment, PATHINFO_EXTENSION);

                    if($ext=='pdf'){

                    }else{
                        echo "<div class='templateFileAttachment'><a href='" . $attachment->attachment . "' data-lightbox='image-1' data-title='Attachments'> <img src='" . $attachment->attachment . "' alt='Default' style='width:200%;'></a> </div>";
                    }
                }
            }
            ?>
        </div>
    </div>
</section>

<div id="myModalNotePicture" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close spanNoteAttachment">&times;</span>
        <div class="modal-header">
            <h2>Add an Attachment</h2>
        </div>
        <div class="modal-body">
            <h3> Pick a file to upload. </h3>
            <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
                <input type='file' name='file' required style="margin-right: 10%;">
                <span style="font-size: 0.8em">Note: you can attach files with formats pdf, doc, docx, png, jpg, jpeg, and gif.</span><br/>
                <button type="submit" name="submit" class="appointmentButton button1" id="btnSubmit">Accept</button>
                <button type="button" id="btnClosePicModal" class="appointmentButton button3">Cancel</button>
            </form>


        </div>
    </div>
</div>


<?php

require ("objectFooter.php");

?>

<script>


    var modalNotePicture = document.getElementById('myModalNotePicture');

    var btnAddAttachment = document.getElementById("addAttachment") != null?document.getElementById("addAttachment"):0;


    var btnClosePicModal = document.getElementById("btnClosePicModal");

    var spanNoteAttachment = document.getElementsByClassName("spanNoteAttachment")[0];

    btnAddAttachment.onclick = function() {
        modalNotePicture.style.display = "block";
        $('body').css('overflow','hidden');
    }


    btnClosePicModal.onclick = function() {
        modalNotePicture.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanNoteAttachment.onclick = function() {
        modalNotePicture.style.display = "none";
        $('body').css('overflow','auto');
    }


    window.onclick = function(event) {
        if (event.target == modalNotePicture || event.target == modalViewNotificationPopUpgg) {
            modalNotePicture.style.display = "none";
            modalViewNotificationPopUpgg.style.display = "none";
            $('body').css('overflow','auto');
        }
    }


    $( document ).ready(function() {
        window.x = document.getElementById("clientFileNotes").value;
    });

    var balance = document.getElementById("balance");
    var pages = document.getElementById("pages");

    balance.onclick = function () {
        location.href = "/appointsync.com/public/balance.php/<?php echo $webID;?>";
    };

    pages.onclick = function () {
        location.href = "/appointsync.com/public/filepages.php/<?php echo $webID;?>";
    };

    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");

    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;

    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });

    window.addEventListener("beforeunload", function() {
        var y = document.getElementById("clientFileNotes").value;


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {'submit':'updateFileNote', 'clientWebID':'<?php echo $sendUserWebID?>', 'pageWebID':'<?php echo $fileTemplate->webID?>', 'clientFileNote':y },
            async:false,
        });
    });


    //});


    var typingTimer;                //timer identifier
    var doneTypingInterval = 5000;  //time in ms, 5 second for example
    var $input = $('#clientFileNotes');

    //on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
        var y = document.getElementById("clientFileNotes").value;
        if(x != y){
            window.x = document.getElementById("clientFileNotes").value;

            $.ajax({
                type: "POST",
                url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: { 'submit':'updateFileNote', 'clientWebID':'<?php echo $sendUserWebID?>', 'pageWebID':'<?php echo $fileTemplate->webID?>', 'clientFileNote':y }
            });

        }

    }





    ///posting the form

    $(document).ready(function () {

        $.ajaxSetup({ cache: false });

        $("#btnSubmit").click(function (event) {

            //stop submit the form, we will post it manually.
            event.preventDefault();

            // Get form
            var form = $('#fileUploadForm')[0];

            // Create an FormData object
            var data = new FormData(form);

            // If you want to add an extra field for the FormData
            data.append('file', $('[type=file]')[0].files[0]);
            data.append("submit", "addFileAttachment");
            data.append("clientWebID", "<?php echo $webID?>");
            data.append("pageWebID", "<?php echo $fileTemplate->webID?>");

            for (var pair of data.entries()) {
                console.log(pair[0]+ ', ' + pair[1]);
            }

            // disabled the submit button
            $("#btnSubmit").prop("disabled", true);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {

                    location.href = "/appointsync.com/public/clientfile.php/<?php echo $webID.'?'.$fileTemplate->webID;?>";

                },
                error: function (e) {


                }
            });

        });




    });

    //


</script>

</body>
</html>
