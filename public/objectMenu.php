<?php
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
// error_reporting(E_PARSE);

// if (isset($_SESSION['host']) && unserialize($_SESSION['host'])->type == 2) {} else {
// header("Location: /appointsync.com/");
// }
$user = unserialize($_SESSION['user']);
require ("notificationview.php");

if($user->type == 2){


   echo '<div class="collapse navbar-collapse navbar-right" id="mainmenu">',
        '<ul class="nav navbar-nav">';
    if($_SESSION['pageName'] == 'appointment' || $_SESSION['pageName'] == 'appointmentNote' || $_SESSION['pageName'] == 'dashboard') {
        echo '<li class="active"><a href="/appointsync.com/public/dashboard.php">Dashboard</a></li>';
    }else{
        echo '<li><a href="/appointsync.com/public/dashboard.php">Dashboard</a></li>';
    }


    if($_SESSION['pageName'] == 'balance' || $_SESSION['pageName'] == 'filePages' || $_SESSION['pageName'] == 'clientFile' || $_SESSION['pageName'] == 'clientProfile' || $_SESSION['pageName'] == 'clients') {
        echo '<li class="active"><a href="/appointsync.com/public/clients.php">Clients</a></li>';
    }else{
        echo '<li><a href="/appointsync.com/public/clients.php">Clients</a></li>';
    }

    echo  '<li><a href="#" id="showNotificationPopupLink">Notifications'; if(isset($newNotificationCountLoop) && $newNotificationCountLoop != 0){echo $newNotificationCountLoopDisplay;} echo '</a></li>';

    if($_SESSION['pageName'] == 'messages') {
        echo '<li class="active"><a href="/appointsync.com/public/messages.php">Messages</a></li>';
    }else{
        echo '<li><a href="/appointsync.com/public/messages.php">Messages</a></li>';
    }


    if($_SESSION['pageName'] == 'profile' || $_SESSION['pageName'] == 'profileSettings') {
        echo '<li class="active"><a href="/appointsync.com/public/profile.php">Profile</a></li>';
    }else{
        echo '<li><a href="/appointsync.com/public/profile.php">Profile</a></li>';
    }

           echo '<li><a href="/appointsync.com/public/logout.php">Log Out</a></li>',
        '</ul>',
    '</div>';

}elseif($user->type == 3){

    echo '<div class="collapse navbar-collapse navbar-right" id="mainmenu">',
    '<ul class="nav navbar-nav">';
    if($_SESSION['pageName'] == 'appointment' || $_SESSION['pageName'] == 'appointmentNote' || $_SESSION['pageName'] == 'dashboard') {
        echo '<li class="active"><a href="/appointsync.com/public/dashboard.php">Dashboard</a></li>';
    }else{
        echo '<li><a href="/appointsync.com/public/dashboard.php">Dashboard</a></li>';
    }


    if( $_SESSION['pageName'] == 'hosts' || $_SESSION['pageName'] == 'hotsProfile') {
        echo '<li class="active"><a href="/appointsync.com/public/hosts.php">Hosts</a></li>';
    }else{
        echo '<li><a href="/appointsync.com/public/hosts.php">Hosts</a></li>';
    }

    echo  '<li><a href="#" id="showNotificationPopupLink">Notifications'; if(isset($newNotificationCountLoop) && $newNotificationCountLoop != 0){echo $newNotificationCountLoopDisplay;} echo '</a></li>';

    if($_SESSION['pageName'] == 'messages') {
        echo '<li class="active"><a href="/appointsync.com/public/messages.php">Messages</a></li>';
    }else{
        echo '<li><a href="/appointsync.com/public/messages.php">Messages</a></li>';
    }


    if($_SESSION['pageName'] == 'profile' || $_SESSION['pageName'] == 'profileSettings') {
        echo '<li class="active"><a href="/appointsync.com/public/profile.php">Profile</a></li>';
    }else{
        echo '<li><a href="/appointsync.com/public/profile.php">Profile</a></li>';
    }

    echo '<li><a href="/appointsync.com/public/logout.php">Log Out</a></li>',
    '</ul>',
    '</div>';


}else{

    ?>

    <div class="collapse navbar-collapse navbar-right" id="mainmenu">
        <ul class="nav navbar-nav">

        </ul>
    </div>

<?php

}