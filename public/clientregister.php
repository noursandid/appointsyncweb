<?php
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
$fileController = new MappedRouterController();
?>
<!DOCTYPE html>
<html>
<head>
        <title>AppointSync - Register</title>
    <?php

    require ("objectHeader.php");

    ?>

    <link rel="stylesheet" href="build/css/intlTelInput.css">
    <link rel="stylesheet" href="build/css/demo.css">

</head>
<body data-spy="scroll" data-target="#mainmenu">
  <style>


      #formclass input{
          /*min-width: 300px;*/
      }

      #formclass select{
          min-width: 300px;
      }

.contentinputs{

   border-radius: 3px;
   min-height:3em;
   max-height: 3em;
   width:35%;
}

#dobDiv{

    width:30%;
    margin:auto;
    min-width: 300px;
    display: inline-block;
    text-align: center;
}

select{

  border-radius: 3px;
  min-height:3em;
  max-height: 3em;
  width:11.6%;

}

  </style>

  <header class="blue-bg relative fix" id="home">
      <div class="section-bg overlay-bg angle-bg ripple">
          <video autoplay muted id="video-background" loop>
              <source src="http://intimissibd.com/video/video-2.webm" type="video/webm">
          </video>
      </div>
      <!--Mainmenu-->
      <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
          <div class="container">
              <div class="navbar-header">
                  <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a href="#" class="navbar-brand">
                      <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                      <!-- AppointSyncLogo.png <h2 class="text-white logo-text">AppointSync</h2>-->
                  </a>
              </div>
              <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                  <ul class="nav navbar-nav">
                      <li><a href="/appointsync.com/public/index.php">Home</a></li>
                      <li><a href="/appointsync.com/public/#work">Work</a></li>
                      <li><a href="/appointsync.com/public/#feature">Features</a></li>
                      <li><a href="/appointsync.com/public/#price">Pricing</a></li>
                      <li><a href="/appointsync.com/public/#contact">Contact</a></li>
                      <li><a href="/appointsync.com/public/login.php">Login</a></li>
                      <li class="active"><a href="/appointsync.com/public/register.php">Register</a></li>
                  </ul>
              </div>
          </div>
      </nav>
      <!--Mainmenu/-->
      <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
      <!--Header-Text-->

      <!--Header-Text/-->
  </header>

  <div style="width:100%;margin:5% auto;text-align: center;">
    <h2>Client Registration </h2><br/> <br/>


      <input class="contentinputs" type="text" id="fname" required placeholder="First Name*" style=""> <br/><br/>


      <input class="contentinputs" type="text" id="lname" required placeholder="Last Name*" style=""> <br/><br/>


      <input class="contentinputs" type="text" id="username" required placeholder="Username*" style=""> <br/><br/>


      <input class="contentinputs" type="password" id="password" required placeholder="Password*" style=""> <br/><br/>


      <input class="contentinputs" type="password" id="confirmPassword" required placeholder="Confirm Password*" style=""> <br/><br/>

      <select id='gender' style="width:35%;text-align:center;">
      <option selected disabled>-Gender-</option>
          <option value="Male">Male</option>
          <option value="Female">Female</option>
          <option value="Other">Other</option>
    </select><br/><br/>

      <input class="contentinputs" type="text" id="email" required placeholder="Email*" style=""> <br/><br/>


      <span id="valid-msg" class="hide">✓ Valid</span>
      <span id="error-msg" class="hide">Invalid number</span><br/>
      <input id="phone" name="phone" type="tel"><br/><br/>

          <div id="dateOfBirth">
    <p>Date of Birth</p>


          <div class="row" style="border:text-align: center;width:35%;margin:auto;">
              <div class='col-sm-6'  style="width:100%;">
                  <input type='text' class="form-control" id='datetimepicker6' readonly style="text-align: center;"/>
              </div>
              <script type="text/javascript">
                  $(function () {
                      $('#datetimepicker6').datetimepicker({
                          viewMode: 'months',
                          format: 'DD/MM/YYYY',
                          ignoreReadonly: true,
                      });
                  });
              </script>
          </div>
          </div><br/><br/>

      <button class="buttonAddClient buttonAddClient2" id="btnSubmitRegistration" style="min-width: 300px;">Sign up</button>

    <br/><br/><br/>






    </div>


  <?php

  require ("objectFooter.php");

  ?>


  <script src="build/js/intlTelInput.js"></script>
  <script>
      $("#phone").intlTelInput({
          // allowDropdown: false,
          // autoHideDialCode: false,
          // autoPlaceholder: "off",
          // dropdownContainer: "body",
          // excludeCountries: ["us"],
          // formatOnDisplay: false,
          // geoIpLookup: function(callback) {
          //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
          //     var countryCode = (resp && resp.country) ? resp.country : "";
          //     callback(countryCode);
          //   });
          // },
          // hiddenInput: "full_number",
          // initialCountry: "auto",
          // localizedCountries: { 'de': 'Deutschland' },
          // nationalMode: false,
          // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
          // placeholderNumberType: "MOBILE",
          // preferredCountries: ['cn', 'jp'],
          // separateDialCode: true,
          initialCountry: "auto",
          geoIpLookup: function(callback) {
              $.get('https://ipinfo.io', function () {
              }, "jsonp").always(function (resp) {
                  var countryCode = (resp && resp.country) ? resp.country : "";
                  callback(countryCode);
              });
          },
          utilsScript: "build/js/utils.js"
      });

      var telInput = $("#phone"),
          errorMsg = $("#error-msg"),
          validMsg = $("#valid-msg");

      // initialise plugin
      telInput.intlTelInput({
          utilsScript: "../../build/js/utils.js"
      });

      var reset = function() {
          telInput.removeClass("error");
          errorMsg.addClass("hide");
          validMsg.addClass("hide");
      };

      // on blur: validate
      telInput.blur(function() {
          reset();
          if ($.trim(telInput.val())) {
              if (telInput.intlTelInput("isValidNumber")) {
                  validMsg.removeClass("hide");
              } else {
                  telInput.addClass("error");
                  errorMsg.removeClass("hide");
              }
          }
      });

      $("#phone").keydown(function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
              // Allow: Ctrl/cmd+A
              (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
              // Allow: Ctrl/cmd+C
              (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
              // Allow: Ctrl/cmd+X
              (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
              // Allow: home, end, left, right
              (e.keyCode >= 35 && e.keyCode <= 39) || (e.keyCode == 189 || e.which == 57 || e.which == 48)) {
              // let it happen, don't do anything
              return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
      });

      // on keyup / change flag: reset
      telInput.on("keyup change", reset);
  </script>


<script>

    $("#btnSubmitRegistration").click(function() {


        const fullNumber = $("#phone").intlTelInput("getSelectedCountryData");


        var firstName = document.getElementById('fname').value;
        var lastName = document.getElementById('lname').value;
        var username = document.getElementById('username').value;
        var password = document.getElementById('password').value;
        var confirmPassword = document.getElementById('confirmPassword').value;
        var email = document.getElementById('email').value;
        var countryCodeData =fullNumber.dialCode;
        var phoneNumber = document.getElementById('phone').value;
        var gender = document.getElementById('gender').value;
        var dateOfBirth = document.getElementById('datetimepicker6').value;
        var dateOfBirthTime = dateOfBirth + ' 17:00:00';
        var dateOfBirthUnix = moment(dateOfBirthTime, "DDMMYYYY, h:m:s").unix();

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->registerClient;?>';
        var firstNameValue = '<?php echo $fileController->rFirstName;?>';
        var LastNameValue = '<?php echo $fileController->rLastName;?>';
        var usernameValue = '<?php echo $fileController->rUsername;?>';
        var passwordValue = '<?php echo $fileController->rPassword;?>';
        var emailValue = '<?php echo $fileController->rEmail;?>';
        var genderValue = '<?php echo $fileController->rGender;?>';
        var phoneNumberValue = '<?php echo $fileController->rPhoneNumber;?>';
        var countryCode = '<?php echo $fileController->countryCode;?>';
        var dateOfBirthValue = '<?php echo $fileController->rDateOfBirth;?>';

        if(gender == '0'){

            alert('Please make sure to submit correct values.');

        }else if(password != confirmPassword || confirmPassword == '') {

            alert('Passwords do not match.');

        }else if(password == '' || confirmPassword == '' || firstName == '' || lastName == '' || username == '' || email == '' || phoneNumber == '' || dateOfBirthUnix == '' || gender == '') {

            alert('Please fill all data.');

        }else if (password == confirmPassword && password != '' && confirmPassword != '' && firstName != '' && lastName != '' && username != '' && email != '' && phoneNumber != '' && dateOfBirthUnix != '' && gender != '') {


            $.ajax({
                type: "POST",
                url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: { [submitValue]: submitData, [firstNameValue]: firstName, [LastNameValue]: lastName, [usernameValue]: username,[passwordValue]: password, [emailValue]: email, [genderValue]: gender, [countryCode]: countryCodeData , [phoneNumberValue]: phoneNumber , [dateOfBirthValue]: dateOfBirthUnix},
                async: false,
                success: function(data){
                    alert(data);
                    if(!JSON.parse(data).success){
                        if(JSON.parse(data).errorCode == '486600'){
                            alert('Username or Email already exists');
                        }else {alert('Oops, Something went wrong, please try again'); }
                    }else if(JSON.parse(data).success){
                        alert('registration successful');
                        location.href = "/appointsync.com/public/login.php/";
                    }
                }
            });



        }

    });

</script>

</body>
</html>
