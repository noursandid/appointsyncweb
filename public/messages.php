<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedThread.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedMessage.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
//error_reporting(E_PARSE);
if (isset($_SESSION['user'])) {
} else {
    header("Location: /appointsync.com/");
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}

$_SESSION['pageName'] = 'messages';

$host = unserialize($_SESSION['user']);


$clientsResponse = APIController::getClients();
if ($clientsResponse->success) {
    $clients = $clientsResponse->data;
    //$_SESSION['clients'] = serialize($clients);
} else {
    header("Location: /appointsync.com/public/login.php");
    exit();
}

//echo $host->webID;
//$threadResponse = APIController::getThreads();
//echo json_encode($threadResponse);
//if($threadResponse->success){
//    $threads = $threadResponse->data;
//    echo '<br/><br/><br/><br/>'.json_encode($threads);
//}else{ header("Location: /appointsync.com/public/dashboard.php"); exit();}
//echo $threads[0]->webID;
//$messageResponse = APIController::getMessages("kLi87ruUhr80op94r7tc54Jn7r2");
//if($messageResponse->success){
//    $messages = $messageResponse->data;
//}else{ header("Location: /appointsync.com/public/dashboard.php"); exit();}

$fileController = new MappedRouterController();
?>

<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Messages</title>

    <?php

    require ("objectHeader.php");

    ?>



<!--    -->

    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="./mock.js"></script>
    <link rel="stylesheet" type="text/css" href="./jquery.dropdown.css">
    <script src="./jquery.dropdown.js"></script>

</head>


<body style='width:100%;'>
<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>


<!-- Reply  -->

<div id="myModalReplyOnMessage" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close spanReplyMessage">&times;</span>
        <div class="modal-header">
            <h2>Reply</h2>
        </div>
        <div class="modal-body">
            <h3>Please fill the below</h3>
            <br />

            <p id="replyingOnUser"></p>
            <p id="replyingOnTitle"></p>

            <input type="hidden" id="selectClientReply" readonly/>
            <input type="hidden" id="threadTitleInputReply" readonly/>
            <label for="messageReply">Message</label><br/> <textarea id="messageReply" required ></textarea><br />
            <br />
            <!--                    --><?php
            //
            //                    require ("test.php");
            //
            //                    ?>
            <button type="submit" name="submit" class="appointmentButton button6" id="btnReplyMessage">Send</button>
        </div>
    </div>
</div>


<header class="blue-bg relative fix" id="home">

    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">
                    <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                </a>
            </div>

            <?php

            require ("objectMenu.php");

            ?>

        </div>
    </nav>
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
</header>

<section style="overflow: auto;">

    <div style='margin-top:5%;'>

    </div>


    <div class="messageContainer" id="messageContainer">

        <div class="threads" id="threads"></div>
        <div class="messages" id="messages"></div>

        <script>

            var selectClientReply = document.getElementById("selectClientReply");
            var threadTitleInputReply = document.getElementById("threadTitleInputReply");

            function searchThreads() {
                var input = document.getElementById("Search");
                var filter = input.value.toLowerCase();
                var nodes = document.getElementsByClassName('thread');

                for (i = 0; i < nodes.length; i++) {
                    if (nodes[i].innerText.toLowerCase().includes(filter)) {
                        nodes[i].style.display = "block";
                    } else {
                        nodes[i].style.display = "none";
                    }
                }
            }


            function getThreads(){


                var submitValue = '<?php echo $fileController->submitValue;?>';
                var submitData = '<?php echo $fileController->getThreads;?>';

                var threads = [] ;
                $.ajax({
                    type: "POST",
                    url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                    data: { [submitValue]: submitData},
                    async: false,
                    success: function(data){

                        if (JSON.parse(data).success) {
                            threads = JSON.parse(data).data;
                        }
                        else{
                            alert(JSON.parse(data).errorMessage);
                        }
                    }
                });
                return threads;
            }

            function getMessages(threadWebID){


                var submitValue = '<?php echo $fileController->submitValue;?>';
                var submitData = '<?php echo $fileController->getMessages;?>';
                var threadWebIDValue = '<?php echo $fileController->threadWebID;?>';
                var threadWebIDData = threadWebID;

                var messages = [] ;
                $.ajax({
                    type: "POST",
                    url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                    data: { [submitValue]: submitData, [threadWebIDValue]: threadWebIDData},
                    async: false,
                    success: function(data){
                        // console.log(data);
                        if (JSON.parse(data).success) {
                            messages = JSON.parse(data).data;
                        }
                        else{
                            alert(JSON.parse(data).errorMessage);
                        }
                    }
                });
            return messages;

            }

            var threads = getThreads();
            var selectedThread = '';
            var selectedMessages = {};
            if ( threads != null && threads.length > 0) {
                selectedThread = threads[0].webID;
                selectedMessages = getMessages(threads[0].webID);
            }


            function findUpTag(el, tag) {

                while (el.parentElement) {
                    if (el.className == tag)
                        return el;
                    el = el.parentElement;
                }
                return null;
            }



            function removeMessages(){
                var myNode = document.getElementById("messages");
                while (myNode.firstChild) {
                    myNode.removeChild(myNode.firstChild);
                }
            }

            function removeThreads(){
                var myNode = document.getElementById("threads");
                while (myNode.firstChild) {
                    myNode.removeChild(myNode.firstChild);
                }
            }

            function loadThreads(){

               var replyingOnUser = document.getElementById('replyingOnUser');
               var replyingOnTitle = document.getElementById('replyingOnTitle');




                var threadsDiv = document.getElementById("threads");
                var threadHeaderDiv = document.createElement("div");
                threadHeaderDiv.className = "threadHeader";
                var imageNewMessage = document.createElement("img");
                imageNewMessage.src = '/appointsync.com/public/img/newMessage.png';
                imageNewMessage.id = 'newMessage';
                var imageReplyMessage = document.createElement("img");
                imageReplyMessage.src = '/appointsync.com/public/img/replyMessage.png';
                imageReplyMessage.id = 'replyMessage';

                imageNewMessage.onclick = function() {
                    myModalNewMessage.style.display = "block";
                    $('body').css('overflow','hidden');
                }


                imageReplyMessage.onclick = function() {
                    myModalReplyOnMessage.style.display = "block";
                    $('body').css('overflow','hidden');
                }

                var searchInput = document.createElement("input");
                searchInput.setAttribute("type", "text");
                searchInput.setAttribute("placeholder", "Please enter a search term..");
                searchInput.id = 'Search';
                searchInput.onkeyup = searchThreads;
                threadsDiv.appendChild(threadHeaderDiv);
                threadHeaderDiv.appendChild(imageNewMessage);
                threadHeaderDiv.appendChild(imageReplyMessage);
                threadHeaderDiv.appendChild(searchInput);

                if (threads.length == 0){

                    var emptyThread = document.createElement("p");
                    var emptyThreadValue = document.createTextNode("You still have no threads");
                    emptyThread.className = "emptyThread";
                    emptyThread.id = "emptyThread";
                    emptyThread.appendChild(emptyThreadValue);
                    threadsDiv.appendChild(emptyThread);
                }
                for (var i =0;i<threads.length;i++) {
                    var thread = threads[i];
                    var threadDiv = document.createElement("div");
                    threadDiv.className = "thread";
                    threadDiv.id = thread.webID;
                    //From:
                    var threadSender = document.createElement("p");
                    threadSender.id = "threadSender";
                    threadSenderTitle = document.createElement("strong");
                    threadSenderTitle.innerHTML = "User: ";

                    threadSenderValue = document.createTextNode(thread.toUsername);


                    var threadLastUpdatedDate = document.createElement("span");
                    threadLastUpdatedDate.id = "threadLastUpdatedDate";
                    threadLastUpdatedDate.innerHTML = moment.unix(thread.lastUpdatedDate).format("lll");


                    threadSender.appendChild(threadSenderTitle);
                    threadSender.appendChild(threadSenderValue);
                    threadSender.appendChild(threadLastUpdatedDate);


                    //Title
                    var threadTitle = document.createElement("p");
                    threadTitle.id = "threadTitle";

                    threadTitleTitle = document.createElement("strong");
                    threadTitleTitle.innerHTML = "Title: ";

                    threadTitleValue = document.createTextNode(thread.title);

                    threadTitle.appendChild(threadTitleTitle);
                    threadTitle.appendChild(threadTitleValue);


                    //Last Message
                    var threadMessage = document.createElement("p");
                    threadMessage.id = "threadMessage";

                    threadMessageTitle = document.createElement("strong");
                    threadMessageTitle.innerHTML = "Message: ";

                    threadMessageValue = document.createTextNode(thread.lastMessage);

                    threadMessage.appendChild(threadMessageTitle);
                    threadMessage.appendChild(threadMessageValue);

                    threadDiv.appendChild(threadSender);
                    threadDiv.appendChild(threadTitle);
                    threadDiv.appendChild(threadMessage);


                    threadDiv.onclick = function(e){
                        var threadDiv = findUpTag(e.target,"thread");
                        var threadWebID = threadDiv.id;
                        var threadNameSelected = self.threads.filter(t => t.webID == threadWebID)[0].toUsername;
                        var threadTitle = self.threads.filter(t => t.webID == threadWebID)[0].title;
                        var threadNameWebSelected = self.threads.filter(t => t.webID == threadWebID)[0].toWebID;
                        var threadWebTitle = self.threads.filter(t => t.webID == threadWebID)[0].webID;
                        // console.log(threadNameWebSelected + ', ' + threadWebTitle);
                        replyingOnUser.textContent = 'Replying on: '+ threadNameSelected;
                        replyingOnTitle.textContent = ' Title: ' + threadTitle;
                        threadTitleInputReply.textContent = threadWebTitle;
                        selectClientReply.textContent = threadNameWebSelected;
                        selectedMessages = getMessages(threadWebID);

                        var thread = document.getElementById(selectedThread);


                        if (thread != null){
                            thread.style.border = "rgba(0,0,0,0.4) solid 1px";
                        }
                        selectedThread =threadWebID;
                        // console.log(selectedThread);

                        var thread = document.getElementById(selectedThread);


                        if (thread != null){
                            thread.style.border = "#1193d4 solid 2px";

                        }
                        reloadPage();

                    };

                    if (i == 0){
                        threadDiv.style.border = "#1193d4 solid 2px";
                        // console.log(thread);
                        replyingOnTitle.textContent = ' Title: ' + thread.title;
                        replyingOnUser.textContent = 'Replying on: '+ thread.toUsername;
                        threadTitleInputReply.textContent = thread.webID;
                        selectClientReply.textContent = thread.toWebID;

                        console.log(thread);
                    }

                    threadsDiv.appendChild(threadDiv);

                }


            }

            function loadMessages(){


                var messagesDiv = document.getElementById("messages");


                for (var i =0;i<selectedMessages.length;i++) {
                    var message = selectedMessages[i];
                    var messageDiv = document.createElement("div");
                    messageDiv.className = "message";

                    if (message.fromWebID == '<?php echo $host->webID;?>'){
                        var blueMessageBox = document.createElement("div");
                        blueMessageBox.className = "blueMessageBox";


                        var messageBlue = document.createElement("div");
                        messageBlue.className = "messageBlue";

                        messageBlue.innerHTML = message.message;



                        var messageDate = document.createElement("p");
                        messageDate.id = "messageDate";

                        messageDateValue = document.createTextNode(moment.unix(message.date).format("lll"));


                        messageDate.appendChild(messageDateValue);
                        messageBlue.appendChild(messageDate)
                        blueMessageBox.append(messageBlue);
                        messageDiv.appendChild(blueMessageBox);
                    }
                    else{
                        var greenMessageBox = document.createElement("div");
                        greenMessageBox.className = "greenMessageBox";


                        var messageGreen = document.createElement("div");
                        messageGreen.className = "messageGreen";

                        messageGreen.innerHTML = message.message;



                        var messageDate = document.createElement("p");
                        messageDate.id = "messageDate";

                        messageDateValue = document.createTextNode(moment.unix(message.date).format("lll"));


                        messageDate.appendChild(messageDateValue);
                        messageGreen.appendChild(messageDate)
                        greenMessageBox.append(messageGreen);
                        messageDiv.appendChild(greenMessageBox);
                    }

                    messagesDiv.appendChild(messageDiv);
                }

            }
    loadThreads();
    loadMessages();

            function reloadPage(){
                self.removeMessages();
                self.loadMessages();
            }

        </script>



        <!-- New Message -->

        <div id="myModalNewMessage" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <span class="close spanNewMessage">&times;</span>
                <div class="modal-header">
                    <h2>New Message</h2>
                </div>
                <div class="modal-body">
                    <h3>Please fill the below</h3>
                    <br />

                    <div class="row">
                        <div class="col-sm-4" style="width:51%;float:none; margin: 0 auto;">
                            <label for="selectClientDropDown">Select Client</label>
                            <div class="dropdown-sin-1">
                                <select  id="selectClient" style="display:none"  id="selectClientDropDown" placeholder="Select">
                                    <?php
                                    if ($host->type == 2 ) {
                                        foreach ($clients as $client) {
                                            if ($client->type == 3) {
                                                echo '<option id="option' . $client->webID . '" value ="' . $client->webID . '">' . $client->identifier . '</option>';
                                            } else {
                                            }
                                        }

                                    } elseif ($host->type == 3) {

                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
<br/>


                        <label for="threadTitleInput">Thread Title</label> <input type='text' id='threadTitleInput' class='form-control' required /><br />
                    <label for="message">Message</label><br/> <textarea id="newMessageData" required ></textarea><br />


                    <br />
                    <button type="submit" name="submit" class="appointmentButton button6" id="btnNewMessage">Send</button>
                </div>
            </div>
        </div>






        <!--    </div>-->

</section>


<script>
    var Random = Mock.Random;
    var json1 = Mock.mock({
        "data|10-50": [{
            name: function () {
                return Random.name(true)
            },
            "id|+1": 1,
            "disabled|1-2": true,
            groupName: 'Group Name',
            "groupId|1-4": 1,
            "selected": false
        }]
    });

    $('.dropdown-mul-1').dropdown({
        data: json1.data,
        limitCount: 40,
        multipleMode: 'label',
        choice: function () {
            // console.log(arguments,this);
        }
    });

    var json2 = Mock.mock({
        "data|10000-10000": [{
            name: function () {
                return Random.name(true)
            },
            "id|+1": 1,
            "disabled": false,
            groupName: 'Group Name',
            "groupId|1-4": 1,
            "selected": false
        }]
    });

    $('.dropdown-sin-1').dropdown({
        readOnly: true,
        input: '<input type="text" maxLength="20" placeholder="Search">'
    });

</script>

<?php

require ("objectFooter.php");

?>


<script>

    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");
    var myModalNewMessage = document.getElementById("myModalNewMessage");
    var myModalReplyOnMessage = document.getElementById("myModalReplyOnMessage");

    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;
    // var btnOpenNewMessage = document.getElementById("newMessage") != null?document.getElementById("newMessage"):0;
    // var btnReplyMessage = document.getElementById("replyMessage") != null?document.getElementById("replyMessage"):0;



    var spanNewMessage = document.getElementsByClassName("spanNewMessage")[0];
    var spanReplyMessage = document.getElementsByClassName("spanReplyMessage")[0];




    spanNewMessage.onclick = function() {
        myModalNewMessage.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanReplyMessage.onclick = function() {
        myModalReplyOnMessage.style.display = "none";
        $('body').css('overflow','auto');
    }


    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    $('#btnNewMessage').click(function() {


        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->postContactMessage;?>';
        var selectClientData = document.getElementById("selectClient").value;
        var selectClientValue = '<?php echo $fileController->threadToWebID;?>';
        var selectTitleData = document.getElementById("threadTitleInput").value;
        var selectTitleValue = '<?php echo $fileController->threadTitle;?>';
        var selectDataData = document.getElementById("newMessageData").value;
        var selectDataValue = '<?php echo $fileController->threadMessage;?>';

        var btnNewMessageSubmit = document.getElementById("btnNewMessage") != null?document.getElementById("btnNewMessage"):0;

        if (selectClientData != '' && selectTitleData != '' && selectDataData !='') {


            btnNewMessageSubmit.disabled = 'true';

            $.ajax({
                type: "POST",
                url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: { [submitValue]: submitData, [selectClientValue]: selectClientData, [selectTitleValue]: selectTitleData, [selectDataValue]: selectDataData},
                async: true,
                success: function(data){
                    // alert(data);
                    if(!JSON.parse(data).success){
                       alert('Oops, Something went wrong, please try again');
                    }else {
                        // alert('qhat');
                        // await sleep(1500);
                        btnNewMessageSubmit.disabled = 'false';
                        myModalNewMessage.style.display = "none";
                        removeThreads();
                        removeMessages();
                        self.threads = getThreads();
                        if (self.threads.length >0)
                        {
                            self.selectedThread = self.threads[0].webID;
                            getMessages(self.threads[0].webID);
                            self.selectedMessages = getMessages(self.threads[0].webID);
                        }
                        loadThreads();
                        loadMessages();

                    }
                }
            });



        }else{

            alert('Incorrect Data');
        }

    });



    $('#btnReplyMessage').click(function() {


        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->postContactMessage;?>';
        var selectClientData = document.getElementById("selectClientReply").innerText;
        var selectClientValue = '<?php echo $fileController->threadToWebID;?>';
        var selectTitleData = document.getElementById("threadTitleInputReply").innerText;
        var selectTitleValue = '<?php echo $fileController->threadWebID;?>';
        var selectDataData = document.getElementById("messageReply").value;
        var selectDataValue = '<?php echo $fileController->threadMessage;?>';

        var btnReplyMessage = document.getElementById("btnReplyMessage") != null?document.getElementById("btnReplyMessage"):0;

        if (selectClientData != '' && selectTitleData != '' && selectDataData !='') {


            btnReplyMessage.disabled = 'true';

            $.ajax({
                type: "POST",
                url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: { [submitValue]: submitData, [selectClientValue]: selectClientData, [selectTitleValue]: selectTitleData, [selectDataValue]: selectDataData},
                async: true,
                success: function(data){
                    // alert(data);
                    if(!JSON.parse(data).success){
                       alert('Oops, Something went wrong, please try again');
                    }else {
                        // alert('qhat');
                        // await sleep(1500);
                        btnReplyMessage.disabled = 'false';
                        myModalReplyOnMessage.style.display = "none";
                        removeThreads();
                        removeMessages();
                        self.threads = getThreads();
                        if (self.threads.length >0)
                        {
                            self.selectedThread = self.threads[0].webID;
                            getMessages(self.threads[0].webID);
                            self.selectedMessages = getMessages(self.threads[0].webID);
                        }
                        loadThreads();
                        loadMessages();
                        btnReplyMessage.disabled = 'true';
                    }
                }
            });



        }else{

            alert('Incorrect Data');
        }

    });

    window.onclick = function(event) {
        if (event.target == modalViewNotificationPopUpgg || event.target == myModalReplyOnMessage || event.target == myModalNewMessage) {
            modalViewNotificationPopUpgg.style.display = "none";
            myModalNewMessage.style.display = "none";
            myModalReplyOnMessage.style.display = "none";
            $('body').css('overflow','auto');
        }
    }


    $("#selectClient").on("change", function () {

        var selection = $(this).find("option:selected").text(),
            labelFor = $(this).attr("id"),
            label = $("[for='" + labelFor + "']");

        label.find(".label-desc").html(selection);

    });


</script>

</body>
</html>