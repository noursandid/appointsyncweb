<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedClient.php';
include_once dirname(__FILE__) . '/../resources/Classes/UserProfile.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
// error_reporting(E_PARSE);
if(isset($_SESSION['user'])) {
}else{header("Location: /appointsync.com/");
    exit();
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}

$_SESSION['pageName'] = 'hosts';

$host = unserialize($_SESSION['user']);
$hostResponse = APIController::getHosts();
if($hostResponse->success){
    $hosts = $hostResponse->data;
}else{ header("Location: /appointsync.com/public/clients.php"); exit();}

include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
$fileController = new MappedRouterController();
?>
<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Hosts</title>

    <?php

    require ("objectHeader.php");

    ?>

    <script>
        function myFunction() {
            var input = document.getElementById("Search");
            var filter = input.value.toLowerCase();
            var nodes = document.getElementsByClassName('relationUserFilter');

            for (i = 0; i < nodes.length; i++) {
                if (nodes[i].innerText.toLowerCase().includes(filter)) {
                    nodes[i].style.display = "block";
                } else {
                    nodes[i].style.display = "none";
                }
            }
        }
    </script>

</head>


<body style='width: 100%;'>

<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg angle-bg ripple">
        <video autoplay muted id="video-background" loop>
            <source src="http://intimissibd.com/video/video-2.webm"
                    type="video/webm">
        </video>
    </div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                        src="/appointsync.com/public/img/newlogo.png"
                        style="min-width: 150px; width: 150px; margin-top: -5%;"
                        alt="Logo">
                </a>
            </div>
            <?php

            require ("objectMenu.php");

            ?>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
    <!--Header-Text-->
    <!--Header-Text/-->
</header>

<section style="overflow: hidden!important;">

    <div style='margin-top: 5%;'></div>

    <br> <br>

    <div class="container">
        <input type="text" id="Search" onkeyup="myFunction()"
               placeholder="Please enter a search term.." title="Type in a name">

        <div>

            <div class="pull-right form-inline" style="width: 25%;">
                <button class="buttonAddClient buttonAddClient2" id="addUser">Add
                    Host</button>
            </div>
        </div>
    </div>
    <div class="relationList">

        <?php
        foreach ($hosts as $host) {
                echo '<div class="relationUser relationUserFilter">',
                    '<h4><a href="/appointsync.com/public/hostprofile.php/' . $host->webID . '"><span style="color:#1193d4;">' . $host->username . '</span></a></h4>',
                    '<p>' . $host->profilePic . '</p><br/>',
                    '<p>Name: ' . $host->firstName . ' ' . $host->lastName . '</p>',
                    '<p>Profession: ' . $host->profession. '</p>',
                    '<p>Email: ' . $host->email . '</p>',
                    '<p>Phone Number: ' . $host->phoneNumber . '</p>',
                    '<p>Age: <script> document.write(moment().diff(moment.unix(' .$host->dob . ').format("LL"), "years"));</script></p>',
                    '<p>Gender: ' . $host->gender . '</p>',
                '</div>';
        }

        if (isset($hosts)) {

        } else {
            echo '<h3 style="margin-top:10%">You don&apos;t have any Hosts yet! go ahead and add some.</h3>';
        }
        ?>
    </div>




</section>


<!-- Online User -->

<div id="myModalNewUser" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close spanOnlineUser">&times;</span>
        <div class="modal-header">
            <h2>New Host</h2>
        </div>
        <div class="modal-body">
            <h3>Please fill the below</h3>
            <br />
            <label for="usernameTab">Username</label> <input type='text'
                                                             id='usernameTab' class='form-control' required /><br />
            <br />
            <button type="submit" name="submit" class="appointmentButton button6"
                    id="btnAddHost">Save</button>
            <button type="button" id="btnCloseUser"
                    class="appointmentButton button3">Cancel</button>
        </div>
    </div>
</div>



<?php

require ("objectFooter.php");

?>

<script>
    var modalNewUser = document.getElementById("myModalNewUser");

    // Get the button that opens the modal
    var btnAddUser = document.getElementById("addUser") != null?document.getElementById("addUser"):0;

    //Close buttons
    var btnCloseUser = document.getElementById("btnCloseUser");


    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");

    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;

    var spanOnlineUser = document.getElementsByClassName("spanOnlineUser")[0];


    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });


    btnAddUser.onclick = function() {
        modalNewUser.style.display = "block";
        $('body').css('overflow','hidden');
    }


    btnCloseUser.onclick = function() {
        modalNewUser.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanOnlineUser.onclick = function() {
        modalNewUser.style.display = "none";
        $('body').css('overflow','auto');
    }



    window.onclick = function(event) {
        if (event.target == modalNewUser || event.target == modalViewNotificationPopUpgg) {
            modalNewUser.style.display = "none";
            modalViewNotificationPopUpgg.style.display = "none";
            $('body').css('overflow','auto');
        }
    }


    $('#btnAddHost').click(function() {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->addHost?>';
        var onlineClientUsernameValue = document.getElementById("usernameTab").value;
        var onlineClientUsername = '<?php echo $fileController->username;?>';


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { [submitValue]: submitData, [onlineClientUsername]: onlineClientUsernameValue},
            async: false,
            success: function(data){
                if(!JSON.parse(data).success){
                    alert(JSON.parse(data).errorDescription);
                    location.href = "/appointsync.com/public/hosts.php";
                }else if(JSON.parse(data).data.webID){
                    // location.href = "/appointsync.com/public/clientprofile.php/"+JSON.parse(data).data.webID;
                    location.href = "/appointsync.com/public/hosts.php";
                }
            }
        });

    });


</script>


<?php
// User Alert handling
if (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 0) {

    echo '<script language="javascript">', 'alert("User added successfully")', '</script>';
    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 1) {

    echo '<script language="javascript">', 'alert("You are not a premium user")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 2) {

    echo '<script language="javascript">', 'alert("User added successfully")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 3) {

    echo '<script language="javascript">', 'alert("User does not exist, make sure you entered the username correctly. ")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 4) {

    echo '<script language="javascript">', 'alert("This user already exist in your relations")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 5) {

    echo '<script language="javascript">', 'alert("Failed adding user.")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
}elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 6) {

    echo '<script language="javascript">', 'alert("Failed deleting user.")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
}

?>

</body>
</html>
