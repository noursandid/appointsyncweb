<!--<!DOCTYPE html>-->
<!--<html>-->
<!--<head>-->
<!--    <style>-->
<!--        /* Set the size of the div element that contains the map */-->
<!--        #map {-->
<!--            height: 400px;  /* The height is 400 pixels */-->
<!--            width: 100%;  /* The width is the width of the web page */-->
<!--        }-->
<!--    </style>-->
<!--</head>-->
<!--<body>-->
<!--<h3>My Google Maps Demo</h3>-->
<!--<!--The div element for the map -->-->
<!--<div id="map"></div>-->
<!--<script>-->
<!--    // Initialize and add the map-->
<!--    function initMap() {-->
<!--        // The location of Uluru-->
<!--        var uluru = {lat: -25.344, lng: 131.036};-->
<!--        // The map, centered at Uluru-->
<!--        var map = new google.maps.Map(-->
<!--            document.getElementById('map'), {zoom: 13, center: uluru});-->
<!--        // The marker, positioned at Uluru-->
<!--        var marker = new google.maps.Marker({position: uluru, map: map});-->
<!--    }-->
<!--</script>-->
<!--<!--Load the API from the specified URL-->
<!--* The async attribute allows the browser to render the page while the API loads-->
<!--* The key parameter will contain your own API key (which is not needed for this tutorial)-->
<!--* The callback parameter executes the initMap() function-->
<!---->
<!--<script async defer-->
<!--        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvSfHqoBwUd4R4_oIEf6_88KaUvi6qbqU&callback=initMap">-->
<!--</script>-->
<!--</body>-->
<!--</html>-->
<!---->



<!DOCTYPE html>
<html>
<head>
    <title>Geolocation</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
        /* Always set the newMap height explicitly to define the size of the div
         * element that contains the newMap. */
        #newMap {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div id="newMap"></div>
<script>
    // Note: This example requires that you consent to location sharing when
    // prompted by your browser. If you see the error "The Geolocation service
    // failed.", it means you probably did not give permission for the browser to
    // locate you.
    var newMap, infoWindow;
    function initnewMap() {
        newMap = new google.maps.Map(document.getElementById('newMap'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 15
        });
        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                console.log('this: '+ pos.lat + ',' + pos.lng);
                var newMarker = new google.maps.Marker({position: pos, map: newMap,  draggable: true});

                google.maps.event.addListener(newMarker, 'dragend', function (event) {
                    console.log('lat:' + this.getPosition().lat());
                    console.log('long:' + this.getPosition().lng());
                });

                infoWindow.setPosition(pos);
                // infoWindow.setContent('Location found.');
                // infoWindow.open(newMap);
                newMap.setCenter(pos);
            }, function() {
                handleLocationError(true, infoWindow, newMap.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, newMap.getCenter());
        }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(newMap);
    }


    window.document.onload = function(e){
        console.log('this: '+ pos);
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvSfHqoBwUd4R4_oIEf6_88KaUvi6qbqU&callback=initnewMap">
</script>

</body>
</html>