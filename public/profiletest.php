<?php
session_start();
//error_reporting(E_PARSE);
if(isset($_SESSION['user'])) {
}else{header("Location: /appointsync.com/");}
include("../resources/appointSyncClasses/Host.php");
include("../resources/appointSyncClasses/Location.php");
require '../resources/appointSyncController/userwitedbappointsyncinator.php';
require_once '../resources/appointSyncController/notificationController.php';
require '../resources/appointSyncController/API/apicontroller.php';
$userId =  $_SESSION['loggedUser']['terminatorID'];
$webId = $_SESSION['loggedUser']['terminatorwebID'];
$userType = $_SESSION['loggedUser']['terminatorUserType'];
$loggedInUSerType = $_SESSION['loggedUser']['terminatorUserType'];
$userProfileResponse = APIController::getProfile($userId);
//echo '<h1>'.json_encode($userProfileResponse->data).'</h1>';
if (!$userProfileResponse->success || !isset($userProfileResponse)) {
    header('Location: /appointsync.com/public/dashboard.php');
    exit();
}else{
    $userProfile = $userProfileResponse->data;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AppointSync - Profile</title>
    <link rel="apple-touch-icon" href="/appointsync.com/webroot/img/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="/appointsync.com/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/themify-icons.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/animate.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/magnific-popup.css">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="/appointsync.com/public/css/space.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/theme.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/overright.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/normalize.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/style.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/responsive.css">

    <script src="/appointsync.com/public/js/vendor/modernizr-2.8.3.min.js"></script>

    <!--  -->

    <link rel="stylesheet" type="text/css" href="clocktest/bootstrap-clockpicker.min.css">
    <link href="/appointsync.com/public/css/timepicki.css" rel="stylesheet">

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


    <script>
        $(document).ready(function() {
            $("#dateAppointment").datepicker({});
        });

        $('#datetimepicker').data("DateTimePicker").FUNCTION();
    </script>

</head>

<style>


    #userProfile{

        width:80%;

    }

    .navbar-default .navbar-nav>.open>a,.navbar-default .navbar-nav>.open>a:focus,.navbar-default .navbar-nav>.open>a:hover {
        color:#555;
        background-color:red !important;
    }

    .profileClass{
        width:100%;
        margin: 0px auto;
    }
    #profilePicture{
        width:50%;
        float:left;
        min-width:300px;
        margin: 0px auto;
    }
    #profileInfo {
        width: 50%;
        float: left;
        padding-top: 10%;
        min-width: 300px;

    }

    @media only screen and (max-width: 650px){
        #profilePicture{

            float:none;

        }

        #profileInfo{
            margin:0 auto;
            float:none;

        }

        .modal-body{

            width:100%;
            margin:0 auto;
        }

        .locations{

            width:70%;
            min-width: 250px;
            margin:0 auto;
            float:none;
            overflow: auto;
        }
    }

</style>

<body style='width:100%;'>


<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg angle-bg ripple">
        <video autoplay muted id="video-background" loop>
            <source src="http://intimissibd.com/video/video-2.webm" type="video/webm">
        </video>
    </div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">
                    <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">
                    <li><a href="/appointsync.com/public/dashboard.php">Dashboard</a></li>
                    <li><a href="/appointsync.com/public/clients.php">Clients</a></li>
                    <li><a href="#" id="showNotificationPopupLink">Notifications <?php if(isset($numberOfNotificationsText)){echo $numberOfNotificationsText;} ?></a></li>
                    <li><a href="/appointsync.com/public/messages.php">Messages</a></li>
                    <li class="active"><a href="/appointsync.com/public/profile.php" style='background-color:transparent !important;'>Profile</a></li>
                    <li><a href="/appointsync.com/public/logout.php">Log Out</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
    <!--Header-Text-->
    <!--Header-Text/-->
</header>

<section>

    <div style='margin-top:5%;'>

    </div>

    <br><br>

    <div class="container">

        <?php
        //if user Host
        if($loggedInUSerType == 2){
//                $userProfile = new Host($rowGetGetHostProfileDetails);

                $userId = $userProfile->id;
                $userProfilePic = $userProfile->profilePic;
                $userFirstName = $userProfile->firstName;
                $userLastName = $userProfile->lastName;
//                $userWebID = $userProfile->webID;
                $userEmail = $userProfile->email;
                $userPhone = $userProfile->phoneNumber;
                $userUsername = $userProfile->username;
//                    $userProfile->username;
                $userProfession = $userProfile->profession;
                $userLoggedInAs = $userProfile->loggedInAs;
                $userGender = $userProfile->gender;
                $hostWorkingHoursFrom = $userProfile->workingHourFrom;
                $hostWorkingHoursTo = $userProfile->workingHourTo;

                $hostWorkingHoursFromAMPM = date("h:i A", strtotime($hostWorkingHoursFrom));
                $hostWorkingHoursToAMPM = date("h:i A", strtotime($hostWorkingHoursTo));

                if ($userProfilePic == null) {
                    $userProfilePicSet = "<img id='userProfile' src='http://appointsync.com/webroot/userProfile/default.jpeg' alt='Default'>";

                } else {

                    $userProfilePicSet = "<img id='userProfile' src='" . $userProfilePic . "' alt='Profile'>";

                }
                ?>

                <div class="profileClass">
                    <div id="profilePicture"><?php echo $userProfilePicSet; ?></div>
                    <div id="profileInfo">
                        <p> Name: <?php echo $userFirstName . " " . $userLastName; ?></p>
                        <p> Profession: <?php echo $userProfession; ?></p>
                        <p> Gender: <?php echo $userGender; ?></p>
                        <p> Working Hours: <?php echo 'From ' . $hostWorkingHoursFromAMPM . ' To ' . $hostWorkingHoursToAMPM; ?></p>
                        <p> Email: <?php echo $userEmail; ?></p>
                        <p> Phone number: <?php echo $userPhone; ?></p>
                        <p> Username: <?php echo $userUsername; ?></p>
                        <!--               <p><a id="btnChangePassword" style="cursor: pointer;color:#1193d4;">Change Password</a></p>-->

                        <button type="button" class="appointmentButton button1" id="btnLocations">See Locations</button>
                        <button type="button" class="appointmentButton button1" id="btnEditProfile">Edit Profile</button>
                        <button type="button" class="appointmentButton button1" id="btnAccountType">Premium</button>
                        <button type="button" class="appointmentButton button1" id="btnSwitchAccount">Switch Views</button>

                        <?php
                        if ($userType == 2) {

                            $queryGetHostType = mysqli_query($syncminatorcon, "SELECT * FROM HOST_PREMIUM WHERE HOST_ID = '$userId'");
                            while ($rowGetGetType = mysqli_fetch_assoc($queryGetHostType)) {

                                $hostAccountType = $rowGetGetType['HOST_ID'];
                                $hostTypeTillDate = $rowGetGetType['TILL_DATE'];

                            }

                        } else {
                        }

                        ?>
                    </div>

                </div>

                <!-- Locations -->

                <div id="myModalLocations" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <div class="modal-header">
                            <h2>Edit Locations</h2>
                        </div>
                        <div class="modal-body">
                            <h3> Delete or add Locations </h3>
                            <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php"
                                  method="post">
                                <input type="hidden" value="1" name="controllerTypeFlag"/>
                                <div class="locations">
                                    <?php

                                    $getHostLocations = mysqli_query($syncminatorcon, "SELECT* FROM HOST_LOCATION WHERE USER_ID = '" . $userId . "'");
                                    while ($rowgetHostLocations = mysqli_fetch_assoc($getHostLocations)) {

                                        $hostLocations = $rowgetHostLocations['DSC'];
                                        $hostLocationsID = $rowgetHostLocations['ID'];
                                        $hostLocationsLong = $rowgetHostLocations['LONGITUDE'];
                                        $hostLocationsLat = $rowgetHostLocations['LATITUDE'];

                                        echo '<div class="location">'
                                            . '<h3>' . $hostLocations . '</h3>'
                                            . '<img src="/appointsync.com/webroot/testing/map.png"/>'
                                            . '<button type="submit" class="appointmentButton button3">Delete</button>'
                                            . '<input type="hidden" value="' . $hostLocationsLong . '" name="LONGITUDE" />'
                                            . '<input type="hidden" value="' . $hostLocationsLat . '" name="LATITUDE" />'
                                            . '</div>';

                                    } ?>
                                    <div class="location">
                                        <h3>Add New Location</h3><br/>
                                        <a id="btnOpenNewLocations"><img src="/appointsync.com/webroot/testing/addLoc.png"/></a>
                                    </div>
                                </div>
                                <br/><br/>
                                <button class="appointmentButton button1" id="btnAccept" style="display:none;">Accept</button>
                                <button type="button" id="btnCloseLocations" class="appointmentButton button3"
                                        style="display:none;">Cancel
                                </button>
                            </form>
                        </div>
                    </div>
                </div>




                <!-- Edit Profile -->

                <div id="myModalEditProfile" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2>Edit Profile</h2>
                        </div>
                        <div class="modal-body">
                            <h3> Edit the below fields to change your profile info. </h3>
                            <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php"
                                  method="post">
                                <input type="hidden" value="4" name="controllerTypeFlag"/>

                                <p>First Name</p><input type="text" name="firstName" value="<?php echo $userFirstName ?>"/><p></p>
                                <p>Last Name</p><input type="text" name="lastName" value="<?php echo $userLastName ?>"/><p></p>
                                <p>Profession</p><input type="text" name="profession" value="<?php echo $userProfession ?>"/><p></p>
                                <?php
                                $getHostWorkingHours = mysqli_query($syncminatorcon, "SELECT* FROM HOST_WORKING_HOURS WHERE HOST_ID = '" . $userId . "'");
                                while ($rowgetHostWorkingHours = mysqli_fetch_assoc($getHostWorkingHours)) {
                                    $hostFromTime = $rowgetHostWorkingHours['FROM_TIME'];
                                    $hostToTime = $rowgetHostWorkingHours['TO_TIME'];

                                    $hostFromTimeAMPM = date("h:i A", strtotime($hostFromTime));
                                    $hostToTimeAMPM = date("h:i A", strtotime($hostToTime));



                                    echo '<br/><div class="timePickerDivInput" style="clear:both;"><p>Working Hours</p>'
                                        .'<div class="workingHoursFrom" style="width:49%;float:left;"><p>From</p><input id="timepicker1" type="text" name="fromTime" value="'. $hostFromTimeAMPM.'"/></div>'
                                        .'<div class="workingHoursTo" style="width:49%;float:left;"><p>To</p><input id="timepicker2" type="text" name="toTime" value="'. $hostToTimeAMPM.'"/></div>'
                                        .'</div><br/>';





                                    $timeFromHourValue = substr($hostFromTimeAMPM, 0,2);
                                    $timeFromMinuteValue = substr($hostFromTimeAMPM, 3,2);
                                    $timeFromCharValue = substr($hostFromTimeAMPM, 6,2);

                                    $timeToHourValue = substr($hostToTimeAMPM, 0,2);
                                    $timeToMinuteValue = substr($hostToTimeAMPM, 3,2);
                                    $timeToCharValue = substr($hostToTimeAMPM, 6,2);
                                }

                                ?>

                                <div class="editProfileButtonClass">
                                    <button type="button" class="appointmentButton button1 buttonEditProfile" id="btnChangePassword">Change Password</button>
                                    <button type="button" class="appointmentButton button1 buttonEditProfile" id="btnProfilePic">Edit Picture</button>
                                </div><br/>
                                <button type="submit" class="appointmentButton button1" id="btnAccept">Save</button>
                                <button type="button" id="btnCloseEditProfile" class="appointmentButton button3">Cancel</button>

                            </form>
                        </div>
                    </div>
                </div>

                <!-- Profile picture -->

                <div id="myModalProfile" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2>Edit Profile Picture</h2>
                        </div>
                        <div class="modal-body">
                            <h3> Pick up a new profile picture. </h3>
                            <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php"
                                  method="post" enctype="multipart/form-data">
                                <input type="hidden" value="3" name="controllerTypeFlag"/>
                                <input type='file' name='file' required style="margin-right: 10%;">
                                <button type="submit" class="appointmentButton button1" id="btnAccept">Accept</button>
                                <button type="button" id="btnCloseProfile" class="appointmentButton button3">Cancel</button>
                            </form>


                        </div>
                    </div>
                </div>



                <!-- New Location -->

                <div id="myModalNewLocation" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2>New Location</h2>
                        </div>
                        <div class="modal-body">
                            <h3> Please fill the below information </h3>
                            <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php"
                                  method="post">
                                <input type="hidden" value="2" name="controllerTypeFlag"/>
                                <p>Title</p>
                                <input type='text' id='locationTitle' name='locationTitle' class='form-control'/><br/>
                                <p>Set Location</p>
                                <img src="/appointsync.com/webroot/testing/map.png" width="400"/><br/><br/>
                                <p>Longitude and Latitude</p>
                                <input type='text' id='locationLongitude' name='locationLongitude' class='form-control'
                                       value="33.9048606"/><input type='text' id='locationLatitude' name='locationLatitude'
                                                                  class='form-control' value="35.573618"/><br/>
                                <button type="submit" class="appointmentButton button1" id="btnAccept">Save</button>
                                <button type="button" id="btnCloseNewLocation" class="appointmentButton button3">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>


                <!-- Account Type -->

                <div id="myModalAccountType" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2>Account Type</h2>
                        </div>
                        <div class="modal-body">
                            <h3> Below is the status of your account. </h3>

                            <?php
                            if (isset($hostAccountType)) {
                            $premiumTillDateUnix = 0;
                            $NowDateUnix = 0;
                            $premiumTillDateUnix = strtotime($hostTypeTillDate) . '<br/>';
                            $dateNow = date("d-m-Y");
                            $NowDateUnix = strtotime($dateNow) . '<br/>';

                            if ($premiumTillDateUnix >= $NowDateUnix) {
                                $checkRemainingDays = abs($premiumTillDateUnix - $NowDateUnix) / 60 / 60 / 24;
                                echo '<h4>Already a premium member.</h4>';
                                echo '<p>You still have ' . $checkRemainingDays . ' days in order to renew.</p>';
                                echo '<button type="button" id="btnCloseAccountType" class="appointmentButton button3">Cancel</button>';
                            } else {
                            ?>
                            <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                                <input type="hidden" value="5" name="controllerTypeFlag"/>
                                <?php
                                $dt1 = new DateTime();
                                $today = $dt1->format("d-m-Y");

                                $dt2 = new DateTime("+1 month");
                                $date = $dt2->format("d-m-Y");

                                echo '<h4> Normal Account </h4>
                                 Upgrade to premium for an additional 4.99$/month to be able to create offline users!!<br/><br/>
                                 <span style="font-size: 0.8em;">Benefits: keep all your appointments in sync in one place even if some of your clients do not
                                 use applications or own a smart phone.</span>
                               <br/>
                                        ';
                                echo '<button type="submit" class="appointmentButton button1" id="btnAccept">Upgrade Now</button><button type="button" id="btnCloseAccountType" class="appointmentButton button3">Cancel</button>'
                                    . '</form>';
                                }

                                }else{

                                ?>
                                <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                                    <input type="hidden" value="5" name="controllerTypeFlag"/>
                                    <?php
                                    $dt1 = new DateTime();
                                    $today = $dt1->format("d-m-Y");

                                    $dt2 = new DateTime("+1 month");
                                    $date = $dt2->format("d-m-Y");

                                    echo '<h4> Normal Account </h4>
                                 Upgrade to premium for an additional 4.99$/month to be able to create offline users!!<br/><br/>
                                 <span style="font-size: 0.8em;">Benefits: keep all your appointments in sync in one place even if some of your clients do not
                                 use applications or own a smart phone.</span>
                               <br/>
                                        ';
                                    echo '<button type="submit" class="appointmentButton button1" id="btnAccept">Upgrade Now</button><button type="button" id="btnCloseAccountType" class="appointmentButton button3">Cancel</button>'
                                        . '</form>';

                                    }
                                    ?>
                        </div>
                    </div>
                </div>



                <!-- Switch View -->

                <div id="myModalSwitchAccount" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2>Switch View</h2>
                        </div>
                        <div class="modal-body">
                            <p>As a host, you can change views from host to client.</p>
                            <p>Host View: Set and manage appointments with clients.<br/>Client View: Take and manage appointments from other Hosts.</p><br/>
                            <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                                <input type="hidden" value="2" name="controllerTypeFlag"/>
                                <?php

                                if($userLoggedInAs == 0){

                                    echo '<h3>You are currently logged in as host.</h3><br/>',
                                    '<button type="button" class="appointmentButton button1" id="btnSwitchAccount">Switch to Client View</button>';

                                }elseif($userLoggedInAs == 1){

                                    echo '<h3>You are currently logged in as client.</h3><br/>',
                                    '<button type="button" class="appointmentButton button1" id="btnSwitchAccount">Switch to Host View</button>';
                                }

                                ?>
                                <button type="button" id="btnCloseSwitchAccount" class="appointmentButton button3">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>



                <!-- Change password -->

                <div id="myModalPassword" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2>Change Password</h2>
                        </div>
                        <div class="modal-body">
                            <h3> Please fill the below fields. </h3>
                            <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php"
                                  method="post">
                                <input type="hidden" value="6" name="controllerTypeFlag"/>
                                <p>Current Password</p>
                                <input type='password' id='currentPassword' name='currentPassword' class='form-control' required/><br/>
                                <p>New Password</p>
                                <input type='password' id='newPassword' name='newPassword' class='form-control'  onkeyup='check();' required/><br/>
                                <p>Confirm Password</p>
                                <input type='password' id='confirmPassword' name='confirmPassword' class='form-control'  onkeyup='check();' required/><br/>
                                <span id='message'></span><br/>
                                <button type="submit" class="appointmentButton button1" id="btnAcceptPassword">Accept</button>
                                <button type="button" id="btnCloseChangePassword" class="appointmentButton button3">Cancel</button>
                            </form>

                            <script>

                                $('#btnAcceptPassword').hide();
                                document.getElementById('message').style.color = 'red';
                                document.getElementById('message').innerHTML = 'not matching';
                                var check = function() {
                                    if (document.getElementById('newPassword').value ==
                                        document.getElementById('confirmPassword').value && document.getElementById('confirmPassword').value != '') {
                                        document.getElementById('message').style.color = 'green';
                                        document.getElementById('message').innerHTML = 'matching';
                                        $('#btnAcceptPassword').show();
                                    } else {
                                        document.getElementById('message').style.color = 'red';
                                        document.getElementById('message').innerHTML = 'not matching';
                                        $('#btnAcceptPassword').hide();
                                    }
                                }
                            </script>

                        </div>
                    </div>
                </div>

                <?php


        }//if User Client
        elseif($loggedInUSerType == 3){


        }
        ?>



        <div id="notificationPopUpBlockDiv">
            <?php

            require ("notificationview.php");

            ?>
        </div>

</section>

<?php

require ("objectFooter.php");

?>
<!--Maps JS-->

<script src="/appointsync.com/public/js/timepicki.js"></script>




<script type="text/javascript" src="clocktest/bootstrap-clockpicker.min.js"></script>

<!-- AmendScript -->
<script>
    // Get the modal
    var modalLocation = document.getElementById('myModalLocations');
    var modalPicture = document.getElementById('myModalProfile');
    var modalEditProfile = document.getElementById('myModalEditProfile');
    var modalNewLocation = document.getElementById('myModalNewLocation');
    var modalAccountType = document.getElementById('myModalAccountType');
    var modalChangePassword = document.getElementById("myModalPassword");
    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");
    var modalSwitchAccount = document.getElementById("myModalSwitchAccount");

    // Get the button that opens the modal
    var btnLocations = document.getElementById("btnLocations") != null?document.getElementById("btnLocations"):0;
    var btnProfile = document.getElementById("btnProfilePic") != null?document.getElementById("btnProfilePic"):0;
    var btnEditProfile = document.getElementById("btnEditProfile") != null?document.getElementById("btnEditProfile"):0;
    var btnOpenNewLocations = document.getElementById("btnOpenNewLocations") != null?document.getElementById("btnOpenNewLocations"):0;
    var btnOpenAccountType = document.getElementById("btnAccountType") != null?document.getElementById("btnAccountType"):0;
    var btnChangePassword = document.getElementById("btnChangePassword") != null?document.getElementById("btnChangePassword"):0;
    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;
    var btnSwitchAccount = document.getElementById("btnSwitchAccount") != null?document.getElementById("btnSwitchAccount"):0;


    var btnCloseLocations = document.getElementById("btnCloseLocations");
    var btnCloseProfile = document.getElementById("btnCloseProfile");
    var btnCloseEditProfile = document.getElementById("btnCloseEditProfile");
    var btnCloseNewLocation = document.getElementById("btnCloseNewLocation");
    var btnCloseAccountType = document.getElementById("btnCloseAccountType");
    var btnCloseChangePassword = document.getElementById("btnCloseChangePassword");
    var btnCloseSwitchAccount = document.getElementById("btnCloseSwitchAccount");

    var span = document.getElementsByClassName("close")[0];


    // When the user clicks the button, open the modal

    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');

        $.ajax({
            type: "POST",
            url: '/appointsync.com/resources/appointSyncController/notificationController.php',
            data: { 'notificationFlag':'1' }
        });

    });

    btnLocations.onclick = function() {
        modalLocation.style.display = "block";
    }

    btnProfile.onclick = function() {
        modalPicture.style.display = "block";
    }

    btnEditProfile.onclick = function() {
        modalEditProfile.style.display = "block";
    }

    btnOpenNewLocations.onclick = function() {
        modalNewLocation.style.display = "block";
    }

    btnOpenAccountType.onclick = function(){
        modalAccountType.style.display = "block";
    }

    btnChangePassword.onclick = function(){
        modalChangePassword.style.display = "block";
    }

    btnSwitchAccount.onclick = function(){
        modalSwitchAccount.style.display = "block";
    }

    btnCloseLocations.onclick = function() {
        modalLocation.style.display = "none";
    }

    btnCloseProfile.onclick = function() {
        modalPicture.style.display = "none";
    }

    btnCloseEditProfile.onclick = function() {
        modalEditProfile.style.display = "none";
    }

    btnCloseNewLocation.onclick = function() {
        modalNewLocation.style.display = "none";
    }

    btnCloseAccountType.onclick = function() {
        modalAccountType.style.display = "none";
    }

    btnCloseChangePassword.onclick = function() {
        modalChangePassword.style.display = "none";
    }

    btnCloseSwitchAccount.onclick = function(){
        modalSwitchAccount.style.display = "none";
    }

    span.onclick = function() {
        modalLocation.style.display = "none";
    }



    window.onclick = function(event) {
        if (event.target == modalLocation) {
            modalLocation.style.display = "none";
        }
    }



    $('#timepicker1').timepicki({start_time: ["<?php echo $timeFromHourValue;?>", "<?php echo $timeFromMinuteValue;?>", "<?php echo $timeFromCharValue;?>"]});
    $('#timepicker2').timepicki({start_time: ["<?php echo $timeToHourValue;?>", "<?php echo $timeToMinuteValue;?>", "<?php echo $timeToCharValue;?>"]});

</script>


<?php
//Location Alert handling
if(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 1){

    echo '<script language="javascript">',
    'alert("The Location was successfully Deleted")',
    '</script>';
    sleep(2);

    unset($_SESSION["ProfileController"]);
}elseif(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 2){

    echo '<script language="javascript">',
    'alert("The Location was successfully Added")',
    '</script>';

    sleep(2);

    unset($_SESSION["ProfileController"]);
}elseif(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 3){

    echo '<script language="javascript">',
    'alert("Profile successfully updated")',
    '</script>';

    sleep(2);

    unset($_SESSION["ProfileController"]);
}elseif(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 4){

    echo '<script language="javascript">',
    'alert("Premium membership successfully activated")',
    '</script>';

    sleep(2);

    unset($_SESSION["ProfileController"]);
}
//Profile Picture Handling
if(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 1){

    echo '<script language="javascript">',
    'alert("ERROR: Your file was larger than 5 Megabytes in size.")',
    '</script>';
    sleep(2);

    unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 2){

    echo '<script language="javascript">',
    'alert("ERROR: Your image was not .gif, .jpg, or .png.")',
    '</script>';
    sleep(2);

    unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 3){

    echo '<script language="javascript">',
    'alert("ERROR: An error occured while processing the file. Try again.")',
    '</script>';
    sleep(2);

    unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 4){

    echo '<script language="javascript">',
    'alert("Image successfully updated.")',
    '</script>';
    sleep(2);

    unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 5){

    echo '<script language="javascript">',
    'alert("ERROR: File not uploaded. Try again.")',
    '</script>';
    sleep(2);

    unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 6){

    echo '<script language="javascript">',
    'alert("ERROR: Not file attached.")',
    '</script>';
    sleep(2);

    unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["passwordController"]) && $_SESSION['passwordController'] == 1){

    echo '<script language="javascript">',
    'alert("Your Current Password is wrong.")',
    '</script>';
    sleep(1.3);

    unset($_SESSION["passwordController"]);
}elseif(isset($_SESSION["passwordController"]) && $_SESSION['passwordController'] == 2){

    echo '<script language="javascript">',
    'alert("Passwords do not match.")',
    '</script>';
    sleep(1.3);

    unset($_SESSION["passwordController"]);
}elseif(isset($_SESSION["passwordController"]) && $_SESSION['passwordController'] == 3){

    echo '<script language="javascript">',
    'alert("Passwords successfully updated.")',
    '</script>';
    sleep(1.3);

    unset($_SESSION["passwordController"]);
}

?>

</body>
</html>
