<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
//error_reporting(E_PARSE);
if (isset($_SESSION['user'])) {
} else {
    header("Location: /appointsync.com/");
}

include_once dirname(__FILE__) . '/../resources/Classes/MappedNotification.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';

$notificationResponse = APIController::getNotification();
if($notificationResponse->success){
    $notifications = $notificationResponse->data;
//echo json_encode($notifications);
}else{}


?>

<script src="/appointsync.com/public/moments.js"></script>
<link rel="stylesheet" href="/appointsync.com/public/css/style.css">
<style>
    .modal-body{

        overflow: auto;
        max-height: 35em;
        width: 100%;
    }


    /*#myModalNotificationsPopup{display:block}*/

</style>
<div id="myModalNotificationsPopup" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close closeNotification">&times;</span>
        <div class="modal-header">
            <h2>Notifications</h2>
        </div>
        <div class="modal-body">
            <form action=""
                  method="post">
                <input type="hidden" value="1" name="controllerTypeFlag"/>
                <div class="notifications" style="margin-top:2%;">
                    <div class="newNotifications">
                        <?php

                        $newNotificationCountLoop = 0;
                        foreach ($notifications as $notification) {

                            $notificationType = $notification->type;
                            if($notification->status == 0){
                        $newNotificationCount = $notification->status;
                        if ($newNotificationCount == 0) {
                            $newNotificationCountLoop++;
                        }
                        if ($newNotificationCountLoop == 0) {
                            unset($newNotificationCountLoop);
                        } else {
                            $newNotificationCountLoopDisplay = '<span class="redDot">' . $newNotificationCountLoop . '</span>';
                        }
                        if ($newNotificationCount == 0) {
                            $newNotification = true;
                        } else {
                            $newNotification = false;
                        }

                        if (isset($notification->post) && $notification->post != '') {

                        ?>

                        <script>
                            var appointmentDate = moment.unix(<?php echo $notification->post ?>);
                            var appointmentFormatted = appointmentDate.format('LL');
                        </script>

                        <?php

                        if ($notificationType == 0 || $notificationType == 1 || $notificationType == 2 || $notificationType == 3 || $notificationType == 4) {

                            echo "<a href='/appointsync.com/public/appointment.php/" . $notification->appointmentWebID . "'><div class='newNotification'> " . $notification->description . " <span id='appointmentTime'><script>document.write(appointmentFormatted);</script></span><br/></div></a>";

                        } elseif ($notificationType == 5) {

                            echo "<a href='/appointsync.com/public/appointmentnotes.php/" . $notification->appointmentWebID . "'><div class='newNotification'>" . $notification->description . " <span id='appointmentTime'><script>document.write(appointmentFormatted);</script></span><br/></div></a>";

                        }
                        }elseif (!isset($notification->post) || $notification->post == '') {


                            if ($notificationType == 10) {

                                echo "<a href='/appointsync.com/public/clientprofile.php/" . $notification->fromWebID . "'><div class='newNotification'>" . $notification->description . "<br/></div></a>";

                            } elseif ($notificationType == 11) {

                                echo "<div class='newNotification'>" . $notification->description . "<br/></div></a>";

                            }

                        }
                        }
                            }

                            if(!isset($newNotification)){echo '<hr/> <h4>No New Notifications</h4>';}

                        foreach ($notifications as $notification) {

                            if ($notification->status == 1) {
                                $olderNotificationFilled = true;
                            }
                        }

                        if (isset($olderNotificationFilled)){echo '<hr/> <h4>Older</h4>';}

                        foreach ($notifications as $notification) {

                            $notificationType = $notification->type;
                            if($notification->status == 1){
                                if (isset($notification->post) && $notification->post != '') {

                                ?>

                                <script>
                                    var appointmentDate = moment.unix(<?php echo $notification->post ?>);
                                    var appointmentFormatted = appointmentDate.format('LL');
                                </script>

                                <?php

                                if ($notificationType == 0 || $notificationType == 1 || $notificationType == 2 || $notificationType == 3 || $notificationType == 4) {

                                    echo "<a href='/appointsync.com/public/appointment.php/" . $notification->appointmentWebID . "'><div class='newNotification'> " . $notification->description . " <span id='appointmentTime'><script>document.write(appointmentFormatted);</script></span><br/></div></a>";

                                } elseif ($notificationType == 5) {

                                    echo "<a href='/appointsync.com/public/appointmentnotes.php/" . $notification->appointmentWebID . "'><div class='newNotification'>" . $notification->description . " <span id='appointmentTime'><script>document.write(appointmentFormatted);</script></span><br/></div></a>";

                                }
                            }elseif (!isset($notification->post) ||  $notification->post == '') {


                                if ($notificationType == 10) {

                                    echo "<a href='/appointsync.com/public/clientprofile.php/".$notification->fromWebID."'><div class='newNotification'>" . $notification->description . "<br/></div></a>";

                                }elseif($notificationType == 11){

                                    echo "<div class='newNotification'>" . $notification->description . "<br/></div></a>";

                                }

                            }

                            }



                        }

                        ?>

                    </div>

                </div>
                <br/><br/>
<!--                <button class="appointmentButton button1" id="btnAccept" >Accept</button>-->
<!--                <button type="button" id="btnCloseNotificationPopUp" class="appointmentButton button3">Close</button>-->
            </form>
        </div>
    </div>



    <script>

        var myModalNotificationsPopup = document.getElementById('myModalNotificationsPopup');

        // var btnCloseNotificationPopUp = document.getElementById("btnCloseNotificationPopUp");

        var spanCloseNotification = document.getElementsByClassName("closeNotification")[0];


        // When the user clicks the button, open the modal

        // btnCloseNotificationPopUp.onclick = function() {
        //
        //     myModalNotificationsPopup.style.display = "none";
        //     $('body').css('overflow','auto');
        // }

        spanCloseNotification.onclick = function() {
            myModalNotificationsPopup.style.display = "none";
            $('body').css('overflow','auto');
        }


    </script>
</div>
