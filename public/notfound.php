<?php
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';

$fileController = new MappedRouterController();
?>

<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Not Found</title>

    <?php

    require ("objectHeader.php");


    ?>

</head>

<body style='width: 100%;'>


<header class="blue-bg relative fix" id="home">

    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                        src="/appointsync.com/public/img/newlogo.png"
                        style="min-width: 150px; width: 150px; margin-top: -5%;"
                        alt="Logo">
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">

                </ul>
            </div>

        </div>
    </nav>
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
</header>

<section style="margin-top:10%;">


    <div style="width:80%;margin:auto;text-align: center;">

     <h4>The page you are looking for is not found.</h4>

    </div>


</section>

<?php

require ("objectFooter.php");

?>


</body>
</html>
