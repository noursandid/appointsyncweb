<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/17/18
 * Time: 4:38 PM
 */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
//error_reporting(E_PARSE);
if (isset($_SESSION['user'])) {
} else {
    header("Location: /appointsync.com/");
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}


$_SESSION['pageName'] = 'appointmentNote';

include_once dirname(__FILE__) . '/../resources/Classes/MappedAppointment.php';
include_once dirname(__FILE__) . '/../resources/Classes/UserProfile.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedAppointmentNote.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedAttachment.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedAmendedAppointment.php';

$host = unserialize($_SESSION['user']);

$getURL = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$sendAppointmentWebID = explode("appointmentnotes.php/", $getURL)[1];
$webID = $sendAppointmentWebID;
$appointmentNotesResponse = APIController::getAppointmentNotes($sendAppointmentWebID);
if ($appointmentNotesResponse->success) {
   $appointmentNotes = $appointmentNotesResponse->data;
} else {
    header("Location: /appointsync.com/public/dashboard.php");
    exit();
}

$appointmentAttachmentsResponse = APIController::getAppointmentAttachments($sendAppointmentWebID);
if ($appointmentAttachmentsResponse->success) {

   $appointmentAttachments = $appointmentAttachmentsResponse->data;



} else {
     header("Location: /appointsync.com/public/dashboard.php");
     exit();
}

$appointmentResponse = APIController::getAppointment($sendAppointmentWebID);
if ($appointmentResponse->success) {
    $appointment = $appointmentResponse->data;
} else {
    header("Location: /appointsync.com/public/dashboard.php");
    exit();
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Notes</title>

    <?php

    require ("objectHeader.php");

    ?>


</head>

<body style='width:100%;'>
<style>

    .navbar-default .navbar-nav>.open>a,.navbar-default .navbar-nav>.open>a:focus,.navbar-default .navbar-nav>.open>a:hover {
        color:#555;
        background-color:red !important;
    }



</style>


<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg angle-bg ripple">
        <video autoplay muted id="video-background" loop>
            <source src="http://intimissibd.com/video/video-2.webm" type="video/webm">
        </video>
    </div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">
                    <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                </a>
            </div>
            <?php

            require ("objectMenu.php");

            ?>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
    <!--Header-Text-->
    <!--Header-Text/-->
</header>

<section>
    <div style='margin-top:5%;'>
<?php //echo '<br/><br/><br/><br/><br/><br/><br/><br/><br/>'.$webID ?>
<?php

if($host->type == 2){

    if ($appointment->webID == $webID) {


//appointmentClass

        $appointmentRealClientID = $appointment->client->id;
        $appointmentHostID = $appointment->appointmentHostID;
        $appointmentRealID = $appointment->appointmentID;
        $appointmentStartTime = $appointment->startDate;
        $appointmentStartStatus = $appointment->status;
        $appointmentHasAttachment =  $appointment->hasAttachment;
        $appointmentClientProfile = $appointment->client->profilePic;
        $appointmentHasAmendment = isset($appointment->amendedAppointment);
        $premiumTillDateUnix = 0;
        $NowDateUnix = 0;
        $dateNow = date("d-m-Y H:i");
        $NowDateUnix = strtotime($dateNow);
        if ($appointment->startDate < $NowDateUnix) {
            $appointmentDateTimeStatus = 'Past';
            $appointmentStartStatus = 6;
            unset($appointmentHasAmendment);
            $appointmentHasAmendment = 0;
        }else{
            $appointmentDateTimeStatus = 'Present';
        }


        $clientUserRealID = $appointment->client->id;
        $clientFirstName = $appointment->client->firstName;
        $clientLastName = $appointment->client->lastName;
        $clientWebID = $appointment->client->webID;
        $clientUsername = $appointment->client->username;


        if (isset($appointment->amendedAppointment)) {

                $appointmentAmendedStartTime = $appointment->amendedAppointment->startDate;
                $appointmentAmendedEndTime = $appointment->amendedAppointment->endDate;

//            if ($appointment->amendedAppointment->locationID != $appointment->locationID) {
//                for ($locations as $location){
//                    if ($location->locationID == $appointmentLocation->locationID){
//                        $appointmentLocation = $location
//                    }
//                }
//                $appointmentLocationString = $appointmentLocation . ' -> <span style="color:red">' . $appointmentAmendedLocation . '</span>';
//            } else {
//                $appointmentLocationString = $appointmentLocation;
//            }

            if ($appointment->amendedAppointment->startDate != $appointment->startDate) {

                $appointmentDateString = $appointment->startDate . ' -> <span style="color:red">' . $appointment->amendedAppointment->startDate . '</span>';
            } else {
                $appointmentDateString = $appointment->startDate;
            }

//                if($appointmendAmendedTime != $appointmentTime){
//
//                  $appointmentTimeString = $appointmentTime .' -> <span style="color:red">'.$appointmendAmendedTime.'</span>';
//                }else{$appointmentTimeString = $appointmentTime;}


        }


        if ($appointmentStartStatus == 1) {
            $appointmentStatus = 'Requested';
        } elseif ($appointmentStartStatus == 2) {
            $appointmentStatus = 'Pending';
        } elseif ($appointmentStartStatus == 3) {
            $appointmentStatus = 'Accepted';
        } elseif ($appointmentStartStatus == 4) {
            $appointmentStatus = 'Rejected';
        }elseif ($appointmentStartStatus == 6) {
            $appointmentStatus = 'Passed';
        }

//Appointment display

        echo "<div class='notesAppointmentObject'>"
            . "<div id='notesAppointmentObjectImg'>".$appointment->client->profilePic."</div>"
            ."<div id='notesAppointmentObjectText'>"
            . '<a href="/appointsync.com/public/clientprofile.php/'.$clientWebID.'" style="color:#1193d4;">'.$clientFirstName .' '.$clientLastName.'</a><br/>';
        if ($appointmentHasAmendment == 1) {
if ($appointment->startDate != $appointment->amendedAppointment->startDate) {
?>

        <script>
            var startDate = moment.unix(<?php echo $appointment->startDate ?>);
            var startDate = startDate.format('LLLL');

            var startDateAmend = moment.unix(<?php echo $appointment->amendedAppointment->startDate ?>);
            var startDateAmend = startDateAmend.format('LLLL');

        </script>

        <?php

        echo  '<strong>From: </strong><span class="tooltipAppointment" onclick="" style="color:red"><script>document.write(startDateAmend);</script>',
        '<span class="tooltipAppointmentText" style="width:27em;">Was on <script>document.write(startDate);</script></span>',
        '</span><br/>';

        }else{
        ?>

        <script>
            var startDate = moment.unix(<?php echo $appointment->startDate ?>);
            var startDate = startDate.format('LLLL');

        </script>

        <?php

        echo "From: <span id='startTime'><script>document.write(startDate);</script></span><br/>";
        }if($appointment->endDate != $appointment->amendedAppointment->endDate) {


        ?>

        <script>
            var endDate = moment.unix(<?php echo $appointment->endDate ?>);
            var endDate = endDate.format('LLLL');

            var endDateAmend = moment.unix(<?php echo $appointment->amendedAppointment->endDate ?>);
            var endDateAmend = endDateAmend.format('LLLL');

        </script>

        <?php

        echo  '<strong>To: </strong><span class="tooltipAppointment" onclick="" style="color:red"><script>document.write(endDateAmend);</script>',
        '<span class="tooltipAppointmentText" style="width:27em;">Was on <script>document.write(endDate);</script></span>',
        '</span><br/>';

        }else{
        ?>

        <script>
            var endDate = moment.unix(<?php echo $appointment->endDate ?>);
            var endDate = endDate.format('LLLL');

        </script>

        <?php

        echo "To: <span id='startTime'><script>document.write(endDate);</script></span><br/>";
        }
        }else {

        ?>

        <script>
            var startDate = moment.unix(<?php echo $appointment->startDate ?>);
            var startDate = startDate.format('LLLL');

            var endDate = moment.unix(<?php echo $appointment->endDate ?>);
            var endDate = endDate.format('LLLL');

        </script> <?php
        echo "From: <span id='startTime'><script>document.write(startDate);</script></span> <br/> To: <span id='endTime'><script>document.write(endDate);</script></span>";
        }
        } else {
        ?>

        <script>
            var startDate = moment.unix(<?php echo $appointment->startDate ?>);
            var startDate = startDate.format('LLLL');

            var endDate = moment.unix(<?php echo $appointment->endDate ?>);
            var endDate = endDate.format('LLLL');

        </script> <?php
        echo "From: <span id='startTime'><script>document.write(startDate);</script></span> <br/>To: <span id='endTime'><script>document.write(endDate);</script></span>";
        }

        echo "<br/>"
            . "Status: " . $appointmentStatus . "<br/>"
            .'<a href="/appointsync.com/public/appointment.php/'.$webID.'"  class="buttonAddClient buttonAddClient2" id="addUser">Back to appointment</a>';



        echo "</div></div></a>";


    }




?>



        <div style="width: 80%; height: 20px; border-bottom: 1px solid #A9A9A9; text-align: center; margin:-5% auto;">
  <span style="font-size: 2em; background-color: #1193d4; padding: 0 10px; color:white">
   Notes
  </span>
        </div>

        <div class="appointmentNotes">

            <?php


                foreach ($appointmentNotes as $appointmentNote) {
                    $appointmentNotesId = $appointmentNote->noteID;
                    $mainAppointmentNoteId = $appointmentNote->noteAppointmentID;
                    $appointmentNoteUserId = $appointmentNote->noteUserID;
                    $appointmentNoteText = $appointmentNote->note;

                    if ($appointmentNoteUserId != $host->id) {
                        echo "<div id='senderAppointmentNote'>" . $appointmentNote->date .' '. $appointmentNoteText . "</div>";
                    } else {
                        echo "<div id='receiverAppointmentNote'>" . $appointmentNoteText . "</div>";
                    }
                }

                    if (isset($appointmentAttachments)){
                        foreach ($appointmentAttachments as $appointmentAttachment) {
                            $appointmentAttachmentUserID = $appointmentAttachment->attachmentUserID;
                            if ($appointmentAttachment->attachmentUserID == $host->id) {

                                $ext = pathinfo($appointmentAttachment->attachment, PATHINFO_EXTENSION);


                                if($ext == 'pdf'){
                                echo "<div id='receiverAppointmentNote' style='min-height: 500px;'><a href='" . $appointmentAttachment->attachment . "' data-lightbox='image-1' data-title='pdf File'><iframe src=\"".$appointmentAttachment->attachment."\" width=\"100%\" style=\"min-height:470px\"></iframe></a></div>";
                                }else {
                                    echo "<div id='receiverAppointmentNote'><a href='" . $appointmentAttachment->attachment . "' data-lightbox='image-1' data-title='image'> <img src='" . $appointmentAttachment->attachment . "' alt='Default'></a> </div>";
                                }
                            } else {
                                $ext = pathinfo($appointmentAttachment->attachment, PATHINFO_EXTENSION);
                                if($ext == 'pdf'){
                                    echo "<div id='senderAppointmentNote' style='min-height: 500px;'><a href='" . $appointmentAttachment->attachment . "' data-lightbox='image-1' data-title='pdf File'><iframe src=\"".$appointmentAttachment->attachment."\" width=\"100%\" style=\"min-height:470px\"></iframe></a></div>";
                                }else {
                                    echo "<div id='senderAppointmentNote'><a href='" . $appointmentAttachment->attachment . "' data-lightbox='image-1' data-title='image'> <img src='" . $appointmentAttachment->attachment . "' alt='Default'></a> </div>";
                                }
                            }
                        }
                }
            if(!isset($appointmentNotesId) && !isset($appointmentAttachments) && !isset($appointmentAttachmentUserID)){

                echo'<br/><h2>This appointment does not contain any notes yet.</h2><br/><br/>';
            }
            ?>

            <div class="appointmentNotesTextAreaControl">
            <form action="/appointsync.com/resources/Controller/API/APIRouter.php"
                  method="post">
               <br/><textarea placeholder="Enter response here" name="userTextNote"></textarea>
                <input type="hidden" value="<?php echo $sendAppointmentWebID ?>" name="addAppointmentNote"/>
            <br/>
                <button type="button" class="appointmentButton button1" id="btnNotePic">Upload File</button>
                <button name="submit" value="addAppointmentNote" type="submit" class="appointmentButton button1" id="btnAccept">Save</button>
            </form>
            </div>
            <br/><br/>
        </div>
</div>


</section>

<div id="myModalNotePicture" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <h2>Add an Attachment</h2>
        </div>
        <div class="modal-body">
            <h3> Pick a file to upload. </h3>
            <form action="/appointsync.com/resources/Controller/API/APIRouter.php"
                  method="post" enctype="multipart/form-data">
                <input type="hidden" value="<?php echo $sendAppointmentWebID ?>" name="addAppointmentAttachment"/>
                <input type='file' name='file' required style="margin-right: 10%;">

                <span style="font-size: 0.8em">Note: you can attach files with formats pdf, doc, docx, png, jpg, jpeg, and gif.</span><br/>
                <button type="submit" name="submit" value="addAttachmentNote" class="appointmentButton button1" id="btnAccept">Accept</button>
                <button type="button" id="btnClosePicModal" class="appointmentButton button3">Cancel</button>
            </form>


        </div>
    </div>
</div>

<?php
require ("objectFooter.php");
?>


<script>
    // Get the modal
    var modalNotePicture = document.getElementById('myModalNotePicture');
    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");


    // Get the button that opens the modal
    var btnNotePic = document.getElementById("btnNotePic") != null?document.getElementById("btnNotePic"):0;
    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;


    var btnClosePicModal = document.getElementById("btnClosePicModal");



    // When the user clicks the button, open the modal


    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });


    btnNotePic.onclick = function() {
        modalNotePicture.style.display = "block";
        $('body').css('overflow','hidden');
    }


    btnClosePicModal.onclick = function() {
        modalNotePicture.style.display = "none";
        $('body').css('overflow','auto');
    }

</script>
<?php
if(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 1){

echo '<script language="javascript">',
    'alert("ERROR: Your file was larger than 5 Megabytes in size.")',
        '</script>';
sleep(2);

unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 2){

echo '<script language="javascript">',
    'alert("ERROR: Your image was not .gif, .jpg, or .png.")',
        '</script>';
sleep(2);

unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 3){

echo '<script language="javascript">',
    'alert("ERROR: An error occured while processing the file. Try again.")',
        '</script>';
sleep(2);

unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 4){

echo '<script language="javascript">',
    'alert("Image successfully updated.")',
        '</script>';
sleep(2);

unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 5){

echo '<script language="javascript">',
    'alert("ERROR: File not uploaded. Try again.")',
        '</script>';
sleep(2);

unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 6){

echo '<script language="javascript">',
    'alert("ERROR: Not file attached.")',
        '</script>';
sleep(2);

unset($_SESSION["imageUploadStatus"]);
}elseif(isset($_SESSION["appointmentController"]) && $_SESSION['appointmentController'] == 4){

echo '<script language="javascript">',
    'alert("ERROR: failed uploading note.")',
        '</script>';
sleep(1.5);

unset($_SESSION["appointmentController"]);
}


?>
</body>

</html>