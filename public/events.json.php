<?php
session_start();
error_reporting(E_PARSE);

include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';


$appointmentsResponse = APIController::getAppointments();

if ($appointmentsResponse->success) {
    $appointments = $appointmentsResponse->data;
    $_SESSION['appointments']=serialize($appointments);
    foreach ($appointments as $appointment) {

        $dateNow = date("d-m-Y H:i");
        $NowDateUnix = strtotime($dateNow);

        $appointmentStatus = $appointment->status;
        if($appointmentStatus == 1){
            $appointmentStatusFlag = 'event-new';
        }elseif($appointmentStatus == 2){
            $appointmentStatusFlag = 'event-amend';
        }elseif($appointmentStatus == 3){
            $appointmentStatusFlag = 'event-accept';
        }elseif($appointmentStatus == 4){
            $appointmentStatusFlag = 'event-reject';
        }
        if($appointment->endDate < $NowDateUnix){
            $appointmentStatusFlag = 'event-old';
        }


        $client = $appointment->client;
        $calendar[] = array(
            'id' => $appointment->appointmentID,
            'title' => $client->identifier,
            'url' => "appointment.php/" . $appointment->webID . "",
            "class" => $appointmentStatusFlag,
            'start' => $appointment->startDate*1000,
            'end' => $appointment->endDate*1000
        );
        $calendarData = array(
            "success" => 1,
            "result" => $calendar
        );
    }
} else {
    die("database error:" . $appointmentsResponse->errorDescription);
}

echo json_encode($calendarData);

