<?php
?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AppointSync - Home</title>
        <link rel="apple-touch-icon" href="/appointsync.com/webroot/img/apple-touch-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

        <!-- Plugin-CSS -->
        <link rel="stylesheet" href="/appointsync.com/public/css/bootstrap.min.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/themify-icons.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/animate.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/magnific-popup.css">

        <!-- Main-Stylesheets -->
        <link rel="stylesheet" href="/appointsync.com/public/css/space.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/theme.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/overright.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/normalize.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/style.css">
        <link rel="stylesheet" href="/appointsync.com/public/css/responsive.css">

     <script src="/appointsync.com/public/js/vendor/modernizr-2.8.3.min.js"></script>


</head>

<footer class="black-bg">
<div class="container" class="black-bg">


    <div class="space-80"></div>
    <div class="row text-white wow fadeIn">
        <div class="col-xs-12 text-center">
            <div class="social-menu">
                <a href="#"><span class="ti-facebook"></span></a>
                <a href="#"><span class="ti-twitter-alt"></span></a>
                <a href="#"><span class="ti-linkedin"></span></a>
                <a href="#"><span class="ti-pinterest-alt"></span></a>
            </div>
            <div class="space-20"></div>
            <p>@ 2018 <a href="http://AppointSync.com/">AppointSync</a> all right reserved.</p>
        </div>
    </div>
    <div class="space-20"></div>
</div>
</footer>

<script src="/appointsync.com/public/js/vendor/jquery-1.12.4.min.js"></script>
<script src="/appointsync.com/public/js/vendor/bootstrap.min.js"></script>
<!--Plugin JS-->
<script src="/appointsync.com/public/js/owl.carousel.min.js"></script>
<script src="/appointsync.com/public/js/scrollUp.min.js"></script>
<script src="/appointsync.com/public/js/magnific-popup.min.js"></script>
<script src="/appointsync.com/public/js/ripples-min.js"></script>
<script src="/appointsync.com/public/js/contact-form.js"></script>
<script src="/appointsync.com/public/js/spectragram.min.js"></script>
<script src="/appointsync.com/public/js/ajaxchimp.js"></script>
<script src="/appointsync.com/public/js/wow.min.js"></script>
<script src="/appointsync.com/public/js/plugins.js"></script>
<script src="/appointsync.com/public/js/maps.js"></script>
<!--Active JS-->
<script src="/appointsync.com/public/js/main.js"></script>
<!--Maps JS-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>


</body>
</html>
