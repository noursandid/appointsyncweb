<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedClient.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedAppointment.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedAmendedAppointment.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedLocation.php';
include_once dirname(__FILE__) . '/../resources/Classes/UserProfile.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (isset($_SESSION['user'])) {
} else {
    header("Location: /appointsync.com/");
}
$loggedInUser = unserialize($_SESSION['user']);
if($loggedInUser->type != 2){
    header("Location: /appointsync.com/");
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}


$_SESSION['pageName'] = 'clientProfile';

$hostResponse = APIController::getProfile();
$getAppointments = APIController::getAppointments();
$locationsResponse = APIController::getLocations();
if ($hostResponse->success) {
    $host = $hostResponse->data;
} else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}
if ($locationsResponse->success) {
    $locations = $locationsResponse->data;
} else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}
if ($getAppointments->success) {
    $appointments = $getAppointments->data;
} else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}

$getURL = $_SERVER["PATH_INFO"];
$sendUserWebID = explode("/", $getURL)[1];
$_SESSION['selectedClientWebID'] = $sendUserWebID;
$clientResponse = APIController::getClient($sendUserWebID);
if($clientResponse->success){
    $client = $clientResponse->data;
}else{ header("Location: /appointsync.com/public/clients.php"); exit();}


$clientPermission = APIController::getClientPermissions($sendUserWebID);
if($clientPermission->success){
    $permission = $clientPermission->data;
}else{
//    header("Location: /appointsync.com/public/dashboard.php"); exit();
}


$webID = $sendUserWebID;
$isFound = false;
    if ($client->webID == $webID) {
        $isFound = true;

}
if (!$isFound) {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}

$fileController = new MappedRouterController();
?>

<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Client Profile</title>

    <?php

    require ("objectHeader.php");

    ?>


    <script>

        function myFunction() {
            var input = document.getElementById("Search");
            var filter = input.value.toLowerCase();
            var nodes = document.getElementsByClassName('clientAppointmentObject');

            for (i = 0; i < nodes.length; i++) {
                if (nodes[i].innerText.toLowerCase().includes(filter)) {
                    nodes[i].style.display = "block";
                } else {
                    nodes[i].style.display = "none";
                }
            }
        }

    </script>


</head>

<style>
    #userProfile {
        width: 80%;
    }

    .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus,
    .navbar-default .navbar-nav > .open > a:hover {
        color: #555;
        background-color: red !important;
    }

    .profileClass {
        width: 100%;
        margin: 0px auto;
        overflow: hidden;
    }

    #profilePicture {
        width: 50%;
        float: left;
        min-width: 300px;
        margin: 0px auto;
        text-align: center;
    }

    #profileInfo {
        text-align: center;
        width: 50%;
        float: left;
        padding-top: 2%;
        min-width: 300px;
    }

    #profileInfo button{
        float: none;
        margin: 1% 1% 1% 1%;
    }

    .profileClass .button1 {
        width: 40% !important;
        max-width: 40% !important;
    }

    .profileClass .button3 {
        width: 40% !important;
        max-width: 40% !important;
    }



    @media only screen and (max-width: 1050px) {
        .clientAppointmentObject {
            margin: 1% auto;
            float: none;
        }

    }
    @media only screen and (max-width: 650px) {
        #profilePicture {
            float: none;
        }

        #profileInfo {
            margin: 0 auto;
            float: none;
        }

        .modal-body {
            width: 100%;
            margin: 0 auto;
        }

        .clientAppointmentObject {
            margin: 1% auto;
            float: none;
        }
    }

    #btnRequestAppointment {
        min-width: 200px !important;
    }

    .clientAppointmentObject a {
        cursor: pointer;
        font-style: normal;
    }

    .clientAppointmentObject {
        cursor: pointer;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

</style>

<body style='width: 100%;'>

<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">

    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                            src="/appointsync.com/public/img/newlogo.png"
                            style="min-width: 150px; width: 150px; margin-top: -5%;"
                            alt="Logo">
                </a>
            </div>
            <?php

            require ("objectMenu.php");

            ?>
        </div>
    </nav>
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
</header>

<section>

    <div style='margin-top: 5%;'></div>

    <br> <br>

    <div class="container">

        <?php
        // if user Host

        if ($host->type == 2) {
                if ($client->webID == $webID) {
                    if ($client->type == 2 || $client->type == 3) {
                        echo '<div class="profileClass">',
                            '<div id="profilePicture">' . $client->profilePic . '</div>',
                        '<div id="profileInfo">',
                            '<br/><br/><p> <strong>Name:</strong> ' . $client->firstName . " " . $client->lastName . '</p>',
                            '<p> <strong>Gender:</strong> ' . $client->gender . '</p>',
                            '<p> <strong>Email: </strong>' . $client->email . '</p>',
                            '<p> <strong>Phone number:</strong> ' . $client->phoneNumber . '</p>',
                            '<p> <strong>Date of birth: </strong><script> document.write(moment.unix(' .$client->dob . ').format("LL"));</script></p>',
                            '<p> <strong>Username:</strong> ' . $client->username . '</p>',
                        '<button class="appointmentButton button6" id="btnRequestAppointment" style="min-width: 250px !important;">Add an Appointment</button>',
                        '<button class="appointmentButton button6" id="btnSendMessage" style="min-width: 250px !important;">Send Message</button>';
                        if($host->hiddenFiles == 0) {
                            echo '<button class="appointmentButton button6" id="btnClientFile" style="min-width: 250px !important;">Client File</button>';
                        }elseif($host->hiddenFiles == 1){
                            echo '<button class="appointmentButton button6" id="btnClientFileSecure" style="min-width: 250px !important;">Client File</button>';
                        }
                       echo '<button class="appointmentButton button6" id="btnUserSettings" style="min-width: 250px !important;">Settings</button>',
//                        '<button class="appointmentButton button3" id="btnDeleteUser" style="min-width: 250px !important;">Delete User</button>',
                        '</div>',
                        '</div>';
                    } else if ($client->type == 4) {
                        echo '<div class="profileClass">',
                            '<div id="profilePicture">' . $client->profilePic . '</div>',
                        '<div id="profileInfo"> <br/><br/>',
                            '<p> <strong>Name: </strong>' . $client->identifier . '</p>',
                            '<p> <strong>Gender: </strong>' . $client->gender . '</p>',
                            '<p> <strong>Phone number:</strong> ' . $client->phoneNumber . '</p>',
                            '<p> <strong>Date of birth: </strong><script> document.write(moment.unix(' .$client->dob . ').format("LL"));</script></p><br/>',
                        '<button class="appointmentButton button6" id="btnRequestAppointment" style="min-width: 250px !important;">Add an Appointment</button>',
                        '<button class="appointmentButton button6" id="btnLinkOfflineUser" style="min-width: 250px !important;">Link to an Online Account</button>';
                           if($host->hiddenFiles == 0) {
                               echo '<button class="appointmentButton button6" id="btnClientFile" style="min-width: 250px !important;">Client File</button>';
                           }elseif($host->hiddenFiles == 1){
                               echo '<button class="appointmentButton button6" id="btnClientFileSecure" style="min-width: 250px !important;">Client File</button>';
                           }
//                        '<button class="appointmentButton button3" id="btnDeleteUser" style="min-width: 250px !important;">Delete User</button>',
                       echo '</div>',
                        '</div>';
                    }
                }
                ?>

                <!-- Modal Add Appointment -->

                <div id="myModal" class="modal">


                    <div class="modal-content">
                        <span class="close spanRequestAppointment">&times;</span>
                        <div class="modal-header">
                            <h2>Add new appointment</h2>
                        </div>
                        <div class="modal-body">
                            <br/> <br/>
                            <p>Client</p>
                                <input type="text" id="clientName"
                                value="<?php echo $client->identifier; ?>"
                                readonly><br/>
                            <br/>
                            <br/>
                            <div class="timePickerDivInput">
                                <p>From</p>
                                <div class='col-md-5' style="width: 100%;">
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker6'>
                                            <input type='text' class="form-control" name="dateTimeFrom" id="dateTimeFrom" readonly/>
                                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="dateTimeFromUTC" name="dateTimeFromUTC" value=""><br/><br/><br/>
                                <p>To</p>
                                <div class='col-md-5' style="width: 100%;">
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker7'>
                                            <input type='text' class="form-control"  name="dateTimeTo" id="dateTimeTo" readonly/>
                                            <span class="input-group-addon">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="dateTimeToUTC" name="dateTimeToUTC" value="">
                            </div><br/><br/>
                            <br/> <br/>
                            <div class="select-box">
                                <label for="selectLocation" class="label select-box1"><span
                                            class="label-desc">Choose Location</span> </label> <select
                                        id="selectLocation" class="select" name="locationSelect">
                                    <option value="0" selected>Choose Location</option>
                                    <?php
                                    if ($host->type == 2) {
                                        foreach ($locations as $location) {
                                            echo '<option value ="' . $location->locationWebID . '">' . $location->locationTitle . '</option>';
                                        }
                                    }
                                    ?>
                                </select><br/>
                                <p>Request Comment


                                <p/>
                                <textarea name='newAppointmentComment' id="newAppointmentComment"
                                          style='max-width: 100%; min-width: 100%; max-height: 150px; min-height: 100px;'></textarea>
                            </div>
                            <br/>
                            <button type="submit" name="submit" class="appointmentButton button1"
                                    id="btnAmSave" >Save
                            </button>
                            <button type="button" id="btnCloseAppointment"
                                    class="appointmentButton button3">Cancel
                            </button>
                        </div>
                    </div>
                    </div>

                </div>


                <!-- Modal Link to online User -->

                <div id="myModalLinkToOnline" class="modal">


                    <div class="modal-content">
                        <span class="close spanOnlineUser">&times;</span>
                        <div class="modal-header">
                            <h2>Link Offline User</h2>
                        </div>
                        <div class="modal-body">
                            <form
                                    action="/appointsync.com/resources/Controller/API/APIRouter.php"
                                    method="POST">
                                <p>
                                    <span style="color: red">Note:</span> When you link an offline
                                    user to an online account, this user will be deleted, and all
                                    the current appointments you have with this user will be moved
                                    to the account you are linking this user with.<br/>You can not
                                    revert this action.
                                </p>
                                <br/> <label for="usernameTab">Username</label> <input
                                        type='text' id='usernameTab' name='clientUsername'
                                        class='form-control' required/><br/> <br/>
                                <input type="hidden" value="<?php echo $client->webID; ?>" name="offlineClientWebID"/>
                                <button type="submit" name="submit" value="linkClient" class="appointmentButton button1"
                                        id="btnConfirmLink">Confirm Link
                                </button>
                                <button type="button" id="btnCloseLinkTab"
                                        class="appointmentButton button3">Cancel
                                </button>
                            </form>
                        </div>
                    </div>
                    <br/> <br/>

                </div>



<!--  Client settings  -->


    <div id="myModalClientSettings" class="modal">


        <div class="modal-content">
            <span class="close spanUserSettings">&times;</span>
            <div class="modal-header">
                <h2>User Settings</h2>
            </div>
            <div class="modal-body">
                <div id="userPermissionSettings">
                    <h3>Client Permission</h3>
                    <?php
                    if (isset($host->tillDate)) {

                    $dateNow = date("d-m-Y");
                    $NowDateUnix = strtotime($dateNow) . '<br/>';

                    if ($host->tillDate >= $NowDateUnix) {
                        $checkRemainingDays = abs($host->tillDate - $NowDateUnix) / 60 / 60 / 24;
                        $checkRemainingDays = floor($checkRemainingDays);
                        echo   '<h3>The below options are client specific.</h3><div class="settingsClientControlDiv"><p>Request a New Appointment</p><br/><label class="switch">';
                        if($permission->canCreateAppointment == '1') {
                            echo '<input id="permissionRequest" type="checkbox" checked onclick="permissionFlag(this.id)">';
                        }elseif($permission->canCreateAppointment == '0'){
                            echo '<input id="permissionRequest" type="checkbox" onclick="permissionFlag(this.id)">';
                        }
                               echo   '<span class="slider round"></span>',
                                '</label></div>';


                           echo '<div class="settingsClientControlDiv"><p>Accept Requested Appointment</p><br/><label class="switch">';
                                if($permission->canAcceptAppointment == '1') {
                                    echo '<input id="permissionAccept" type="checkbox" checked onclick="permissionFlag(this.id)">';
                                }elseif($permission->canAcceptAppointment == '0'){
                                    echo '<input id="permissionAccept" type="checkbox" onclick="permissionFlag(this.id)">';
                                }
                             echo
                                  '<span class="slider round"></span>',
                                  '</label></div>',

                                '<div class="settingsClientControlDiv"><p>Amend Appointment</p><br/><label class="switch">';
                                if($permission->canAmendAppointment == '1') {
                                echo '<input id="permissionAmend" type="checkbox" checked onclick="permissionFlag(this.id)">';
                               }elseif($permission->canAmendAppointment == '0'){
                                 echo '<input id="permissionAmend" type="checkbox" onclick="permissionFlag(this.id)">';
                              }
                               echo '<span class="slider round"></span>',
                                '</label></div>',

                                '<div class="settingsClientControlDiv"><p>Cancel Appointment</p><br/><label class="switch">';
                                 if($permission->canCancelAppointment == '1') {
                                     echo '<input id="permissionCancel" type="checkbox" checked onclick="permissionFlag(this.id)">';
                                 }elseif($permission->canCancelAppointment == '0'){
                                     echo '<input id="permissionCancel" type="checkbox" onclick="permissionFlag(this.id)">';
                                 }
                               echo
                                '<span class="slider round"></span>',
                                '</label></div>';

                    } else {
                    ?>
                    <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                        <input type="hidden" value="5" name="controllerTypeFlag"/>
                        <?php
                        $dt1 = new DateTime();
                        $today = $dt1->format("d-m-Y");

                        $dt2 = new DateTime("+1 month");
                        $date = $dt2->format("d-m-Y");

                        echo 'This feature is for Premium users!!<br/><br/>',
                        '<span style="font-size: 0.8em;">Benefits: With this feature, you can forbid all your clients, or selected clients, from creating, amending, rejecting, or cancelling appointments.</span>',
                        '<br/>';

                        echo '<button type="submit" class="appointmentButton button6" id="btnAccept">Upgrade Now</button>',
                        '</form>';
                        }

                        }else{

                        ?>
                        <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                            <input type="hidden" value="5" name="controllerTypeFlag"/>
                            <?php
                            $dt1 = new DateTime();
                            $today = $dt1->format("d-m-Y");

                            $dt2 = new DateTime("+1 month");
                            $date = $dt2->format("d-m-Y");

                            echo '<h4> Normal Account </h4>',
                            'This feature is for Premium users!!<br/><br/>',
                            '<span style="font-size: 0.8em;">Benefits: With this feature, you can forbid all your clients, or selected clients from creating, amending, rejecting, or cancelling appointments.</span> <br/>';

                            echo '<button type="submit" class="appointmentButton button6" id="btnAccept">Upgrade Now</button>',
                            '</form>';

                            }
                            ?>

                </div>

<!--                            <div id="clientSettingsDeleteUser">-->
<!--                                <h3>Delete User</h3>-->
<!---->
<!--                                <p>-->
<!--                                    <span style="color: red">Note:</span> When you delete a user,-->
<!--                                    all the current and passed appointments you have with this user-->
<!--                                    will be deleted, along with all the notes and messages.<br/>You-->
<!--                                    can not revert this action.-->
<!--                                </p> <br/>-->
<!--                                <button  type="submit" name="submit" class="appointmentButton button3" id="btnConfirmDelete">Confirm Delete</button>-->
<!---->
<!---->
<!--                            </div>-->



                    <button type="button" id="btnCloseModalClientSettings" class="appointmentButton button3">Close</button>
            </div>
        </div>
        <br/> <br/>

    </div>


    <!--  Modal Delete User   -->


<!--    <div id="myModalDeleteUser" class="modal">-->
<!---->
<!---->
<!--        <div class="modal-content">-->
<!--            <span class="close spanDeleteUser">&times;</span>-->
<!--            <div class="modal-header">-->
<!--                <h2>Delete User</h2>-->
<!--            </div>-->
<!--            <div class="modal-body">-->
<!--                    <p>-->
<!--                        <span style="color: red">Note:</span> When you delete a user,-->
<!--                        all the current and passed appointments you have with this user-->
<!--                        will be deleted, along with all the notes and messages.<br/>You-->
<!--                        can not revert this action.-->
<!--                    </p>-->
<!--                    <br/><br/><br/>-->
<!--                    <button type="submit" name="submit" class="appointmentButton button6" id="btnConfirmDelete">Confirm Delete</button>-->
<!--                    <button type="button" id="btnCloseDeleteTab" class="appointmentButton button3">Cancel</button>-->
<!--            </div>-->
<!--        </div>-->
<!--        <br/> <br/>-->
<!---->
<!--    </div>-->



<!--      Modal Send Pin-->

    <div id="myModalVerifyPin" class="modal">


        <div class="modal-content">
            <span class="close spanVerifyPin">&times;</span>
            <div class="modal-header">
                <h2>Verify Pin</h2>
            </div>
            <div class="modal-body">
                    <p>
                      Please enter your pin in order to continue.
                    </p>
                <form method="POST" action="/appointsync.com/public/setPinSession.php/<?php echo $webID;?>">
                <input type="password" id="pin" name="pin">
                    <br/><br/><br/>
                    <button type="submit" name="submit" class="appointmentButton button6">Confirm Pin</button>
                    <button type="button" id="btnClosePin" class="appointmentButton button3">Cancel</button>
                </form>
            </div>
        </div>
        <br/> <br/>

    </div>


    <!--      Modal Send Message-->

    <div id="myModalSendMessage" class="modal">


        <div class="modal-content">
            <span class="close spanSendMessage">&times;</span>
            <div class="modal-header">
                <h2>Send Message</h2>
            </div>
            <div class="modal-body">
                <h3>Please fill the below</h3>
                <br />

                <div class="row">

                        <label for="sendMessageToClient">Client</label><br/>
                           <input type="text" id="sendMessageToClient" class='form-contro' value="<?php echo $client->identifier; ?>" readonly/>

                </div>
                <br/><br/>


                <label for="threadTitleInput">Thread Title</label> <input type='text' id='threadTitleInput' class='form-control' required /><br />
                <label for="message">Message</label><br/> <textarea id="newMessageData" required ></textarea><br />


                <br />
                <button type="submit" name="submit" class="appointmentButton button6" id="btnNewMessage">Send</button>
            </div>
        <br/> <br/>

    </div>

                <?php

        } // if User Client
        //        elseif ($loggedInUserType == 3) {}
        ?>


</section>

<section>
    <hr style="width: 85%;"/>
    <br/> <br/>

    <div class="clientAppointmentList">
        <input type="text" id="Search" onkeyup="myFunction()"
               placeholder="Please enter a search term.." title="Type in a name"><br/>
        <br/>


        <?php
        // $queryUserProfileID = mysqli_query($syncminatorcon,"SELECT ID FROM USER WHERE WEB_ID = '$webID'");
        // while($rowGetqueryUserProfileID = mysqli_fetch_assoc($queryUserProfileID)) {

        // $selectedUserProfileID = $rowGetqueryUserProfileID['ID'];

        // //gettingAppointmentDetails note: if nour changes query, dont forget to add location again
        // $queryGetAppointmentDetails = mysqli_query($syncminatorcon, "SELECT A.ID, A.WEB_ID, A.HOST_ID,A.USER_ID,A.STATUS, A.DATE, A.START_TIME, A.END_TIME, A.REQUESTER_ID,A.HAS_AMENDMENT ,U.ID AS USERID, U.WEB_ID AS CLIENT_WEB_ID FROM APPOINTMENT A INNER JOIN USER U ON A.USER_ID = U.ID WHERE A.USER_ID = '$selectedUserProfileID' AND A.HOST_ID = '$userID' UNION SELECT AA.ID, AA.WEB_ID, AA.HOST_ID,AA.USER_ID,AA.STATUS, AA.DATE, AA.START_TIME, AA.END_TIME, AA.REQUESTER_ID,AA.HAS_AMENDMENT ,U.ID AS USERID, U.WEB_ID AS CLIENT_WEB_ID FROM APPOINTMENT_ARCHIVE AA INNER JOIN USER U ON AA.USER_ID = U.ID WHERE AA.USER_ID = '$selectedUserProfileID' AND AA.HOST_ID = '$userID' AND AA.STATUS <> 5 ORDER BY ID ASC ");
        // while ($rowGetAppointmentDetails = mysqli_fetch_assoc($queryGetAppointmentDetails)) {
        // $appointment = new Appointment($rowGetAppointmentDetails);

        // appointmentClass
        foreach ($appointments as $appointment) {
            if ($appointment->client->webID == $webID) {
                $appointmentRealClientID = $appointment->client->id;
                $appointmentHostID = $appointment->appointmentHostID;
                $appointmentRealID = $appointment->appointmentID;
                $appointmentRealWeblID = $appointment->webID;
                $appointmentLocationID = $appointment->locationID;

                foreach ($locations as $location) {
                    $appointmentLocation = $location->locationTitle;
                }

                $appointmentDate = date("D d-M-Y", substr($appointment->startDate, 0, 10));
                $appointmentRealDate = date("d-M-Y", substr($appointment->startDate, 0, 10));
                $appointmentStartTime = date("H:i", substr($appointment->startDate, 0, 10));
                $appointmentEndTime = date("H:i", substr($appointment->endDate, 0, 10));
                $appointmentStartStatus = $appointment->status;
                $appointmentrequestComment = $appointment->requestComment;
                $appointmentHasAmendment = isset($appointment->amendedAppointment);
                $appointmentRequesterID = $appointment->requesterID;
                $appointmentClientProfile = $appointment->client->profilePic;
                $appointmentDateTimeString = $appointmentDate . ' ' . $appointmentStartTime;
                $premiumTillDateUnix = 0;
                $NowDateUnix = 0;
                $premiumTillDateUnix = strtotime($appointmentDateTimeString);
                $dateNow = date("d-m-Y H:i");
                $NowDateUnix = strtotime($dateNow);
                if ($premiumTillDateUnix < $NowDateUnix) {
                    $appointmentDateTimeStatus = 'Past';
                    $appointmentStartStatus = 6;
                    unset($appointmentHasAmendment);
                    $appointmentHasAmendment = 0;
                } else {
                    $appointmentDateTimeStatus = 'Present';
                }

                $appointmentLocationString = $appointmentLocation;
                $appointmentDateString = $appointmentDate;
                // $appointmentTimeString = $appointmentTime;
                // if($appointmentHostID == $appointmentRequesterID){
                // $userHostID = $appointmentRequesterID;
                // $userClientID = 0;
                // }elseif($appointmentRealClientID == $appointmentRequesterID){
                // $userClientID = $appointmentRequesterID;
                // $userHostID = 0;
                // }
                // userClass

                $clientUserRealID = $appointment->client->id;
                $clientFirstName = $appointment->client->firstName;
                $clientLastName = $appointment->client->lastName;
                $clientWebID = $appointment->client->webID;
                $clientEmail = $appointment->client->email;
                $clientUsername = $appointment->client->username;
                $clientGender = $appointment->client->gender;


                if (isset($appointment->amendedAppointment)){

                 $appointmentAmendedDate = date("D d-M-Y", substr($appointment->amendedAppointment->startDate, 0, 10));
                 $appointmentRealAmendedDate = date("D d-M-Y", substr($appointment->amendedAppointment->startDate, 0, 10));
                 $appointmentAmendedStartTime = $appointment->amendedAppointment->startDate;
                 $appointmentAmendedEndTime = $appointment->amendedAppointment->endDate;
                 $appointmentAmendedTime = $appointmentAmendedStartTime." - ".$appointmentAmendedEndTime;


                }



                if ($appointmentStartStatus == 1) {
                    $appointmentStatus = 'Requested';
                } elseif ($appointmentStartStatus == 2) {
                    $appointmentStatus = 'Pending';
                } elseif ($appointmentStartStatus == 3) {
                    $appointmentStatus = 'Accepted';
                } elseif ($appointmentStartStatus == 4) {
                    $appointmentStatus = 'Rejected';
                } elseif ($appointmentStartStatus == 6) {
                    $appointmentStatus = 'Passed';
                    unset($appointment->amendedAppointment);
                }

                $clientPhoneNumber = $appointment->client->phoneNumber;

                if ($appointmentClientProfile == null) {
                    $userProfile = "<img id='userProfile' src='http://appointsync.com/webroot/userProfile/default.jpeg' alt='Default'>";
                } else {

                    $userProfile = "<img id='userProfile' src='" . $appointmentClientProfile . "' alt='Profile'>";
                }

                // Appointment display

                echo "<a href='/appointsync.com/public/appointment.php/" . $appointmentRealWeblID . "'><div class='clientAppointmentObject'>";

                if (isset($appointment->amendedAppointment)) {

                    if ($appointment->startDate != $appointment->amendedAppointment->startDate) {
                        ?>

                    <script>
                        var startDate = moment.unix(<?php echo $appointment->startDate ?>);
                        var startDate =  startDate.format('LLL');



                        var amendStartDate = moment.unix(<?php echo $appointment->amendedAppointment->startDate ?>);
                        var amendStartDate =  amendStartDate.format('LLL');



                    </script>  <?php
                echo " <span><script>document.write(startDate);</script></span> -> <span style='color:red'><script>document.write(amendStartDate);</script></span><br/>";



                  }if($appointment->startDate == $appointment->amendedAppointment->startDate){
                    echo    '<script>',
                    'var startDate = moment.unix('. $appointment->startDate .');',
                    'var startDate =  startDate.format("LLLL");',
                    '</script>',
                    '<span><script>document.write(startDate);</script></span><br/>';


                }if($appointment->endDate != $appointment->amendedAppointment->endDate){

                        ?>
                    <script>
                    var endDate = moment.unix(<?php echo $appointment->endDate ?>);
                    var endDate =  endDate.format('LLL');

                    var amendEndDate = moment.unix(<?php echo $appointment->amendedAppointment->endDate ?>);
                    var amendEndDate =  amendEndDate.format('LLL');

                    </script>
                    <?php

                       echo  "<span><script>document.write(endDate);</script></span> -> <span  style='color:red'><script>document.write(amendEndDate);</script></span>";

                }if($appointment->endDate == $appointment->amendedAppointment->endDate){

                        ?>
                    <script>
                    var endDate = moment.unix(<?php echo $appointment->endDate ?>);
                    var endDate =  endDate.format('LLLL');


                    </script>
                    <?php

                       echo  "<span><script>document.write(endDate);</script></span>";


                }
                } else {
                    ?>

                    <script>
                        var startDate = moment.unix(<?php echo $appointment->startDate ?>);
                        var startDate =  startDate.format('LLLL');

                        var endDate = moment.unix(<?php echo $appointment->endDate ?>);
                        var endDate =  endDate.format('LLLL');

                    </script>  <?php
                    echo " <span><script>document.write(startDate);</script></span> <br/> <span><script>document.write(endDate);</script></span>";
                }

                echo "<br/>" . "Status: " . $appointmentStatus . "<br/>";

                echo "</div></a>";
            }
        }
        if (isset($appointmentRealWeblID)) {

        } else {
            echo '<h3>You never had appointments with this user.</h3>';
        }


        ?></div>
    <br/><br/>


</section>

<?php

require ("objectFooter.php");

?>



<!--Maps JS-->

<script>
    // Get the modal
    var modalRequestAppointment = document.getElementById('myModal');
    var modalLinkToOnline = document.getElementById('myModalLinkToOnline');
    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");
    // var modalDeleteUser = document.getElementById("myModalDeleteUser");
    var modalClientSettings = document.getElementById("myModalClientSettings");
    var modalVerifyPin = document.getElementById("myModalVerifyPin");
    var modalSendMessage = document.getElementById("myModalSendMessage");

    // Get the button that opens the modal
    var btnRequestAppointment = document.getElementById("btnRequestAppointment") != null ? document.getElementById("btnRequestAppointment") : 0;
    var btnLinkOfflineUser = document.getElementById("btnLinkOfflineUser") != null ? document.getElementById("btnLinkOfflineUser") : 0;
    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null ? document.getElementById("showNotificationPopupLink") : 0;
    // var btnDeleteUser = document.getElementById("btnDeleteUser") != null ? document.getElementById("btnDeleteUser") : 0;
    var btnClientFile = document.getElementById("btnClientFile") != null ? document.getElementById("btnClientFile") : 0;
    var btnUserSettings = document.getElementById("btnUserSettings") != null ? document.getElementById("btnUserSettings") : 0;
    var btnClientFileSecure = document.getElementById("btnClientFileSecure") != null ? document.getElementById("btnClientFileSecure") : 0;
    var btnSendMessage = document.getElementById("btnSendMessage") != null ? document.getElementById("btnSendMessage") : 0;

    var btnCloseAppointment = document.getElementById("btnCloseAppointment");
    var btnCloseLinkTab = document.getElementById("btnCloseLinkTab");
    var btnCloseModalClientSettings = document.getElementById("btnCloseModalClientSettings");
    // var btnCloseDeleteTab = document.getElementById("btnCloseDeleteTab");
    var btnClosePin = document.getElementById("btnClosePin");
    var btnNewMessage = document.getElementById("btnNewMessage");

    var spanRequestAppointment = document.getElementsByClassName("spanRequestAppointment")[0];
    // var spanDeleteUser = document.getElementsByClassName("spanDeleteUser")[0];
    var spanOnlineUser = document.getElementsByClassName("spanOnlineUser")[0];
    var spanUserSettings = document.getElementsByClassName("spanUserSettings")[0];
    var spanVerifyPin = document.getElementsByClassName("spanVerifyPin")[0];
    var spanSendMessage = document.getElementsByClassName("spanSendMessage")[0];


    // When the user clicks the button, open the modal

    // btnViewNotificationsPopup.onclick = function() {
    //     modalViewNotificationPopUpgg.style.display = "block";
    // }

    btnClientFile.onclick = function () {

        location.href = "/appointsync.com/public/clientfile.php/<?php echo $webID;?>";


    };



    //btnClientFile.onclick = function () {
    //    $.ajax({
    //        type: "POST",
    //        url:  "/appointsync.com/public/testpost.php",
    //        data: { 'submit':'getFileTemplates1', 'clientWebID':'<?php //echo $webID;?>//'},
    //        success: function($data){
    //
    //            var emak = '<?php //if(!isset($_SESSION["postSub"])){}else{echo $_SESSION["postSub"];}?>//';
    //            alert($data);
    //            //location.href = "/appointsync.com/public/testpost.php/<?php ////echo $webID;?>////";
    //        },
    //    });
    //}

    btnRequestAppointment.onclick = function () {
        modalRequestAppointment.style.display = "block";
        $('body').css('overflow','hidden');
    }

    btnLinkOfflineUser.onclick = function () {
        modalLinkToOnline.style.display = "block";
        $('body').css('overflow','hidden');
    }

    btnUserSettings.onclick = function () {
        modalClientSettings.style.display = "block";
        $('body').css('overflow','hidden');
    }

    btnClientFileSecure.onclick = function () {
        modalVerifyPin.style.display = "block";
        $('body').css('overflow','hidden');
    }

    btnSendMessage.onclick = function () {
        modalSendMessage.style.display = "block";
        $('body').css('overflow','hidden');
    }

    // btnDeleteUser.onclick = function () {
    //     modalDeleteUser.style.display = "block";
    //     $('body').css('overflow','hidden');
    // }

    btnCloseAppointment.onclick = function () {
        modalRequestAppointment.style.display = "none";
        $('body').css('overflow','auto');
    }

    btnCloseModalClientSettings.onclick = function () {
        modalClientSettings.style.display = "none";
        $('body').css('overflow','auto');
    }

    btnCloseLinkTab.onclick = function () {
        modalLinkToOnline.style.display = "none";
        $('body').css('overflow','auto');
    }


    btnClosePin.onclick = function () {
        modalVerifyPin.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanSendMessage.onclick = function () {
        modalSendMessage.style.display = "none";
        $('body').css('overflow','auto');
    }

    // btnCloseDeleteTab.onclick = function () {
    //     modalDeleteUser.style.display = "none";
    //     $('body').css('overflow','auto');
    // }


    spanRequestAppointment.onclick = function () {
        modalRequestAppointment.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanOnlineUser.onclick = function () {
        modalLinkToOnline.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanUserSettings.onclick = function () {
        modalClientSettings.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanVerifyPin.onclick = function () {
        modalVerifyPin.style.display = "none";
        $('body').css('overflow','auto');
    }

    // spanDeleteUser.onclick = function () {
    //     modalDeleteUser.style.display = "none";
    //     $('body').css('overflow','auto');
    // }

    window.onclick = function(event) {
        if (event.target == modalClientSettings || event.target == modalLinkToOnline || event.target == modalRequestAppointment || event.target == modalViewNotificationPopUpgg
            || event.target == modalVerifyPin || event.target == modalSendMessage) {
            modalClientSettings.style.display = "none";
            modalLinkToOnline.style.display = "none";
            modalViewNotificationPopUpgg.style.display = "none";
            modalRequestAppointment.style.display = "none";
            modalVerifyPin.style.display = "none";
            modalSendMessage.style.display = "none";
            $('body').css('overflow','auto');
        }
    }


    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });



    $("select").on("click", function () {

        $(this).parent(".select-box").toggleClass("open");

    });

    $(document).mouseup(function (e) {
        var container = $(".select-box");

        if (container.has(e.target).length === 0) {
            container.removeClass("open");
        }
    });


    $("#selectLocation").on("change", function () {

        var selection = $(this).find("option:selected").text(),
            labelFor = $(this).attr("id"),
            label = $("[for='" + labelFor + "']");

        label.find(".label-desc").html(selection);

    });

    $(function () {
        $('#datetimepicker6').datetimepicker({


            date: moment(),
            minDate: moment(),
            ignoreReadonly: true,
            format: ('LLLL'),

        });
        $('#datetimepicker7').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            ignoreReadonly: true,
            minDate: moment(),
            format: ('LLLL'),
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);

            var selectedDateFrom = document.getElementById("dateTimeFrom").value;
            var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
            console.log(selectedDateFormatFrom);
            var startDate = moment(selectedDateFormatFrom).unix();

        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);

            var selectedDateFrom = document.getElementById("dateTimeFrom").value;
            var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
            console.log(selectedDateFormatFrom);
            var startDate = moment(selectedDateFormatFrom).unix();

            $('#dateTimeFromUTC').val(startDate);
            var selectedDateTo = document.getElementById("dateTimeTo").value;
            var selectedDateFormatTo = moment(selectedDateTo).format('LLLL');
            console.log(selectedDateFormatTo);
            var startDate = moment(selectedDateFormatTo).unix();

            $('#dateTimeToUTC').val(startDate);

        });
    });



    $('#btnAmSave').click(function() {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->createAppointmentFlag;?>';
        var selectClient = '<?php echo $client->webID;?>';
        var selectClientID = '<?php echo $fileController->selectClient;?>';
        var dateTimeFromUTC = document.getElementById("dateTimeFromUTC").value;
        var dateTimeToUTC = document.getElementById("dateTimeToUTC").value;
        var dateTimeFromUTCID = '<?php echo $fileController->dateTimeFromUTC;?>';
        var dateTimeToUTCID = '<?php echo $fileController->dateTimeToUTC;?>';
        var selectLocation = document.getElementById("selectLocation").value;
        var selectLocationID = '<?php echo $fileController->selectLocation;?>';
        var newAppointmentComment = document.getElementById("newAppointmentComment").value;
        var newAppointmentCommentID = '<?php echo $fileController->newAppointmentComment;?>';


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { [submitValue]: submitData, [selectClientID]: selectClient, [dateTimeFromUTCID]:dateTimeFromUTC, [dateTimeToUTCID]:dateTimeToUTC, [selectLocationID]: selectLocation, [newAppointmentCommentID]: newAppointmentComment},
            async: false,
            success: function(data){
                if(!JSON.parse(data).success){
                    location.href = "/appointsync.com/public/dashboard.php";
                }else if(JSON.parse(data).data.webID){
                    location.href = "/appointsync.com/public/appointment.php/"+JSON.parse(data).data.webID;
                }
            }
        });

    });


    //$('#btnConfirmDelete').click(function() {
    //
    //    var submitValue = '<?php //echo $fileController->submitValue;?>//';
    //    var submitData = '<?php //echo $fileController->removeClient;?>//';
    //    var selectClient = '<?php //echo $client->webID;?>//';
    //    var selectClientID = '<?php //echo $fileController->selectClient;?>//';
    //
    //
    //    $.ajax({
    //        type: "POST",
    //        url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
    //        data: { [submitValue]: submitData, [selectClientID]: selectClient},
    //        async: false,
    //        success: function(data){
    //                location.href = "/appointsync.com/public/clients.php";
    //        }
    //    });
    //
    //});


    function permissionFlag(clicked_id){

        clicked_id.disabled = 'true';
        if(clicked_id == 'permissionRequest'){
            var permissionSentValueDataSet = document.getElementById('permissionRequest').checked?'1':'0';
            var permissionValueSet = '<?php echo $fileController->permissionToRequest;?>';
        }else if(clicked_id == 'permissionAccept'){
            var permissionSentValueDataSet = document.getElementById('permissionAccept').checked?'1':'0';
            var permissionValueSet = '<?php echo $fileController->permissionToAccept;?>';
        }else if(clicked_id == 'permissionAmend'){
            var permissionSentValueDataSet = document.getElementById('permissionAmend').checked?'1':'0';
            var permissionValueSet = '<?php echo $fileController->permissionToAmend;?>';
        }else if(clicked_id == 'permissionCancel'){
            var permissionSentValueDataSet = document.getElementById('permissionCancel').checked?'1':'0';
            var permissionValueSet = '<?php echo $fileController->permissionToCancel;?>';
        }
            var clientWebIDValue = '<?php echo $fileController->clientWebID;?>';
            var clientWebIDData = '<?php echo $client->webID;?>';


        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->changeUserPermission;?>';
        var permissionFlag = '<?php echo $fileController->permissionValue;?>';
        var permissionValue = permissionValueSet;
        var permissionSentValue = '<?php echo $fileController->permissionSentValue;?>';
        var permissionSentValueData = permissionSentValueDataSet;


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { [submitValue]: submitData,[clientWebIDValue]: clientWebIDData ,[permissionFlag]: permissionValue, [permissionSentValue]: permissionSentValueData},
            async: false,
            success: function(data){
                clicked_id.disabled = 'false';
            }
        });


    }


    $('#btnNewMessage').click(function() {


        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->postContactMessage;?>';
        var selectClientData = '<?php echo $client->webID;?>';
        var selectClientValue = '<?php echo $fileController->threadToWebID;?>';
        var selectTitleData = document.getElementById("threadTitleInput").value;
        var selectTitleValue = '<?php echo $fileController->threadTitle;?>';
        var selectDataData = document.getElementById("newMessageData").value;
        var selectDataValue = '<?php echo $fileController->threadMessage;?>';

        var btnNewMessageSubmit = document.getElementById("btnNewMessage") != null?document.getElementById("btnNewMessage"):0;

        if (selectClientData != '' && selectTitleData != '' && selectDataData !='') {


            btnNewMessageSubmit.disabled = 'true';

            $.ajax({
                type: "POST",
                url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: { [submitValue]: submitData, [selectClientValue]: selectClientData, [selectTitleValue]: selectTitleData, [selectDataValue]: selectDataData},
                async: true,
                success: function(data){
                    // alert(data);
                    if(!JSON.parse(data).success){
                        alert('Oops, Something went wrong, please try again');
                    }else {
                        // alert('qhat');
                        // await sleep(1500);
                        btnNewMessageSubmit.disabled = 'false';
                        modalSendMessage.style.display = "none";
                        location.href = "/appointsync.com/public/messages.php/";


                    }
                }
            });



        }else{

            alert('Incorrect Data');
        }

    });

</script>


<?php
// User Alert handling
if (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 0) {

    echo '<script language="javascript">', 'alert("User added successfully")', '</script>';
    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 1) {

    echo '<script language="javascript">', 'alert("You are not a premium user")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 2) {

    echo '<script language="javascript">', 'alert("User added successfully")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 3) {

    echo '<script language="javascript">', 'alert("User does not exist, make sure you entered the username correctly. ")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 4) {

    echo '<script language="javascript">', 'alert("This user already exist in your relations")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
}elseif (isset($_SESSION["AuthFail"]) && $_SESSION['AuthFail'] == 4) {

    echo '<script language="javascript">', 'alert("Authentication Failed!")', '</script>';

    unset($_SESSION["AuthFail"]);
}

?>


</body>
</html>
