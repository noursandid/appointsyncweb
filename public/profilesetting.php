<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedClient.php';
include_once dirname(__FILE__) . '/../resources/Classes/UserProfile.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}

$_SESSION['pageName'] = 'profileSettings';

$loggedInUserType = unserialize($_SESSION['user'])->type;

//error_reporting(E_PARSE);
if(isset($_SESSION['user'])) {
}else{header("Location: /appointsync.com/");}

if($loggedInUserType == 2) {
    $hostResponse = APIController::getProfile();
    $clientsPermissions = APIController::getClientsPermissions();
    if ($hostResponse->success) {
        $host = $hostResponse->data;
    } else {
        header("Location: /appointsync.com/public/dashboard.php");
        exit();
    }
    if ($clientsPermissions->success) {
        $permissions = $clientsPermissions->data;
    } else {
        header("Location: /appointsync.com/public/dashboard.php");
        exit();
    }
}

$tillDate = 1546507110;
$fileController = new MappedRouterController();
?>
<!DOCTYPE html>
<html>
<head>

    <title>AppointSync - Profile Settings</title>

    <?php
    include ("objectHeader.php")
    ?>


    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">



</head>
<?php if ($loggedInUserType == 2){

?>
<body style='width:100%;'>

<style>

    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #1193d4;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #1193d4;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }





    .containerPlan {max-width: 850px; width: 100%; margin: 0 auto;
        text-align: center; align-content: center;}
    .containerPlan .four { width: 32.26%; max-width: 32.26%;}


    /* COLUMNS */

    .containerPlan .col {
        display: block;
        float:left;
        margin: 1% 0 1% 1.6%;
    }

    .containerPlan .col:first-of-type { margin-left: 0; }

    /* CLEARFIX */

    .containerPlan .cf:before,
    .containerPlan .cf:after {
        content: " ";
        display: table;
    }

    .containerPlan .cf:after {
        clear: both;
    }

    .containerPlan .cf {
        *zoom: 1;
        text-align: center;
        margin: 0 auto;
        display: inline-block;
        min-width: 80%;
        float: none;
    }

    /* FORM */

    .containerPlan .form .plan input, .form .payment-plan input, .form .payment-type input{
        display: none;
    }

    .containerPlan .form label{
        position: relative;
        color: #fff;
        background-color: #aaa;
        font-size: 26px;
        text-align: center;
        height: 150px;
        line-height: 150px;
        display: block;
        cursor: pointer;
        border: 3px solid transparent;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        white-space: pre-wrap;
        min-width: 247px !important;
    }

    .containerPlan .form .plan input:checked + label, .form .payment-plan input:checked + label, .form .payment-type input:checked + label{
        border: 3px solid #333;
        background-color: #1193d4;
        min-width: 250px !important;
        margin: auto !important;
    }

    .containerPlan .form .plan input:checked + label:after, form .payment-plan input:checked + label:after, .form .payment-type input:checked + label:after{
        content: "\2713";
        width: 40px;
        height: 40px;
        line-height: 40px;
        border-radius: 100%;
        border: 2px solid #333;
        background-color: #1193d4;
        z-index: 999;
        position: absolute;
        top: -10px;
        right: -10px;
    }


</style>

<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg angle-bg ripple">
        <video autoplay muted id="video-background" loop>
            <source src="http://intimissibd.com/video/video-2.webm" type="video/webm">
        </video>
    </div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">
                    <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                </a>
            </div>
           <?php
           include ("objectMenu.php")
           ?>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
    <!--Header-Text-->
    <!--Header-Text/-->
</header>

<section>

    <div style='margin-top:5%;'>

    </div>

    <br><br>

    <div class="container">
    <h3>Settings</h3><br/>
        <?php


        //if user Host
        if($host->type == 2) {

            $hostWorkingHoursFromAMPM = date("h:i A", strtotime($host->workingHourFrom));
            $hostWorkingHoursToAMPM = date("h:i A", strtotime($host->workingHourTo));
            $timeFromHourValue = substr($hostWorkingHoursFromAMPM, 0, 2);
            $timeFromMinuteValue = substr($hostWorkingHoursFromAMPM, 3, 2);
            $timeFromCharValue = substr($hostWorkingHoursFromAMPM, 6, 2);

            $timeToHourValue = substr($hostWorkingHoursToAMPM, 0, 2);
            $timeToMinuteValue = substr($hostWorkingHoursToAMPM, 3, 2);
            $timeToCharValue = substr($hostWorkingHoursToAMPM, 6, 2);

            if ($host->profilePic == null || $host->profilePic == '') {
                $userProfilePicSet = "<img id='userProfile' src='http://appointsync.com/webroot/userProfile/default.jpeg' alt='Default'>";
            } else {
                $userProfilePicSet = "<img id='userProfile' src='" . $host->profilePic . "' alt='Profile'>";
            }



        ?>

            <div class="settingsLeftTab">

                <button class="settingsButtonClass" id="btnSubscription">Subscription</button>
<!--                <button class="settingsButtonClass" id="btnPremium">Premium</button><br/>-->
                <button class="settingsButtonClass" id="btnClientControl">Client Permissions</button>
                <button class="settingsButtonClass" id="btnFileSecurity">File Security</button>
                <button class="settingsButtonClass" id="btnNotifications">Notifications</button>
<!--                <button class="settingsButtonClass" id="btnReportIssue">Report Issue</button><br/>-->
                <button class="settingsButtonClass" id="btnContactUs">Contact Us</button>
                <button class="settingsButtonClass" id="btnAbout">About</button>
                <button class="settingsButtonClass" id="btnPrivacy">Privacy</button>
                <button class="settingsButtonClass" id="btnTermsConditions">Terms &amp; Conditions</button>
                <button class="settingsButtonClass" id="btnHelp">Help</button>
            </div>



            <div class="settingsRightTab">


                <div class="settingsSubscription">

                    <?php

                    ?>

                    <h3> Below is the status of your account. </h3><br/>

                    <?php
                    if (isset($host->tillDate)) {

                    $dateNow = date("d-m-Y");
                    $NowDateUnix = strtotime($dateNow) . '<br/>';

                    if ($host->tillDate >= $NowDateUnix) {
                        $checkRemainingDays = abs((float)$host->tillDate - (float)$NowDateUnix) / 60 / 60 / 24;
                        $checkRemainingDays = floor($checkRemainingDays);
                        echo '<h4>Premium member.</h4>';
                        echo '<p>You still have ' . $checkRemainingDays . ' days in order to renew.</p><br/>',
                        '<button type="button" name="submit" value="addNewLocation" class="appointmentButton button6" id="btnRenewPremium">Renew Now</button>',
                        '<button type="button" id="btnCloseNewLocation" class="appointmentButton button3">Cancel Subscription</button>';

                    } else {
                    ?>
                    <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                        <input type="hidden" value="5" name="controllerTypeFlag"/>
                        <?php
                        $dt1 = new DateTime();
                        $today = $dt1->format("d-m-Y");

                        $dt2 = new DateTime("+1 month");
                        $date = $dt2->format("d-m-Y");

                        echo '<h4> Normal Account </h4>';
                        $checkRemainingDays = abs((float)$host->subscriptionDate - (float)$tillDate) / 60 / 60 / 24;
                        $checkRemainingDays = floor($checkRemainingDays);
                        echo '<p>Subscription start date: <em><strong><script> document.write(moment.unix(' . $host->subscriptionDate . ').format("LL"));</script></strong></em></p>',
                            '<p>Subscription end date: <em><strong><script> document.write(moment.unix(' .$tillDate . ').format("LL"));</script></strong></em></p>',
                            '<p>Time remaining to renew: ' . $checkRemainingDays . ' days in order to renew.</p><br/>',
                        '<button type="submit" name="submit" value="addNewLocation" class="appointmentButton button6" id="btnAccept">Renew Now</button>';
//                        echo '<p>Upgrade to premium for an additional 9.99$/month to be able to use the mobile app offline, secure your clients files with a pin,
//                                and manage what your clients can do with appointments!!</p><br/><br/>',
//                        '<br/>';

                        echo '<button type="button" class="appointmentButton button6" id="btnUpgradeToPremium">Upgrade Now</button><br/>',
                        '<button type="button" id="btnCloseNewLocation" class="appointmentButton button3">Cancel Subscription</button>',
                        '</form>';
                        }

                        }else{

                        ?>
                        <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                            <input type="hidden" value="5" name="controllerTypeFlag"/>
                            <?php
                            $dt1 = new DateTime();
                            $today = $dt1->format("d-m-Y");

                            $dt2 = new DateTime("+1 month");
                            $date = $dt2->format("d-m-Y");

                            echo '<h4> Normal Account </h4>';
                            $checkRemainingDays = abs((float)$host->subscriptionDate - (float)$tillDate) / 60 / 60 / 24;
                            $checkRemainingDays = floor($checkRemainingDays);
                            echo '<p>Subscription start date: <em><strong><script> document.write(moment.unix(' . $host->subscriptionDate . ').format("LL"));</script></strong></em></p>',
                                '<p>Subscription end date: <em><strong><script> document.write(moment.unix(' .$tillDate . ').format("LL"));</script></strong></em></p>',
                                '<p>Time remaining to renew: ' . $checkRemainingDays . ' days in order to renew.</p><br/>',
                            '<button type="submit" name="submit" value="addNewLocation" class="appointmentButton button6" id="btnAccept">Renew Now</button>';


//                            echo '<p>Upgrade to premium for an additional 9.99$/month to be able to use the mobile app offline, secure your clients files with a pin,
//                                        and manage what your clients can do with appointments!!</p><br/><br/>';

                            echo '<button type="button" class="appointmentButton button6" id="btnUpgradeToPremium">Upgrade Now</button><br/>',
                            '<button type="button" id="btnCloseNewLocation" class="appointmentButton button3">Cancel Subscription</button>',
                            '</form>';

                            }
                            ?>
                </div>

                <div class="settingsPremium">



                </div>

                <div class="settingsClientControl">
                    <?php
                    if (isset($host->tillDate)) {

                    $dateNow = date("d-m-Y");
                    $NowDateUnix = strtotime($dateNow) . '<br/>';

                    if ($host->tillDate >= $NowDateUnix) {
                        $checkRemainingDays = abs((float)$host->tillDate - (float)$NowDateUnix) / 60 / 60 / 24;
                        $checkRemainingDays = floor($checkRemainingDays);
                        echo   '<h3>The below options are for all your client.</h3><div class="settingsClientControlDiv"><p>Request a New Appointment</p><br/><label class="switch">';
                        if($permissions->canCreateAppointment == '1') {
                            echo '<input id="permissionRequest" type="checkbox" checked onclick="permissionFlag(this.id)">';
                        }elseif($permissions->canCreateAppointment == '0'){
                            echo '<input id="permissionRequest" type="checkbox" onclick="permissionFlag(this.id)">';
                        }
                               echo   '<span class="slider round"></span>',
                                '</label></div>';


                           echo '<div class="settingsClientControlDiv"><p>Accept Requested Appointment</p><br/><label class="switch">';
                                if($permissions->canAcceptAppointment == '1') {
                                    echo '<input id="permissionAccept" type="checkbox" checked onclick="permissionFlag(this.id)">';
                                }elseif($permissions->canAcceptAppointment == '0'){
                                    echo '<input id="permissionAccept" type="checkbox" onclick="permissionFlag(this.id)">';
                                }
                             echo
                                  '<span class="slider round"></span>',
                                  '</label></div>',

                                '<div class="settingsClientControlDiv"><p>Amend Appointment</p><br/><label class="switch">';
                                if($permissions->canAmendAppointment == '1') {
                                echo '<input id="permissionAmend" type="checkbox" checked onclick="permissionFlag(this.id)">';
                               }elseif($permissions->canAmendAppointment == '0'){
                                 echo '<input id="permissionAmend" type="checkbox" onclick="permissionFlag(this.id)">';
                              }
                               echo '<span class="slider round"></span>',
                                '</label></div>',

                                '<div class="settingsClientControlDiv"><p>Cancel Appointment</p><br/><label class="switch">';
                                 if($permissions->canCancelAppointment == '1') {
                                     echo '<input id="permissionCancel" type="checkbox" checked onclick="permissionFlag(this.id)">';
                                 }elseif($permissions->canCancelAppointment == '0'){
                                     echo '<input id="permissionCancel" type="checkbox" onclick="permissionFlag(this.id)">';
                                 }
                               echo
                                '<span class="slider round"></span>',
                                '</label></div>';

                    } else {
                    ?>
                    <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                        <input type="hidden" value="5" name="controllerTypeFlag"/>
                        <?php
                        $dt1 = new DateTime();
                        $today = $dt1->format("d-m-Y");

                        $dt2 = new DateTime("+1 month");
                        $date = $dt2->format("d-m-Y");

                        echo '<br/>',
                        'This feature is for Premium users!!<br/><br/>',
                        '<span style="font-size: 0.8em;">Benefits: With this feature, you can forbid all your clients, or selected clients, from creating, amending, rejecting, or cancelling appointments.</span>',
                        '<br/>';

                        echo '<br/><button type="button" class="appointmentButton button6" id="btnUpgradeToPremiumPermission">Upgrade Now</button>',
                        '</form>';
                        }

                        }else{

                        ?>
                        <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                            <input type="hidden" value="5" name="controllerTypeFlag"/>
                            <?php
                            $dt1 = new DateTime();
                            $today = $dt1->format("d-m-Y");

                            $dt2 = new DateTime("+1 month");
                            $date = $dt2->format("d-m-Y");

                            echo '<br/>',
                            'This feature is for Premium users!!<br/><br/>',
                            '<span style="font-size: 0.8em;">Benefits: With this feature, you can forbid all your clients, or selected clients from creating, amending, rejecting, or cancelling appointments.</span> <br/>';

                            echo '<br/><button type="button" class="appointmentButton button6" id="btnUpgradeToPremiumPermission">Upgrade Now</button>',
                            '</form>';

                            }
                            ?>

                </div>

                <div class="settingsFileControl">
                    <?php
                    if (isset($host->tillDate)) {

                    $dateNow = date("d-m-Y");
                    $NowDateUnix = strtotime($dateNow) . '<br/>';
                    if ($host->tillDate >= $NowDateUnix) {
                        $checkRemainingDays = abs((float)$host->tillDate - (float)$NowDateUnix) / 60 / 60 / 24;
                        $checkRemainingDays = floor($checkRemainingDays);
                        echo    '<h3>Below you can set a security pin for your files.</h3><div class="settingsFileControlDiv"><p>File Security pin</p><br/><label class="switch">';
                                if($host->hiddenFiles == '0') {
                                  echo  '<input type="checkbox" id="secureFolderFlag" onclick="secureFolderFlag();">';
                                }elseif($host->hiddenFiles == '1') {
                                    echo  '<input type="checkbox" id="secureFolderFlag" checked onclick="secureFolderFlag();">';
                                }
                                echo '<span class="slider round"></span>',
                                '</label></div>';


                    } else {
                    ?>
                    <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                        <input type="hidden" value="5" name="controllerTypeFlag"/>
                        <?php
                        $dt1 = new DateTime();
                        $today = $dt1->format("d-m-Y");

                        $dt2 = new DateTime("+1 month");
                        $date = $dt2->format("d-m-Y");

                        echo '<br/>',
                        'This feature is for Premium users!!<br/><br/>',
                        '<span style="font-size: 0.8em;">Benefits: With this feature, you can secure all your client&apos;s files with a pin, thus no one can access these files even if you leave your account opened.</span>',
                        '<br/>';

                        echo '<br/><button type="button" class="appointmentButton button6" id="btnUpgradeToPremiumFlag">Upgrade Now</button>',
                        '</form>';
                        }

                        }else{

                        ?>
                        <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                            <input type="hidden" value="5" name="controllerTypeFlag"/>
                            <?php
                            $dt1 = new DateTime();
                            $today = $dt1->format("d-m-Y");

                            $dt2 = new DateTime("+1 month");
                            $date = $dt2->format("d-m-Y");

                            echo '<br/>',
                            'This feature is for Premium users!!<br/><br/>',
                            '<span style="font-size: 0.8em;">Benefits: With this feature, you can secure all your client&apos;s files with a pin, thus no one can access these files even if you leave your account opened.</span> <br/>';

                            echo '<br/><button type="button" class="appointmentButton button6" id="btnUpgradeToPremiumFlag">Upgrade Now</button>',
                            '</form>';

                            }
                            ?>

                </div>



                <div class="settingsNotificationControl">

                    <h3>Below you can change the options for your notifications.</h3><br/>

                    <div class="settingsClientControlDiv"><p>Email Notifications</p><br/><label class="switch">
                            <input id="permissionCancel" type="checkbox" checked onclick="">
                            <span class="slider round"></span>
                            </label></div>

                    <div class="settingsClientControlDiv"><p>App Push Notifications</p><br/><label class="switch">
                            <input id="permissionCancel" type="checkbox" checked onclick="">
                            <span class="slider round"></span>
                            </label></div><br/>
                    <p style="visibility: hidden">test</p>


            </div>



                <!--   Contact us   -->

                <div class="settingsContactUsSection">

                    <h3>Contact Us</h3><br/>


                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <select id='clientContactSupportType' style="text-align:center!important;min-width: 200px;width:100% !important;">
                                    <option value="0" selected disabled>-Support Type-</option>
                                    <option value="1">Contact Us</option>
                                    <option value="2">Report an Issue</option>
                                    <option value="3">Suggestions</option>
                                </select>
                            </div>
                            <div class="space-10"></div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="form-subject" class="sr-only">Subject</label>
                                <input type="text" class="form-control" id="form-subject" name="form-subject" placeholder="Subject" required>
                            </div>
                            <div class="space-10"></div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="form-message" class="sr-only">comment</label>
                                <textarea class="form-control" rows="6" id="form-message" name="form-message" placeholder="Message" required style="max-width: 100%;max-height: 10em;"></textarea>
                            </div>
                            <div class="space-10"></div>
                            <button class="btn btn-link no-round text-uppercase" type="submit">Send message</button>
                        </div>
                    </div>

                </div>


                <!--   About   -->

                <div class="settingsAboutSection">

                    <h3>About.</h3><br/>

                </div>


                <!--   Privacy   -->

                <div class="settingsPrivacySection">

                    <h1>Privacy Policy</h1><br/>


                    <p>Effective date: September 11, 2018</p><br/>


                    <p>AppointSync operates the appointsync.com website and the appointsync mobile application (the "Service").</p>

                    <p>This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.</p>

                    <p>We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.</p>


                    <br/><h2>Information Collection And Use</h2><br/>

                    <p>We collect several different types of information for various purposes to provide and improve our Service to you.</p>

                    <br/><h3>Types of Data Collected</h3><br/>

                    <h4>Personal Data</h4><br/>

                    <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you ("Personal Data"). Personally identifiable information may include, but is not limited to:</p>

                    <ul>
                        <li>Email address</li><li>First name and last name</li><li>Phone number</li><li>Address, State, Province, ZIP/Postal code, City</li><li>Cookies and Usage Data</li>
                    </ul>

                    <br/><h4>Usage Data</h4><br/>

                    <p>We may also collect information that your browser sends whenever you visit our Service or when you access the Service by or through a mobile device ("Usage Data").</p>
                    <p>This Usage Data may include information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>
                    <p>When you access the Service by or through a mobile device, this Usage Data may include information such as the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data.</p>

                    <br/><h4>Tracking & Cookies Data</h4>
                    <p>We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>
                    <p>Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.</p>
                    <p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>
                    <p>Examples of Cookies we use:</p>
                    <ul>
                        <li><strong>Session Cookies.</strong> We use Session Cookies to operate our Service.</li>
                        <li><strong>Preference Cookies.</strong> We use Preference Cookies to remember your preferences and various settings.</li>
                        <li><strong>Security Cookies.</strong> We use Security Cookies for security purposes.</li>
                    </ul>

                    <br/><h2>Use of Data</h2><br/>
                    <p>AppointSync uses the collected data for various purposes:</p>
                    <ul>
                        <li>To provide and maintain the Service</li>
                        <li>To notify you about changes to our Service</li>
                        <li>To allow you to participate in interactive features of our Service when you choose to do so</li>
                        <li>To provide customer care and support</li>
                        <li>To provide analysis or valuable information so that we can improve the Service</li>
                        <li>To monitor the usage of the Service</li>
                        <li>To detect, prevent and address technical issues</li>
                    </ul>

                    <br/><h2>Transfer Of Data</h2><br/>
                    <p>Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
                    <p>If you are located outside Lebanon and choose to provide information to us, please note that we transfer the data, including Personal Data, to Lebanon and process it there.</p>
                    <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
                    <p>AppointSync will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>

                    <br/><h2>Disclosure Of Data</h2><br/>

                    <h3>Legal Requirements</h3><br/>
                    <p>AppointSync may disclose your Personal Data in the good faith belief that such action is necessary to:</p>
                    <ul>
                        <li>To comply with a legal obligation</li>
                        <li>To protect and defend the rights or property of AppointSync</li>
                        <li>To prevent or investigate possible wrongdoing in connection with the Service</li>
                        <li>To protect the personal safety of users of the Service or the public</li>
                        <li>To protect against legal liability</li>
                    </ul>

                    <br/><h2>Security Of Data</h2><br/>
                    <p>The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>

                    <br/><h2>Service Providers</h2><br/>
                    <p>We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>
                    <p>These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>

                    <br/><h3>Analytics</h3><br/>
                    <p>We may use third-party Service Providers to monitor and analyze the use of our Service.</p>
                    <ul>
                        <li>
                            <p><strong>Google Analytics</strong></p>
                            <p>Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of our Service. This data is shared with other Google services. Google may use the collected data to contextualize and personalize the ads of its own advertising network.</p>
                            <p>For more information on the privacy practices of Google, please visit the Google Privacy & Terms web page: <a href="https://policies.google.com/privacy?hl=en">https://policies.google.com/privacy?hl=en</a></p>
                        </li>
                    </ul>


                    <br/><h2>Links To Other Sites</h2><br/>
                    <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
                    <p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>


                    <br/><h2>Children's Privacy</h2><br/>
                    <p>Our Service does not address anyone under the age of 18 ("Children").</p>
                    <p>We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.</p>


                    <br/><h2>Changes To This Privacy Policy</h2><br/>
                    <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>
                    <p>We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.</p>
                    <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>


                    <br/><h2>Contact Us</h2><br/>
                    <p>If you have any questions about this Privacy Policy, please contact us:</p>
                    <ul>
                        <li>By email: info@appointsync.com</li>
                    </ul>


                </div>

                <!--   Terms   -->

                <div class="settingsTermsSection">

                    <h2><strong>Terms and Conditions</strong></h2>

                    <p>Welcome to appointsync!</p>

                    <p>These terms and conditions outline the rules and regulations for the use of AppointSync's Website, located at www.appointsync.com.</p>

                    <p>By accessing this website we assume you accept these terms and conditions. Do not continue to use appointsync if you do not agree to take all of the terms and conditions stated on this page.</p>

                    <p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Lebanon. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>

                    <br/><h3><strong>Cookies</strong></h3>

                    <p>We employ the use of cookies. By accessing appointsync, you agreed to use cookies in agreement with the AppointSync's Privacy Policy.</p>

                    <p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p>

                    <br/><h3><strong>License</strong></h3>

                    <p>Unless otherwise stated, AppointSync and/or its licensors own the intellectual property rights for all material on appointsync. All intellectual property rights are reserved. You may access this from appointsync for your own personal use subjected to restrictions set in these terms and conditions.</p>

                    <p>You must not:</p>
                    <ul>
                        <li>Republish material from appointsync</li>
                        <li>Sell, rent or sub-license material from appointsync</li>
                        <li>Reproduce, duplicate or copy material from appointsync</li>
                        <li>Redistribute content from appointsync</li>
                    </ul>

                    <p>This Agreement shall begin on the date hereof.</p>

                    <p>Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. AppointSync does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of AppointSync,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, AppointSync shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</p>

                    <p>AppointSync reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p>

                    <p>You warrant and represent that:</p>

                    <ul>
                        <li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li>
                        <li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li>
                        <li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li>
                        <li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li>
                    </ul>

                    <p>You hereby grant AppointSync a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</p>

                   <br/><h3><strong>Hyperlinking to our Content</strong></h3>

                    <p>The following organizations may link to our Website without prior written approval:</p>

                    <ul>
                        <li>Government agencies;</li>
                        <li>Search engines;</li>
                        <li>News organizations;</li>
                        <li>Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and</li>
                        <li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li>
                    </ul>

                    <p>These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party’s site.</p>

                    <p>We may consider and approve other link requests from the following types of organizations:</p>

                    <ul>
                        <li>commonly-known consumer and/or business information sources;</li>
                        <li>dot.com community sites;</li>
                        <li>associations or other groups representing charities;</li>
                        <li>online directory distributors;</li>
                        <li>internet portals;</li>
                        <li>accounting, law and consulting firms; and</li>
                        <li>educational institutions and trade associations.</li>
                    </ul>

                    <p>We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of AppointSync; and (d) the link is in the context of general resource information.</p>

                    <p>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party’s site.</p>

                    <p>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to AppointSync. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.</p>

                    <p>Approved organizations may hyperlink to our Website as follows:</p>

                    <ul>
                        <li>By use of our corporate name; or</li>
                        <li>By use of the uniform resource locator being linked to; or</li>
                        <li>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party’s site.</li>
                    </ul>

                    <p>No use of AppointSync's logo or other artwork will be allowed for linking absent a trademark license agreement.</p>

                    <br/><h3><strong>iFrames</strong></h3>

                    <p>Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p>

                    <br/><h3><strong>Content Liability</strong></h3>

                    <p>We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>

                    <br/><h3><strong>Reservation of Rights</strong></h3>

                    <p>We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</p>

                    <br/><h3><strong>Removal of links from our website</strong></h3>

                    <p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</p>

                    <p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p>

                    <br/><h3><strong>Disclaimer</strong></h3>

                    <p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p>

                    <ul>
                        <li>limit or exclude our or your liability for death or personal injury;</li>
                        <li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
                        <li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
                        <li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
                    </ul>

                    <p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p>

                </div>

                <!--   Help   -->

                <div class="settingsHelpSection">

                    <h3>Help.</h3><br/>


                </div>


            </div>

            <!-- Renew premium -->

            <div id="myModalRenewPremium" class="modal">

                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close spanRenewPremium">&times;</span>
                    <div class="modal-header">
                        <h2>Renew Premium</h2>
                    </div>
                    <div class="modal-body">
                        <br/>
                        <br/>
                        <p>Renew now to add subscription days to your account!!</p><br/><br/>
                        <p>Your payment will be calculated based on you current plan and remaining time.</p>
                        <br/>
                        <div class="containerPlan">
                            <form class="form cf">
                                <section id="premiumRenewPackage" class="payment-plan cf">
                                    <input class='messageCheckbox' type="radio" name="radio2" id="monthly" value="monthly" checked><label class="monthly-label four col" for="monthly">Monthly $24.99</label>
                                    <input class='messageCheckbox' type="radio" name="radio2" id="yearly" value="yearly"><label class="yearly-label four col" for="yearly">Yearly $274.99</label>
                                </section>
                            </form>
                        </div>

                        <button type="button" class="appointmentButton button6"
                                id="btnSubmitRenewPremium">Renew Now</button>
                        <button type="button" id="btnCloseRenewPremium"
                                class="appointmentButton button3">Cancel</button>
                    </div>
                </div>
            </div>

            <!-- Upgrade to premium -->

            <div id="myModalUpgradeToPremium" class="modal">

                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close spanUpgradeToPremium">&times;</span>
                    <div class="modal-header">
                        <h2>Upgrade to Premium</h2>
                    </div>
                    <div class="modal-body">
                        <br/>
                        <br/>
                        <p>Upgrade to premium for an additional $9.99/month to be able to use the mobile app offline, secure your clients files with a pin,
                            and manage what your clients can do with appointments!!</p><br/><br/>
                        <p>Your payment will be calculated based on you current plan and remaining time.</p>
                        <br/>
                        <div class="containerPlan">
                            <form class="form cf">
                                <section class="payment-plan cf">
                                    <input type="radio" name="radio2" id="monthly" value="monthly" checked><label class="monthly-label four col" for="monthly">Monthly $24.99</label>
                                    <input type="radio" name="radio2" id="yearly" value="yearly"><label class="yearly-label four col" for="yearly">Yearly $274.99</label>
                                </section>
                            </form>
                        </div>

                        <button type="submit" name="submit" class="appointmentButton button6"
                                id="btnSubmitUpgradeToPremium">Upgrade Now</button>
                        <button type="button" id="btnCloseUpgradeToPremium"
                                class="appointmentButton button3">Cancel</button>
                    </div>
                </div>
            </div>


        <?php }?>



</section>

<?php

require ("objectFooter.php");

?>
<!--Maps JS-->

<script src="/appointsync.com/public/js/timepicki.js"></script>



<!-- AmendScript -->
<script>
    // Get the modal
    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");
    var myModalUpgradeToPremium = document.getElementById("myModalUpgradeToPremium");
    var myModalRenewPremium = document.getElementById("myModalRenewPremium");


    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;
    var btnUpgradeToPremium = document.getElementById("btnUpgradeToPremium") != null?document.getElementById("btnUpgradeToPremium"):0;
    var btnUpgradeToPremiumPermission = document.getElementById("btnUpgradeToPremiumPermission") != null?document.getElementById("btnUpgradeToPremiumPermission"):0;
    var btnUpgradeToPremiumFlag = document.getElementById("btnUpgradeToPremiumFlag") != null?document.getElementById("btnUpgradeToPremiumFlag"):0;


    var settingsSubscription = document.getElementsByClassName("settingsSubscription");
    // var settingsPremium = document.getElementsByClassName("settingsPremium");
    var settingsClientControl = document.getElementsByClassName("settingsClientControl");
    var settingsFileControl = document.getElementsByClassName("settingsFileControl");
    var settingsNotificationControl = document.getElementsByClassName("settingsNotificationControl");
    var settingsContactUsSection = document.getElementsByClassName("settingsContactUsSection");
    var settingsHelpSection = document.getElementsByClassName("settingsHelpSection");
    var settingsTermsSection = document.getElementsByClassName("settingsTermsSection");
    var settingsPrivacySection = document.getElementsByClassName("settingsPrivacySection");
    var settingsAboutSection = document.getElementsByClassName("settingsAboutSection");



    var btnSubmitUpgradeToPremium = document.getElementsByClassName("btnSubmitUpgradeToPremium") != null?document.getElementById("btnSubmitUpgradeToPremium"):0;
    var btnSubmitRenewPremium = document.getElementsByClassName("btnSubmitRenewPremium") != null?document.getElementById("btnSubmitRenewPremium"):0;
    var btnRenewPremium = document.getElementsByClassName("btnRenewPremium") != null?document.getElementById("btnRenewPremium"):0;

    var btnSubscription = document.getElementById("btnSubscription");
    // var btnPremium = document.getElementById("btnPremium");
    var btnClientControl = document.getElementById("btnClientControl");
    var btnFileSecurity = document.getElementById("btnFileSecurity");
    var btnNotifications = document.getElementById("btnNotifications");
    var btnHelp = document.getElementById("btnHelp");
    var btnTermsConditions = document.getElementById("btnTermsConditions");
    var btnPrivacy = document.getElementById("btnPrivacy");
    var btnAbout = document.getElementById("btnAbout");
    var btnContactUs = document.getElementById("btnContactUs");




    var btnCloseUpgradeToPremium = document.getElementById("btnCloseUpgradeToPremium");
    var btnCloseRenewPremium = document.getElementById("btnCloseRenewPremium");

    var spanUpgradeToPremium = document.getElementsByClassName("spanUpgradeToPremium")[0];
    var spanRenewPremium = document.getElementsByClassName("spanRenewPremium")[0];

    btnRenewPremium.onclick = function() {
        myModalRenewPremium.style.display = "block";
        $('body').css('overflow','hidden');
    }


    btnUpgradeToPremium.onclick = function() {
        myModalUpgradeToPremium.style.display = "block";
        $('body').css('overflow','hidden');
    }


    btnUpgradeToPremiumPermission.onclick = function() {
        myModalUpgradeToPremium.style.display = "block";
        $('body').css('overflow','hidden');
    }

    btnUpgradeToPremiumFlag.onclick = function() {
        myModalUpgradeToPremium.style.display = "block";
        $('body').css('overflow','hidden');
    }



    btnCloseRenewPremium.onclick = function() {
        myModalRenewPremium.style.display = "none";
        $('body').css('overflow','auto');
    }

    btnCloseUpgradeToPremium.onclick = function() {
        myModalUpgradeToPremium.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanUpgradeToPremium.onclick = function() {
        myModalUpgradeToPremium.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanRenewPremium.onclick = function() {
        myModalRenewPremium.style.display = "none";
        $('body').css('overflow','auto');
    }

    $( document ).ready(function() {
        $(settingsSubscription).css('display', 'block');
        $(settingsClientControl).css('display', 'none');
        $(settingsFileControl).css('display', 'none');
        $(settingsNotificationControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'none');

    });

    btnSubscription.onclick = function() {
        $(settingsSubscription).css('display', 'block');
        $(settingsClientControl).css('display', 'none');
        $(settingsFileControl).css('display', 'none');
        $(settingsNotificationControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'none');
    }


    btnClientControl.onclick = function() {
        $(settingsClientControl).css('display', 'block');
        $(settingsSubscription).css('display', 'none');
        $(settingsFileControl).css('display', 'none');
        $(settingsNotificationControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'none');
    }

    btnFileSecurity.onclick = function() {
        $(settingsNotificationControl).css('display', 'none');
        $(settingsFileControl).css('display', 'block');
        $(settingsSubscription).css('display', 'none');
        $(settingsClientControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'none');
    }


    btnNotifications.onclick = function() {
        $(settingsNotificationControl).css('display', 'block');
        $(settingsFileControl).css('display', 'none');
        $(settingsSubscription).css('display', 'none');
        $(settingsClientControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'none');
    }



    btnHelp.onclick = function() {
        $(settingsNotificationControl).css('display', 'none');
        $(settingsFileControl).css('display', 'none');
        $(settingsSubscription).css('display', 'none');
        $(settingsClientControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'block');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'none');
    }


    btnTermsConditions.onclick = function() {
        $(settingsNotificationControl).css('display', 'none');
        $(settingsFileControl).css('display', 'none');
        $(settingsSubscription).css('display', 'none');
        $(settingsClientControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'block');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'none');
    }


    btnPrivacy.onclick = function() {
        $(settingsNotificationControl).css('display', 'none');
        $(settingsFileControl).css('display', 'none');
        $(settingsSubscription).css('display', 'none');
        $(settingsClientControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'block');
        $(settingsAboutSection).css('display', 'none');
    }

    btnAbout.onclick = function() {
        $(settingsNotificationControl).css('display', 'none');
        $(settingsFileControl).css('display', 'none');
        $(settingsSubscription).css('display', 'none');
        $(settingsClientControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'none');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'block');
    }

    btnContactUs.onclick = function() {
        $(settingsNotificationControl).css('display', 'none');
        $(settingsFileControl).css('display', 'none');
        $(settingsSubscription).css('display', 'none');
        $(settingsClientControl).css('display', 'none');
        $(settingsContactUsSection).css('display', 'block');
        $(settingsHelpSection).css('display', 'none');
        $(settingsTermsSection).css('display', 'none');
        $(settingsPrivacySection).css('display', 'none');
        $(settingsAboutSection).css('display', 'none');
    }


    function secureFolderFlag(){

        // document.getElementById('secureFolderFlag').disabled = 'true';

        var pinValue = prompt("Please enter your pin:");
        if (pinValue == null || pinValue == "") {
        var secureFolderFlag = document.getElementById('secureFolderFlag').checked?'1':'0';
           if(secureFolderFlag == '0'){
               document.getElementById('secureFolderFlag').checked = true;
           }else if(secureFolderFlag == '1'){
               document.getElementById('secureFolderFlag').checked = false;
            }
        } else {

            var secureFolderFlag = document.getElementById('secureFolderFlag').checked?'1':'0';

            if(secureFolderFlag == '0'){

                var submitValue = '<?php echo $fileController->submitValue;?>';
                var submitData = '<?php echo $fileController->verifyPin;?>';
                var pinCodeValue = '<?php echo $fileController->pinValue;?>';
                var pinCodeData = pinValue;

                $.ajax({
                    type: "POST",
                    url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                    data: { [submitValue]: submitData, [pinCodeValue]: pinCodeData},
                    async: false,
                    success: function(data){
                        // alert(data);
                        if(!JSON.parse(data).success){
                            alert('Incorrect Pin!');
                            document.getElementById('secureFolderFlag').checked = true;
                        }else if(JSON.parse(data).success){

                            var submitValue = '<?php echo $fileController->submitValue;?>';
                            var submitData = '<?php echo $fileController->changeSecureFileFlag;?>';
                            var secureFolderFlagValue = '<?php echo $fileController->secureFileValue;?>';
                            var secureFolderFlagData = document.getElementById('secureFolderFlag').checked?'1':'0';


                            $.ajax({
                                type: "POST",
                                url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                                data: { [submitValue]: submitData, [secureFolderFlagValue]: secureFolderFlagData},
                                async: false,
                                    success: function(data){
                                }
                             });
                        }
                    }
                });

            }else if(secureFolderFlag == '1'){

                var submitValue = '<?php echo $fileController->submitValue;?>';
                var submitData = '<?php echo $fileController->changeTemplatePin;?>';
                var pinCodeValue = '<?php echo $fileController->pinValue;?>';
                var pinCodeData = pinValue;

                $.ajax({
                    type: "POST",
                    url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                    data: { [submitValue]: submitData, [pinCodeValue]: pinCodeData},
                    async: false,
                    success: function(data){

                        if(!JSON.parse(data).success){
                            alert('An error occured, please try again!');
                            document.getElementById('secureFolderFlag').checked = false;
                        }else if(JSON.parse(data).success){

                            var submitValue = '<?php echo $fileController->submitValue;?>';
                            var submitData = '<?php echo $fileController->changeSecureFileFlag;?>';
                            var secureFolderFlagValue = '<?php echo $fileController->secureFileValue;?>';
                            var secureFolderFlagData = document.getElementById('secureFolderFlag').checked?'1':'0';


                            $.ajax({
                                type: "POST",
                                url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                                data: { [submitValue]: submitData, [secureFolderFlagValue]: secureFolderFlagData},
                                async: false,
                                success: function(data){
                                }
                            });
                        }

                    }
                });


            }

        }
    }



    function permissionFlag(clicked_id){

        clicked_id.disabled = 'true';
            if(clicked_id == 'permissionRequest'){
            var permissionSentValueDataSet = document.getElementById('permissionRequest').checked?'1':'0';
            var permissionValueSet = '<?php echo $fileController->permissionToRequest;?>';
            }else if(clicked_id == 'permissionAccept'){
            var permissionSentValueDataSet = document.getElementById('permissionAccept').checked?'1':'0';
            var permissionValueSet = '<?php echo $fileController->permissionToAccept;?>';
            }else if(clicked_id == 'permissionAmend'){
            var permissionSentValueDataSet = document.getElementById('permissionAmend').checked?'1':'0';
            var permissionValueSet = '<?php echo $fileController->permissionToAmend;?>';
            }else if(clicked_id == 'permissionCancel'){
            var permissionSentValueDataSet = document.getElementById('permissionCancel').checked?'1':'0';
            var permissionValueSet = '<?php echo $fileController->permissionToCancel;?>';
            }


        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->changePermission;?>';
        var permissionFlag = '<?php echo $fileController->permissionValue;?>';
        var permissionValue = permissionValueSet;
        var permissionSentValue = '<?php echo $fileController->permissionSentValue;?>';
        var permissionSentValueData = permissionSentValueDataSet;


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { [submitValue]: submitData, [permissionFlag]: permissionValue, [permissionSentValue]: permissionSentValueData},
            async: false,
            success: function(data){
                clicked_id.disabled = 'false';
                }
        });


    }

    // When the user clicks the button, open the modal

    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });


    window.onclick = function(event) {
        if (event.target == modalViewNotificationPopUpgg || event.target == myModalUpgradeToPremium || event.target == myModalRenewPremium) {
            modalViewNotificationPopUpgg.style.display = "none";
            myModalUpgradeToPremium.style.display = "none";
            myModalRenewPremium.style.display = "none";
            $('body').css('overflow','auto');
        }
    }



    $('#btnSubmitRenewPremium').click(function() {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->renewPremiumSubscription;?>';
        var renewPremiumPeriodValue = '<?php echo $fileController->renewPremiumPeriodFlag;?>';
        var renewPremiumPeriodFlagData = $('.messageCheckbox:checked').val();

        var renewPremiumValue;

        if(renewPremiumPeriodFlagData == 'monthly'){
            renewPremiumValue = 'M';
        }else if(renewPremiumPeriodFlagData == 'yearly'){
            renewPremiumValue = 'Y';
        }

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { [submitValue]: submitData, [renewPremiumPeriodValue]: renewPremiumValue},
            async: false,
            success: function(data){
                if(!JSON.parse(data).success){
                    alert('Something went wrong, please try again!');
                }else if(JSON.parse(data).success){
                    location.href = "/appointsync.com/public/profilesetting.php/";
                }
            }
        });

    });

    //function changePassword(){
    //
    //    var oldPassword = document.getElementById("oldPassword").value;
    //    var newPassword = document.getElementById("newPassword").value;
    //
    //
    //    $.ajax({
    //        type: "POST",
    //        url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
    //        data: {'submit':'changePassword', 'oldPassword': oldPassword, 'newPassword':newPassword },
    //        async:false,
    //        success: function (data) {
    //
    //            location.href = "/appointsync.com/public/profile.php/<?php //echo $hostResponse->webID;?>//";
    //
    //        }
    //    });
    //
    //
    //}

</script>

</body>
<?php } ?>


<?php if ($loggedInUserType == 3){

    ?>
    <body style='width:100%;'>

    <style>

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {display:none;}

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #1193d4;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #1193d4;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }





        .containerPlan {max-width: 850px; width: 100%; margin: 0 auto;
            text-align: center; align-content: center;}
        .containerPlan .four { width: 32.26%; max-width: 32.26%;}


        /* COLUMNS */

        .containerPlan .col {
            display: block;
            float:left;
            margin: 1% 0 1% 1.6%;
        }

        .containerPlan .col:first-of-type { margin-left: 0; }

        /* CLEARFIX */

        .containerPlan .cf:before,
        .containerPlan .cf:after {
            content: " ";
            display: table;
        }

        .containerPlan .cf:after {
            clear: both;
        }

        .containerPlan .cf {
            *zoom: 1;
            text-align: center;
            margin: 0 auto;
            display: inline-block;
            min-width: 80%;
            float: none;
        }

        /* FORM */

        .containerPlan .form .plan input, .form .payment-plan input, .form .payment-type input{
            display: none;
        }

        .containerPlan .form label{
            position: relative;
            color: #fff;
            background-color: #aaa;
            font-size: 26px;
            text-align: center;
            height: 150px;
            line-height: 150px;
            display: block;
            cursor: pointer;
            border: 3px solid transparent;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            white-space: pre-wrap;
            min-width: 247px !important;
        }

        .containerPlan .form .plan input:checked + label, .form .payment-plan input:checked + label, .form .payment-type input:checked + label{
            border: 3px solid #333;
            background-color: #1193d4;
            min-width: 250px !important;
            margin: auto !important;
        }

        .containerPlan .form .plan input:checked + label:after, form .payment-plan input:checked + label:after, .form .payment-type input:checked + label:after{
            content: "\2713";
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 100%;
            border: 2px solid #333;
            background-color: #1193d4;
            z-index: 999;
            position: absolute;
            top: -10px;
            right: -10px;
        }


    </style>

    <div id="notificationPopUpBlockDiv">
        <?php

        require ("notificationview.php");

        ?>
    </div>

    <header class="blue-bg relative fix" id="home">
        <div class="section-bg overlay-bg angle-bg ripple">
            <video autoplay muted id="video-background" loop>
                <source src="http://intimissibd.com/video/video-2.webm" type="video/webm">
            </video>
        </div>
        <!--Mainmenu-->
        <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">
                        <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                    </a>
                </div>
                <?php
                include ("objectMenu.php")
                ?>
            </div>
        </nav>
        <!--Mainmenu/-->
        <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
        <!--Header-Text-->
        <!--Header-Text/-->
    </header>

    <section>

        <div style='margin-top:5%;'>

        </div>

        <br><br>

        <div class="container">
            <h3>Settings</h3><br/>


                <div class="settingsLeftTab">

                    <button class="settingsButtonClass" id="btnNotifications">Notifications</button>
                    <button class="settingsButtonClass" id="btnContactUs">Contact Us</button>
                    <button class="settingsButtonClass" id="btnAbout">About</button>
                    <button class="settingsButtonClass" id="btnPrivacy">Privacy</button>
                    <button class="settingsButtonClass" id="btnTermsConditions">Terms &amp; Conditions</button>
                    <button class="settingsButtonClass" id="btnHelp">Help</button>
                </div>



                <div class="settingsRightTab">



                    <div class="settingsNotificationControl">

                        <h3>Below you can change the options for your notifications.</h3><br/>

                        <div class="settingsClientControlDiv"><p>Email Notifications</p><br/><label class="switch">
                                <input id="permissionCancel" type="checkbox" checked onclick="">
                                <span class="slider round"></span>
                            </label></div>

                        <div class="settingsClientControlDiv"><p>App Push Notifications</p><br/><label class="switch">
                                <input id="permissionCancel" type="checkbox" checked onclick="">
                                <span class="slider round"></span>
                            </label></div><br/>
                        <p style="visibility: hidden">test</p>


                    </div>



                    <!--   Contact us   -->

                    <div class="settingsContactUsSection">

                        <h3>Contact Us</h3><br/>


                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <select id='clientContactSupportType' style="text-align:center!important;min-width: 300px;width:100% !important;">
                                        <option value="0" selected disabled>-Support Type-</option>
                                        <option value="1">Contact Us</option>
                                        <option value="2">Report an Issue</option>
                                        <option value="3">Suggestions</option>
                                    </select>
                                </div>
                                <div class="space-10"></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="form-subject" class="sr-only">Subject</label>
                                    <input type="text" class="form-control" id="form-subject" name="form-subject" placeholder="Subject" required>
                                </div>
                                <div class="space-10"></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="form-message" class="sr-only">comment</label>
                                    <textarea class="form-control" rows="6" id="form-message" name="form-message" placeholder="Message" required style="max-width: 100%;max-height: 10em;"></textarea>
                                </div>
                                <div class="space-10"></div>
                                <button class="btn btn-link no-round text-uppercase" type="submit">Send message</button>
                            </div>
                        </div>

                    </div>


                    <!--   About   -->

                    <div class="settingsAboutSection">

                        <h3>About</h3><br/>

                        <p>First we would like to welcome you, and thank you for using and trusting AppointSync.
                            At AppointSync we strive to provide you with the best service for your business, and clients.
                            We started this project with a vision to make life easier and enhancing the appointment experience between two people, it then started to grow and evolve to what it is today.
                            Our vision today is to make AppointSync to be truly useful for all businesses, updating it constantly, making it an appointment management system, that includes all the necessary needs for a business. From appointments, to the clients and their files, payments, notes and messages, making every little important information for your business one click away.
                            To achieve the above, and to be able to provide you with the utmost experience, we listen to every inquiry, and request sent by you, taking all different opinions seriously, because you made us grow to this point, and you make us grow each day. </p>


                    </div>


                    <!--   Privacy   -->

                    <div class="settingsPrivacySection">

                        <h1>Privacy Policy</h1><br/>


                        <p>Effective date: September 11, 2018</p><br/>


                        <p>AppointSync operates the appointsync.com website and the appointsync mobile application (the "Service").</p>

                        <p>This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.</p>

                        <p>We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.</p>


                        <br/><h2>Information Collection And Use</h2><br/>

                        <p>We collect several different types of information for various purposes to provide and improve our Service to you.</p>

                        <br/><h3>Types of Data Collected</h3><br/>

                        <h4>Personal Data</h4><br/>

                        <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you ("Personal Data"). Personally identifiable information may include, but is not limited to:</p>

                        <ul>
                            <li>Email address</li><li>First name and last name</li><li>Phone number</li><li>Address, State, Province, ZIP/Postal code, City</li><li>Cookies and Usage Data</li>
                        </ul>

                        <br/><h4>Usage Data</h4><br/>

                        <p>We may also collect information that your browser sends whenever you visit our Service or when you access the Service by or through a mobile device ("Usage Data").</p>
                        <p>This Usage Data may include information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>
                        <p>When you access the Service by or through a mobile device, this Usage Data may include information such as the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data.</p>

                        <br/><h4>Tracking & Cookies Data</h4>
                        <p>We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>
                        <p>Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.</p>
                        <p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>
                        <p>Examples of Cookies we use:</p>
                        <ul>
                            <li><strong>Session Cookies.</strong> We use Session Cookies to operate our Service.</li>
                            <li><strong>Preference Cookies.</strong> We use Preference Cookies to remember your preferences and various settings.</li>
                            <li><strong>Security Cookies.</strong> We use Security Cookies for security purposes.</li>
                        </ul>

                        <br/><h2>Use of Data</h2><br/>
                        <p>AppointSync uses the collected data for various purposes:</p>
                        <ul>
                            <li>To provide and maintain the Service</li>
                            <li>To notify you about changes to our Service</li>
                            <li>To allow you to participate in interactive features of our Service when you choose to do so</li>
                            <li>To provide customer care and support</li>
                            <li>To provide analysis or valuable information so that we can improve the Service</li>
                            <li>To monitor the usage of the Service</li>
                            <li>To detect, prevent and address technical issues</li>
                        </ul>

                        <br/><h2>Transfer Of Data</h2><br/>
                        <p>Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
                        <p>If you are located outside Lebanon and choose to provide information to us, please note that we transfer the data, including Personal Data, to Lebanon and process it there.</p>
                        <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
                        <p>AppointSync will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>

                        <br/><h2>Disclosure Of Data</h2><br/>

                        <h3>Legal Requirements</h3><br/>
                        <p>AppointSync may disclose your Personal Data in the good faith belief that such action is necessary to:</p>
                        <ul>
                            <li>To comply with a legal obligation</li>
                            <li>To protect and defend the rights or property of AppointSync</li>
                            <li>To prevent or investigate possible wrongdoing in connection with the Service</li>
                            <li>To protect the personal safety of users of the Service or the public</li>
                            <li>To protect against legal liability</li>
                        </ul>

                        <br/><h2>Security Of Data</h2><br/>
                        <p>The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>

                        <br/><h2>Service Providers</h2><br/>
                        <p>We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>
                        <p>These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>

                        <br/><h3>Analytics</h3><br/>
                        <p>We may use third-party Service Providers to monitor and analyze the use of our Service.</p>
                        <ul>
                            <li>
                                <p><strong>Google Analytics</strong></p>
                                <p>Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of our Service. This data is shared with other Google services. Google may use the collected data to contextualize and personalize the ads of its own advertising network.</p>
                                <p>For more information on the privacy practices of Google, please visit the Google Privacy & Terms web page: <a href="https://policies.google.com/privacy?hl=en">https://policies.google.com/privacy?hl=en</a></p>
                            </li>
                        </ul>


                        <br/><h2>Links To Other Sites</h2><br/>
                        <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
                        <p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>


                        <br/><h2>Children's Privacy</h2><br/>
                        <p>Our Service does not address anyone under the age of 18 ("Children").</p>
                        <p>We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.</p>


                        <br/><h2>Changes To This Privacy Policy</h2><br/>
                        <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>
                        <p>We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.</p>
                        <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>


                        <br/><h2>Contact Us</h2><br/>
                        <p>If you have any questions about this Privacy Policy, please contact us:</p>
                        <ul>
                            <li>By email: info@appointsync.com</li>
                        </ul>


                    </div>

                    <!--   Terms   -->

                    <div class="settingsTermsSection">

                        <h2><strong>Terms and Conditions</strong></h2>

                        <p>Welcome to appointsync!</p>

                        <p>These terms and conditions outline the rules and regulations for the use of AppointSync's Website, located at www.appointsync.com.</p>

                        <p>By accessing this website we assume you accept these terms and conditions. Do not continue to use appointsync if you do not agree to take all of the terms and conditions stated on this page.</p>

                        <p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Lebanon. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>

                        <br/><h3><strong>Cookies</strong></h3>

                        <p>We employ the use of cookies. By accessing appointsync, you agreed to use cookies in agreement with the AppointSync's Privacy Policy.</p>

                        <p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p>

                        <br/><h3><strong>License</strong></h3>

                        <p>Unless otherwise stated, AppointSync and/or its licensors own the intellectual property rights for all material on appointsync. All intellectual property rights are reserved. You may access this from appointsync for your own personal use subjected to restrictions set in these terms and conditions.</p>

                        <p>You must not:</p>
                        <ul>
                            <li>Republish material from appointsync</li>
                            <li>Sell, rent or sub-license material from appointsync</li>
                            <li>Reproduce, duplicate or copy material from appointsync</li>
                            <li>Redistribute content from appointsync</li>
                        </ul>

                        <p>This Agreement shall begin on the date hereof.</p>

                        <p>Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. AppointSync does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of AppointSync,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, AppointSync shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</p>

                        <p>AppointSync reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p>

                        <p>You warrant and represent that:</p>

                        <ul>
                            <li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li>
                            <li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li>
                            <li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li>
                            <li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li>
                        </ul>

                        <p>You hereby grant AppointSync a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</p>

                        <br/><h3><strong>Hyperlinking to our Content</strong></h3>

                        <p>The following organizations may link to our Website without prior written approval:</p>

                        <ul>
                            <li>Government agencies;</li>
                            <li>Search engines;</li>
                            <li>News organizations;</li>
                            <li>Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and</li>
                            <li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li>
                        </ul>

                        <p>These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party’s site.</p>

                        <p>We may consider and approve other link requests from the following types of organizations:</p>

                        <ul>
                            <li>commonly-known consumer and/or business information sources;</li>
                            <li>dot.com community sites;</li>
                            <li>associations or other groups representing charities;</li>
                            <li>online directory distributors;</li>
                            <li>internet portals;</li>
                            <li>accounting, law and consulting firms; and</li>
                            <li>educational institutions and trade associations.</li>
                        </ul>

                        <p>We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of AppointSync; and (d) the link is in the context of general resource information.</p>

                        <p>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party’s site.</p>

                        <p>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to AppointSync. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.</p>

                        <p>Approved organizations may hyperlink to our Website as follows:</p>

                        <ul>
                            <li>By use of our corporate name; or</li>
                            <li>By use of the uniform resource locator being linked to; or</li>
                            <li>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party’s site.</li>
                        </ul>

                        <p>No use of AppointSync's logo or other artwork will be allowed for linking absent a trademark license agreement.</p>

                        <br/><h3><strong>iFrames</strong></h3>

                        <p>Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p>

                        <br/><h3><strong>Content Liability</strong></h3>

                        <p>We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>

                        <br/><h3><strong>Reservation of Rights</strong></h3>

                        <p>We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</p>

                        <br/><h3><strong>Removal of links from our website</strong></h3>

                        <p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</p>

                        <p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p>

                        <br/><h3><strong>Disclaimer</strong></h3>

                        <p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p>

                        <ul>
                            <li>limit or exclude our or your liability for death or personal injury;</li>
                            <li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
                            <li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
                            <li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
                        </ul>

                        <p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p>

                    </div>

                    <!--   Help   -->

                    <div class="settingsHelpSection">

                        <h3>Help.</h3><br/>


                    </div>


                </div>

                <!-- Renew premium -->

                <div id="myModalRenewPremium" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <span class="close spanRenewPremium">&times;</span>
                        <div class="modal-header">
                            <h2>Renew Premium</h2>
                        </div>
                        <div class="modal-body">
                            <br/>
                            <br/>
                            <p>Renew now to add subscription days to your account!!</p><br/><br/>
                            <p>Your payment will be calculated based on you current plan and remaining time.</p>
                            <br/>
                            <div class="containerPlan">
                                <form class="form cf">
                                    <section id="premiumRenewPackage" class="payment-plan cf">
                                        <input class='messageCheckbox' type="radio" name="radio2" id="monthly" value="monthly" checked><label class="monthly-label four col" for="monthly">Monthly $24.99</label>
                                        <input class='messageCheckbox' type="radio" name="radio2" id="yearly" value="yearly"><label class="yearly-label four col" for="yearly">Yearly $274.99</label>
                                    </section>
                                </form>
                            </div>

                            <button type="button" class="appointmentButton button6"
                                    id="btnSubmitRenewPremium">Renew Now</button>
                            <button type="button" id="btnCloseRenewPremium"
                                    class="appointmentButton button3">Cancel</button>
                        </div>
                    </div>
                </div>

                <!-- Upgrade to premium -->

                <div id="myModalUpgradeToPremium" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <span class="close spanUpgradeToPremium">&times;</span>
                        <div class="modal-header">
                            <h2>Upgrade to Premium</h2>
                        </div>
                        <div class="modal-body">
                            <br/>
                            <br/>
                            <p>Upgrade to premium for an additional $9.99/month to be able to use the mobile app offline, secure your clients files with a pin,
                                and manage what your clients can do with appointments!!</p><br/><br/>
                            <p>Your payment will be calculated based on you current plan and remaining time.</p>
                            <br/>
                            <div class="containerPlan">
                                <form class="form cf">
                                    <section class="payment-plan cf">
                                        <input type="radio" name="radio2" id="monthly" value="monthly" checked><label class="monthly-label four col" for="monthly">Monthly $24.99</label>
                                        <input type="radio" name="radio2" id="yearly" value="yearly"><label class="yearly-label four col" for="yearly">Yearly $274.99</label>
                                    </section>
                                </form>
                            </div>

                            <button type="submit" name="submit" class="appointmentButton button6"
                                    id="btnSubmitUpgradeToPremium">Upgrade Now</button>
                            <button type="button" id="btnCloseUpgradeToPremium"
                                    class="appointmentButton button3">Cancel</button>
                        </div>
                    </div>
                </div>

    </section>

    <?php

    require ("objectFooter.php");

    ?>



    <!-- AmendScript -->
    <script>
        // Get the modal
        var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");


        var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;

        var settingsNotificationControl = document.getElementsByClassName("settingsNotificationControl");
        var settingsContactUsSection = document.getElementsByClassName("settingsContactUsSection");
        var settingsHelpSection = document.getElementsByClassName("settingsHelpSection");
        var settingsTermsSection = document.getElementsByClassName("settingsTermsSection");
        var settingsPrivacySection = document.getElementsByClassName("settingsPrivacySection");
        var settingsAboutSection = document.getElementsByClassName("settingsAboutSection");

        var btnNotifications = document.getElementById("btnNotifications");
        var btnHelp = document.getElementById("btnHelp");
        var btnTermsConditions = document.getElementById("btnTermsConditions");
        var btnPrivacy = document.getElementById("btnPrivacy");
        var btnAbout = document.getElementById("btnAbout");
        var btnContactUs = document.getElementById("btnContactUs");


        $( document ).ready(function() {
            $(settingsNotificationControl).css('display', 'block');
            $(settingsContactUsSection).css('display', 'none');
            $(settingsHelpSection).css('display', 'none');
            $(settingsTermsSection).css('display', 'none');
            $(settingsPrivacySection).css('display', 'none');
            $(settingsAboutSection).css('display', 'none');

        });

        btnNotifications.onclick = function() {
            $(settingsNotificationControl).css('display', 'block');
            $(settingsContactUsSection).css('display', 'none');
            $(settingsHelpSection).css('display', 'none');
            $(settingsTermsSection).css('display', 'none');
            $(settingsPrivacySection).css('display', 'none');
            $(settingsAboutSection).css('display', 'none');
        }



        btnHelp.onclick = function() {
            $(settingsNotificationControl).css('display', 'none');
            $(settingsContactUsSection).css('display', 'none');
            $(settingsHelpSection).css('display', 'block');
            $(settingsTermsSection).css('display', 'none');
            $(settingsPrivacySection).css('display', 'none');
            $(settingsAboutSection).css('display', 'none');
        }


        btnTermsConditions.onclick = function() {
            $(settingsNotificationControl).css('display', 'none');
            $(settingsContactUsSection).css('display', 'none');
            $(settingsHelpSection).css('display', 'none');
            $(settingsTermsSection).css('display', 'block');
            $(settingsPrivacySection).css('display', 'none');
            $(settingsAboutSection).css('display', 'none');
        }


        btnPrivacy.onclick = function() {
            $(settingsNotificationControl).css('display', 'none');
            $(settingsContactUsSection).css('display', 'none');
            $(settingsHelpSection).css('display', 'none');
            $(settingsTermsSection).css('display', 'none');
            $(settingsPrivacySection).css('display', 'block');
            $(settingsAboutSection).css('display', 'none');
        }

        btnAbout.onclick = function() {
            $(settingsNotificationControl).css('display', 'none');
            $(settingsContactUsSection).css('display', 'none');
            $(settingsHelpSection).css('display', 'none');
            $(settingsTermsSection).css('display', 'none');
            $(settingsPrivacySection).css('display', 'none');
            $(settingsAboutSection).css('display', 'block');
        }

        btnContactUs.onclick = function() {
            $(settingsNotificationControl).css('display', 'none');
            $(settingsContactUsSection).css('display', 'block');
            $(settingsHelpSection).css('display', 'none');
            $(settingsTermsSection).css('display', 'none');
            $(settingsPrivacySection).css('display', 'none');
            $(settingsAboutSection).css('display', 'none');
        }



        // When the user clicks the button, open the modal

        $(btnViewNotificationsPopup).click(function() {
            // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
            $('.redDot').css('visibility','hidden');
            $(modalViewNotificationPopUpgg).css('display', 'block');
            $('body').css('overflow','hidden');

            $.ajax({
                type: "POST",
                url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: { 'submit':'readNotifications' }
            });

        });


    </script>

    </body>
<?php } ?>


</html>