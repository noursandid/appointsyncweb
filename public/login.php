<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
// error_reporting(E_PARSE);
unset($_SESSION['user']);
?>
<!DOCTYPE html>
<html>
<head>

    <title>AppointSync - Login</title>

    <?php

    require ("objectHeader.php");

    ?>

</head>

<body data-spy="scroll" data-target="#mainmenu">
<style>
    .buttonLogin {
        margin-top: 20px;
        background: -webkit-linear-gradient(left, #000000 0%, #1193d4 0%, #000000 100%);
        border: none;
        font-size: 1.6em;
        font-weight: 300;
        padding: 5px 0;
        width: 35%;
        border-radius: 3px;

    &
    : hover {
        -webkit-animation: hop 2s;
        animation: hop 2s;
    }

    }
    .float {
        display: inline-block;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-property: transform;
        transition-property: transform;
        -webkit-transform: translateZ(0);
        transform: translateZ(0);
        box-shadow: 0 0 1px rgba(0, 0, 0, 0);
    }

    .float:hover, .float:focus, .float:active {
        -webkit-transform: translateY(-3px);
        transform: translateY(-3px);
    }
</style>

<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg angle-bg ripple">
        <video autoplay muted id="video-background" loop>
            <source src="http://intimissibd.com/video/video-2.webm"
                    type="video/webm">
        </video>
    </div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                            src="/appointsync.com/public/img/newlogo.png"
                            style="min-width: 150px; width: 150px; margin-top: -5%;"
                            alt="Logo"> <!-- AppointSyncLogo.png <h2 class="text-white logo-text">AppointSync</h2>-->
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">
                    <li><a href="/appointsync.com/public/index.php">Home</a></li>
                    <li><a href="/appointsync.com/public/#work">Work</a></li>
                    <li><a href="/appointsync.com/public/#feature">Features</a></li>
                    <!--  <li><a href="#team">Team</a></li> -->
                    <!--  <li><a href="#client">Client</a></li> -->
                    <li><a href="/appointsync.com/public/#price">Pricing</a></li>
                    <!--  <li><a href="#blog">Blog</a></li> -->
                    <li><a href="/appointsync.com/public/#contact">Contact</a></li>
                    <li class="active"><a href="/appointsync.com/public/login.php">Login</a></li>
                    <li><a href="/appointsync.com/public/register.php">Register</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
    <!--Header-Text-->

    <!--Header-Text/-->
</header>


<div style="width: 100%; margin: 10% auto; text-align: center;"><br/>


    <!--    <form action="/appointsync.com/resources/Controller/API/APIRouter.php" method="post" style="margin-top: 15%;">-->
    <?php
    $fileController = new MappedRouterController();
    if (isset($_SESSION['loginCheck'])) {
        echo '<p style="color:red;">' . $_SESSION['loginCheck'] . '</p>';
        unset($_SESSION['loginCheck']);
    }
    ?>
    <p>Username* <br/></p>
    <input id="userName" type="text" name="username" class="form-control" required
           style="width: 35%; margin: 0.5% auto;"> <br/>
    <p>Password* <br/></p>
    <input id="password" type="password" name="password" class="form-control" required
           style="width: 35%; margin: 0.5% auto;"> <br/>
    <button id="submit" class="float buttonLogin" name="submit" style="color: white">Login</button>
    <!--    </form>-->

    <br/>
    <br/>
    <p>
        <spam><a href="#" id="btnForgotPassword"> Forgot Password?</a>
        </spam>


    <p/>
    <br/> <br/>

    <p>
        Not a member?
        <spam><a href="#"> Register. </a></spam>


    <p/>
    <br/>


</div>


<!-- Forgot Password-->

<div id="myModalForgotPassword" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close spanForgotPassword">&times;</span>
        <div class="modal-header">
            <h2>Forgot Password</h2>
        </div>
        <div class="modal-body">
            <h3>Please fill the below</h3><br/>
            <p>We will send you an email with the required steps to reset your password.</p>
            <br />
            <label for="emailTab">Email</label> <input type='text'
                                                             id='emailTab' class='form-control' required /><br />
            <br />
            <button type="button" name="submit" class="appointmentButton button6"
                    id="btnPostForgotPassword">Submit</button>
            <button type="button" id="btnCloseForgotPassword"
                    class="appointmentButton button3">Cancel</button>
        </div>
    </div>
</div>


<script src="/appointsync.com/public/js/vendor/jquery-1.12.4.min.js"></script>
<?php

require ('objectFooter.php')

?>


<script>


    var modalForgotPassword = document.getElementById("myModalForgotPassword");

    // Get the button that opens the modal
    var btnForgotPassword = document.getElementById("btnForgotPassword") != null?document.getElementById("btnForgotPassword"):0;

    //Close buttons
    var btnPostForgotPassword = document.getElementById("btnPostForgotPassword");
    var btnCloseForgotPassword = document.getElementById("btnCloseForgotPassword");


    var spanForgotPassword = document.getElementsByClassName("spanForgotPassword")[0];

    btnForgotPassword.onclick = function() {
        modalForgotPassword.style.display = "block";
        $('body').css('overflow','hidden');
    }

    btnPostForgotPassword.onclick = function() {
        modalForgotPassword.style.display = "none";
        $('body').css('overflow','auto');
    }

    btnCloseForgotPassword.onclick = function() {
        modalForgotPassword.style.display = "none";
        $('body').css('overflow','auto');
    }

    spanForgotPassword.onclick = function() {
        modalForgotPassword.style.display = "none";
        $('body').css('overflow','auto');
    }


    // Get the input field

    // Execute a function when the user releases a key on the keyboard
    window.addEventListener("keyup", function(event) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode == 13) {
            // Trigger the button element with a click
            document.getElementById("submit").click();
        }
    });

    $('#submit').click(function() {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->loginFlag;?>';
        var usernameValue = '<?php echo $fileController->username;?>';
        var userName = document.getElementById("userName").value;
        var passwordValue = '<?php echo $fileController->password;?>';
        var password = document.getElementById("password").value;


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { [submitValue]: submitData, [usernameValue]: userName, [passwordValue]:password },
            async: false,
            success: function(data){
                if(!JSON.parse(data).success){
                    location.href = "/appointsync.com/public/login.php";
                }else if(JSON.parse(data).success){
                    location.href = "/appointsync.com/public/dashboard.php";
                }
            }
        });
    });


    $('#btnPostForgotPassword').click(function() {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->resetPassword;?>';
        var emailValue = '<?php echo $fileController->rEmail;?>';
        var emailData = document.getElementById("emailTab").value;


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { [submitValue]: submitData, [emailValue]: emailData},
            async: false,
            success: function(data){
                if(!JSON.parse(data).success){
                    alert('Something went wrong, please try again!');
                }else if(JSON.parse(data).success){
                    alert('Please check your email!');
                }
            }
        });
    });





</script>
</body>
</html>
