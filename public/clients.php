<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedClient.php';
include_once dirname(__FILE__) . '/../resources/Classes/UserProfile.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
// error_reporting(E_PARSE);
if(isset($_SESSION['user'])) {
}else{header("Location: /appointsync.com/");
exit();
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}

$_SESSION['pageName'] = 'clients';

$host = unserialize($_SESSION['user']);
if($host->type != 2){
header("Location: /appointsync.com/");
}
$clientResponse = APIController::getClients();
if($clientResponse->success){
    $clients = $clientResponse->data;
}else{ header("Location: /appointsync.com/public/clients.php"); exit();}

include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
$fileController = new MappedRouterController();
?>
<!DOCTYPE html>
<html>
<head>
<title>AppointSync - Clients</title>

    <?php

    require ("objectHeader.php");

    ?>

<script>
        function myFunction() {
            var input = document.getElementById("Search");
            var filter = input.value.toLowerCase();
            var nodes = document.getElementsByClassName('relationUserFilter');

            for (i = 0; i < nodes.length; i++) {
                if (nodes[i].innerText.toLowerCase().includes(filter)) {
                    nodes[i].style.display = "block";
                } else {
                    nodes[i].style.display = "none";
                }
            }
        }
    </script>

</head>


<body style='width: 100%;'>

<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

	<header class="blue-bg relative fix" id="home">
		<div class="section-bg overlay-bg angle-bg ripple">
			<video autoplay muted id="video-background" loop>
				<source src="http://intimissibd.com/video/video-2.webm"
					type="video/webm">
			</video>
		</div>
		<!--Mainmenu-->
		<nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
			data-spy="affix">
			<div class="container">
				<div class="navbar-header">
					<button type="button" data-toggle="collapse" class="navbar-toggle"
						data-target="#mainmenu">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand"> <img
						src="/appointsync.com/public/img/newlogo.png"
						style="min-width: 150px; width: 150px; margin-top: -5%;"
						alt="Logo">
					</a>
				</div>
                <?php

                require ("objectMenu.php");

                ?>
			</div>
		</nav>
		<!--Mainmenu/-->
		<div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
		<!--Header-Text-->
		<!--Header-Text/-->
	</header>

	<section style="overflow: hidden!important;">

		<div style='margin-top: 5%;'></div>

		<br> <br>

		<div class="container">
			<input type="text" id="Search" onkeyup="myFunction()"
				placeholder="Please enter a search term.." title="Type in a name">

			<div>

				<div class="pull-right form-inline" style="width: 25%;">
					<button class="buttonAddClient buttonAddClient2" id="addUser">Add
						User</button>
                <?php
                echo '<button class="buttonAddClient buttonAddClient2" id="addOfflineUser">Add offline User</button>';
                $hostIsPremium = true;
                ?>
            </div>
			</div>
		</div>
		<div class="relationList">

        <?php
        foreach ($clients as $client) {
            if ($client->type == '3') {
               echo '<div class="relationUser relationUserFilter">',
                    '<h4><a href="/appointsync.com/public/clientprofile.php/' . $client->webID . '"><span style="color:#1193d4;">' . $client->identifier . '</span></a></h4>',
                    '<p>' . $client->profilePic . '</p><br/>',
                    '<p>Name: ' . $client->firstName . ' ' . $client->lastName . '</p>',
                    '<p>Email: ' . $client->email . '</p>',
                    '<p>Phone Number: ' . $client->phoneNumber . '</p>',
                    '<p>Age: <script> document.write(moment().diff(moment.unix(' .$client->dob . ').format("LL"), "years"));</script></p>',
                    '<p>Gender: ' . $client->gender . '</p>',
                    '</div>';
            } elseif ($client->type == '4') {
                echo '<div class="offlineRelationUser relationUserFilter">',
	                 '<h4><a href="clientprofile.php/' . $client->webID . '"><span style="color:#1193d4;">' . $client->identifier . '</span></a></h4>',
                     '<p>' . $client->profilePic . '</p><br/>';
                echo '<p>Name: ' . $client->identifier . '</p>';
                echo '<p>Phone Number: ' . $client->phoneNumber . '</p>',
                     '<p>Age: <script> document.write(moment().diff(moment.unix(' .$client->dob . ').format("LL"), "years"));</script></p>',
                     '<p>Gender: ' . $client->gender . '</p>',
	                 '</div>';
            }
        }

        if (isset($client)) {

        } else {
            echo '<h3 style="margin-top:10%">You don&apos;t have any users yet! go ahead and add some.</h3>';
        }
        ?>
</div>




	</section>


	<!-- Online User -->

	<div id="myModalNewUser" class="modal">

		<!-- Modal content -->
		<div class="modal-content">
            <span class="close spanOnlineUser">&times;</span>
			<div class="modal-header">
				<h2>New User</h2>
			</div>
			<div class="modal-body">
				<h3>Please fill the below</h3>
				<br />
                <label for="usernameTab">Username</label> <input type='text'
						id='usernameTab' class='form-control' required /><br />
					<br />
					<button type="submit" name="submit" class="appointmentButton button1"
						id="btnAddClient">Save</button>
					<button type="button" id="btnCloseUser"
						class="appointmentButton button3">Cancel</button>
			</div>
		</div>
	</div>



	<!-- Offline User -->

	<div id="myModalNewOfflineUser" class="modal">

		<!-- Modal content -->

        <div class="modal-content" style="min-width: 400px;">
            <span class="close spanOfflineUser">&times;</span>
            <div class="modal-header">
                <h2>New Offline User</h2>
            </div>
            <div class="modal-body">
                <h3>Please fill the below information</h3>
                <p>Values with '*' are mandatory</p>
                <input type='text' id='clientFirstName' class='form-control' placeholder="First Name*" required />
                <br/>
                <input type='text' id='clientMiddleName' class='form-control' placeholder="Middle Name" />
                <br/>
                <input type='text' id='clientLastName' class='form-control' placeholder="Last Name*" required />
                <br />
                <div class="select-box">
                    <label for="genderSelect" class="label select-box1">
				<span
                        class="label-desc">Gender*</span>
                    </label>
                    <select id="genderSelect" class="select" required>
                        <option value="" selected></option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <br/>
                <p>Date of Birth</p>
                <br/>
                <div class="form-group" style="margin:0 auto;float:none;">
                    <div class='input-group date' id='datetimepicker10' style="width:50%;margin:0 auto">
                        <input type='text' class="form-control" id="offlineClientDOB" required readonly/>
                        <span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
                    </div>
                </div>
                <br/>
                <br />
                <input type='text' id='phoneNumber' class='form-control' placeholder="Phone Number*" required />
                <br />
                <button type="submit" class="appointmentButton button1" id="btnAddOfflineClient">Save</button>
                <button type="button" id="btnCloseOfflineUser" class="appointmentButton button3">Cancel</button>
            </div>
        </div></div>



<?php

require ("objectFooter.php");

?>

	<script>
var modalNewOfflineUser = document.getElementById("myModalNewOfflineUser");
var modalNewUser = document.getElementById("myModalNewUser");

// Get the button that opens the modal
var btnAddOfflineUser = document.getElementById("addOfflineUser") != null?document.getElementById("addOfflineUser"):0;
var btnAddUser = document.getElementById("addUser") != null?document.getElementById("addUser"):0;

//Close buttons
var btnCloseOfflineUser = document.getElementById("btnCloseOfflineUser");
var btnCloseUser = document.getElementById("btnCloseUser");


var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");

var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;

var spanOnlineUser = document.getElementsByClassName("spanOnlineUser")[0];
var spanOfflineUser = document.getElementsByClassName("spanOfflineUser")[0];

$(btnViewNotificationsPopup).click(function() {
    // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
    $('.redDot').css('visibility','hidden');
    $(modalViewNotificationPopUpgg).css('display', 'block');
    $('body').css('overflow','hidden');

    $.ajax({
        type: "POST",
        url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
        data: { 'submit':'readNotifications' }
    });

});

btnAddOfflineUser.onclick = function() {
    modalNewOfflineUser.style.display = "block";
    $('body').css('overflow','hidden');
}

btnAddUser.onclick = function() {
    modalNewUser.style.display = "block";
    $('body').css('overflow','hidden');
}



btnCloseOfflineUser.onclick = function() {
    modalNewOfflineUser.style.display = "none";
    $('body').css('overflow','auto');
}

btnCloseUser.onclick = function() {
    modalNewUser.style.display = "none";
    $('body').css('overflow','auto');
}

spanOfflineUser.onclick = function() {
    modalNewOfflineUser.style.display = "none";
    $('body').css('overflow','auto');
}

spanOnlineUser.onclick = function() {
    modalNewUser.style.display = "none";
    $('body').css('overflow','auto');
}



window.onclick = function(event) {
    if (event.target == modalNewUser || event.target == modalNewOfflineUser || event.target == modalViewNotificationPopUpgg) {
        modalNewUser.style.display = "none";
        modalNewOfflineUser.style.display = "none";
        modalViewNotificationPopUpgg.style.display = "none";
        $('body').css('overflow','auto');
    }
}


$('#btnAddClient').click(function() {

    var submitValue = '<?php echo $fileController->submitValue;?>';
    var submitData = '<?php echo $fileController->addClient;?>';
    var onlineClientUsernameValue = document.getElementById("usernameTab").value;
    var onlineClientUsername = '<?php echo $fileController->clientUsername;?>';


    $.ajax({
        type: "POST",
        url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
        data: { [submitValue]: submitData, [onlineClientUsername]: onlineClientUsernameValue},
        async: false,
        success: function(data){
            if(!JSON.parse(data).success){
                location.href = "/appointsync.com/public/clients.php";
            }else if(JSON.parse(data).data.webID){
                location.href = "/appointsync.com/public/clientprofile.php/"+JSON.parse(data).data.webID;
            }
        }
    });

});



$('#btnAddOfflineClient').click(function() {

    var submitValue = '<?php echo $fileController->submitValue;?>';
    var submitData = '<?php echo $fileController->addOfflineClient;?>';
    var clientFirstNameValue = '<?php echo $fileController->clientFirstName;?>';
    var clientFirstName = document.getElementById("clientFirstName").value;
    var clientMiddleNameValue = '<?php echo $fileController->clientMiddleName;?>';
    var clientMiddleName = document.getElementById("clientMiddleName").value;
    var clientLastNameValue = '<?php echo $fileController->clientLastName;?>';
    var clientLastName = document.getElementById("clientLastName").value;
    var genderSelectValue = '<?php echo $fileController->genderSelect;?>';
    var genderSelect = document.getElementById("genderSelect").value;
    var offlineClientDOBValue = '<?php echo $fileController->offlineClientDOB;?>';
    var offlineClientDOB = document.getElementById("offlineClientDOB").value;
    var offlineClientDOBTime = offlineClientDOB +" 17:00:00";
    var offlineClientDOBUnix = moment(offlineClientDOBTime, "DDMMYYYY, h:m:s").unix();
    var phoneNumberValue = '<?php echo $fileController->phoneNumber;?>';
    var phoneNumber = document.getElementById("phoneNumber").value;


    $.ajax({
        type: "POST",
        url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
        data: { [submitValue]: submitData, [clientFirstNameValue]: clientFirstName, [clientMiddleNameValue]: clientMiddleName, [clientLastNameValue]: clientLastName
        ,[genderSelectValue]: genderSelect, [offlineClientDOBValue]: offlineClientDOBUnix, [phoneNumberValue]: phoneNumber},
        async: false,
        success: function(data){
            if(!JSON.parse(data).success){
                location.href = "/appointsync.com/public/clients.php";
            }else if(JSON.parse(data).data.webID){
                location.href = "/appointsync.com/public/clientprofile.php/"+JSON.parse(data).data.webID;
            }
        }
    });

});


$(document).mouseup(function (e)
{
    var container = $(".select-box");

    if (container.has(e.target).length === 0)
    {
        container.removeClass("open");
    }
});

$("#genderSelect").on("change" , function() {

    var selection = $(this).find("option:selected").text(),
        labelFor = $(this).attr("id"),
        label = $("[for='" + labelFor + "']");

    label.find(".label-desc").html(selection);

});

$(function () {
    $('#datetimepicker10').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY',
        ignoreReadonly: true,
    });
});

</script>


<?php
// User Alert handling
if (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 0) {
    
    echo '<script language="javascript">', 'alert("User added successfully")', '</script>';
    sleep(1.3);
    
    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 1) {
    
    echo '<script language="javascript">', 'alert("You are not a premium user")', '</script>';
    
    sleep(1.3);
    
    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 2) {
    
    echo '<script language="javascript">', 'alert("User added successfully")', '</script>';
    
    sleep(1.3);
    
    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 3) {
    
    echo '<script language="javascript">', 'alert("User does not exist, make sure you entered the username correctly. ")', '</script>';
    
    sleep(1.3);
    
    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 4) {
    
    echo '<script language="javascript">', 'alert("This user already exist in your relations")', '</script>';
    
    sleep(1.3);
    
    unset($_SESSION["addUserController"]);
} elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 5) {

    echo '<script language="javascript">', 'alert("Failed adding user.")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
}elseif (isset($_SESSION["addUserController"]) && $_SESSION['addUserController'] == 6) {

    echo '<script language="javascript">', 'alert("Failed deleting user.")', '</script>';

    sleep(1.3);

    unset($_SESSION["addUserController"]);
}

?>

</body>
</html>
