<?php
session_start();
if(isset($_SESSION['loggedUser']) && $_SESSION['loggedUser']['terminatorUserType'] == 2) {
}else{header("Location: /appointsync.com/");}
require '../resources/appointSyncController/userwitedbappointsyncinator.php';
$userID =  $_SESSION['loggedUser']['terminatorID'];
$userType = $_SESSION['loggedUser']['terminatorUserType'];
$loggedInUSerType = $_SESSION['loggedUser']['terminatorUserType'];

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AppointSync - Notifications</title>
    <link rel="apple-touch-icon" href="/appointsync.com/webroot/img/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="/appointsync.com/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/themify-icons.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/animate.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/magnific-popup.css">


    <!-- Main-Stylesheets -->

    <link rel="stylesheet" href="/appointsync.com/public/css/space.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/theme.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/overright.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/normalize.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/style.css">
    <link rel="stylesheet" href="/appointsync.com/public/css/responsive.css">

    <script src="/appointsync.com/public/js/vendor/modernizr-2.8.3.min.js"></script>


    <link href="/appointsync.com/public/css/timepicki.css" rel="stylesheet">
    <!--  -->

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


</head>


<body style='width:100%;'>


<header class="blue-bg relative fix" id="home">

    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">
                    <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                </a>
            </div>

            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">
                    <li><a href="/appointsync.com/public/dashboard.php">Dashboard</a></li>
                    <li><a href="/appointsync.com/public/clients.php">Clients</a></li>
                    <li class="active"><a href="/appointsync.com/public/notification.php">Notifications</a></li>
                    <li><a href="/appointsync.com/public/messages.php">Messages</a></li>
                    <li><a href="/appointsync.com/public/profile.php" style='background-color:transparent !important;'>Profile</a></li>
                    <li><a href="/appointsync.com/public/logout.php">Log Out</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
</header>

<section>

    <div style='margin-top:5%;'>

    </div>

    <br><br>

    <div class="container">


    </div>


</section>

<footer class="black-bg" style='margin-top:10%'>
    <div class="container" class="black-bg">


        <div class="space-80"></div>
        <div class="row text-white wow fadeIn">
            <div class="col-xs-12 text-center">
                <div class="social-menu">
                    <a href="#"><span class="ti-facebook"></span></a>
                    <a href="#"><span class="ti-twitter-alt"></span></a>
                    <a href="#"><span class="ti-linkedin"></span></a>
                    <a href="#"><span class="ti-pinterest-alt"></span></a>
                </div>
                <div class="space-20"></div>
                <p>@ 2018 <a href="http://AppointSync.com/">AppointSync</a> all right reserved.</p>
            </div>
        </div>
    </div>
    <div class="space-20"></div>
</footer>

<script src="/appointsync.com/public/js/vendor/bootstrap.min.js"></script>
<!--Plugin JS-->
<script src="/appointsync.com/public/js/owl.carousel.min.js"></script>
<script src="/appointsync.com/public/js/scrollUp.min.js"></script>
<script src="/appointsync.com/public/js/magnific-popup.min.js"></script>
<script src="/appointsync.com/public/js/ripples-min.js"></script>
<script src="/appointsync.com/public/js/contact-form.js"></script>
<script src="/appointsync.com/public/js/spectragram.min.js"></script>
<script src="/appointsync.com/public/js/ajaxchimp.js"></script>
<script src="/appointsync.com/public/js/wow.min.js"></script>
<script src="/appointsync.com/public/js/plugins.js"></script>
<!--Active JS-->
<script src="/appointsync.com/public/js/main.js"></script>


</body>
</html>