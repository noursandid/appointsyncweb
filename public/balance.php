<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedPayments.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (isset($_SESSION['user'])) {
} else {
    header("Location: /appointsync.com/");
}

$loggedInUser = unserialize($_SESSION['user']);
if($loggedInUser->type != 2){
    header("Location: /appointsync.com/");
}

$_SESSION['pageName'] = 'balance';

$getURL = $_SERVER["PATH_INFO"];
$sendUserWebID = explode("/", $getURL)[1];
$webID = $sendUserWebID;
$clientResponse = APIController::getClient($_SESSION['selectedClientWebID']);
if($clientResponse->success){
    $client = $clientResponse->data;
}else{ header("Location: /appointsync.com/public/clients.php"); exit();}

$webID = $sendUserWebID;
$isFound = false;
if ($client->webID == $_SESSION['selectedClientWebID']) {
    $isFound = true;

}
if (!$isFound) {
    header("Location: /appointsync.com/public/clients.php");
//    exit();
}


$hostResponse = APIController::getProfile();
if ($hostResponse->success) {
    $host = $hostResponse->data;
} else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}

if($host->hiddenFiles == '0') {
$getPayments = APIController::getPayments($_SESSION['selectedClientWebID'],null);
if ($getPayments->success) {
    $getPayment = $getPayments->data;
    $currencyIsSet = 'true';
}elseif($getPayments->errorCode == 363800){

    $currencyIsSet = 'false';
}else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}

}elseif($host->hiddenFiles == '1') {
    if(!isset($_SESSION['hiddenFilePin'])){   header("Location: /appointsync.com/public/clients.php"); exit();}
    $getPayments = APIController::getPayments($_SESSION['selectedClientWebID'], $_SESSION['hiddenFilePin']);
    if ($getPayments->success) {
        $getPayment = $getPayments->data;
        $currencyIsSet = 'true';
    } elseif ($getPayments->errorCode == 363800) {

        $currencyIsSet = 'false';
    }
}else {
    header("Location: /appointsync.com/public/clients.php");
    exit();
}

if($currencyIsSet == 'false') {


    ?>

<div id="myModalReject" class="modal" style="display:block;">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <h2>Set Currency</h2>
        </div>
        <div class="modal-body">
                <br/> <label>Since this is the first time you open your balance, Please select your preferred currency. <br/> Please note that
                    you can not change the currency later on.</label><br/><br/>
                <select name="currencies" id="currencyList">
                    <option selected value="">Select currency</option>
                    <option value="USD">America (United States) Dollars – USD</option>
                    <option value="AFN">Afghanistan Afghanis – AFN</option>
                    <option value="ALL">Albania Leke – ALL</option>
                    <option value="DZD">Algeria Dinars – DZD</option>
                    <option value="ARS">Argentina Pesos – ARS</option>
                    <option value="AUD">Australia Dollars – AUD</option>
                    <option value="ATS">Austria Schillings – ATS</option>

                    <option value="BSD">Bahamas Dollars – BSD</option>
                    <option value="BHD">Bahrain Dinars – BHD</option>
                    <option value="BDT">Bangladesh Taka – BDT</option>
                    <option value="BBD">Barbados Dollars – BBD</option>
                    <option value="BEF">Belgium Francs – BEF</option>
                    <option value="BMD">Bermuda Dollars – BMD</option>

                    <option value="BRL">Brazil Reais – BRL</option>
                    <option value="BGN">Bulgaria Leva – BGN</option>
                    <option value="CAD">Canada Dollars – CAD</option>
                    <option value="XOF">CFA BCEAO Francs – XOF</option>
                    <option value="XAF">CFA BEAC Francs – XAF</option>
                    <option value="CLP">Chile Pesos – CLP</option>

                    <option value="CNY">China Yuan Renminbi – CNY</option>
                    <option value="CNY">RMB (China Yuan Renminbi) – CNY</option>
                    <option value="COP">Colombia Pesos – COP</option>
                    <option value="XPF">CFP Francs – XPF</option>
                    <option value="CRC">Costa Rica Colones – CRC</option>
                    <option value="HRK">Croatia Kuna – HRK</option>

                    <option value="CYP">Cyprus Pounds – CYP</option>
                    <option value="CZK">Czech Republic Koruny – CZK</option>
                    <option value="DKK">Denmark Kroner – DKK</option>
                    <option value="DEM">Deutsche (Germany) Marks – DEM</option>
                    <option value="DOP">Dominican Republic Pesos – DOP</option>
                    <option value="NLG">Dutch (Netherlands) Guilders – NLG</option>

                    <option value="XCD">Eastern Caribbean Dollars – XCD</option>
                    <option value="EGP">Egypt Pounds – EGP</option>
                    <option value="EEK">Estonia Krooni – EEK</option>
                    <option value="EUR">Euro – EUR</option>
                    <option value="FJD">Fiji Dollars – FJD</option>
                    <option value="FIM">Finland Markkaa – FIM</option>

                    <option value="FRF">France Francs – FRF</option>
                    <option value="DEM">Germany Deutsche Marks – DEM</option>
                    <option value="XAU">Gold Ounces – XAU</option>
                    <option value="GRD">Greece Drachmae – GRD</option>
                    <option value="GTQ">Guatemalan Quetzal – GTQ</option>
                    <option value="NLG">Holland (Netherlands) Guilders – NLG</option>
                    <option value="HKD">Hong Kong Dollars – HKD</option>

                    <option value="HUF">Hungary Forint – HUF</option>
                    <option value="ISK">Iceland Kronur – ISK</option>
                    <option value="XDR">IMF Special Drawing Right – XDR</option>
                    <option value="INR">India Rupees – INR</option>
                    <option value="IDR">Indonesia Rupiahs – IDR</option>
                    <option value="IRR">Iran Rials – IRR</option>

                    <option value="IQD">Iraq Dinars – IQD</option>
                    <option value="IEP">Ireland Pounds – IEP</option>
                    <option value="ILS">Israel New Shekels – ILS</option>
                    <option value="ITL">Italy Lire – ITL</option>
                    <option value="JMD">Jamaica Dollars – JMD</option>
                    <option value="JPY">Japan Yen – JPY</option>

                    <option value="JOD">Jordan Dinars – JOD</option>
                    <option value="KES">Kenya Shillings – KES</option>
                    <option value="KRW">Korea (South) Won – KRW</option>
                    <option value="KWD">Kuwait Dinars – KWD</option>
                    <option value="LBP">Lebanon Pounds – LBP</option>
                    <option value="LUF">Luxembourg Francs – LUF</option>

                    <option value="MYR">Malaysia Ringgits – MYR</option>
                    <option value="MTL">Malta Liri – MTL</option>
                    <option value="MUR">Mauritius Rupees – MUR</option>
                    <option value="MXN">Mexico Pesos – MXN</option>
                    <option value="MAD">Morocco Dirhams – MAD</option>
                    <option value="NLG">Netherlands Guilders – NLG</option>

                    <option value="NZD">New Zealand Dollars – NZD</option>
                    <option value="NOK">Norway Kroner – NOK</option>
                    <option value="OMR">Oman Rials – OMR</option>
                    <option value="PKR">Pakistan Rupees – PKR</option>
                    <option value="XPD">Palladium Ounces – XPD</option>
                    <option value="PEN">Peru Nuevos Soles – PEN</option>

                    <option value="PHP">Philippines Pesos – PHP</option>
                    <option value="XPT">Platinum Ounces – XPT</option>
                    <option value="PLN">Poland Zlotych – PLN</option>
                    <option value="PTE">Portugal Escudos – PTE</option>
                    <option value="QAR">Qatar Riyals – QAR</option>
                    <option value="RON">Romania New Lei – RON</option>

                    <option value="ROL">Romania Lei – ROL</option>
                    <option value="RUB">Russia Rubles – RUB</option>
                    <option value="SAR">Saudi Arabia Riyals – SAR</option>
                    <option value="XAG">Silver Ounces – XAG</option>
                    <option value="SGD">Singapore Dollars – SGD</option>
                    <option value="SKK">Slovakia Koruny – SKK</option>

                    <option value="SIT">Slovenia Tolars – SIT</option>
                    <option value="ZAR">South Africa Rand – ZAR</option>
                    <option value="KRW">South Korea Won – KRW</option>
                    <option value="ESP">Spain Pesetas – ESP</option>
                    <option value="XDR">Special Drawing Rights (IMF) – XDR</option>
                    <option value="LKR">Sri Lanka Rupees – LKR</option>

                    <option value="SDD">Sudan Dinars – SDD</option>
                    <option value="SEK">Sweden Kronor – SEK</option>
                    <option value="CHF">Switzerland Francs – CHF</option>
                    <option value="TWD">Taiwan New Dollars – TWD</option>
                    <option value="THB">Thailand Baht – THB</option>
                    <option value="TTD">Trinidad and Tobago Dollars – TTD</option>

                    <option value="TND">Tunisia Dinars – TND</option>
                    <option value="TRY">Turkey New Lira – TRY</option>
                    <option value="AED">United Arab Emirates Dirhams – AED</option>
                    <option value="GBP">United Kingdom Pounds – GBP</option>
                    <option value="USD">United States Dollars – USD</option>
                    <option value="VEB">Venezuela Bolivares – VEB</option>

                    <option value="VND">Vietnam Dong – VND</option>
                    <option value="ZMK">Zambia Kwacha – ZMK</option>
                </select>
                <br/><br/>
                <button type="submit" class="appointmentButton button1" id="btnRejReject" onclick="saveCurrency();">Save</button>
        </div>
    </div>
</div>
<?php


}else{
}



?>

<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Client Balance</title>

    <?php

    require ("objectHeader.php");

    ?>

</head>

<style>

    limiter {
        width: 100%;
        margin: 0 auto;
    }

    .container-table100 {
        width: 100%;

        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
        padding: 33px 30px;
    }

    .wrap-table100 {
        width: 90%;
    }

    .table100 table {
        border-spacing: 1px;
        border-collapse: collapse;
        background: white;
        border-radius: 10px;
        overflow: hidden;
        width: 100%;
        margin: 0 auto;
        position: relative;
    }
    .table100 table * {
        position: relative;
    }
    .table100 table td, table th {
        padding-left: 8px;
    }
    .table100 table thead tr {
        height: 60px;
        background: #36304a;
    }
    .table100 table tbody tr {
        height: 50px;
    }
    .table100 table tbody tr:last-child {
        border: 0;
    }
    .table100 table td, table th {
        text-align: center;
    }
    .table100 table td.l, table th.l {
        text-align: right;
    }
    .table100 table td.c, table th.c {
        text-align: center;
    }
    .table100 table td.r, table th.r {
        text-align: center;
    }


    .table100-head th{
        font-family: OpenSans-Regular;
        font-size: 18px;
        color: #fff;
        line-height: 1.2;
        font-weight: unset;
    }

    .table100 tbody tr:nth-child(even) {
        background-color: #f5f5f5;
    }

    .table100 tbody tr {
        font-family: OpenSans-Regular;
        font-size: 15px;
        color: #808080;
        line-height: 1.2;
        font-weight: unset;
    }

    .table100 tbody tr:hover {
        color: #555555;
        background-color: #f5f5f5;
        cursor: pointer;
    }

    .column1 {
        width: 150px;
        padding-left: 40px;
    }

    .column2 {
        width: 150px;
        text-align: center;
    }

    .column3 {
        width: 150px;
        text-align: center;
    }

    .column4 {
        width: 150px;
        text-align: center;
    }

    .column5 {
        width: 300px;
        text-align: center;
    }

    @media screen and (max-width: 650px) {
        .table100 table {
            display: block;
        }
        .table100  table > *, table tr, table td, table th {
            display: block;
        }
        .table100 table thead {
            display: none;
        }
        .table100 table tbody tr {
            height: auto;
            padding: 37px 0;
        }
        .table100  table tbody tr td {
            padding-left: 40% !important;
            margin-bottom: 24px;
        }
        .table100 table tbody tr td:last-child {
            margin-bottom: 0;
        }
        .table100 table tbody tr td:before {
            font-family: OpenSans-Regular;
            font-size: 14px;
            color: #999999;
            line-height: 1.2;
            font-weight: unset;
            position: absolute;
            width: 40%;
            left: 15px;
            top: 0;
            text-align: left;
            /*padding-right:15em;*/
        }
        .table100 table tbody tr td:nth-child(1):before {
            content: "";
        }
        .table100 table tbody tr td:nth-child(2):before {
            content: "Payment Due";
        }
        .table100 table tbody tr td:nth-child(3):before {
            content: "Payment Received";
        }
        .table100 table tbody tr td:nth-child(4):before {
            content: "Remaining Amount	";
        }
        .table100 table tbody tr td:nth-child(5):before {
            content: "Note";
        }

        .column4,
        .column5 {
            text-align: right;
        }

        .column4,
        .column5,
        .column1,
        .column2,
        .column3 {
            text-align: center !important;
            width: 100%;
        }

        .table100 tbody tr {
            font-size: 14px;
        }
    }

    @media (max-width: 576px) {
        .container-table100 {
            padding-left: 15px;
            padding-right: 15px;
        }
    }

    .table100 button{
        max-width:200px;
        min-width: 180px;
        float:right;
    }
</style>

<body style='width: 100%;'>

<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">

    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                        src="/appointsync.com/public/img/newlogo.png"
                        style="min-width: 150px; width: 150px; margin-top: -5%;"
                        alt="Logo">
                </a>
            </div>

            <?php

            require ("objectMenu.php");

            ?>
        </div>
    </nav>
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
</header>
<br/><br/>
<section style="margin-top: 5%;overflow: hidden;">

    <div class="pull-right form-inline" style="width: 25%;">
        <button class="buttonAddClient buttonAddClient2" id="back">Back</button>
    </div>

    <div class="clientBalanceHeader">

       <h2 style="font-size:120%;">Balance Sheet for <?php echo $client->identifier; ?></h2><br/><br/>


    </div>


    <div class="balanceBody">

        <div class="limiter">
            <div class="container-table100">
                <div class="wrap-table100">
                    <div class="table100">
                        <button class="buttonAddClient buttonAddClient2" id="btnAddPaymentReceived">Add Payment Received</button>
                        <button class="buttonAddClient buttonAddClient2" id="btnAddPaymentDue">Add Payment Due</button>
                        <br/>
                      <br/>  <table>
                            <thead>
                            <tr class="table100-head">
                                <th class="column1">Date</th>
                                <th class="column2">Payment Due</th>
                                <th class="column3">Payment Received</th>
                                <th class="column4">Remaining Amount</th>
                                <th class="column5">Note</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(isset($getPayment)){
                            foreach($getPayment as $payment) {

                                if ($payment == end($getPayment)) {
                                    echo '<tr><td class="column1">' . $payment->date . '</td>',
                                        '<td class="column2">' . ($payment->paymentDue == "0" ? "-" : $payment->paymentDue ." USD") . '</td>',
                                        '<td class="column3">' . ($payment->paymentReceived == "0" ? "-" : $payment->paymentReceived." USD") . '</td>',
                                        '<td class="column4">' . $payment->remainingAmount .' '. $payment->currency.'</td>',
                                        '<td class="column5">' . ($payment->note == "" ? "-" : $payment->note) . '</td></tr>';

                                } else {
                                    echo '<tr><td class="column1"><script>document.write(moment.unix(' . $payment->date . ').format("LL"))</script></td>',
                                        '<td class="column2">' . ($payment->paymentDue == "0" ? "-" : $payment->paymentDue." USD") . ' </td>',
                                        '<td class="column3">' . ($payment->paymentReceived == "0" ? "-" : $payment->paymentReceived ." USD") . '</td>',
                                        '<td class="column4">' . $payment->remainingAmount .' '. $payment->currency.'</td>',
                                        '<td class="column5">' . ($payment->note == "" ? "-" : $payment->note) . '</td></tr>';
                                }
                            }

                            }

                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="clientFileFooter"></div>

</section>



<!--my modal add due-->


<div id="modalAddPaymentDue" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close spanAddDue">&times;</span>
        <div class="modal-header">
            <h2>Add Payment Due</h2>
        </div>
        <div class="modal-body">
            <p>Amount Due</p>
            <input type="text" id="paymentDueAmount" pattern="[0-9]*" required>
            <br>
            <input type="hidden" id="paymentDueAmountFormatted">
                <div class="timePickerDivInput">
                    <p>Date</p><br/>
                    <div class='col-md-5' style="width: 100%;">
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker10'>
                                <input type='text' class="form-control" name="PaymentDate" id="PaymentDate" readonly required/>
                                <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div><br/><br/>
                <br/><p>Payment Note</p>
                <input type="text" id="paymentNote" name="paymentNote" value=""><br/><br/>
                <br/>
                <button type="submit" name="submit" class="appointmentButton button1" id="btnAmSave" onclick="addPaymentDue();">Save</button>
                <button type="button" id="btnCloseAddPaymentDue" class="appointmentButton button3">Cancel</button>
        </div>
    </div>
</div>


<!--my modal add Payment-->


<div id="modalAddPaymentReceived" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close spanPaymentReceived">&times;</span>
        <div class="modal-header">
            <h2>Add Payment Received</h2>
        </div>
        <div class="modal-body">
            <p>Amount Received</p><input type="text" id="paymentReceivedAmount" pattern="[0-9]*" required>
            <br>
            <input type="hidden" id="paymentReceivedAmountFormatted">
            <div class="timePickerDivInput">
                <p>Date</p><br/>
                <div class='col-md-5' style="width: 100%;">
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker11'>
                            <input type='text' class="form-control" name="PaymentDate" id="receivedPaymentDate" readonly required/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div><br/><br/>
            <br/><p>Payment Note</p>
            <input type="text" id="receivePaymentNote" name="receivePaymentNote" value=""><br/><br/>
            <br/>
            <button type="submit" name="submit" class="appointmentButton button1" id="btnSavePaymentReceived" onclick="addPaymentReceived();">Save</button>
            <button type="button" id="btnCloseAddPaymentReceived" class="appointmentButton button3">Cancel</button>
        </div>
    </div>
</div>


<?php

require ("objectFooter.php");

?>


<script>

    var back = document.getElementById("back");

    var modalAddPaymentDue = document.getElementById("modalAddPaymentDue");
    var modalAddPaymentReceived = document.getElementById("modalAddPaymentReceived");

    // Get the button that opens the modal
    var btnAddPaymentDue = document.getElementById("btnAddPaymentDue") != null?document.getElementById("btnAddPaymentDue"):0;
    var btnAddPaymentReceived = document.getElementById("btnAddPaymentReceived") != null?document.getElementById("btnAddPaymentReceived"):0;

    //Close buttons
    var btnCloseAddPaymentDue = document.getElementById("btnCloseAddPaymentDue");
    var btnCloseAddPaymentReceived = document.getElementById("btnCloseAddPaymentReceived");


    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");
    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null?document.getElementById("showNotificationPopupLink"):0;

    var spanAddDue = document.getElementsByClassName("spanAddDue")[0];
    var spanPaymentReceived = document.getElementsByClassName("spanPaymentReceived")[0];

        btnAddPaymentDue.onclick = function() {
            modalAddPaymentDue.style.display = "block";
            $('body').css('overflow','hidden');
        }

        btnAddPaymentReceived.onclick = function() {
            modalAddPaymentReceived.style.display = "block";
            $('body').css('overflow','hidden');
        }

        btnCloseAddPaymentDue.onclick = function() {
            modalAddPaymentDue.style.display = "none";
            $('body').css('overflow','auto');
        }

         btnCloseAddPaymentReceived.onclick = function() {
            modalAddPaymentReceived.style.display = "none";
             $('body').css('overflow','auto');
        }

            spanAddDue.onclick = function() {
            modalAddPaymentDue.style.display = "none";
             $('body').css('overflow','auto');
        }

            spanPaymentReceived.onclick = function() {
            modalAddPaymentReceived.style.display = "none";
             $('body').css('overflow','auto');
        }


    window.onclick = function(event) {
        if (event.target == modalAddPaymentReceived || event.target == modalAddPaymentDue || event.target == modalViewNotificationPopUpgg) {
            modalAddPaymentReceived.style.display = "none";
            modalAddPaymentDue.style.display = "none";
            modalViewNotificationPopUpgg.style.display = "none";
            $('body').css('overflow','auto');
        }
    }

    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });

    function addPaymentDue(){

        var paymentDue = document.getElementById('paymentDueAmountFormatted').value;
        var paymentDate = document.getElementById('PaymentDate').value;
        var paymentDateTime = paymentDate+' 07:00:00';
        var paymentDateUnix = moment(paymentDateTime, "DDMMYYYY, h:m:s").unix();
        var paymentNote = document.getElementById('paymentNote').value;
        var clientWebID  = '<?php echo $webID ?>';

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'addPaymentDue', 'clientWebID':clientWebID,'paymentDue':paymentDue,'note':paymentNote,'date':paymentDateUnix },
            async: false,
            success: function($data){
                location.href = "/appointsync.com/public/balance.php/<?php echo $webID;?>";
                // console.log($data);
            }
        });


    }

    function addPaymentReceived(){

        var paymentReceived = document.getElementById('paymentReceivedAmountFormatted').value;
        var paymentDate = document.getElementById('receivedPaymentDate').value;
        var paymentDateTime = paymentDate +' 17:00:00';
        var paymentDateUnix = moment(paymentDateTime, "DDMMYYYY, h:m:s").unix();
        var receivePaymentNote = document.getElementById('receivePaymentNote').value;
        var clientWebID  = '<?php echo $webID ?>';


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'addPaymentReceived','clientWebID': clientWebID,'paymentReceived':paymentReceived,'note':receivePaymentNote,'date':paymentDateUnix },
            async: false,
            success: function($data){
                location.href = "/appointsync.com/public/balance.php/<?php echo $webID;?>";
                // console.log($data);
            }
         });


    }

    back.onclick = function () {
        location.href = "/appointsync.com/public/clientfile.php/<?php echo $webID;?>";
    };


    function saveCurrency(){

        var currency = document.getElementById('currencyList').value;

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'saveCurrency', 'currency':currency},
            async: false,
            success: function($data){
                location.href = "/appointsync.com/public/balance.php/<?php echo $webID;?>";
                // console.log($data);
            }
        });

    }



    $(function () {
        $('#datetimepicker10').datetimepicker({
            viewMode: 'months',
            format: 'DD/MM/YYYY',
            ignoreReadonly: true,
        });
    });


    $(function () {
        $('#datetimepicker11').datetimepicker({
            viewMode: 'months',
            format: 'DD/MM/YYYY',
            ignoreReadonly: true,
        });
    });

    document.getElementById("paymentDueAmount").onblur =function (){

        //number-format the user input
        this.value = parseFloat(this.value.replace(/,/g, ""))
            .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        //set the numeric value to a number input
        document.getElementById("paymentDueAmountFormatted").value = this.value.replace(/,/g, "")

    }


    document.getElementById("paymentReceivedAmount").onblur =function (){

        //number-format the user input
        this.value = parseFloat(this.value.replace(/,/g, ""))
            .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        //set the numeric value to a number input
        document.getElementById("paymentReceivedAmountFormatted").value = this.value.replace(/,/g, "")

    }



</script>

</body>
</html>
