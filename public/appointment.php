<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
// error_reporting(E_PARSE);
if (isset($_SESSION['user'])) {
} else {
    header("Location: /appointsync.com/");
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}

$_SESSION['pageName'] = 'appointment';

include_once dirname(__FILE__) . '/../resources/Classes/MappedAppointment.php';
include_once dirname(__FILE__) . '/../resources/Classes/UserProfile.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedLocation.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedAppointmentNote.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';

$host = unserialize($_SESSION['user']);


$locationsResponse = APIController::getLocations();
if ($locationsResponse->success) {
    $locations = $locationsResponse->data;
//        $_SESSION['locations'] = serialize($locations);
} else {
    header("Location: /appointsync.com/public/dashboard.php");
    exit();
}

$getURL = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$sendAppointmentWebID = explode("appointment.php/", $getURL)[1];
$webID = $sendAppointmentWebID;
$appointmentResponse = APIController::getAppointment($sendAppointmentWebID);
if ($appointmentResponse->success) {
    $appointment = $appointmentResponse->data;
} else {
    header("Location: /appointsync.com/public/dashboard.php");
    exit();
}

$fileController = new MappedRouterController();
?>
<!DOCTYPE html>
<html>
<head>


    <title>AppointSync - Appointment</title>

    <?php

    require ("objectHeader.php");

    ?>


</head>

<style>


    #userProfile {

        width: 80%;

    }

</style>
<body style='width:100%;height:100%;'>
<style>

    .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover {
        color: #555;
        background-color: red !important;
    }


</style>

<div id="notificationPopUpBlockDiv">
    <?php

    require("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg angle-bg ripple">
        <video autoplay muted id="video-background" loop>
            <source src="http://intimissibd.com/video/video-2.webm" type="video/webm">
        </video>
    </div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">
                    <img src="/appointsync.com/public/img/newlogo.png"
                         style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                </a>
            </div>
            <?php

            require ("objectMenu.php");

            ?>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
    <!--Header-Text-->
    <!--Header-Text/-->
</header>

<section style="overflow: hidden!important;">

    <div style='margin-top:5%;'>

    </div>

    <br><br>

    <div class="container">

        <?php
        //gettingAppointmentDetails   note: if nour changes query, dont forget to add location again
        // $queryGetAppointmentDetails = mysqli_query($syncminatorcon,"SELECT A.ID, A.WEB_ID, A.HOST_ID,A.USER_ID,A.STATUS, A.DATE, A.START_TIME, A.END_TIME, A.HAS_ATTACHMENT, A.REQUESTER_ID,A.HAS_AMENDMENT,A.LOCATION ,O.USERNAME,U.ID AS USERID, U.WEB_ID AS CLIENT_WEB_ID, U.FIRST_NAME,U.LAST_NAME,  U.TYPE AS USER_TYPE,O.DOB,U.GENDER,U.PHONE_NUMBER,O.EMAIL,I.IMAGE_NAME,R.USER_ID AS REJECTER_ID,ARC.COMMENT AS REASON, L.LONGITUDE, L.LATITUDE, L.DSC, L.USER_ID,ARQC.COMMENT FROM APPOINTMENT A INNER JOIN USER U ON A.USER_ID = U.ID LEFT JOIN APPOINTMENT_REQUEST_COMMENT ARQC ON ARQC.APPOINTMENT_ID = A.ID LEFT JOIN APPOINTMENT_REJECT_REASON R ON A.ID = R.APPOINTMENT_ID LEFT JOIN APPOINTMENT_REJECT_COMMENT ARC ON ARC.ID = R.ID LEFT JOIN USER_IMAGE I ON A.USER_ID = I.USER_ID INNER JOIN HOST_LOCATION L ON A.LOCATION = L.ID LEFT JOIN ONLINE_USER O ON O.USER_ID = U.ID WHERE A.WEB_ID = '$webID'
        // UNION SELECT AA.ID, AA.WEB_ID, AA.HOST_ID,AA.USER_ID,AA.STATUS, AA.DATE, AA.START_TIME, AA.END_TIME, AA.HAS_ATTACHMENT, AA.REQUESTER_ID,AA.HAS_AMENDMENT,AA.LOCATION ,O.USERNAME,U.ID AS USERID, U.WEB_ID AS CLIENT_WEB_ID, U.FIRST_NAME,U.LAST_NAME,  U.TYPE AS USER_TYPE,O.DOB,U.GENDER,U.PHONE_NUMBER,O.EMAIL,I.IMAGE_NAME,R.USER_ID AS REJECTER_ID,ARC.COMMENT AS REASON, L.LONGITUDE, L.LATITUDE, L.DSC, L.USER_ID,ARQC.COMMENT FROM APPOINTMENT_ARCHIVE AA INNER JOIN USER U ON AA.USER_ID = U.ID LEFT JOIN APPOINTMENT_REQUEST_COMMENT ARQC ON ARQC.APPOINTMENT_ID = AA.ID LEFT JOIN APPOINTMENT_REJECT_REASON R ON AA.ID = R.APPOINTMENT_ID LEFT JOIN APPOINTMENT_REJECT_COMMENT ARC ON ARC.ID = R.ID LEFT JOIN USER_IMAGE I ON AA.USER_ID = I.USER_ID INNER JOIN HOST_LOCATION L ON AA.LOCATION = L.ID LEFT JOIN ONLINE_USER O ON O.USER_ID = U.ID WHERE AA.WEB_ID = '$webID'");
        // while($rowGetAppointmentDetails = mysqli_fetch_assoc($queryGetAppointmentDetails)) {
        //     $appointment = new Appointment($rowGetAppointmentDetails);


        if ($appointment->webID == $webID) {

            $appointmentRealClientID = $appointment->client->id;
            $appointmentHostID = $appointment->appointmentHostID;
            $appointmentRealID = $appointment->appointmentID;
            $appointmentRealWeblID = $appointment->webID;
            $appointmentLocationID = $appointment->locationID;
            foreach ($locations as $location) {
                if ($location->locationID == $appointment->locationID) {
                    $appointmentLocation = $location->locationTitle;
                }
            }
            $appointmentStartStatus = $appointment->status;
            $appointmentRequestComment = $appointment->requestComment;
            $appointmentHasAmendment = isset($appointment->amendedAppointment);
            $appointmentRequesterID = $appointment->requesterID;
            $premiumTillDateUnix = 0;
            $NowDateUnix = 0;
            $dateNow = date("d-m-Y H:i");
            $NowDateUnix = strtotime($dateNow);
            if ($appointment->startDate < $NowDateUnix) {
                $appointmentDateTimeStatus = 'Past';
                $appointmentStartStatus = 6;
                unset($appointmentHasAmendment);
                $appointmentHasAmendment = 0;
            } else {
                $appointmentDateTimeStatus = 'Present';
            }

            $appointmentLocationString = $appointmentLocation;
            $clientUserRealID = $appointment->client->id;
            $clientFirstName = $appointment->client->firstName;
            $clientLastName = $appointment->client->lastName;
            $clientWebID = $appointment->client->webID;
            $clientEmail = $appointment->client->email;
            $clientUsername = $appointment->client->username;
            $clientGender = $appointment->client->gender;
            $clientType = $appointment->client->type;


            if ($appointmentStartStatus == 1) {
                $appointmentStatus = 'Requested';
            } elseif ($appointmentStartStatus == 2) {
                $appointmentStatus = 'Pending';
            } elseif ($appointmentStartStatus == 3) {
                $appointmentStatus = 'Accepted';
            } elseif ($appointmentStartStatus == 4) {
                $appointmentStatus = 'Rejected';
            } elseif ($appointmentStartStatus == 6) {
                $appointmentStatus = 'Passed';
            }
            if (isset($appointment->client->dob) && $appointment->client->dob != '') {
                $clientDOB = date("Y-m-d H:i:s", substr($appointment->client->dob, 0, 10));
                // get age from date or birthdate
                $age = (new DateTime())->diff(new DateTime($clientDOB))->y;
            }
            $clientPhoneNumber = $appointment->client->phoneNumber;



            //Appointment display
            if ($clientType == 2 || $clientType == 3) {
                echo "<div class='appointmentDivClass'>"
                    . "<div class='appointmentDivImg'>" . $appointment->client->profilePic . "</div>"
                    . "<div class='appointmentDivContent'><strong>Name:</strong><a href='/appointsync.com/public/clientprofile.php/" . $clientWebID . "'><span style='color:#1193d4'> " . $clientFirstName . ' ' . $clientLastName . "</span></a> - <script> document.write(moment().diff(moment.unix(" . $appointment->client->dob . ").format('LL'), 'years'));</script> years<br/>"
                    . "<strong>Email</strong>: " . $clientEmail . "<br/>"
                    . "<strong>Gender:</strong> " . $clientGender . "<br/>"
                    . "<strong>Number:</strong> " . $clientPhoneNumber . "<br/>";

            if ($appointmentHasAmendment == 1) {
                if ($appointment->locationID != $appointment->amendedAppointment->locationID) {
                    foreach ($locations as $location) {
                        if ($location->locationID == $appointment->amendedAppointment->locationID) {
                            $appointmentLocation = $location->locationTitle;

                            echo '<strong>Location: </strong><span class="tooltipAppointment" onclick="" style="color:red">' . $appointmentLocation,
                                '<span class="tooltipAppointmentText" style="width:10em;">Was in ' . $appointmentLocationString . '</span>',
                            '</span><br/>';
                        }
                    }
                } else {
                    echo "<strong>Location: </strong>" . $appointmentLocationString . "<br/>";
                }

            if ($appointment->startDate != $appointment->amendedAppointment->startDate) {
                ?>

                <script>
                    var startDate = moment.unix(<?php echo $appointment->startDate ?>);
                    var startDateFormatted = startDate.format('LLLL');

                    var startDateAmend = moment.unix(<?php echo $appointment->amendedAppointment->startDate ?>);
                    var startDateAmendFormatted = startDateAmend.format('LLLL');

                </script>


            <?php

            echo '<strong>From: </strong><span class="tooltipAppointment" onclick="" style="color:red"><script>document.write(startDateAmendFormatted);</script>',
            '<span class="tooltipAppointmentText" style="width:27em;">Was on <script>document.write(startDateFormatted);</script></span>',
            '</span><br/>';

            }else{
            ?>

                <script>
                    var startDate = moment.unix(<?php echo $appointment->startDate ?>);
                    var startDateFormatted = startDate.format('LLLL');

                </script>

            <?php

            echo "<strong>From:</strong> <span id='startTime'><script>document.write(startDateFormatted);</script></span><br/>";
            }if ($appointment->endDate != $appointment->amendedAppointment->endDate){


            ?>

                <script>
                    var endDate = moment.unix(<?php echo $appointment->endDate ?>);
                    var endDateFormatted = endDate.format('LLLL');

                    var endDateAmend = moment.unix(<?php echo $appointment->amendedAppointment->endDate ?>);
                    var endDateAmendFormatted = endDateAmend.format('LLLL');

                </script>

            <?php

            echo '<strong>To: </strong><span class="tooltipAppointment" onclick="" style="color:red"><script>document.write(endDateAmendFormatted);</script>',
            '<span class="tooltipAppointmentText" style="width:27em;">Was on <script>document.write(endDateFormatted);</script></span>',
            '</span><br/>';

            }else{
            ?>

                <script>
                    var endDate = moment.unix(<?php echo $appointment->endDate ?>);
                    var endDateFormatted = endDate.format('LLLL');

                </script>

            <?php

            echo "<strong>To:</strong> <span id='startTime'><script>document.write(endDateFormatted);</script></span><br/>";
            }
            }else {

            ?>

                <script>
                    var startDate = moment.unix(<?php echo $appointment->startDate ?>);
                    var startDateFormatted = startDate.format('LLLL');

                    var endDate = moment.unix(<?php echo $appointment->endDate ?>);
                    var endDateFormatted = endDate.format('LLLL');

                </script> <?php
            echo "<strong>Location: </strong>" . $appointmentLocationString . "<br/>";
            echo "<strong>From: </strong><span id='startTime'><script>document.write(startDateFormatted);</script></span> <br/> <strong>To:</strong> <span id='endTime'><script>document.write(endDateFormatted);</script></span><br/>";
            }

            echo
                "<strong>Status: </strong>" . $appointmentStatus . "<br/>";
            if ($appointmentStartStatus == 1 && $appointmentRequesterID != $host->id) {
                echo "<strong>Comment: </strong>" . $appointmentRequestComment . "<br/>"
                    . '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>'
                    . '<button class="appointmentButton button1" id="btnAccept">Accept</button><button class="appointmentButton button2" id="btnAmend">Amend</button><button class="appointmentButton button3" id="btnReject">Reject</button>';

            } elseif ($appointmentStartStatus == 1 && $appointmentRequesterID == $host->id) {
                echo "<strong>Comment: </strong>" . $appointmentRequestComment . "<br/>"
                    . '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>'
                    . '<button class="appointmentButton button3" id="btnDelete">Cancel</button>';
            } elseif ($appointmentStartStatus == 2 && $appointment->amendedAppointment->amendingUserID != $host->id) {
                echo '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>';
                echo '<button class="appointmentButton button1" id="btnAccept">Accept</button><button class="appointmentButton button2" id="btnAmend">Amend</button><button class="appointmentButton button3" id="btnDelete">Cancel</button>';
            } elseif ($appointmentStartStatus == 2 && $appointment->amendedAppointment->amendingUserID == $host->id) {
                echo '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>';
                echo '<button class="appointmentButton button4" id="btnCancelAmend">Cancel Amend</button><button class="appointmentButton button5" id="btnDelete">Cancel</button>';
            } elseif ($appointmentStartStatus == 3) {
                echo '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>';
                echo '<button class="appointmentButton button2" id="btnAmend">Amend</button><button class="appointmentButton button5" id="btnDelete">Cancel</button>';
            } elseif ($appointmentStartStatus == 4) {
                $rejectionReason = $appointment->reasonOfRejection;
                $RejectionUserID = $appointment->rejecterID;
                if ($host->id == $RejectionUserID) {
                    $personName = 'You';
                } else {
                    $personName = $clientFirstName . ' ' . $clientLastName;
                }
                echo '<strong>Rejected by:</strong> ' . $personName . '<br/><strong>' . 'Reason: </strong>' . $appointment->reasonOfRejection . '<br/>';
                echo '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>';
                echo '<button class="appointmentButton button2" id="btnAmend">Amend</button><button class="appointmentButton button3" id="btnDelete">Cancel</button>';
            } elseif ($appointmentStartStatus == 6) {
                echo "<strong>Comment:</strong> Appointment date has passed, you can not edit this appointment.<br/>"
                    . '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>'
                    . '<button class="appointmentButton button3" id="btnRemove">Remove</button>';
            }

            echo "</div></div>";

            $_SESSION['StoreWebIDFromURI'] = $appointmentRealWeblID;

            } elseif ($clientType == 4) {

            echo "<div class='appointmentDivClass'>"
                . "<div class='appointmentDivImg'>" . $appointment->client->profilePic . "</div>"
                . "<div class='appointmentDivContent'><strong>Name:</strong> <a href='/appointsync.com/public/clientprofile.php/" . $clientWebID . "'><span style='color:#1193d4'>" . $clientFirstName . ' ' . $clientLastName . "</span></a> - <script> document.write(moment().diff(moment.unix(" . $appointment->client->dob . ").format('LL'), 'years'));</script> years<br/>"
                . "<strong>Gender:</strong> " . $clientGender . "<br/>"
                . "<strong>Number:</strong> " . $clientPhoneNumber . "<br/>"
                . "<strong>Location:</strong> " . $appointmentLocationString . "<br/>"; ?>

                <script>

                    var startDate = moment.unix(<?php echo $appointment->startDate ?>);
                    var startDateFormatted = startDate.format('LLLL');

                    var endDate = moment.unix(<?php echo $appointment->endDate ?>);
                    var endDateFormatted = endDate.format('LLLL');

                </script>
                <?php
                echo "<strong>From:</strong> <span id='startTime'><script>document.write(startDateFormatted);</script></span> <br/> <strong>To:</strong> <span id='endTime'><script>document.write(endDateFormatted);</script></span>";


                echo "<br/>"
                    . "<strong>Status:</strong> " . $appointmentStatus . "<br/>";
                if ($appointmentStartStatus == 3) {
                    echo '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>';
                    echo '<button class="appointmentButton button2" id="btnAmend">Amend</button><button class="appointmentButton button5" id="btnDelete">Delete</button>';
                } elseif ($appointmentStartStatus == 6) {
                    echo "<strong>Comment: </strong>Appointment date has passed, you can not edit this appointment.<br/>"
                        . '<a href="/appointsync.com/public/appointmentnotes.php/' . $appointmentRealWeblID . '" style="color:#1193d4">Appointment Notes</a><br/>'
                        . '<button class="appointmentButton button3" id="btnRemove">Remove</button>';
                }

                echo "</div></div>";

                $_SESSION['StoreWebIDFromURI'] = $appointmentRealWeblID;

            }
        }
        ?>


    </div>

    <!-- Accept -->

    <div id="myModalAccept" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanAcceptAppointment">&times;</span>
            <div class="modal-header">
                <h2>Accept Appointment</h2>
            </div>
            <div class="modal-body">
                <h3> Are you sure you want to accept the appointment? </h3>
                    <button type="submit" class="appointmentButton button1" id="btnAcceptAppointment">Accept</button>
                    <button type="button" id="btnCloseAcc" class="appointmentButton button3">Cancel</button>
            </div>
        </div>
    </div>


    <!-- AmendPopup -->

    <?php

    ?>
    <div id="myModalAmend" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanAmendAppointment">&times;</span>
            <div class="modal-header">
                <h2>Edit Appointment</h2>
            </div>
            <div class="modal-body">
                    <p style="color:red"></p>
                    <h4>Edit Appointment </h4><br/>
                    <div class="timePickerDivInput">
                        <p>From</p><br/>
                        <div class='col-md-5' style="width: 100%;">
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker6'>
                                    <input type='text' class="form-control" name="dateTimeFrom" id="dateTimeFrom"
                                           readonly/>
                                    <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="dateTimeFromUTC" name="dateTimeFromUTC" value=""><br/><br/><br/>
                        <p>To</p><br/>
                        <div class='col-md-5' style="width: 100%;">
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker7'>
                                    <input type='text' class="form-control" name="dateTimeTo" id="dateTimeTo" readonly/>
                                    <span class="input-group-addon">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="dateTimeToUTC" name="dateTimeToUTC" value="">
                    </div>
                    <br/><br/><br/><br/>
                    <div class="select-box">
                        <label for="selectLocation" class="label select-box1"><span class="label-desc">Locations</span>
                        </label>
                        <select id="selectLocation" class="select" name="locationSelect">
                            <?php
                            foreach ($locations as $location) {
                                if ($location->locationID == $appointment->locationID) {
                                    $appointmentLocation = $location->locationTitle;

                                    echo '<option value ="' . $location->locationWebID . '" selected>' . $location->locationTitle . '</option>';
                                } elseif ($location->locationID != $appointment->locationID) {
                                    echo '<option value ="' . $location->locationWebID . '">' . $location->locationTitle . '</option>';
                                }

                            }
                            ?>
                        </select></div>
                    <br/>
                    <button type="submit" name="submit" class="appointmentButton button1" id="btnSubmitAmendment">Save</button>
                    <button type="button" id="btnCloseAm" class="appointmentButton button3">Cancel</button>
            </div>
        </div>
    </div>

    <!-- Reject -->

    <div id="myModalReject" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanRejectAppointment">&times;</span>
            <div class="modal-header">
                <h2>Reject Appointment</h2>
            </div>
            <div class="modal-body">
                    <br/> <label>Reject Reason<span class="required">*</span></label><br/><br/>
                    <textarea name="rejectMessage" id="rejectAppointmentTextArea" class="field-long5"
                              style='max-width:80%;max-height:250px;'></textarea>

                    <br/><br/>
                    <button type="submit" class="appointmentButton button1" id="btnRejectAppointment">Reject</button>
                    <button type="button" class="appointmentButton button3" id="btnCloseRej">Cancel</button>
            </div>
        </div>
    </div>

    <!--  Cancel Amend  -->
    <div id="myModalCancelAmend" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanCancelAppointment">&times;</span>
            <div class="modal-header">
                <h2>Cancel Amendment</h2>
            </div>
            <div class="modal-body">
                <br/>
                <h3> Are you sure you want to cancel the amendment?</h3>

                <br/><br/>
                <button class="appointmentButton button1" id="btnCancelAmendment">Yes</button>
                <button type="button" class="appointmentButton button3" id="btnCloseCanAmend">Cancel</button>
            </div>
        </div>
    </div>

    <!--  Delete Appointment  -->

    <div id="myModalDeleteAppointment" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanDeleteAppointment">&times;</span>
            <div class="modal-header">
                <h2>Cancel Appointment</h2>
            </div>
            <div class="modal-body">
                <br/>
                <h3> Are you sure you want to Cancel the appointment? The other person will be notified that you are
                    cancelling the appointment.</h3>

                <br/><br/>
                    <button name="submit" value="deleteAppointment" type="submit" class="appointmentButton button1"
                            id="btnDeleteAppointment">Yes
                    </button>
                    <button type="button" class="appointmentButton button3" id="btnCloseDel">Cancel</button>
            </div>
        </div>
    </div>


    </div>


    <!--  Remove Appointment  -->

    <div id="myModalRemoveAppointment" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanRemoveAppointment">&times;</span>
            <div class="modal-header">
                <h2>Remove Appointment</h2>
            </div>
            <div class="modal-body">
                <br/>
                <h5> Are you sure you want to Remove this appointment? This action will remove the appointment from your
                    calendar along with its notes.<br/>
                    You can not revert this action.</h5>

                <br/><br/>
                    <button name="submit" type="submit" class="appointmentButton button1" id="btnRemoveAppointment">Yes
                    </button>
                    <button type="button" class="appointmentButton button3" id="btnCloseRemove">Cancel</button>
            </div>
        </div>
    </div>


    </div>

</section>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

<?php

require ("objectFooter.php");

?>

<!-- AmendScript -->
<script>
    // Get the modal
    var modalAmend = document.getElementById('myModalAmend');
    var modalAccept = document.getElementById('myModalAccept');
    var modalReject = document.getElementById('myModalReject');
    var modalCancelAmend = document.getElementById('myModalCancelAmend');
    var modalDeleteAppointment = document.getElementById('myModalDeleteAppointment');
    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");
    var modalRemoveAppointment = document.getElementById("myModalRemoveAppointment");

    // Get the button that opens the modal
    var btnAmend = document.getElementById("btnAmend") != null ? document.getElementById("btnAmend") : 0;
    var btnAccept = document.getElementById("btnAccept") != null ? document.getElementById("btnAccept") : 0;
    var btnReject = document.getElementById("btnReject") != null ? document.getElementById("btnReject") : 0;
    var btnCancelAmend = document.getElementById("btnCancelAmend") != null ? document.getElementById("btnCancelAmend") : 0;
    var btnDelete = document.getElementById("btnDelete") != null ? document.getElementById("btnDelete") : 0;
    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null ? document.getElementById("showNotificationPopupLink") : 0;
    var btnRemove = document.getElementById("btnRemove") != null ? document.getElementById("btnRemove") : 0;


    var btnCloseAcc = document.getElementById("btnCloseAcc");
    var btnCloseAm = document.getElementById("btnCloseAm");
    var btnCloseRej = document.getElementById("btnCloseRej");
    var btnCloseCanAmend = document.getElementById("btnCloseCanAmend");
    var btnCloseDel = document.getElementById("btnCloseDel");
    var btnCloseRemove = document.getElementById("btnCloseRemove");
    var btnCancelAmendment = document.getElementById("btnCancelAmendment");

    var spanAmendAppointment = document.getElementsByClassName("spanAmendAppointment")[0];
    var spanAcceptAppointment = document.getElementsByClassName("spanAcceptAppointment")[0];
    var spanRejectAppointment = document.getElementsByClassName("spanRejectAppointment")[0];
    var spanCancelAppointment = document.getElementsByClassName("spanCancelAppointment")[0];
    var spanDeleteAppointment = document.getElementsByClassName("spanDeleteAppointment")[0];
    var spanRemoveAppointment = document.getElementsByClassName("spanRemoveAppointment")[0];

    // When the user clicks the button, open the modal


    btnAmend.onclick = function () {
        modalAmend.style.display = "block";
        $('body').css('overflow', 'hidden');
    }

    btnReject.onclick = function () {
        modalReject.style.display = "block";
        $('body').css('overflow', 'hidden');
    }

    btnAccept.onclick = function () {
        modalAccept.style.display = "block";
        $('body').css('overflow', 'hidden');
    }

    btnCancelAmend.onclick = function () {
        modalCancelAmend.style.display = "block";
        $('body').css('overflow', 'hidden');
    }

    btnDelete.onclick = function () {
        modalDeleteAppointment.style.display = "block";
        $('body').css('overflow', 'hidden');
    }

    btnRemove.onclick = function () {
        modalRemoveAppointment.style.display = "block";
        $('body').css('overflow', 'hidden');
    }

    btnCloseAm.onclick = function () {
        modalAmend.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    btnCloseAcc.onclick = function () {
        modalAccept.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    btnCloseRej.onclick = function () {
        modalReject.style.display = "none";
        $('body').css('overflow', 'auto');
    }


    btnCloseCanAmend.onclick = function () {
        modalCancelAmend.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    btnCloseDel.onclick = function () {
        modalDeleteAppointment.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    btnCloseRemove.onclick = function () {
        modalRemoveAppointment.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    spanAcceptAppointment.onclick = function () {
        modalAccept.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    spanAmendAppointment.onclick = function () {
        modalAmend.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    spanRejectAppointment.onclick = function () {
        modalReject.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    spanCancelAppointment.onclick = function () {
        modalCancelAmend.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    spanDeleteAppointment.onclick = function () {
        modalDeleteAppointment.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    spanRemoveAppointment.onclick = function () {
        modalRemoveAppointment.style.display = "none";
        $('body').css('overflow', 'auto');
    }

    window.onclick = function (event) {
        if (event.target == modalAccept || event.target == modalAmend || event.target == modalReject || event.target == modalCancelAmend
            || event.target == modalViewNotificationPopUpgg || event.target == modalDeleteAppointment || event.target == modalRemoveAppointment) {
            modalRemoveAppointment.style.display = "none";
            modalDeleteAppointment.style.display = "none";
            modalViewNotificationPopUpgg.style.display = "none";
            modalCancelAmend.style.display = "none";
            modalReject.style.display = "none";
            modalAmend.style.display = "none";
            modalAccept.style.display = "none";
            $('body').css('overflow', 'auto');
        }
    }

    $(btnViewNotificationsPopup).click(function () {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility', 'hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow', 'hidden');

        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {'submit': 'readNotifications'}
        });

    });

    function forceNumeric() {
        var $input = $(this);
        $input.val($input.val().replace(/[^\d]+/g, ''));
    }

    $('body').on('propertychange input', 'input[type="number"]', forceNumeric);


    $("select").on("click", function () {

        $(this).parent(".select-box").toggleClass("open");

    });

    $(document).mouseup(function (e) {
        var container = $(".select-box");

        if (container.has(e.target).length === 0) {
            container.removeClass("open");
        }
    });

    $("#selectLocation").on("change", function () {

        var selection = $(this).find("option:selected").text(),
            labelFor = $(this).attr("id"),
            label = $("[for='" + labelFor + "']");

        label.find(".label-desc").html(selection);

    });

    // Catch all events related to changes

    $(function () {
        $('#datetimepicker6').datetimepicker({


            // date: moment(),
            minDate: moment(),
            ignoreReadonly: true,
            format: ('LLLL'),

        });
        $('#datetimepicker7').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            ignoreReadonly: true,
            minDate: moment(),
            format: ('LLLL'),
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);


            var selectedDateFrom = document.getElementById("dateTimeFrom").value;
            var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
            var startDate = moment(selectedDateFormatFrom).unix();
            $('#dateTimeFromUTC').val(startDate);
            $('#dateTimeTo').val('');


        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);


            var selectedDateTo = document.getElementById("dateTimeTo").value;
            var selectedDateFormatTo = moment(selectedDateTo).format('LLLL');
            var startDate = moment(selectedDateFormatTo).unix();

            $('#dateTimeToUTC').val(startDate);

        });
    });


    $('document').ready(function () {

        var startDateUnix = <?php echo $appointment->startDate ?>;
        var startDate = moment.unix(<?php echo $appointment->startDate ?>);
        var startDateFormatted = startDate.format('LLLL');

        document.getElementById('dateTimeFrom').value = startDateFormatted;
        document.getElementById('dateTimeFromUTC').value = startDateUnix;

        var endDateUnix = <?php echo $appointment->endDate ?>;
        var endDate = moment.unix(<?php echo $appointment->endDate ?>);
        var endDateFormatted = endDate.format('LLLL');


        document.getElementById('dateTimeTo').value = endDateFormatted;
        document.getElementById('dateTimeToUTC').value = endDateUnix;


    });


    $('#btnCancelAmendment').click(function () {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->cancelAmendedAppointment;?>';
        var appointmentWebID = '<?php echo $fileController->appointmentWebID;?>';
        var appointmentWebIDValue = '<?php echo $sendAppointmentWebID;?>';

        console.log(submitValue+': ' + submitData + appointmentWebID+': ' + appointmentWebIDValue);


        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {[submitValue]: submitData, [appointmentWebID]: appointmentWebIDValue},
            async: false,
            success: function(data){
                console.log(data);
                if (!JSON.parse(data).success) {
                    location.href = "/appointsync.com/public/dashboard.php";
                } else {
                    location.href = "/appointsync.com/public/appointment.php/<?php echo $sendAppointmentWebID?>";
                }
            }
        });

    });

    $('#btnRemoveAppointment').click(function () {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->deleteAppointment;?>';
        var appointmentWebID = '<?php echo $fileController->appointmentWebID;?>';
        var appointmentWebIDValue = '<?php echo $sendAppointmentWebID;?>';

        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {[submitValue]: submitData, [appointmentWebID]: appointmentWebIDValue},
            async: false,
            success: function(data){
                console.log(data);
                if (!JSON.parse(data).success) {
                    location.href = "/appointsync.com/public/dashboard.php";
                } else {
                    location.href = "/appointsync.com/public/dashboard.php";
                }
            }
        });

    });

    $('#btnDeleteAppointment').click(function () {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->deleteAppointment;?>';
        var appointmentWebID = '<?php echo $fileController->appointmentWebID;?>';
        var appointmentWebIDValue = '<?php echo $sendAppointmentWebID;?>';

        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {[submitValue]: submitData, [appointmentWebID]: appointmentWebIDValue},
            async: false,
            success: function(data){
                console.log(data);
                if (!JSON.parse(data).success) {
                    location.href = "/appointsync.com/public/dashboard.php";
                } else {
                    location.href = "/appointsync.com/public/dashboard.php";
                }
            }
        });

    });

    $('#btnSubmitAmendment').click(function () {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->amendAppointment;?>';
        var appointmentWebID = '<?php echo $fileController->appointmentWebID;?>';
        var appointmentWebIDValue = '<?php echo $sendAppointmentWebID;?>';
        var appointmentLocationWebID = '<?php echo $fileController->selectLocation;?>';
        var appointmentLocationWebIDValue = document.getElementById("selectLocation").value;
        var appointmentStartDate= '<?php echo $fileController->dateTimeFromUTC;?>';
        var appointmentStartDateValue = document.getElementById("dateTimeFromUTC").value;
        var appointmentEndDate = '<?php echo $fileController->dateTimeToUTC;?>';
        var appointmentEndDateValue = document.getElementById("dateTimeToUTC").value;

        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {[submitValue]: submitData, [appointmentWebID]: appointmentWebIDValue,[appointmentLocationWebID]: appointmentLocationWebIDValue
                , [appointmentStartDate]: appointmentStartDateValue, [appointmentEndDate]: appointmentEndDateValue},
            async: false,
            success: function(data){
                console.log(data);
                if (!JSON.parse(data).success) {
                    location.href = "/appointsync.com/public/dashboard.php";
                } else {
                    location.href = "/appointsync.com/public/appointment.php/<?php echo $sendAppointmentWebID?>";
                }
            }
        });

    });

    $('#btnAcceptAppointment').click(function () {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->acceptAppointment;?>';
        var appointmentWebID = '<?php echo $fileController->appointmentWebID;?>';
        var appointmentWebIDValue = '<?php echo $sendAppointmentWebID;?>';


        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {[submitValue]: submitData, [appointmentWebID]: appointmentWebIDValue},
            async: false,
            success: function(data){
                console.log(data);
                if (!JSON.parse(data).success) {
                    location.href = "/appointsync.com/public/dashboard.php";
                } else {
                    location.href = "/appointsync.com/public/appointment.php/<?php echo $sendAppointmentWebID?>";
                }
            }
        });

    });

    $('#btnRejectAppointment').click(function () {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->rejectAppointment;?>';
        var appointmentWebID = '<?php echo $fileController->appointmentWebID;?>';
        var appointmentWebIDValue = '<?php echo $sendAppointmentWebID;?>';
        var appointmentReject = '<?php echo $fileController->rejectReason;?>';
        var appointmentRejectValue = document.getElementById("rejectAppointmentTextArea").value;

        console.log(submitValue+': ' + submitData + appointmentWebID+': ' + appointmentWebIDValue);


        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {[submitValue]: submitData, [appointmentWebID]: appointmentWebIDValue, [appointmentReject]: appointmentRejectValue},
            async: false,
            success: function(data){
                console.log(data);
                if (!JSON.parse(data).success) {
                    location.href = "/appointsync.com/public/dashboard.php";
                } else {
                    location.href = "/appointsync.com/public/appointment.php/<?php echo $sendAppointmentWebID?>";
                }
            }
        });

    });


</script>


</body>
</html>
