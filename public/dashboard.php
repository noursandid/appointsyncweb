<?php
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/UserProfile.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedClient.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}
// error_reporting(E_PARSE);

// if (isset($_SESSION['host']) && unserialize($_SESSION['host'])->type == 2) {} else {
// header("Location: /appointsync.com/");
// }


$_SESSION['pageName'] = 'dashboard';

$host = unserialize($_SESSION['user']);
if ($host->type == 2) {
    $clientsResponse = APIController::getClients();
    if ($clientsResponse->success) {
        $clients = $clientsResponse->data;
        //$_SESSION['clients'] = serialize($clients);
    } else {
        header("Location: /appointsync.com/public/login.php");
        exit();
    }
    $locationsResponse = APIController::getLocations();
    if ($locationsResponse->success) {
        $locations = $locationsResponse->data;
    } else {
        header("Location: /appointsync.com/public/login.php");
        exit();
    }

    $appointmentsResponse = APIController::getAppointments();
    if ($appointmentsResponse->success) {
        $appointments = $appointmentsResponse->data;
        echo '<script> var userID = "'.$host->id.'"; var appointments = '.json_encode($appointments).'; var locations = '.json_encode($locations).'; </script>';
        echo '<script>',
        'var requestedColor = "rgba(76,175,80,0.5)";',
        'var pendingColor = "rgba(255, 204, 51,0.5)";',
        'var rejectedColor = "rgba(244,67,54,0.5)";',
        'var acceptedColor = "rgba(17,147,212,0.5)";',
        'var passedColor = "rgba(136,136,136,0.5)";',
        '</script>';
    }
    else{
        header("Location: /appointsync.com/public/login.php");
        exit();
    }


} else {
}
$fileController = new MappedRouterController();



?>
<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Dashboard</title>

    <?php

    require ("objectHeader.php");

    ?>

    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="./jquery.dropdown.css">
    <script src="./jquery.dropdown.js"></script>

</head>
<body style='width: 100%;'>
<style>
    .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus,
    .navbar-default .navbar-nav > .open > a:hover {
        color: #555;
        background-color: red !important;
        z-index: 1 !important;
    }

    .modal {
        z-index: 800 !important; /* Sit on top */
        margin: 0px auto;
    }

    #dateTimeFrom{
        z-index: 900 !important;
    }
</style>

<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg angle-bg ripple">
        <video autoplay muted id="video-background" loop>
            <source src="http://intimissibd.com/video/video-2.webm"
                    type="video/webm">
        </video>
    </div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top"
         data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle"
                        data-target="#mainmenu">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"> <img
                            src="/appointsync.com/public/img/newlogo.png"
                            style="min-width: 150px; width: 150px; margin-top: -5%;"
                            alt="Logo"> <!-- AppointSyncLogo.png <h2 class="text-white logo-text">AppointSync</h2>-->
                </a>
            </div>
            <?php

            require ("objectMenu.php");

            ?>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
    <!--Header-Text-->
    <!--Header-Text/-->
</header>

<section>
    <div style='margin-top: 5%;'>
    </div>
    <!--  -->

    <!--		<br> <br>-->
    <!-- insert here -->

    <div class="container">


        <div class="page-header">

            <div class="pull-right form-inline" style="width:50%">
                <div class="btn-group">
                    <button class="btn btn-primary" onclick=previousButtonPressed() data-calendar-nav="prev"><< Prev</button>
                    <button class="btn btn-primary" onclick="todayButtonPressed()">Today</button>
                    <button class="btn btn-primary" onclick=nextButtonPressed() data-calendar-nav="next">Next >></button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-warning" onclick=yearViewButtonPressed()>Year</button>
                    <button class="btn btn-warning" onclick=monthViewButtonPressed()>Month</button>
                    <!--						<button class="btn btn-warning" data-calendar-view="week">Week</button>-->
                    <button class="btn btn-warning" onclick=dayViewButtonPressed()>Day</button>

                </div>

                <!-- <div style="float:right;">
    <button class="btn btn-warning" >Add Appointment</button>
    <div> -->
                <button id="myBtn" class="appointmentDashboardButton">Add Appointment</button>
            </div>
            <h3 id="test"></h3>
        </div>

        <div class="rowm">
            <div class="span9">

            </div>
            <div class="span3"></div>
        </div>

    </div>
    <div class="calendarMenu">
        <ul class="calendarMenuOptions" id="calendarMenuOptions">
        </ul>
    </div>
    <div id="Title"></div>
    <div id="Calendar"></div>



    <!--    Calendar-->




    <script>
        appointments.sort(function(a, b){
            return a.startDate-b.startDate;
        })
        class Calendar{
            constructor(appointments,locations){
                this.appointments = appointments;
                this.locations = locations;
                this.years = new Array();
                var i;
                for (i=0;i<this.appointments.length;i++){
                    var appointmentStartDate = new Date(0);
                    appointmentStartDate.setUTCSeconds(this.appointments[i].startDate);
                    if (this.years.filter(year => year.title == appointmentStartDate.getFullYear()).length != 0){
                        this.years.filter(year => year.title == appointmentStartDate.getFullYear())[0].addAppointment(this.appointments[i]);
                    }
                    else{
                        var year = new Year(this.appointments[i]);
                        this.years.push(year);
                    }
                }
            }
            getYear(year){
                return this.years.filter(y => y.title == year)[0];
            }
            getMonth(month,year){
                if (this.years.filter(y => y.title == year).length != 0){
                    return this.years.filter(y => y.title == year)[0].months.filter(m => m.title == month)[0];
                }
            }
            getDay(day,month,year){
                if (this.years.filter(y => y.title == year).length != 0 && this.years.filter(y => y.title == year)[0].months.filter(m => m.title == month).length != 0){

                    return this.years.filter(y => y.title == year)[0].months.filter(m => m.title == month)[0].days.filter(d => d.title == day)[0];
                }
            }
            getAppointmentsForDay(day,month,year){
                return this.appointments.filter(function(a) {

                    var startDate = new Date(0);
                    startDate.setUTCSeconds(a.startDate);
                    var endDate = new Date(0);
                    endDate.setUTCSeconds(a.endDate);

                    if (a.amendedAppointment != null && a.amendedAppointment.amendingUserID == userID){
                        var startDate = new Date(0);
                        startDate.setUTCSeconds(a.amendedAppointment.startDate);
                        var endDate = new Date(0);
                        endDate.setUTCSeconds(a.amendedAppointment.endDate);
                    }

                    var thisDay = new Date(year,month-1,day);
                    return (startDate.getFullYear() == year && startDate.getMonth() == month-1 && startDate.getDate() == day) || (endDate.getFullYear() == year && endDate.getMonth() == month-1 && endDate.getDate() == day) || ((startDate<thisDay && endDate > thisDay));
                });
            }

        }
        class Year{
            constructor(appointment){
                var appointmentStartDate = new Date(0);
                appointmentStartDate.setUTCSeconds(appointment.startDate);
                this.title = appointmentStartDate.getFullYear();
                this.months = new Array();
                var month = new Month(appointment);
                this.months.push(month);
            }
            addAppointment(appointment){
                var appointmentStartDate = new Date(0);
                appointmentStartDate.setUTCSeconds(appointment.startDate);
                if (this.months.filter(month => month.title == appointmentStartDate.getMonth()+1).length != 0){
                    this.months.filter(month => month.title == appointmentStartDate.getMonth()+1)[0].addAppointment(appointment);
                }
                else{
                    var month = new Month(appointment);
                    this.months.push(month);
                }
            }



        }
        class Month{

            constructor(appointment){
                var appointmentStartDate = new Date(0);
                appointmentStartDate.setUTCSeconds(appointment.startDate);
                this.title = appointmentStartDate.getMonth()+1;
                this.year = appointmentStartDate.getFullYear();
                this.days = new Array();
                var day = new Day(appointment);
                this.days.push(day);

            }

            addAppointment(appointment){
                var appointmentStartDate = new Date(0);
                appointmentStartDate.setUTCSeconds(appointment.startDate);
                if (this.days.filter(day => day.title == appointmentStartDate.getDate()).length != 0){
                    this.days.filter(day => day.title == appointmentStartDate.getDate())[0].addAppointment(appointment);
                }
                else{
                    var day = new Day(appointment);
                    this.days.push(day);
                }
            }

        }
        class Day{

            constructor(appointment){
                var appointmentStartDate = new Date(0);
                appointmentStartDate.setUTCSeconds(appointment.startDate);
                this.title = appointmentStartDate.getDate();
                this.month = appointmentStartDate.getMonth()+1;
                this.year = appointmentStartDate.getFullYear();
                this.appointments = new Array();
                this.appointments.push(appointment);
            }

            addAppointment(appointment){
                if (this.appointments.filter(app => app.appointmentID == appointment.appointmentID).length == 0){
                    this.appointments.push(appointment);
                }
            }

        }

        this.calendar = new Calendar(appointments,locations);
        const menu = document.querySelector(".calendarMenu");
        let menuVisible = false;

        function fadeOut(el, duration) {
            var s = el.style, step = 25/(duration || 300);
            s.opacity = s.opacity || 1;
            (function fade() { (s.opacity -= step) < 0 ? s.display = "none" : setTimeout(fade, 25); })();
        }

        // fade out an element from the current state to full transparency in "duration" ms
        // display is the display style the element is assigned after the animation is done
        function fadeIn(el, duration, display) {
            var s = el.style, step = 25/(duration || 300);
            s.opacity = s.opacity || 0;
            s.display = display || "block";
            (function fade() { (s.opacity = parseFloat(s.opacity)+step) > 1 ? s.opacity = 1 : setTimeout(fade, 25); })();
        }

        function appointmentsConflict(appointment1,appointment2){

            return (appointment1.appointmentID != appointment2.appointmentID) && ( (appointment1.startDate >= appointment2.startDate && appointment1.startDate <= appointment2.endDate) || (appointment1.endDate >= appointment2.startDate && appointment1.endDate <= appointment2.endDate) || (appointment2.startDate >= appointment1.startDate && appointment2.startDate <= appointment1.endDate) || (appointment2.endDate >= appointment1.startDate && appointment2.endDate <= appointment1.endDate));
        }
        const toggleMenu = command => {
//  menu.style.display = command === "show" ? "block" : "none";
            if (command === "show"){
                fadeIn(menu,90,"block");
            }
            else{
                fadeOut(menu,90);
            }
            menuVisible = !menuVisible;
        };
        const setPosition = ({ top, left }) => {
            menu.style.left = `${left}px`;
            menu.style.top = `${top}px`;
            toggleMenu("show");
        };

        function findUpTag(el, tag) {

            while (el.parentElement) {
                if (el.className == tag)
                    return el;
                el = el.parentElement;
            }
            return null;
        }


        if (document.addEventListener) { // IE >= 9; other browsers

            document.addEventListener('contextmenu', function(e) {
                if (viewType == ViewType.YEAR){
                    if ( e.target.className == "Month" || e.target.className == "monthTitle" ) {
                        removeRightClickMenu();
                        var element = document.getElementById("calendarMenuOptions");
                        var showMonth = document.createElement("li");
                        showMonth.className = "calendarMenuOption"
                        showMonth.id = "ShowAppointment";
                        showMonth.appendChild(document.createTextNode("Show Month"));
                        element.appendChild(showMonth);

                        showMonth.onclick = function(){
                            var monthTitle = findUpTag(e.target,"Month");
                            selectedMonth = monthTitle.id;
                            sessionStorage.setItem("selectedMonth",selectedMonth);
                            viewType = ViewType.MONTH;
                            sessionStorage.setItem("selectedViewType",viewType);
                            removeEverything();
                            loadEverything();
                        }

                        const origin = {
                            left: e.pageX,
                            top: e.pageY
                        };
                        setPosition(origin);

                    }
                    else if (e.target.className == "Day" || e.target.className == "dayTitle"){
                        removeRightClickMenu();
                        var element = document.getElementById("calendarMenuOptions");
                        var showMonth = document.createElement("li");
                        showMonth.className = "calendarMenuOption"
                        showMonth.id = "ShowAppointment";
                        showMonth.appendChild(document.createTextNode("Show Month"));
                        element.appendChild(showMonth);

                        var showDay = document.createElement("li");
                        showDay.className = "calendarMenuOption"
                        showDay.id = "ShowDay";
                        showDay.appendChild(document.createTextNode("Show Day"));
                        element.appendChild(showDay);



                        showMonth.onclick = function(){
                            var monthTitle = findUpTag(e.target,"Month");
                            selectedMonth = parseInt(monthTitle.id)+1;
                            sessionStorage.setItem("selectedMonth",selectedMonth);
                            viewType = ViewType.MONTH;
                            sessionStorage.setItem("selectedViewType",viewType);
                            removeEverything();
                            loadEverything();
                        }

                        showDay.onclick = function(){
                            var monthTitle = findUpTag(e.target,"Month");
                            selectedMonth = parseInt(monthTitle.id)+1;
                            sessionStorage.setItem("selectedMonth",selectedMonth);

                            var dayTitle = findUpTag(e.target,"Day");
                            selectedDay = parseInt(dayTitle.id)+1;
                            sessionStorage.setItem("selectedDay",selectedDay);
                            viewType = ViewType.DAY;
                            sessionStorage.setItem("selectedViewType",viewType);

                            removeEverything();
                            loadEverything();
                        }


                        const origin = {
                            left: e.pageX,
                            top: e.pageY
                        };
                        setPosition(origin);
                    }
                    else{
                        console.log(e.target.className);
                    }
                }
                else if (viewType == ViewType.MONTH){
                    if ( e.target.className == "appointmentBox" || e.target.className == "appointmentTitle" || e.target.className == "appointmentTimeTitle" || e.target.className == "appointmentTime") {
                        removeRightClickMenu();
                        var element = document.getElementById("calendarMenuOptions");

                        var showAppointment = document.createElement("li");
                        showAppointment.className = "calendarMenuOption"
                        showAppointment.id = "ShowAppointment";
                        showAppointment.appendChild(document.createTextNode("Show Appointment"));
                        element.appendChild(showAppointment);
                        var showClient = document.createElement("li");
                        showClient.className = "calendarMenuOption"
                        showClient.id = "ShowClient";
                        showClient.appendChild(document.createTextNode("Show Client"));
                        element.appendChild(showClient);
                        var copyAppointment = document.createElement("li");
                        copyAppointment.className = "calendarMenuOption"
                        copyAppointment.id = "CopyAppointment";
                        copyAppointment.appendChild(document.createTextNode("Copy"));
                        element.appendChild(copyAppointment);


                        var hr = document.createElement("hr");
                        element.appendChild(hr);
                        var showDay = document.createElement("li");
                        showDay.className = "calendarMenuOption"
                        showDay.id = "ShowDay";
                        showDay.appendChild(document.createTextNode("Show Day"));
                        element.appendChild(showDay);



                        showAppointment.onclick = function(){
                            var appointmentBox = findUpTag(e.target,"appointmentBox");

                            var appointmentWebID = appointmentBox.id.split("-")[0];
                            window.location.href = "/appointsync.com/public/appointment.php/"+appointmentWebID;
                        };

                        copyAppointment.onclick = function(){
                            var appointmentBox = findUpTag(e.target,"appointmentBox");

                            var appointmentWebID = appointmentBox.id.split("-")[0];

                            var appointment = calendar.appointments.filter(a => a.webID == appointmentWebID)[0];

                            var clientWebID = appointment.client.webID;

                            var location = calendar.locations.filter(l => l.locationID == appointment.locationID)[0];


                            document.getElementById("selectClient").value = clientWebID;
                            document.getElementsByClassName("dropdown-selected")[0].innerHTML = appointment.client.identifier;
                            // document.getElementsByClassName("dropdown-selected")[selected].className = 'dropdown-option dropdown-chose';
                            document.getElementsByClassName("dropdown-option dropdown-chose")[0].className = 'dropdown-option';
                            // alert(document.getElements);

                            console.log(location);

                            document.getElementById("selectLocation").value = location.locationWebID;
                            document.getElementById("locationSelection").innerHTML = document.getElementById("option"+location.locationWebID).innerHTML;

                            //open add Appointment View
                            var selectedStartDate = new Date(0);
                            selectedStartDate.setUTCSeconds(appointment.startDate);
                            var selectedDate = moment(selectedStartDate);
                            var selectedDateFormatFrom = selectedDate.format('LLLL');
                            $('#dateTimeFrom').val(selectedDateFormatFrom);
                            var startDate = moment(selectedDateFormatFrom).unix();
                            $('#dateTimeFromUTC').val(startDate);

                            var selectedEndDate = new Date(0);
                            selectedEndDate.setUTCSeconds(appointment.endDate);
                            var selectedToDate = moment(selectedEndDate);
                            var selectedDateFormatTo = selectedToDate.format('LLLL');
                            $('#dateTimeTo').val(selectedDateFormatTo);
                            var endDate = moment(selectedDateFormatTo).unix();
                            $('#dateTimeToUTC').val(endDate);


                            $('#datetimepicker6').datetimepicker({
                                date: selectedDate,
                                minDate: selectedDate,
                                ignoreReadonly: true,
                                format: ('LLLL'),

                            });
                            $('#datetimepicker7').datetimepicker({
                                useCurrent: false, //Important! See issue #1075
                                ignoreReadonly: true,
                                date: selectedToDate,
                                minDate: selectedToDate,
                                format: ('LLLL'),
                            });
                            $("#datetimepicker6").on("dp.change", function (e) {
                                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
                                var selectedDateFrom = document.getElementById("dateTimeFrom").value;
                                var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
                                // console.log(selectedDateFormatFrom);
                                var startDate = moment(selectedDateFormatFrom).unix();

                                $('#dateTimeFromUTC').val(startDate);

                            });
                            $("#datetimepicker7").on("dp.change", function (e) {
                                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);

                                var selectedDateFrom = document.getElementById("dateTimeFrom").value;
                                var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
                                var startDate = moment(selectedDateFormatFrom).unix();

                                $('#dateTimeFromUTC').val(startDate);
                                var selectedDateTo = document.getElementById("dateTimeTo").value;
                                var selectedDateFormatTo = moment(selectedDateTo).format('LLLL');
                                var startDate = moment(selectedDateFormatTo).unix();

                                $('#dateTimeToUTC').val(startDate);

                            });

                            modal.style.display = "block";
                            $('body').css('overflow','hidden');








                        }

                        showClient.onclick = function(){
                            var appointmentBox = findUpTag(e.target,"appointmentBox");

                            var appointmentWebID = appointmentBox.id.split("-")[0];

                            var appointment = calendar.appointments.filter(a => a.webID == appointmentWebID)[0];

                            var clientWebID = appointment.client.webID;
                            window.location.href = "/appointsync.com/public/clientprofile.php/"+clientWebID;
                        }
                        showDay.onclick = function(){
                            var dayTitle = findUpTag(e.target,"DayDay");
                            selectedDay = dayTitle.id;
                            sessionStorage.setItem("selectedDay",selectedDay);
                            viewType = ViewType.DAY;
                            sessionStorage.setItem("selectedViewType",viewType);
                            removeEverything();
                            loadEverything();
                        }

                        const origin = {
                            left: e.pageX,
                            top: e.pageY
                        };
                        setPosition(origin);


                    }
                    else if (e.target.className == "dayTitle" || e.target.className == "dayNumber" || e.target.className == "appointmentsBox" || e.target.className == "DayDay"){
                        removeRightClickMenu();
                        var element = document.getElementById("calendarMenuOptions");

                        var showDay = document.createElement("li");
                        showDay.className = "calendarMenuOption"
                        showDay.id = "ShowDay";
                        showDay.appendChild(document.createTextNode("Show Day"));
                        element.appendChild(showDay);

                        showDay.onclick = function(){
                            var dayTitle = findUpTag(e.target,"DayDay");
                            selectedDay = dayTitle.id;
                            sessionStorage.setItem("selectedDay",selectedDay);
                            viewType = ViewType.DAY;
                            sessionStorage.setItem("selectedViewType",viewType);
                            removeEverything();
                            loadEverything();
                        }


                        const origin = {
                            left: e.pageX,
                            top: e.pageY
                        };
                        setPosition(origin);

                    }
                    else{
                        console.log(e.target);
                    }
                }
                else if (viewType == ViewType.DAY){
                    if (e.target.className == "appointmentTitleValue" || e.target.className == "dayAppointmentTitle"){
                        removeRightClickMenu();
                        var element = document.getElementById("calendarMenuOptions");

                        var showAppointment = document.createElement("li");
                        showAppointment.className = "calendarMenuOption"
                        showAppointment.id = "ShowAppointment";
                        showAppointment.appendChild(document.createTextNode("Show Appointment"));
                        element.appendChild(showAppointment);

                        var showClient = document.createElement("li");
                        showClient.className = "calendarMenuOption"
                        showClient.id = "ShowClient";
                        showClient.appendChild(document.createTextNode("Show Client"));
                        element.appendChild(showClient);


                        showAppointment.onclick = function(){
                            var dayAppointmentTitle = findUpTag(e.target,"dayAppointmentTitle");
                            var appointmentWebID = dayAppointmentTitle.id;
                            window.location.href = "/appointsync.com/public/appointment.php/"+appointmentWebID;
                        }
                        showClient.onclick = function(){
                            var dayAppointmentTitle = findUpTag(e.target,"dayAppointmentTitle");

                            var appointmentWebID = dayAppointmentTitle.id;

                            var appointment = calendar.appointments.filter(a => a.webID == appointmentWebID)[0];
                            var clientWebID = appointment.client.webID;
                            window.location.href = "/appointsync.com/public/clientprofile.php/"+clientWebID;
                        }

                        const origin = {
                            left: e.pageX,
                            top: e.pageY
                        };
                        setPosition(origin);

                    }
                    else if (e.target.className == "dayAppointments" || e.target.className == "appointmentViewOdd" || e.target.className == "appointmentViewEven"){

                        var y = e.layerY%30;
                        var selectedMinutes = y*60/30;
                        var selectedHour = (e.layerY - y)/30;
                        var formattedHour = ("0" + selectedHour).slice(-2);
                        var formattedMinute = ("0" + selectedMinutes).slice(-2);
                        removeRightClickMenu();
                        var element = document.getElementById("calendarMenuOptions");

                        var addAppointment = document.createElement("li");
                        addAppointment.className = "calendarMenuOption"
                        addAppointment.id = "addAppointment";
                        addAppointment.appendChild(document.createTextNode("Add Appointment"));
                        element.appendChild(addAppointment);

                        addAppointment.onclick = function(){
                            var y = e.layerY%30;
                            var selectedMinutes = y*60/30;
                            var selectedHour = (e.layerY - y)/30;
                            var formattedHour = ("0" + selectedHour).slice(-2);
                            var formattedMinute = ("0" + selectedMinutes).slice(-2);

                            var selectedDate = moment(self.selectedYear+"-"+("0" + selectedMonth).slice(-2)+"-"+("0" + selectedDay).slice(-2)+" "+formattedHour+":"+formattedMinute);
                            var selectedDateFormatFrom = selectedDate.format('LLLL');
                            $('#dateTimeFrom').val(selectedDateFormatFrom);
                            var startDate = moment(selectedDateFormatFrom).unix();
                            $('#dateTimeFromUTC').val(startDate);

                            var selectedDateTo = moment(self.selectedYear+"-"+("0" + selectedMonth).slice(-2)+"-"+("0" + selectedDay).slice(-2)+" "+formattedHour+":"+formattedMinute);
                            selectedDateTo.add(30,"minutes");
                            var selectedDateFormatTo = selectedDateTo.format('LLLL');
                            $('#dateTimeTo').val(selectedDateFormatTo);
                            var endDate = moment(selectedDateFormatTo).unix();
                            $('#dateTimeToUTC').val(endDate);

                            $('#datetimepicker6').datetimepicker({
                                minDate: selectedDate,
                                ignoreReadonly: true,
                                format: ('LLLL'),
                            });
                            $('#datetimepicker7').datetimepicker({
                                useCurrent: false, //Important! See issue #1075
                                ignoreReadonly: true,
                                date: selectedDateTo,
                                minDate: selectedDateTo,
                                format: ('LLLL'),
                            });
                            $("#datetimepicker6").on("dp.change", function (e) {
                                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
                                var selectedDateFrom = document.getElementById("dateTimeFrom").value;
                                var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
                                // console.log(selectedDateFormatFrom);
                                var startDate = moment(selectedDateFormatFrom).unix();

                                $('#dateTimeFromUTC').val(startDate);

                            });
                            $("#datetimepicker7").on("dp.change", function (e) {
                                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);

                                var selectedDateFrom = document.getElementById("dateTimeFrom").value;
                                var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
                                // console.log(selectedDateFormatFrom);
                                var startDate = moment(selectedDateFormatFrom).unix();

                                $('#dateTimeFromUTC').val(startDate);
                                var selectedDateTo = document.getElementById("dateTimeTo").value;
                                var selectedDateFormatTo = moment(selectedDateTo).format('LLLL');
                                // console.log(selectedDateFormatTo);
                                var startDate = moment(selectedDateFormatTo).unix();

                                $('#dateTimeToUTC').val(startDate);

                            });

                            modal.style.display = "block";
                            $('body').css('overflow','hidden');







                        }


                        const origin = {
                            left: e.pageX,
                            top: e.pageY
                        };
                        setPosition(origin);
                    }
                    else{
                        console.log(e);
                    }
                }
                else{
                    console.log(e.target.className);
                }
                e.preventDefault();
            }, false);

            document.addEventListener("click", e => {
                toggleMenu("hide");
            });



        }

        const ViewType = {
            YEAR: 0,
            MONTH: 1,
            DAY: 2
        };

        function daysInMonth (month, year) {
            return new Date(year, month, 0).getDate();
        }

        if (sessionStorage.getItem("selectedViewType") == null){
            var viewType = ViewType.MONTH;
            sessionStorage.setItem("selectedViewType",ViewType.MONTH);
        }
        else{
            var viewType = sessionStorage.getItem("selectedViewType")
        }

        if (sessionStorage.getItem("selectedYear") == null){
            sessionStorage.setItem("selectedYear",moment().year());
            var selectedYear = moment().year();
        }
        else{
            var selectedYear = parseInt(sessionStorage.getItem("selectedYear"));
        }

        if (sessionStorage.getItem("selectedMonth") == null){
            sessionStorage.setItem("selectedMonth",moment().add(1,'M').month());
            var selectedMonth = moment().add(1,'M').month();
        }
        else{
            var selectedMonth = parseInt(sessionStorage.getItem("selectedMonth"));
        }
        if (sessionStorage.getItem("selectedDay") == null){
            sessionStorage.setItem("selectedDay",moment().date());
            var selectedDay = moment().date();
        }
        else{
            var selectedDay = parseInt(sessionStorage.getItem("selectedDay"));
        }

        var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        var days = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"];
        loadEverything();

        function loadEverything(){



            if (viewType == ViewType.YEAR){

                var title = document.createElement("div");
                title.className = "pageTitle"
                title.id = "pageTitle";
                var element = document.getElementById("Calendar");
                element.appendChild(title);

                var title = document.createElement("p");
                title.className = "pageTitleValue";
                title.id = "pageTitleValue";
                var titleValue = document.createTextNode(""+selectedYear);
                title.appendChild(titleValue);
                element.appendChild(title);

                var monthsDiv = document.createElement("div");
                monthsDiv.className = "Months"
                monthsDiv.id = "Months";
                var element = document.getElementById("Calendar");

                for (i=0; i<12;i++){
                    var month = document.createElement("div");
                    month.className = "Month"
                    month.id = i;
                    var element = document.getElementById("Calendar");


                    var dayName = document.createElement("div");
                    dayName.className = "DayName"
                    dayName.id = months[i] + "DayName";

                    var title = document.createElement("p");
                    title.className = "monthTitle";
                    title.id = "monthTitle";
                    var value = document.createTextNode(months[i]);
                    title.appendChild(value);
                    month.appendChild(title);

                    var days = document.createElement("div");
                    days.className = "Days"
                    days.id = months[i] + "Days";
                    var firstDayOfMonth = new Date(this.selectedYear, i, 0).getDay();
                    for (j=0;j<daysInMonth(i+1,selectedYear)+firstDayOfMonth;j++){

                        if (j < firstDayOfMonth
                        ){
                            var day = document.createElement("div");
                            day.className = "YearEmptyDay";
                            day.id = "YearEmptyDay";
                            days.appendChild(day);
                        }
                        else{
                            var day = document.createElement("div");
                            day.className = "Day";
                            day.id = j-firstDayOfMonth;
                            var todaysDate = new Date();
                            todaysDate.setHours(0,0,0,0);
                            if (new Date(selectedYear,i,j-firstDayOfMonth+1).toDateString() == todaysDate.toDateString()){
                                day.style.background = "rgba(100,100,100,0.5)";
                            }

                            var title = document.createElement("p");
                            title.className = "dayTitle";
                            title.id = "dayTitle";
                            var value = document.createTextNode(j-firstDayOfMonth+1);
                            title.appendChild(value);
                            day.appendChild(title);
                            days.appendChild(day);
                        }
                    }
                    month.appendChild(days);
                    month.addEventListener("click", yearClicked, true);
                    monthsDiv.appendChild(month);
                }
                element.appendChild(monthsDiv);


            }
            else if (viewType == ViewType.MONTH){


                var title = document.createElement("div");
                title.className = "pageTitle"
                title.id = "pageTitle";
                var element = document.getElementById("Calendar");
                element.appendChild(title);

                var title = document.createElement("p");
                title.id = "pageTitleValue";
                var titleValue = document.createTextNode(this.selectedYear+ " " +self.months[selectedMonth-1]  );
                title.appendChild(titleValue);
                element.appendChild(title);


                var daysTab = document.createElement("div");
                daysTab.className = "DaysTab";
                daysTab.id ="DaysTab";
                element.appendChild(daysTab);

                for (i=0;i<7;i++){
                    var dayTitle = document.createElement("div");
                    dayTitle.className = "BigDayTitle";
                    dayTitle.id = "BigDayTitle"+i;

                    var title = document.createElement("p");
                    var value = document.createTextNode(self.days[i]);
                    title.appendChild(value);
                    dayTitle.appendChild(title);


                    daysTab.appendChild(dayTitle);
                }
                var firstDayOfMonth = new Date(this.selectedYear, this.selectedMonth-1, 0).getDay();
                for (j=0;j<daysInMonth(this.selectedMonth,selectedYear)+firstDayOfMonth;j++){



                    if (j < firstDayOfMonth
                    ){
                        var day = document.createElement("div");
                        day.className = "EmptyDay";
                        day.id = "EmptyDay";
                        element.appendChild(day);
                    }
                    else{
                        var day = document.createElement("div");
                        day.className = "DayDay";
                        day.id = (j-firstDayOfMonth+1);
                        day.addEventListener("drop",onDrop);
                        day.addEventListener("dragover", onDragOver);


                        var dayNumberBox = document.createElement("div");
                        dayNumberBox.className = "dayNumber";
                        dayNumberBox.id = (j-firstDayOfMonth+1);
                        var dayNumber = document.createElement("p");
                        dayNumber.className = "dayTitle";
                        dayNumber.id = (j-firstDayOfMonth+1);
                        var dayNumberValue = document.createTextNode(j-firstDayOfMonth+1);

                        dayNumber.appendChild(dayNumberValue);
                        dayNumberBox.appendChild(dayNumber);
                        day.appendChild(dayNumberBox);


                        var dayAppointments = self.calendar.getAppointmentsForDay(j-firstDayOfMonth+1,self.selectedMonth,self.selectedYear);

                        var appointmentsBox = document.createElement("div");
                        appointmentsBox.className = "appointmentsBox";
                        appointmentsBox.id = "appointmentsBox";
                        day.appendChild(appointmentsBox);
                        for (k=0;k<dayAppointments.length;k++){
                            var appBox = document.createElement("div");
                            appBox.className = "appointmentBox";

                            appBox.id = dayAppointments[k].webID+"-"+k;
                            appBox.draggable = false;
                            appBox.addEventListener("dragstart", onDrag);



                            var appointmentTimeDiv = document.createElement("div");
                            appointmentTimeDiv.className = "appointmentTime";
                            appointmentTimeDiv.id = dayAppointments[k].webID;


                            if (dayAppointments[k].amendedAppointment != null && dayAppointments[k].amendedAppointment.amendingUserID == userID){
                                var startDate = new Date(0);
                                startDate.setUTCSeconds(dayAppointments[k].amendedAppointment.startDate);
                            }
                            else{
                                var startDate = new Date(0);
                                startDate.setUTCSeconds(dayAppointments[k].startDate);
                            }
                            switch (dayAppointments[k].status){
                                case 1:
                                    appBox.style.background = this.requestedColor;
                                    break;
                                case 2:
                                    appBox.style.background = this.pendingColor;
                                    break;
                                case 3:
                                    appBox.style.background = this.acceptedColor;
                                    break;
                                case 4:
                                    appBox.style.background = this.rejectedColor;
                                    break;
                                case 5:
                                    appBox.style.background = this.rejectedColor;
                                    break;
                                default:
                                    break;
                            }
                            if (startDate < new Date()){
                                appBox.style.background = this.passedColor;
                            }

                            var appointmentTime = (dayAppointments[k].startDate)/60;
                            var appointmentFormattedHour = ("0" + startDate.getHours()).slice(-2);
                            var appointmentFormattedMinute = ("0" + startDate.getMinutes()).slice(-2);



                            var appointmentTimeTitle = document.createElement("p");
                            appointmentTimeTitle.className = "appointmentTimeTitle";
                            appointmentTimeTitle.id = "appointmentTimeTitle";
                            var appointmentTimeTitleValue = document.createTextNode(appointmentFormattedHour+":"+appointmentFormattedMinute);
                            appointmentTimeTitle.appendChild(appointmentTimeTitleValue);
                            appointmentTimeDiv.appendChild(appointmentTimeTitle);

                            var appointmentTitle = document.createElement("p");
                            appointmentTitle.className = "appointmentTitle";
                            appointmentTitle.id = dayAppointments[k].webID;
                            var appointmentTitleValue = document.createTextNode(dayAppointments[k].client.username != ""?dayAppointments[k].client.username:dayAppointments[k].client.firstName );
                            appointmentTitle.appendChild(appointmentTitleValue);


                            appBox.appendChild(appointmentTimeDiv);
                            appBox.appendChild(appointmentTitle);
                            appointmentsBox.appendChild(appBox);
                        }
                        element.appendChild(day);
                        day.addEventListener("click", monthClicked, true);
                    }


                }
            }
            else if (viewType == ViewType.DAY){

                var title = document.createElement("div");
                title.className = "pageTitle"
                title.id = "pageTitle";
                var element = document.getElementById("Calendar");
                element.appendChild(title);

                var title = document.createElement("p");
                title.className = "pageTitleValue";
                title.id = "pageTitleValue";
                var titleValue = document.createTextNode(self.selectedYear+' '+self.months[selectedMonth-1]+' '+self.selectedDay);
                title.appendChild(titleValue);
                element.appendChild(title);

                var dayView = document.createElement("div");
                dayView.className = "dayView"
                dayView.id = "dayView";
                element.appendChild(dayView);


                var dayAppointments = self.calendar.getAppointmentsForDay(self.selectedDay,self.selectedMonth,self.selectedYear);

                var dayAppointmentsDiv = document.createElement("div");
                dayAppointmentsDiv.className = "dayAppointments"
                dayAppointmentsDiv.id = "dayAppointments";


                var dayBackLines = document.createElement("div");
                dayBackLines.className = "dayBackLines"
                dayBackLines.id = "dayBackLines";





                //creating times on the left
                for (i=0;i<24;i++){

                    var timeDiv = document.createElement("div");
                    timeDiv.className = "timeDiv";
                    var date = new Date(0,0,0,i,0,0);
                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0'+minutes : minutes;
                    var t = hours + ':' + minutes + ' ' + ampm;


                    timeDiv.id = t;

                    var timeTitle = document.createElement("div");
                    timeTitle.className = "timeTitle"
                    timeTitle.id = "timeTitle";

                    var title = document.createElement("p");
                    title.className = "timeTitleValue";
                    title.id = "timeTitleValue";
                    var titleValue = document.createTextNode(t);
                    timeTitle.appendChild(titleValue);

                    timeDiv.appendChild(timeTitle);
                    dayView.appendChild(timeDiv);
                }



                dayView.appendChild(dayBackLines);
                //creating striped views
                for (i=0;i<24;i++){
                    var appointmentView = document.createElement("div");
                    appointmentView.id = "appointmentView";

                    if (i%2 == 0){
                        appointmentView.className = "appointmentView"+"Even";
                    }
                    else{
                        appointmentView.className = "appointmentView"+"Odd";
                    }

                    dayBackLines.appendChild(appointmentView);


                }



                //creating appointments
                for (i=0;i<24;i++){
                    var hours = parseInt(i);
                    var formattedHour = ("0" + hours).slice(-2);
                    var minutes = ((i)-hours)*60;
                    var formattedMinute = ("0" + minutes).slice(-2);
                    var t = formattedHour+":"+formattedMinute;
                    var j=0;
                    for (j=0;j<dayAppointments.length;j++){

                        if (dayAppointments[j].amendedAppointment != null && dayAppointments[j].amendedAppointment.amendingUserID == userID){
                            var startDate = new Date(0);
                            startDate.setUTCSeconds(dayAppointments[j].amendedAppointment.startDate);
                            var endDate = new Date(0);
                            endDate.setUTCSeconds(dayAppointments[j].amendedAppointment.endDate);
                        }
                        else{

                            var startDate = new Date(0);
                            startDate.setUTCSeconds(dayAppointments[j].startDate);

                            var endDate = new Date(0);
                            endDate.setUTCSeconds(dayAppointments[j].endDate);

                        }

                        if (endDate.getDate() > startDate.getDate() || endDate.getMonth() > startDate.getMonth() || endDate.getFullYear() > startDate.getFullYear()){
                            var thisDay = new Date(this.selectedYear,this.selectedMonth-1,this.selectedDay);
                            if (startDate.getDate() == this.selectedDay){
                                endDate.setDate(startDate.getDate());
                                endDate.setMonth(startDate.getMonth());
                                endDate.setFullYear(startDate.getFullYear());
                                endDate.setHours(24);
                                endDate.setMinutes(0);
                            }
                            else if (endDate.getDate() == this.selectedDay){
                                startDate.setDate(endDate.getDate());
                                startDate.setMonth(endDate.getMonth());
                                startDate.setFullYear(endDate.getFullYear());
                                startDate.setHours(0);
                                startDate.setMinutes(0);
                            }
                            else if (startDate < thisDay && endDate > thisDay){
                                endDate.setDate(thisDay.getDate());
                                endDate.setMonth(thisDay.getMonth());
                                endDate.setFullYear(thisDay.getFullYear());
                                endDate.setHours(24);
                                endDate.setMinutes(0);

                                startDate.setDate(thisDay.getDate());
                                startDate.setMonth(thisDay.getMonth());
                                startDate.setFullYear(thisDay.getFullYear());
                                startDate.setHours(0);
                                startDate.setMinutes(0);
                            }

                        }
                        var appointmentTime = ( endDate.getTime() - startDate.getTime())/60000;


                        var appointmentFormattedHour = ("0" + startDate.getHours()).slice(-2);

                        if (appointmentFormattedHour+":00" == t){
                            var paddingFromTop = parseInt(appointmentFormattedHour) * 30;

                            var numberOfConflictingAppointments = 1;
                            for (var h=0;h<dayAppointments.length;h++){
                                if (appointmentsConflict(dayAppointments[j],dayAppointments[h])){
                                    numberOfConflictingAppointments += 1;
                                }
                            }
                            var appointmentsWidth = (100/numberOfConflictingAppointments);

                            var appointmentLeftMargin = 0;
                            for (var h=0;h<dayAppointments.length;h++){
                                if (dayAppointments[h].appointmentID == dayAppointments[j].appointmentID){
                                    break;
                                }
                                if (appointmentsConflict(dayAppointments[j],dayAppointments[h])){
                                    appointmentLeftMargin += appointmentsWidth;
                                }
                            }


                            var padding = startDate.getMinutes()*30/60;
                            var height = (appointmentTime)*30/60;
                            var appointmentTitle = document.createElement("div");
                            appointmentTitle.className = "dayAppointmentTitle"
                            appointmentTitle.id = dayAppointments[j].webID;
                            appointmentTitle.style.width = appointmentsWidth+"%";
                            appointmentTitle.style.marginTop = paddingFromTop+padding+"px";
                            appointmentTitle.style.marginLeft = appointmentLeftMargin+"%";
                            appointmentTitle.style.height = height+"px";

                            switch (dayAppointments[j].status){
                                case 1:
                                    appointmentTitle.style.background = this.requestedColor;
                                    break;
                                case 2:
                                    appointmentTitle.style.background = this.pendingColor;
                                    break;
                                case 3:
                                    appointmentTitle.style.background = this.acceptedColor;
                                    break;
                                case 4:
                                    appointmentTitle.style.background = this.rejectedColor;
                                    break;
                                case 5:
                                    appointmentTitle.style.background = this.rejectedColor;
                                    break;
                                default:
                                    break;
                            }
                            if (startDate < new Date()){
                                appointmentTitle.style.background = this.passedColor;
                            }
                            var title = document.createElement("p");
                            title.id = "appointmentTitleValue";
                            title.className = "appointmentTitleValue";


                            if (dayAppointments[j].client.type != 4){

                                var titleValue = document.createTextNode(dayAppointments[j].client.username);
                            }
                            else{
                                var titleValue = document.createTextNode(dayAppointments[j].client.firstName+" "+dayAppointments[j].client.lastName);
                            }
                            title.style.height = "17px";
                            title.style.marginTop = ((height/2)-(17/2))+"px";
                            title.appendChild(titleValue);
                            appointmentTitle.appendChild(title);
                            dayAppointmentsDiv.appendChild(appointmentTitle);
                            appointmentTitle.addEventListener("click", dayClicked, true);
                        }
                    }


                }
                dayView.appendChild(dayAppointmentsDiv);




            }
        }
        function onDrag(event){
            event.dataTransfer.setData("text", event.target.id);
        }
        function onDrop(event){
            console.log(event.target.className);
//            if (event.target.className === "DayDay"){
            event.preventDefault();
            var data = event.dataTransfer.getData("text");
            event.target.appendChild(document.getElementById(data));
//            }
        }
        function onDragOver(event){

            event.preventDefault();
        }


        function yearClicked(element){
            if (element.target.className == "Month"){
                selectedMonth = parseInt(element.target.id)+1;
                viewType = ViewType.MONTH;
                sessionStorage.setItem("selectedViewType",viewType); sessionStorage.setItem("selectedMonth",selectedMonth);

            }else if (element.target.className == "dayTitle"){
                selectedDay = parseInt(element.target.parentElement.id)+1;
                selectedMonth = parseInt(element.target.parentElement.parentElement.parentElement.id)+1;
                viewType = ViewType.DAY;
                sessionStorage.setItem("selectedViewType",viewType);
                sessionStorage.setItem("selectedDay",selectedDay);
                sessionStorage.setItem("selectedMonth",selectedMonth);

            }else if (element.target.className == "Day"){

                selectedDay = parseInt(element.target.id)+1;
                selectedMonth = parseInt(element.target.parentElement.parentElement.id)+1;
                viewType = ViewType.DAY;
                sessionStorage.setItem("selectedViewType",viewType);
                sessionStorage.setItem("selectedDay",selectedDay);
                sessionStorage.setItem("selectedMonth",selectedMonth);


            }else{
                if (parseInt(element.target.parentElement.id) != null){
                    selectedMonth = parseInt(element.target.parentElement.id)+1;
                }
                else{
                    selectedMonth = parseInt(element.target.parentElement.parentElement.id)+1;
                }
                sessionStorage.setItem("selectedMonth",selectedMonth);
                viewType = ViewType.MONTH;

            }
            removeEverything();
            loadEverything();
        }

        function monthClicked(element){
            if (element.target.className == "appointmentBox" || element.target.className == "appointmentTitle" || element.target.className == "appointmentDot" || element.target.className == "appointmentTimeTitle" || element.target.className == "appointmentTime"){
                var appointmentWebID = findUpTag(element.target,"appointmentBox").id.split("-")[0];;
                window.location.href = "/appointsync.com/public/appointment.php/"+appointmentWebID;
            }
            else if (element.target.className == "dayTitle"){
                selectedDay = parseInt(element.target.id);
                viewType = ViewType.DAY;
                sessionStorage.setItem("selectedViewType",viewType);
                sessionStorage.setItem("selectedDay",selectedDay);
                removeEverything();
                loadEverything();
            }
            else {

                selectedDay = parseInt(element.target.parentElement.id);
                viewType = ViewType.DAY;
                sessionStorage.setItem("selectedViewType",viewType);
                sessionStorage.setItem("selectedDay",selectedDay);
                removeEverything();
                loadEverything();
            }
        }

        function dayClicked(element){
            if (element.target.className == "dayAppointmentTitle" || element.target.className == "appointmentTitleValue"){
                var appointmentWebID = findUpTag(element.target,"dayAppointmentTitle").id;
                window.location.href = "/appointsync.com/public/appointment.php/"+appointmentWebID;
            }
        }

        function previousButtonPressed(){

            if (viewType == ViewType.YEAR) {
                selectedYear = selectedYear-1;
                sessionStorage.setItem("selectedYear",selectedYear);
            }
            else if (viewType == ViewType.MONTH){
                if (selectedMonth > 1){
                    selectedMonth = selectedMonth-1;
                    sessionStorage.setItem("selectedMonth",selectedMonth);
                }
                else{
                    selectedMonth = 12;
                    sessionStorage.setItem("selectedMonth",selectedMonth);
                    selectedYear = selectedYear-1;
                    sessionStorage.setItem("selectedYear",selectedYear);
                }
            }
            else{
                if (selectedDay > 1){
                    selectedDay = selectedDay-1;
                    sessionStorage.setItem("selectedDay",selectedDay);

                }
                else{
                    if (selectedMonth != 1){
                        selectedMonth = selectedMonth-1;
                        sessionStorage.setItem("selectedMonth",selectedMonth);
                    }
                    else{
                        selectedYear = selectedYear-1;
                        sessionStorage.setItem("selectedYear",selectedYear);
                        selectedMonth = 12;
                        sessionStorage.setItem("selectedMonth",selectedMonth);
                    }

                    selectedDay = daysInMonth(selectedMonth,selectedYear);
                    sessionStorage.setItem("selectedDay",selectedDay);

                }
            }
            removeEverything();
            loadEverything();
        }
        function todayButtonPressed(){

            if (viewType == ViewType.YEAR) {
                selectedYear = (new Date()).getFullYear();
                sessionStorage.setItem("selectedYear",selectedYear);
            }
            else if (viewType == ViewType.MONTH){
                selectedYear = (new Date()).getFullYear();
                sessionStorage.setItem("selectedYear",selectedYear);
                selectedMonth = (new Date()).getMonth()+1;
                sessionStorage.setItem("selectedMonth",selectedMonth);
            }
            else{
                selectedYear = (new Date()).getFullYear();
                sessionStorage.setItem("selectedYear",selectedYear);
                selectedMonth = (new Date()).getMonth()+1;
                sessionStorage.setItem("selectedMonth",selectedMonth);
                selectedDay = (new Date()).getDate();
                sessionStorage.setItem("selectedDay",selectedDay);
            }
            removeEverything();
            loadEverything();
        }
        function yearViewButtonPressed(){

            viewType = ViewType.YEAR;
            sessionStorage.setItem("selectedViewType",viewType);
            removeEverything();
            loadEverything();
        }

        function monthViewButtonPressed(){

            viewType = ViewType.MONTH;
            sessionStorage.setItem("selectedViewType",viewType);
            removeEverything();
            loadEverything();
        }
        function dayViewButtonPressed(){

            viewType = ViewType.DAY;
            sessionStorage.setItem("selectedViewType",viewType);
            removeEverything();
            loadEverything();
        }

        function nextButtonPressed(){
            if (viewType == ViewType.YEAR) {
                selectedYear = selectedYear+1;
                sessionStorage.setItem("selectedYear",selectedYear);
                removeEverything();
                loadEverything();
            }
            else if (viewType == ViewType.MONTH){
                if (selectedMonth < 12){
                    selectedMonth = selectedMonth+1;
                    sessionStorage.setItem("selectedMonth",selectedMonth);
                    removeEverything();

                }
                else{
                    selectedMonth = 1;
                    sessionStorage.setItem("selectedMonth",selectedMonth);
                    selectedYear = selectedYear+1;
                    sessionStorage.setItem("selectedYear",selectedYear);
                }
                removeEverything();
                loadEverything();
            }
            else{
                if (selectedDay < daysInMonth(selectedMonth,selectedYear)){
                    selectedDay = selectedDay+1;
                    sessionStorage.setItem("selectedDay",selectedDay);

                }
                else{
                    if (selectedMonth != 12){
                        selectedMonth = selectedMonth+1;
                        sessionStorage.setItem("selectedMonth",selectedMonth);
                    }
                    else{
                        selectedYear = selectedYear+1;
                        sessionStorage.setItem("selectedYear",selectedYear);
                        selectedMonth = 1;
                        sessionStorage.setItem("selectedMonth",selectedMonth);
                    }
                    selectedDay = 1;
                    sessionStorage.setItem("selectedDay",selectedDay);

                }

                removeEverything();
                loadEverything();
            }

        }

        function removeEverything(){
            var myNode = document.getElementById("Calendar");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
        }

        function removeRightClickMenu(){
            var myNode = document.getElementById("calendarMenuOptions");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
        }

    </script>



    <!--    End Calendar-->



</section>

<!-- Adding new appointment Box -->


<div id="myModal" class="modal">


    <div class="modal-content">
        <span class="close spanCloseAppointment">&times;</span>
        <div class="modal-header">
            <h2>Add new appointment</h2>
        </div>
        <div class="modal-body">

            <!--            <div class="select-box">-->
            <!--                <label for="selectClient" class="label select-box1"><span-->
            <!--                            class="label-desc" id="clientSelection">Choose Client</span> </label>-->
            <!--                <select-->
            <!--                        id="selectClient" class="select" name="clientSelect">-->
            <!--                    <option value="0" selected>Choose Client</option>-->
            <!--                    --><?php
            //                    if ($host->type == 2) {
            //                        foreach ($clients as $client) {
            //                            echo '<option id="option'.$client->webID.'" value ="' . $client->webID . '">' . $client->identifier . '</option>';
            //                        }
            //
            //                    } elseif ($host->type == 3) {
            //
            //                    }
            //                    ?>
            <!--                </select>-->
            <!--            </div>-->

            <div class="row">
                <div class="col-sm-4" style="width:51%;float:none; margin: 0 auto;">
                    <br/><p>Select Client</p>
                    <div class="dropdown-sin-1">
                        <select  id="selectClient" style="display:none"  name="clientSelect" placeholder="Select">
                            <?php
                            if ($host->type == 2 ) {
                                foreach ($clients as $client) {
                                    echo '<option id="option' . $client->webID . '" value ="' . $client->webID . '">' . $client->identifier . '</option>';
                                }

                            } elseif ($host->type == 3) {

                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <br/>

            <div class="timePickerDivInput">
                <p>From</p>
                <div class='col-md-5' style="width: 100%;">
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker6'>
                            <input type='text' class="form-control" name="dateTimeFrom" id="dateTimeFrom" readonly/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="dateTimeFromUTC" name="dateTimeFromUTC" value=""><br/><br/><br/>
                <p>To</p>
                <div class='col-md-5' style="width: 100%;">
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker7'>
                            <input type='text' class="form-control"  name="dateTimeTo" id="dateTimeTo" readonly/>
                            <span class="input-group-addon">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="dateTimeToUTC" name="dateTimeToUTC" value="">
            </div><br/><br/>
            <br/> <br/>
            <div class="select-box">
                <label for="selectLocation" class="label select-box1"><span
                            class="label-desc" id="locationSelection">Choose Location</span> </label> <select
                        id="selectLocation" class="select" name="locationSelect">
                    <option value="0" selected>Choose Location</option>
                    <?php
                    if ($host->type == 2) {
                        foreach ($locations as $location) {
                            echo '<option id="option'.$location->locationWebID.'" value ="' . $location->locationWebID . '">' . $location->locationTitle . '</option>';
                        }
                    }
                    ?>
                </select><br/>
                <p>Request Comment


                <p/>
                <textarea name='newAppointmentComment' id="newAppointmentComment"
                          style='max-width: 100%; min-width: 100%; max-height: 150px; min-height: 100px;'></textarea>
            </div>
            <br/>
            <button type="submit" name="submit" class="appointmentButton button1"
                    id="btnAmSave" >Save
            </button>
            <button type="button" id="btnCloseAm"
                    class="appointmentButton button3">Cancel
            </button>
        </div>
    </div>

</div>

<script>


    // var Random = Mock.Random;
    // var json1 = Mock.mock({
    //     "data|10-50": [{
    //         name: function () {
    //             return Random.name(true)
    //         },
    //         "id|+1": 1,
    //         "disabled|1-2": true,
    //         groupName: 'Group Name',
    //         "groupId|1-4": 1,
    //         "selected": false
    //     }]
    // });
    //
    // $('.dropdown-mul-1').dropdown({
    //     data: json1.data,
    //     limitCount: 40,
    //     multipleMode: 'label',
    //     choice: function () {
    //         // console.log(arguments,this);
    //     }
    // });
    //
    // var json2 = Mock.mock({
    //     "data|10000-10000": [{
    //         name: function () {
    //             return Random.name(true)
    //         },
    //         "id|+1": 1,
    //         "disabled": false,
    //         groupName: 'Group Name',
    //         "groupId|1-4": 1,
    //         "selected": false
    //     }]
    // });

    $('.dropdown-sin-1').dropdown({
        readOnly: true,
        input: '<input type="text" maxLength="20" placeholder="Search">'
    });

</script>
<div class="space-40"></div>


<?php

require ("objectFooter.php");

?>

<!--Maps JS-->
<script>

    var modal = document.getElementById('myModal');
    var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");

    // Get the button that opens the modal
    var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null ? document.getElementById("showNotificationPopupLink") : 0;
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];


    var btnCloseAm = document.getElementById("btnCloseAm");

    // When the user clicks the button, open the modal

    $(btnViewNotificationsPopup).click(function() {
        // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
        $('.redDot').css('visibility','hidden');
        $(modalViewNotificationPopUpgg).css('display', 'block');
        $('body').css('overflow','hidden');

        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { 'submit':'readNotifications' }
        });

    });


    var spanCloseAppointment = document.getElementsByClassName("spanCloseAppointment")[0];


    btn.onclick = function () {
        // document.getElementById("selectClient").value = '';
        // document.getElementById("clientSelection").innerHTML = "Choose Client";

        // document.getElementById("selectLocation").value = null;
        document.getElementById("locationSelection").innerHTML = "Choose Location";

        if (viewType == ViewType.YEAR){
            self.selectedDay = 1;
            self.selectedMonth = 1;
        }
        else if (viewType == ViewType.MONTH){
            self.selectedDay = 1;
        }
        var selectedDate = moment(self.selectedYear+"-"+("0" + self.selectedMonth).slice(-2)+"-"+("0" + self.selectedDay).slice(-2)+" "+moment().format("HH:mm"));
        var selectedDateFormatFrom = selectedDate.format('LLLL');
        $('#dateTimeFrom').val(selectedDateFormatFrom);
        var startDate = moment(selectedDateFormatFrom).unix();
        $('#dateTimeFromUTC').val(startDate);

        var selectedToDate = moment(self.selectedYear+"-"+("0" + self.selectedMonth).slice(-2)+"-"+("0" + self.selectedDay).slice(-2)+" "+moment().format("HH:mm"));
        selectedToDate.add(30, "minutes");
        var selectedDateFormatTo = selectedToDate.format('LLLL');
        $('#dateTimeTo').val(selectedDateFormatTo);
        var endDate = moment(selectedDateFormatTo).unix();
        $('#dateTimeToUTC').val(endDate);


        $('#datetimepicker6').datetimepicker({
            date: selectedDate,
            minDate: selectedDate,
            ignoreReadonly: true,
            format: ('LLLL'),

        });
        $('#datetimepicker7').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            ignoreReadonly: true,
            minDate: selectedToDate,
            format: ('LLLL'),
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            var selectedDateFrom = document.getElementById("dateTimeFrom").value;
            var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
            // console.log(selectedDateFormatFrom);
            var startDate = moment(selectedDateFormatFrom).unix();

            $('#dateTimeFromUTC').val(startDate);

        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);

            var selectedDateFrom = document.getElementById("dateTimeFrom").value;
            var selectedDateFormatFrom = moment(selectedDateFrom).format('LLLL');
            var startDate = moment(selectedDateFormatFrom).unix();

            $('#dateTimeFromUTC').val(startDate);
            var selectedDateTo = document.getElementById("dateTimeTo").value;
            var selectedDateFormatTo = moment(selectedDateTo).format('LLLL');
            var startDate = moment(selectedDateFormatTo).unix();

            $('#dateTimeToUTC').val(startDate);

        });

        modal.style.display = "block";
        $('body').css('overflow','hidden');
    }

    // When the user clicks on <span> (x), close the modal
    spanCloseAppointment.onclick = function () {
        modal.style.display = "none";
        $('body').css('overflow','auto');
    }

    btnCloseAm.onclick = function () {
        modal.style.display = "none";
        $('body').css('overflow','auto');
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal || event.target == modalViewNotificationPopUpgg) {
            modal.style.display = "none";
            modalViewNotificationPopUpgg.style.display = "none";
            $('body').css('overflow','auto');
        }
    }


    $("select").on("click", function () {

        $(this).parent(".select-box").toggleClass("open");

    });

    $(document).mouseup(function (e) {
        var container = $(".select-box");

        if (container.has(e.target).length === 0) {
            container.removeClass("open");
        }
    });


    $("#selectLocation").on("change", function () {

        var selection = $(this).find("option:selected").text(),
            labelFor = $(this).attr("id"),
            label = $("[for='" + labelFor + "']");

        label.find(".label-desc").html(selection);

    });



    $('#btnAmSave').click(function() {

        var submitValue = '<?php echo $fileController->submitValue;?>';
        var submitData = '<?php echo $fileController->createAppointmentFlag;?>';
        var selectClient = document.getElementById("selectClient").value;
        var selectClientID = '<?php echo $fileController->selectClient;?>';
        var dateTimeFromUTC = document.getElementById("dateTimeFromUTC").value;
        var dateTimeToUTC = document.getElementById("dateTimeToUTC").value;
        var dateTimeFromUTCID = '<?php echo $fileController->dateTimeFromUTC;?>';
        var dateTimeToUTCID = '<?php echo $fileController->dateTimeToUTC;?>';
        var selectLocation = document.getElementById("selectLocation").value;
        var selectLocationID = '<?php echo $fileController->selectLocation;?>';
        var newAppointmentComment = document.getElementById("newAppointmentComment").value;
        var newAppointmentCommentID = '<?php echo $fileController->newAppointmentComment;?>';


        $.ajax({
            type: "POST",
            url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: { [submitValue]: submitData, [selectClientID]: selectClient, [dateTimeFromUTCID]:dateTimeFromUTC, [dateTimeToUTCID]:dateTimeToUTC, [selectLocationID]: selectLocation, [newAppointmentCommentID]: newAppointmentComment},
            async: false,
            success: function(data){
                // alert (data);
                if(!JSON.parse(data).success){
                    location.href = "/appointsync.com/public/dashboard.php";
                }else if(JSON.parse(data).data.webID){
                    location.href = "/appointsync.com/public/appointment.php/"+JSON.parse(data).data.webID;
                }
            }
        });

    });

</script>


<?php
// Location Alert handling
if (isset($_SESSION["appointmentController"]) && $_SESSION['appointmentController'] == 1) {

    echo '<script language="javascript">', 'alert("An error occurred while trying to create the appointment.")', '</script>';
    sleep(1.3);

    unset($_SESSION["appointmentController"]);
}elseif (isset($_SESSION["appointmentController"]) && $_SESSION['appointmentController'] == 2) {

    echo '<script language="javascript">', 'alert("An error occurred while trying to amend the appointment.")', '</script>';
    sleep(1.3);

    unset($_SESSION["appointmentController"]);
}elseif (isset($_SESSION["appointmentController"]) && $_SESSION['appointmentController'] == 3) {

    echo '<script language="javascript">', 'alert("An error occurred while trying to delete the appointment.")', '</script>';
    sleep(1.3);

    unset($_SESSION["appointmentController"]);
}

?></body>
</html>
