<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="/appointsync.com/webroot/img/apple-touch-icon.png">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>

<!-- Plugin-CSS -->
<link rel="stylesheet" href="/appointsync.com/public/css/bootstrap.min.css">
<link rel="stylesheet" href="/appointsync.com/public/css/owl.carousel.min.css">
<link rel="stylesheet" href="/appointsync.com/public/css/themify-icons.css">
<link rel="stylesheet" href="/appointsync.com/public/css/animate.css">
<link rel="stylesheet" href="/appointsync.com/public/css/magnific-popup.css">

<!-- Main-Stylesheets -->
<link rel="stylesheet" href="/appointsync.com/public/css/space.css">
<link rel="stylesheet" href="/appointsync.com/public/css/theme.css">
<link rel="stylesheet" href="/appointsync.com/public/css/overright.css">
<link rel="stylesheet" href="/appointsync.com/public/css/normalize.css">
<link rel="stylesheet" href="/appointsync.com/public/css/style.css">
<link rel="stylesheet" href="/appointsync.com/public/css/responsive.css">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>