<?php
include_once dirname(__FILE__) . '/../resources/Classes/MappedClient.php';
include_once dirname(__FILE__) . '/../resources/Classes/UserProfile.php';
include_once dirname(__FILE__) . '/../resources/Controller/API/ApiController.php';
include_once dirname(__FILE__) . '/../resources/Classes/MappedRouterController.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(isset($_SESSION['hiddenFilePin'])){
    unset($_SESSION['hiddenFilePin']);
}

$_SESSION['pageName'] = 'profile';


$loggedInUserType = unserialize($_SESSION['user'])->type;

 error_reporting(E_PARSE);
if(isset($_SESSION['user'])) {
}else{header("Location: /appointsync.com/");}

if($loggedInUserType == 2){

$hostResponse  = APIController::getProfile();
$locationsResponse = APIController::getLocations();
if($hostResponse->success){
    $host = $hostResponse->data;
}else{ header("Location: /appointsync.com/public/dashboard.php"); exit();}
if($locationsResponse->success){
    $locations = $locationsResponse->data;
}else{ header("Location: /appointsync.com/public/dashboard.php"); exit();}

}elseif($loggedInUserType == 3){

    $clientResponse  = APIController::getClientProfile();
//    echo json_encode($clientResponse);
    if($clientResponse->success){
        $client = $clientResponse->data;
    }else{ header("Location: /appointsync.com/public/dashboard.php"); exit();}

}

$fileController = new MappedRouterController();
?>
<!DOCTYPE html>
<html>
<head>
    <title>AppointSync - Profile</title>

    <?php

    require ("objectHeader.php");

    ?>

</head>

<style>


#userProfile{

width:80%;

}

.navbar-default .navbar-nav>.open>a,.navbar-default .navbar-nav>.open>a:focus,.navbar-default .navbar-nav>.open>a:hover {
    color:#555;
    background-color:red !important;
}

.profileClass{
    width:100%;
    margin: 0px auto;
}
#profilePicture{
    width:50%;
    float:left;
    min-width:300px;
    margin: 0px auto;
}
#profileInfo {
    width: 50%;
    float: left;
    padding-top: 10%;
    min-width: 300px;

}

@media only screen and (max-width: 650px){
    #profilePicture{

        float:none;

    }

    #profileInfo{
        margin:0 auto;
        float:none;

    }

    .modal-body{

        width:100%;
        margin:0 auto;
    }

    .locations{

        width:70%;
        min-width: 250px;
        margin:0 auto;
        float:none;
        overflow: auto;
    }


    #map{
        width: 10px;
        height: 10px !important;  /* The height is 400 pixels */-->
           }

    #newMap {
        width: 200px !important;
        height: 200px !important;
        float: left;
    }
}

</style>

<body style='width:100%;'>

<div id="notificationPopUpBlockDiv">
    <?php

    require ("notificationview.php");

    ?>
</div>

  <header class="blue-bg relative fix" id="home">
      <div class="section-bg overlay-bg angle-bg ripple">
          <video autoplay muted id="video-background" loop>
              <source src="http://intimissibd.com/video/video-2.webm" type="video/webm">
          </video>
      </div>
      <!--Mainmenu-->
      <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix">
          <div class="container">
              <div class="navbar-header">
                  <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a href="#" class="navbar-brand">
                      <img src="/appointsync.com/public/img/newlogo.png" style="min-width:150px; width:150px;margin-top:-5%;" alt="Logo">
                  </a>
              </div>
              <?php

              require ("objectMenu.php");

              ?>
          </div>
      </nav>
      <!--Mainmenu/-->
      <div class="navbar navbar-default mainmenu-area navbar-fixed-top"></div>
      <!--Header-Text-->
      <!--Header-Text/-->
  </header>

  <section>

    <div style='margin-top:5%;'>

   </div>

   	<br><br>

   <div class="container">

<?php


//if user Host
if($loggedInUserType == 2){

    $hostWorkingHoursFromAMPM = date("h:i A", strtotime($host->workingHourFrom));
    $hostWorkingHoursToAMPM = date("h:i A", strtotime($host->workingHourTo));
    $timeFromHourValue = substr($hostWorkingHoursFromAMPM, 0,2);
    $timeFromMinuteValue = substr($hostWorkingHoursFromAMPM, 3,2);
    $timeFromCharValue = substr($hostWorkingHoursFromAMPM, 6,2);

    $timeToHourValue = substr($hostWorkingHoursToAMPM, 0,2);
    $timeToMinuteValue = substr($hostWorkingHoursToAMPM, 3,2);
    $timeToCharValue = substr($hostWorkingHoursToAMPM, 6,2);

?>

       <div class="profileClass">
           <div id="profilePicture"><?php echo $host->profilePic; ?></div>
           <div id="profileInfo">
               <p> Name: <?php echo $host->firstName . " " . $host->lastName; ?></p>
               <p> Profession: <?php echo $host->profession; ?></p>
               <p> Gender: <?php echo $host->gender; ?></p>
               <p> Working Hours: <?php echo 'From ' . $hostWorkingHoursFromAMPM . ' To ' . $hostWorkingHoursToAMPM; ?></p>
               <p> Email: <?php echo $host->email; ?></p>
               <p> Phone number: <?php echo $host->phoneNumber; ?></p>
               <p> Username: <?php echo $host->username; ?></p>
<!--               <p><a id="btnChangePassword" style="cursor: pointer;color:#1193d4;">Change Password</a></p>-->

               <button type="button" class="appointmentButton button6" id="btnLocations">See Locations</button>
               <button type="button" class="appointmentButton button6" id="btnEditProfile">Edit Profile</button>
               <button type="button" class="appointmentButton button6" id="btnSwitchAccount">Switch Views</button>
               <button type="button" class="appointmentButton button6" id="btnOpenSettings">Settings</button>
<!--               <button type="button" class="appointmentButton button1" id="btnAccountType">Premium</button>-->

           </div>

       </div>

       <!-- Locations -->

       <div id="myModalLocations" class="modal">

           <!-- Modal content -->
           <div class="modal-content">
               <span class="close spanLocation">&times;</span>
               <div class="modal-header">
                   <h2>Edit Locations</h2>
               </div>
               <div class="modal-body">
                   <h3> Delete or add Locations </h3>

                   <div class="locations">



                           <?php

                            $counter = 0;
                           foreach ($locations as $location) {
?>
                   <form action="/appointsync.com/resources/Controller/API/APIRouter.php"
                         method="post"><?php

                               echo '<div class="location">'
                                   . '<h3>' . $location->locationTitle . '</h3>';?>


                       <div class="map" id="<?php echo $location->locationWebID?>" style="height: 300px;"></div>


                                   <?php echo ''
                                   . '<input type="hidden" value="'.$location->locationWebID.'" name="locationWebID" />'
                                   . '<button type="submit" name="submit" value="deleteLocation" class="appointmentButton button3">Delete</button>'
                                   . '</div>'
                                   . '</form>';

                           } ?>

                           <div class="location">
                               <h3>Add New Location</h3><br/>
                               <a id="btnOpenNewLocations"><img src="/appointsync.com/webroot/testing/addLoc.png"/></a>
                           </div>
                       </div>
                       <br/><br/>
                       <button class="appointmentButton button1" id="btnAccept" style="display:none;">Accept</button>
                       <button type="button" id="btnCloseLocations" class="appointmentButton button3"
                               style="display:none;">Cancel
                       </button>

               </div>
           </div>
       </div>




       <!-- Edit Profile -->

       <div id="myModalEditProfile" class="modal">

           <!-- Modal content -->
           <div class="modal-content">
               <span class="close spanEditProfile">&times;</span>
               <div class="modal-header">
                   <h2>Edit Profile</h2>
               </div>
               <div class="modal-body">
                   <h3> Edit the below fields to change your profile info. </h3>
                       <input type="hidden" value="4" name="controllerTypeFlag"/>

                       <p>First Name</p><input type="text" name="firstName" id="hostFirstName" value="<?php echo $host->firstName ?>"/><p></p>
                       <p>Last Name</p><input type="text" name="lastName" id="hostLastName" value="<?php echo $host->lastName ?>"/><p></p>
                       <p>Profession</p><input type="text" name="profession" id="hostProfession" value="<?php echo $host->profession ?>"/><p></p>
                       <?php

                            echo '<br/><div class="timePickerDivInput" style="clear:both;"><p>Working Hours</p>'
                                .'<div class="workingHoursFrom" style="width:49%;float:left;"><p>From</p><input id="timepicker1" type="text" name="fromTime" value="'. $hostWorkingHoursFromAMPM.'"/></div>'
                                .'<div class="workingHoursTo" style="width:49%;float:left;"><p>To</p><input id="timepicker2" type="text" name="toTime" value="'. $hostWorkingHoursToAMPM.'"/></div>'
                                .'</div><br/>';

                       ?>

                       <div class="editProfileButtonClass">
                       <button type="button" class="appointmentButton button6 buttonEditProfile" id="btnChangePassword">Change Password</button>
                       <button type="button" class="appointmentButton button6 buttonEditProfile" id="btnProfilePic">Edit Picture</button>
                       </div><br/>
                       <button type="submit" class="appointmentButton button1" id="btnEditProfileSubmit">Save</button>
                       <button type="button" id="btnCloseEditProfile" class="appointmentButton button3">Cancel</button>

               </div>
           </div>
       </div>

    <!-- Profile picture -->

    <div id="myModalProfile" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanProfile">&times;</span>
            <div class="modal-header">
                <h2>Edit Profile Picture</h2>
            </div>
            <div class="modal-body">
                <h3> Pick up a new profile picture. </h3>
                <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
                    <input type='file' name='file' id="file" required style="margin-right: 10%;">
                    <button type="submit" class="appointmentButton button1" id="btnChangeProfilePicture">Accept</button>
                    <button type="button" id="btnCloseProfile" class="appointmentButton button3">Cancel</button>
                </form>
            </div>
        </div>
    </div>



    <!-- New Location -->

       <div id="myModalNewLocation" class="modal">

           <!-- Modal content -->
           <div class="modal-content">
               <span class="close spanNewLocation">&times;</span>
               <div class="modal-header">
                   <h2>New Location</h2>
               </div>
               <div class="modal-body">
                   <h3> Please fill the below information </h3>
                       <p>Title</p>
                       <input type='text' id='locationTitle' name='locationTitle' class='form-control' value=""/><br/>
                       <p>Set Location</p>

                       <div id="newMap" style="width: 90%;height: 450px; margin:auto;"></div>
                       <script>

                           function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                               infoWindow.setPosition(pos);
                               infoWindow.setContent(browserHasGeolocation ?
                                   'Error: The Geolocation service failed.' :
                                   'Error: Your browser doesn\'t support geolocation.');
                               infoWindow.open(newMap);
                           }


                           window.document.onload = function(e){
                               console.log('this: '+ pos);
                           }
                       </script>
                       <script>
                           // Initialize and add the map
                           <?php

                           $counter = 0;
                           foreach ($locations as $location) {
                               echo 'var map' . $counter . ';
                               ';
                               $counter++;
                           }
                           ?>



                           var newMap, infoWindow;
                           function initMap() {
                               <?php
                                      $counter = 0;
                                      foreach ($locations as $location) {
                                      // The location of Uluru
                                       echo 'var uluru'.$counter.' = {lat: '.$location->locationLatitude.', lng: '.$location->locationLongitude.'};
                                       ',
                               'map'.$counter.' = new google.maps.Map(',
                                   'document.getElementById("'.$location->locationWebID.'"), {zoom: 13, center: uluru'.$counter.'});
                                   ';
                                   $counter++;
                                   }
                                   ?>
                               <?php
                               $counter = 0;
                               foreach ($locations as $location) {
                                   echo 'map'.$counter.' = new google.maps.Map(
                                        document.getElementById("'.$location->locationWebID.'"), {zoom: 13, center: uluru'.$counter.'});';
                                   $counter++;
                                   // The marker, positioned at Uluru

                               }
                               ?>
                               <?php
                               $counter = 0;
                               foreach ($locations as $location) {
                                   echo 'var marker' . $counter . ' = new google.maps.Marker({position: uluru'.$counter.', map: map' . $counter . '});';


                                   $counter++;
                               }
                               ?>

                               newMap = new google.maps.Map(document.getElementById('newMap'), {
                                   center: {lat: -34.397, lng: 150.644},
                                   zoom: 15
                               });
                               infoWindow = new google.maps.InfoWindow;

                               // Try HTML5 geolocation.
                               if (navigator.geolocation) {
                                   navigator.geolocation.getCurrentPosition(function(position) {
                                       var pos = {
                                           lat: position.coords.latitude,
                                           lng: position.coords.longitude
                                       };
                                       document.getElementById('locationLongitude').value = pos.lng;
                                       document.getElementById('locationLatitude').value = pos.lat;
                                       var newMarker = new google.maps.Marker({position: pos, map: newMap,  draggable: true});

                                       google.maps.event.addListener(newMarker, 'dragend', function (event) {
                                           console.log('lat:' + this.getPosition().lat());
                                           console.log('long:' + this.getPosition().lng());
                                           document.getElementById('locationLongitude').value = this.getPosition().lng();
                                           document.getElementById('locationLatitude').value = this.getPosition().lat();
                                       });

                                       infoWindow.setPosition(pos);
                                       // infoWindow.setContent('Location found.');
                                       // infoWindow.open(newMap);
                                       newMap.setCenter(pos);
                                   }, function() {
                                       handleLocationError(true, infoWindow, newMap.getCenter());
                                   });
                               } else {
                                   // Browser doesn't support Geolocation
                                   handleLocationError(false, infoWindow, newMap.getCenter());
                               }

                           }
                       </script>
                       <input type='hidden' id='locationLongitude' name='locationLongitude' class='form-control' value="0"/>
                       <input type='hidden' id='locationLatitude' name='locationLatitude' class='form-control' value="0"/><br/>
                       <button type="button" class="appointmentButton button6" id="btnAddNewLocation">Save</button>
                       <button type="button" id="btnCloseNewLocation" class="appointmentButton button3">Cancel</button>
               </div>
           </div>
       </div>


       <!-- Account Type -->

       <div id="myModalAccountType" class="modal">

           <!-- Modal content -->
           <div class="modal-content">
               <span class="close spanAccountType">&times;</span>
               <div class="modal-header">
                   <h2>Account Type</h2>
               </div>
               <div class="modal-body">
                   <h3> Below is the status of your account. </h3>

                       <?php
                       if (isset($host->tillDate)) {

                           $dateNow = date("d-m-Y");
                           $NowDateUnix = strtotime($dateNow) . '<br/>';

                           if ($host->tillDate >= $NowDateUnix) {
                               $checkRemainingDays = abs($host->tillDate - $NowDateUnix) / 60 / 60 / 24;
                               $checkRemainingDays = floor($checkRemainingDays);
                               echo '<h4>Already a premium member.</h4>';
                               echo '<p>You still have ' . $checkRemainingDays . ' days in order to renew.</p>';
                               echo '<button type="button" id="btnCloseAccountType" class="appointmentButton button3">Cancel</button>';
                           } else {
                               ?>
                         <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                       <input type="hidden" value="5" name="controllerTypeFlag"/>
                             <?php
                             $dt1 = new DateTime();
                             $today = $dt1->format("d-m-Y");

                             $dt2 = new DateTime("+1 month");
                             $date = $dt2->format("d-m-Y");

                               echo '<h4> Normal Account </h4>
                                 Upgrade to premium for an additional 4.99$/month to be able to create offline users!!<br/><br/>
                                 <span style="font-size: 0.8em;">Benefits: keep all your appointments in sync in one place even if some of your clients do not
                                 use applications or own a smart phone.</span>
                               <br/>
                                        ';
                               echo '<button type="submit" class="appointmentButton button6" id="btnAccept">Upgrade Now</button><button type="button" id="btnCloseAccountType" class="appointmentButton button3">Cancel</button>'
                                  . '</form>';
                       }

                       }else{

                             ?>
                             <form action="/appointsync.com/resources/appointSyncController/appointsyncinatorProfileController.php" method="post">
                                 <input type="hidden" value="5" name="controllerTypeFlag"/>
                                 <?php
                                 $dt1 = new DateTime();
                                 $today = $dt1->format("d-m-Y");

                                 $dt2 = new DateTime("+1 month");
                                 $date = $dt2->format("d-m-Y");

                                 echo '<h4> Normal Account </h4>
                                 Upgrade to premium for an additional 4.99$/month to be able to create offline users!!<br/><br/>
                                 <span style="font-size: 0.8em;">Benefits: keep all your appointments in sync in one place even if some of your clients do not
                                 use applications or own a smart phone.</span>
                               <br/>
                                        ';
                                 echo '<button type="submit" class="appointmentButton button6" id="btnAccept">Upgrade Now</button><button type="button" id="btnCloseAccountType" class="appointmentButton button3">Cancel</button>'
                                     . '</form>';

                             }
                    ?>
            </div>
        </div>
    </div>



    <!-- Switch View -->

    <div id="myModalSwitchAccount" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanSwitchAccount">&times;</span>
            <div class="modal-header">
                <h2>Switch View</h2>
            </div>
            <div class="modal-body">
                <p>As a host, you can change views from host to client.</p>
                <p>Host View: Set and manage appointments with clients.<br/>Client View: Take and manage appointments from other Hosts.</p><br/>
                    <?php
                    if($host->loggedInAs == 0){

                        echo '<h3>You are currently logged in as host.</h3><br/>',
                        '<button type="button" class="appointmentButton button6" id="btnSwitchClient" onclick="changeView(this.id)">Switch to Client View</button>';

                    }elseif($host->loggedInAs == 1){

                        echo '<h3>You are currently logged in as client.</h3><br/>',
                        '<button type="button" class="appointmentButton button6" id="btnSwitchHost" onclick="changeView(this.id)">Switch to Host View</button>';
                    }

                    ?>
                    <button type="button" id="btnCloseSwitchAccount" class="appointmentButton button3">Cancel</button>
            </div>
        </div>
    </div>



    <!-- Change password -->

    <div id="myModalPassword" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanPassword">&times;</span>
            <div class="modal-header">
                <h2>Change Password</h2>
            </div>
            <div class="modal-body">
                <h3> Please fill the below fields. </h3>
                    <p>Current Password</p>
                    <input type='password' id='oldPassword' name='oldPassword' class='form-control' required/><br/>
                    <p>New Password</p>
                    <input type='password' id='newPasswordTryOne' name='newPasswordTryOne' class='form-control'  onkeyup='check();' required/><br/>
                    <p>Confirm Password</p>
                    <input type='password' id='newPassword' name='newPassword' class='form-control'  onkeyup='check();' required/><br/>
                    <span id='message'></span><br/>
                    <button type="submit" class="appointmentButton button1" id="btnAcceptPassword" onclick="changePassword();">Accept</button>
                    <button type="button" id="btnCloseChangePassword" class="appointmentButton button3">Cancel</button>

                <script>

                    $('#btnAcceptPassword').hide();
                    document.getElementById('message').style.color = 'red';
                    document.getElementById('message').innerHTML = 'not matching';
                    var check = function() {
                        if (document.getElementById('newPasswordTryOne').value ==
                            document.getElementById('newPassword').value && document.getElementById('newPassword').value != '') {
                            document.getElementById('message').style.color = 'green';
                            document.getElementById('message').innerHTML = 'matching';
                            $('#btnAcceptPassword').show();
                        } else {
                            document.getElementById('message').style.color = 'red';
                            document.getElementById('message').innerHTML = 'not matching';
                            $('#btnAcceptPassword').hide();
                        }
                    }
                </script>

            </div>
        </div>
    </div>

<?php


}

//if User Client
elseif($loggedInUserType == 3){
    ?>


       <div class="profileClass">
           <div id="profilePicture"><?php echo $client->profilePic; ?></div>
      <div id="profileInfo">
          <p> Name: <?php echo $client->firstName . " " . $client->lastName; ?></p>
          <p> Gender: <?php echo $client->gender; ?></p>
          <p> Email: <?php echo $client->email; ?></p>
          <p> Phone number: <?php echo $client->phoneNumber; ?></p>
          <p> Username: <?php echo $client->username; ?></p>
          <!--               <p><a id="btnChangePassword" style="cursor: pointer;color:#1193d4;">Change Password</a></p>-->


          <button type="button" class="appointmentButton button6" id="btnEditProfileClient">Edit Profile</button>
          <button type="button" class="appointmentButton button6" id="btnOpenSettingsClient">Settings</button>
          <!--               <button type="button" class="appointmentButton button1" id="btnAccountType">Premium</button>-->

      </div>

      </div>

    <!-- Edit Profile -->

    <div id="myModalEditProfileClient" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanEditProfile">&times;</span>
            <div class="modal-header">
                <h2>Edit Profile</h2>
            </div>
            <div class="modal-body">
                <h3> Edit the below fields to change your profile info. </h3>
                <input type="hidden" value="4" name="controllerTypeFlag"/>

                <p>First Name</p><input type="text" name="firstName" id="clientFirstName" value="<?php echo $client->firstName ?>"/><p></p>
                <p>Last Name</p><input type="text" name="lastName" id="clientLastName" value="<?php echo $client->lastName ?>"/><p></p>

                <div class="editProfileButtonClass">
                    <button type="button" class="appointmentButton button6 buttonEditProfile" id="btnChangePassword">Change Password</button>
                    <button type="button" class="appointmentButton button6 buttonEditProfile" id="btnProfilePic">Edit Picture</button>
                </div><br/>
                <button type="submit" class="appointmentButton button1" id="btnEditProfileSubmit">Save</button>
                <button type="button" id="btnCloseEditProfile" class="appointmentButton button3">Cancel</button>

            </div>
        </div>
    </div>

    <!-- Profile picture -->

    <div id="myModalProfileClient" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanProfile">&times;</span>
            <div class="modal-header">
                <h2>Edit Profile Picture</h2>
            </div>
            <div class="modal-body">
                <h3> Pick up a new profile picture. </h3>
                <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
                    <input type='file' name='file' id="file" required style="margin-right: 10%;">
                    <button type="submit" class="appointmentButton button1" id="btnChangeProfilePicture">Accept</button>
                    <button type="button" id="btnCloseProfile" class="appointmentButton button3">Cancel</button>
                </form>
            </div>
        </div>
    </div>

    <div id="myModalPasswordClient" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close spanPassword">&times;</span>
            <div class="modal-header">
                <h2>Change Password</h2>
            </div>
            <div class="modal-body">
                <h3> Please fill the below fields. </h3>
                <p>Current Password</p>
                <input type='password' id='oldPassword' name='oldPassword' class='form-control' required/><br/>
                <p>New Password</p>
                <input type='password' id='newPasswordTryOne' name='newPasswordTryOne' class='form-control'  onkeyup='check();' required/><br/>
                <p>Confirm Password</p>
                <input type='password' id='newPassword' name='newPassword' class='form-control'  onkeyup='check();' required/><br/>
                <span id='message'></span><br/>
                <button type="submit" class="appointmentButton button1" id="btnAcceptPassword" onclick="changePassword();">Accept</button>
                <button type="button" id="btnCloseChangePassword" class="appointmentButton button3">Cancel</button>

                <script>

                    $('#btnAcceptPassword').hide();
                    document.getElementById('message').style.color = 'red';
                    document.getElementById('message').innerHTML = 'not matching';
                    var check = function() {
                        if (document.getElementById('newPasswordTryOne').value ==
                            document.getElementById('newPassword').value && document.getElementById('newPassword').value != '') {
                            document.getElementById('message').style.color = 'green';
                            document.getElementById('message').innerHTML = 'matching';
                            $('#btnAcceptPassword').show();
                        } else {
                            document.getElementById('message').style.color = 'red';
                            document.getElementById('message').innerHTML = 'not matching';
                            $('#btnAcceptPassword').hide();
                        }
                    }
                </script>

            </div>
        </div>
    </div>

<?php

}

echo '</section>';


require ("objectFooter.php");

?>
    <!--Maps JS-->

  <script src="/appointsync.com/public/js/timepicki.js"></script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvSfHqoBwUd4R4_oIEf6_88KaUvi6qbqU&callback=initMap">
</script>

<!--<script async defer-->
<!--        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvSfHqoBwUd4R4_oIEf6_88KaUvi6qbqU&callback=initnewMap">-->
<!--</script>-->


<!-- AmendScript -->
<?php if($loggedInUserType == 2) {
    ?>
    <script>
        // Get the modal
        var modalLocation = document.getElementById('myModalLocations');
        var modalPicture = document.getElementById('myModalProfile');
        var modalEditProfile = document.getElementById('myModalEditProfile');
        var modalNewLocation = document.getElementById('myModalNewLocation');
        var modalAccountType = document.getElementById('myModalAccountType');
        var modalChangePassword = document.getElementById("myModalPassword");
        var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");
        var modalSwitchAccount = document.getElementById("myModalSwitchAccount");

        // Get the button that opens the modal
        var btnLocations = document.getElementById("btnLocations") != null ? document.getElementById("btnLocations") : 0;
        var btnProfile = document.getElementById("btnProfilePic") != null ? document.getElementById("btnProfilePic") : 0;
        var btnEditProfile = document.getElementById("btnEditProfile") != null ? document.getElementById("btnEditProfile") : 0;
        var btnOpenNewLocations = document.getElementById("btnOpenNewLocations") != null ? document.getElementById("btnOpenNewLocations") : 0;
        var btnOpenAccountType = document.getElementById("btnAccountType") != null ? document.getElementById("btnAccountType") : 0;
        var btnChangePassword = document.getElementById("btnChangePassword") != null ? document.getElementById("btnChangePassword") : 0;
        var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null ? document.getElementById("showNotificationPopupLink") : 0;
        var btnSwitchAccount = document.getElementById("btnSwitchAccount") != null ? document.getElementById("btnSwitchAccount") : 0;
        var btnOpenSettings = document.getElementById("btnOpenSettings") != null ? document.getElementById("btnOpenSettings") : 0;


        var btnCloseLocations = document.getElementById("btnCloseLocations");
        var btnCloseProfile = document.getElementById("btnCloseProfile");
        var btnCloseEditProfile = document.getElementById("btnCloseEditProfile");
        var btnCloseNewLocation = document.getElementById("btnCloseNewLocation");
        var btnCloseAccountType = document.getElementById("btnCloseAccountType");
        var btnCloseChangePassword = document.getElementById("btnCloseChangePassword");
        var btnCloseSwitchAccount = document.getElementById("btnCloseSwitchAccount");

        var spanLocation = document.getElementsByClassName("spanLocation")[0];
        var spanProfile = document.getElementsByClassName("spanProfile")[0];
        var spanEditProfile = document.getElementsByClassName("spanEditProfile")[0];
        var spanNewLocation = document.getElementsByClassName("spanNewLocation")[0];
        var spanAccountType = document.getElementsByClassName("spanAccountType")[0];
        var spanPassword = document.getElementsByClassName("spanPassword")[0];
        var spanSwitchAccount = document.getElementsByClassName("spanSwitchAccount")[0];


        // When the user clicks the button, open the modal

        $(btnViewNotificationsPopup).click(function () {
            // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
            $('.redDot').css('visibility', 'hidden');
            $(modalViewNotificationPopUpgg).css('display', 'block');
            $('body').css('overflow', 'hidden');

            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {'submit': 'readNotifications'}
            });

        });


        btnLocations.onclick = function () {
            modalLocation.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnOpenSettings.onclick = function () {
            window.location.href = "/appointsync.com/public/profilesetting.php/";
        }

        btnProfile.onclick = function () {
            modalPicture.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnEditProfile.onclick = function () {
            modalEditProfile.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnOpenNewLocations.onclick = function () {
            modalNewLocation.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnOpenAccountType.onclick = function () {
            modalAccountType.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnChangePassword.onclick = function () {
            modalChangePassword.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnSwitchAccount.onclick = function () {
            modalSwitchAccount.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnCloseLocations.onclick = function () {
            modalLocation.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        btnCloseProfile.onclick = function () {
            modalPicture.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        btnCloseEditProfile.onclick = function () {
            modalEditProfile.style.display = "none";
            $('body').css('overflow', 'auto');
        }


        btnCloseNewLocation.onclick = function () {
            modalNewLocation.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        btnCloseAccountType.onclick = function () {
            modalAccountType.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        btnCloseChangePassword.onclick = function () {
            modalChangePassword.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        btnCloseSwitchAccount.onclick = function () {
            modalSwitchAccount.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanLocation.onclick = function () {
            modalLocation.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanProfile.onclick = function () {
            modalPicture.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanEditProfile.onclick = function () {
            modalEditProfile.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanNewLocation.onclick = function () {
            modalNewLocation.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanAccountType.onclick = function () {
            modalAccountType.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanPassword.onclick = function () {
            modalChangePassword.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanSwitchAccount.onclick = function () {
            modalSwitchAccount.style.display = "none";
            $('body').css('overflow', 'auto');
        }


        window.onclick = function (event) {
            if (event.target == modalLocation || event.target == modalSwitchAccount || event.target == modalChangePassword || event.target == modalAccountType ||
                event.target == modalNewLocation || event.target == modalEditProfile || event.target == modalPicture || event.target == modalViewNotificationPopUpgg) {
                modalLocation.style.display = "none";
                modalSwitchAccount.style.display = "none";
                modalChangePassword.style.display = "none";
                modalAccountType.style.display = "none";
                modalNewLocation.style.display = "none";
                modalEditProfile.style.display = "none";
                modalPicture.style.display = "none";
                modalViewNotificationPopUpgg.style.display = "none";
                $('body').css('overflow', 'auto');
            }
        }

        function changePassword() {

            var oldPasswordData = document.getElementById("oldPassword").value;
            var newPasswordData = document.getElementById("newPassword").value;

            var submitValue = '<?php echo $fileController->submitValue;?>';
            var submitData = '<?php echo $fileController->changePassword;?>';
            var oldPasswordValue = '<?php echo $fileController->oldPassword;?>';
            var newPasswordValue = '<?php echo $fileController->newPassword;?>';


            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {
                    [submitValue]: submitData,
                    [oldPasswordValue]: oldPasswordData,
                    [newPasswordValue]: newPasswordData
                },
                async: false,
                success: function (data) {

                    location.href = "/appointsync.com/public/profile.php/";

                }
            });


        }


        function changeView(clicked_id) {

            clicked_id.disabled = 'true';
            var loggedInAsValue = '<?php echo $fileController->loggedInAs;?>';
            if (clicked_id == 'btnSwitchClient') {
                var loggedInAsValueData = '<?php echo $fileController->loggedInAsValueClient;?>';
            } else if (clicked_id == 'btnSwitchHost') {
                var loggedInAsValueData = '<?php echo $fileController->loggedInAsValueHost;?>';
            }

            var submitValue = '<?php echo $fileController->submitValue;?>';
            var submitData = '<?php echo $fileController->hostChangeView;?>';


            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {[submitValue]: submitData, [loggedInAsValue]: loggedInAsValueData},
                async: false,
                success: function (data) {
                    location.href = "/appointsync.com/public/profile.php/<?php echo $hostResponse->webID;?>";
                    clicked_id.disabled = 'false';
                }
            });


        }


        $('#btnEditProfileSubmit').click(function () {

            var submitValue = '<?php echo $fileController->submitValue;?>';
            var submitData = '<?php echo $fileController->editProfile;?>';
            var hostFirstNameValue = '<?php echo $fileController->firstName;?>';
            var hostFirstNameData = document.getElementById("hostFirstName").value;
            var hostLastNameValue = '<?php echo $fileController->lastName;?>';
            var hostLastNameData = document.getElementById("hostLastName").value;
            var hostProfessionValue = '<?php echo $fileController->profession;?>';
            var hostProfessionData = document.getElementById("hostProfession").value;
            var hostWorkingHourFromValue = '<?php echo $fileController->workingHourFrom;?>';
            var hostWorkingHourFromData = document.getElementById("timepicker1").value;
            var hostWorkingHourToValue = '<?php echo $fileController->workingHourTo;?>';
            var hostWorkingHourToData = document.getElementById("timepicker2").value;


            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {
                    [submitValue]: submitData,
                    [hostFirstNameValue]: hostFirstNameData,
                    [hostLastNameValue]: hostLastNameData,
                    [hostProfessionValue]: hostProfessionData
                    ,
                    [hostWorkingHourFromValue]: hostWorkingHourFromData,
                    [hostWorkingHourToValue]: hostWorkingHourToData
                },
                async: false,
                success: function (data) {
                    location.href = "/appointsync.com/public/profile.php/<?php echo $hostResponse->webID;?>";
                }
            });

        });


        $('#btnAddNewLocation').click(function () {

            var submitValue = '<?php echo $fileController->submitValue;?>';
            var submitData = '<?php echo $fileController->addNewLocation;?>';

            var locationTitleValue = '<?php echo $fileController->locationTitle;?>';
            var locationTitleData = document.getElementById("locationTitle").value;

            var locationLongitudeValue = '<?php echo $fileController->locationLongitude;?>';
            var locationLongitudeData = document.getElementById("locationLongitude").value;

            var locationLatitudeValue = '<?php echo $fileController->locationLatitude;?>';
            var locationLatitudeData = document.getElementById("locationLatitude").value;


            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {
                    [submitValue]: submitData,
                    [locationTitleValue]: locationTitleData,
                    [locationLongitudeValue]: locationLongitudeData,
                    [locationLatitudeValue]: locationLatitudeData
                },
                async: false,
                success: function (data) {
                    location.href = "/appointsync.com/public/profile.php/<?php echo $hostResponse->webID;?>";
                }
            });

        });


        $("#btnChangeProfilePicture").click(function (event) {

            //stop submit the form, we will post it manually.
            event.preventDefault();

            // Get form
            var form = $('#fileUploadForm')[0];

            // Create an FormData object
            var data = new FormData(form);

            // If you want to add an extra field for the FormData
            data.append('file', $('[type=file]')[0].files[0]);
            data.append("<?php echo $fileController->submitValue;?>", "<?php echo $fileController->editProfilePicture;?>");

            for (var pair of data.entries()) {
                console.log(pair[0] + ', ' + pair[1]);
            }

            // disabled the submit button
            // $("#btnChangeProfilePicture").prop("disabled", true);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {

                    location.href = "/appointsync.com/public/profile.php/<?php echo $hostResponse->webID;?>";

                },
                error: function (e) {


                }
            });

        });


        $('#timepicker1').timepicki({start_time: ["<?php echo $timeFromHourValue;?>", "<?php echo $timeFromMinuteValue;?>", "<?php echo $timeFromCharValue;?>"]});
        $('#timepicker2').timepicki({start_time: ["<?php echo $timeToHourValue;?>", "<?php echo $timeToMinuteValue;?>", "<?php echo $timeToCharValue;?>"]});

    </script>


    <?php
}

elseif($loggedInUserType == 3){
    ?>
    <script>


        var modalPicture = document.getElementById('myModalProfileClient');
        var modalEditProfile = document.getElementById('myModalEditProfileClient');
        var modalChangePassword = document.getElementById("myModalPasswordClient");
        var modalViewNotificationPopUpgg = document.getElementById("myModalNotificationsPopup");

        // Get the button that opens the modal
        var btnEditProfileClient = document.getElementById("btnEditProfileClient") != null ? document.getElementById("btnEditProfileClient") : 0;
        var btnOpenSettingsClient = document.getElementById("btnOpenSettingsClient") != null ? document.getElementById("btnOpenSettingsClient") : 0;
        var btnChangePassword = document.getElementById("btnChangePassword") != null ? document.getElementById("btnChangePassword") : 0;
        var btnProfilePic = document.getElementById("btnProfilePic") != null ? document.getElementById("btnProfilePic") : 0;
        var btnViewNotificationsPopup = document.getElementById("showNotificationPopupLink") != null ? document.getElementById("showNotificationPopupLink") : 0;


        var btnCloseProfile = document.getElementById("btnCloseProfile");
        var btnCloseEditProfile = document.getElementById("btnCloseEditProfile");
        var btnCloseChangePassword = document.getElementById("btnCloseChangePassword");

        var spanPassword = document.getElementsByClassName("spanPassword")[0];
        var spanProfile = document.getElementsByClassName("spanProfile")[0];
        var spanEditProfile = document.getElementsByClassName("spanEditProfile")[0];



        // When the user clicks the button, open the modal

        $(btnViewNotificationsPopup).click(function () {
            // $(this).parent().find('#modalViewNotificationPopUpgg').css('display', 'block');
            $('.redDot').css('visibility', 'hidden');
            $(modalViewNotificationPopUpgg).css('display', 'block');
            $('body').css('overflow', 'hidden');

            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {'submit': 'readNotifications'}
            });

        });

        btnOpenSettingsClient.onclick = function () {
            window.location.href = "/appointsync.com/public/profilesetting.php/";
        }

        btnProfilePic.onclick = function () {
            modalPicture.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnEditProfileClient.onclick = function () {
            modalEditProfile.style.display = "block";
            $('body').css('overflow', 'hidden');
        }

        btnChangePassword.onclick = function () {
            modalChangePassword.style.display = "block";
            $('body').css('overflow', 'hidden');
        }


        btnCloseProfile.onclick = function () {
            modalPicture.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        btnCloseEditProfile.onclick = function () {
            modalEditProfile.style.display = "none";
            $('body').css('overflow', 'auto');
        }


        btnCloseChangePassword.onclick = function () {
            modalChangePassword.style.display = "none";
            $('body').css('overflow', 'auto');
        }


        spanProfile.onclick = function () {
            modalPicture.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanEditProfile.onclick = function () {
            modalEditProfile.style.display = "none";
            $('body').css('overflow', 'auto');
        }

        spanPassword.onclick = function () {
            modalChangePassword.style.display = "none";
            $('body').css('overflow', 'auto');
        }


        window.onclick = function (event) {
            if (event.target == modalChangePassword || event.target == modalEditProfile || event.target == modalPicture || event.target == modalViewNotificationPopUpgg) {
                modalChangePassword.style.display = "none";
                modalEditProfile.style.display = "none";
                modalPicture.style.display = "none";
                modalViewNotificationPopUpgg.style.display = "none";
                $('body').css('overflow', 'auto');
            }
        }

        $('#btnEditProfileSubmit').click(function () {

            var submitValue = '<?php echo $fileController->submitValue;?>';
            var submitData = '<?php echo $fileController->editClientProfile;?>';
            var clientFirstNameValue = '<?php echo $fileController->firstName;?>';
            var clientFirstNameData = document.getElementById("clientFirstName").value;
            var clientLastNameValue = '<?php echo $fileController->lastName;?>';
            var clientLastNameData = document.getElementById("clientLastName").value;


            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {
                    [submitValue]: submitData,
                    [clientFirstNameValue]: clientFirstNameData,
                    [clientLastNameValue]: clientLastNameData,
                },
                async: false,
                success: function (data) {
                    location.href = "/appointsync.com/public/profile.php/";
                }
            });

        });


        $("#btnChangeProfilePicture").click(function (event) {

            //stop submit the form, we will post it manually.
            event.preventDefault();

            // Get form
            var form = $('#fileUploadForm')[0];

            // Create an FormData object
            var data = new FormData(form);

            // If you want to add an extra field for the FormData
            data.append('file', $('[type=file]')[0].files[0]);
            data.append("<?php echo $fileController->submitValue;?>", "<?php echo $fileController->editClientProfilePicture;?>");

            for (var pair of data.entries()) {
                console.log(pair[0] + ', ' + pair[1]);
            }

            // disabled the submit button
            // $("#btnChangeProfilePicture").prop("disabled", true);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    location.href = "/appointsync.com/public/profile.php/";

                },
                error: function (e) {


                }
            });

        });


        function changePassword() {

            var oldPasswordData = document.getElementById("oldPassword").value;
            var newPasswordData = document.getElementById("newPassword").value;

            var submitValue = '<?php echo $fileController->submitValue;?>';
            var submitData = '<?php echo $fileController->changeClientPassword;?>';
            var oldPasswordValue = '<?php echo $fileController->oldPassword;?>';
            var newPasswordValue = '<?php echo $fileController->newPassword;?>';


            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {
                    [submitValue]: submitData,
                    [oldPasswordValue]: oldPasswordData,
                    [newPasswordValue]: newPasswordData
                },
                async: false,
                success: function (data) {

                    location.href = "/appointsync.com/public/profile.php/";

                }
            });


        }

       </script>

<?php
}
  //Location Alert handling

  if(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 1){

      echo '<script language="javascript">',
      'alert("The Location was successfully Deleted")',
      '</script>';
      sleep(2);

      unset($_SESSION["ProfileController"]);
  }elseif(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 2){

      echo '<script language="javascript">',
      'alert("The Location was successfully Added")',
      '</script>';

      sleep(2);

      unset($_SESSION["ProfileController"]);
  }elseif(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 3){

      echo '<script language="javascript">',
      'alert("Profile successfully updated")',
      '</script>';

      sleep(2);

      unset($_SESSION["ProfileController"]);
  }elseif(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 4){

      echo '<script language="javascript">',
      'alert("Premium membership successfully activated")',
      '</script>';

      sleep(2);

      unset($_SESSION["ProfileController"]);
  }elseif(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 5){

      echo '<script language="javascript">',
      'alert("Adding location failed")',
      '</script>';

      sleep(2);

      unset($_SESSION["ProfileController"]);
  }elseif(isset($_SESSION["ProfileController"]) && $_SESSION['ProfileController'] == 7){

      echo '<script language="javascript">',
      'alert("Delete location failed")',
      '</script>';

      sleep(2);

      unset($_SESSION["ProfileController"]);
  }
  //Profile Picture Handling
  if(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 1){

      echo '<script language="javascript">',
      'alert("ERROR: Your file was larger than 5 Megabytes in size.")',
      '</script>';
      sleep(2);

      unset($_SESSION["imageUploadStatus"]);
  }elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 2){

      echo '<script language="javascript">',
      'alert("ERROR: Your image was not .gif, .jpg, or .png.")',
      '</script>';
      sleep(2);

      unset($_SESSION["imageUploadStatus"]);
  }elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 3){

      echo '<script language="javascript">',
      'alert("ERROR: An error occured while processing the file. Try again.")',
      '</script>';
      sleep(2);

      unset($_SESSION["imageUploadStatus"]);
  }elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 4){

      echo '<script language="javascript">',
      'alert("Image successfully updated.")',
      '</script>';
      sleep(2);

      unset($_SESSION["imageUploadStatus"]);
  }elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 5){

      echo '<script language="javascript">',
      'alert("ERROR: File not uploaded. Try again.")',
      '</script>';
      sleep(2);

      unset($_SESSION["imageUploadStatus"]);
  }elseif(isset($_SESSION["imageUploadStatus"]) && $_SESSION['imageUploadStatus'] == 6){

      echo '<script language="javascript">',
      'alert("ERROR: Not file attached.")',
      '</script>';
      sleep(2);

      unset($_SESSION["imageUploadStatus"]);
  }elseif(isset($_SESSION["passwordController"]) && $_SESSION['passwordController'] == 1){

      echo '<script language="javascript">',
      'alert("Your Current Password is wrong.")',
      '</script>';
      sleep(1.3);

      unset($_SESSION["passwordController"]);
  }elseif(isset($_SESSION["passwordController"]) && $_SESSION['passwordController'] == 2){

      echo '<script language="javascript">',
      'alert("Passwords do not match.")',
      '</script>';
      sleep(1.3);

      unset($_SESSION["passwordController"]);
  }elseif(isset($_SESSION["passwordController"]) && $_SESSION['passwordController'] == 3){

      echo '<script language="javascript">',
      'alert("Passwords successfully updated.")',
      '</script>';
      sleep(1.3);

      unset($_SESSION["passwordController"]);
  }elseif(isset($_SESSION["profileRequestError"]) && $_SESSION['profileRequestError'] == 1){

      echo '<script language="javascript">',
      'alert("Something went wrong, please try again.")',
      '</script>';
      sleep(1.3);

      unset($_SESSION["profileRequestError"]);
  }

  ?>

</body>
</html>
