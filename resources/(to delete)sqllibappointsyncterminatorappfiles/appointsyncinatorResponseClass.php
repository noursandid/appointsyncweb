<?php

class Response
{

   private $errorCode = 0;
   private $errorDescription = '';
   private $data;

   function withError($errorC,$errorD)
    {
      $this->errorCode = $errorC;
      $this->errorDescription = $errorD;
      $this->data = null;

    }
    function withData($d)
     {
       $this->errorCode = 0;
       $this->errorDescription = 'No Error';
       $this->data = $d;

     }
   function getResponse() {
     return array('errorCode' => $this->errorCode,'errorDescription' => $this->errorDescription,'data'=>$this->data);
   }

}

?>
