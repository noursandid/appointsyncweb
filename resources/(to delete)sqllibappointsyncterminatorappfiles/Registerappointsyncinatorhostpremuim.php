<?php

require 'userwitedbappointsyncinator.php';


$fname = stripslashes($_POST["fname"]);
$fname = mysqli_real_escape_string($syncminatorcon, $_POST["fname"]);
$lname = stripslashes($_POST["lname"]);
$lname = mysqli_real_escape_string($syncminatorcon, $_POST["lname"]);
$username = stripslashes($_POST["username"]);
$username = mysqli_real_escape_string($syncminatorcon, $_POST["username"]);
$password = stripslashes($_POST["password"]);
$password = mysqli_real_escape_string($syncminatorcon, $_POST["password"]);
$confirmPassword = stripslashes($_POST["confirmPassword"]);
$confirmPassword = mysqli_real_escape_string($syncminatorcon, $_POST["confirmPassword"]);
if ($password == $confirmPassword){
$password = hash('sha512',$_POST["password"]);
}else {
    $notMatchPassword = 'true';
    echo $notMatchPassword;
}

$gender = stripslashes($_POST["gender"]);
$gender = mysqli_real_escape_string($syncminatorcon, $_POST["gender"]);
$email = stripslashes($_POST["email"]);
$email = mysqli_real_escape_string($syncminatorcon, $_POST["email"]);
$phone = stripslashes($_POST["phone"]);
$phone = mysqli_real_escape_string($syncminatorcon, $_POST["phone"]);
$workingFrom = stripslashes($_POST["workingFrom"]);
$workingFrom = mysqli_real_escape_string($syncminatorcon, $_POST["workingFrom"]);
$workingTo = stripslashes($_POST["workingTo"]);
$workingTo = mysqli_real_escape_string($syncminatorcon, $_POST["workingTo"]);
$profession = stripslashes($_POST["profession"]);
$profession = mysqli_real_escape_string($syncminatorcon, $_POST["profession"]);
$userType = stripslashes($_POST["usertype"]);
$DOD = stripslashes($_POST["dob-day"]);
$DOM = stripslashes($_POST["dob-month"]);
$DOY = stripslashes($_POST["dob-year"]);
$DOB = $DOD.'-'.$DOM.'-'.$DOY;
$dt = new DateTime();
$date = $dt->format('Y-m-d H:i:s');
function randomString($length = 25) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;

}


function gen_uuid() {
 $uuid = array(
  'time_low'  => 0,
  'time_mid'  => 0,
  'time_hi'  => 0,
  'clock_seq_hi' => 0,
  'clock_seq_low' => 0,
  'node'   => array()
 );

 $uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
 $uuid['time_mid'] = mt_rand(0, 0xffff);
 $uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
 $uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
 $uuid['clock_seq_low'] = mt_rand(0, 255);

 for ($i = 0; $i < 6; $i++) {
  $uuid['node'][$i] = mt_rand(0, 255);
 }

 $uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
  $uuid['time_low'],
  $uuid['time_mid'],
  $uuid['time_hi'],
  $uuid['clock_seq_hi'],
  $uuid['clock_seq_low'],
  $uuid['node'][0],
  $uuid['node'][1],
  $uuid['node'][2],
  $uuid['node'][3],
  $uuid['node'][4],
  $uuid['node'][5]
 );

 return $uuid;
}
$qrCode = gen_uuid();

$queryHostRegister = mysqli_query($syncminatorcon,"INSERT INTO USER (FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD, GENDER, TYPE, DOB, PHONE_NUMBER, QR_CODE) VALUES ('$fname', '$lname', '$email', '$username', '$password', '$gender', '$userType', '$DOB', '$phone', '$qrCode')");
$queryGetHostId = mysqli_query($syncminatorcon,"SELECT * FROM USER WHERE EMAIL = '" .$email. "' ");
    while($getHost = mysqli_fetch_assoc($queryGetHostId)){
        $hostID = $getHost['ID'];

$webKey = randomString().$hostID;
$queryInsertWorkingHours = mysqli_query($syncminatorcon,"INSERT INTO HOST_WORKING_HOURS (HOST_ID, FROM_TIME, TO_TIME) VALUES ('$hostID', '$workingFrom', '$workingTo')");
$queryInsertHostDetails = mysqli_query($syncminatorcon,"INSERT INTO HOST (PROFESSION, TYPE, SUBSCRIPTION_DATE, USER_ID) VALUES ('$profession', '2', '$date', '$hostID')");
$queryInsertWebId = mysqli_query($syncminatorcon,"UPDATE USER SET WEB_ID = '$webKey' WHERE ID = '$hostID' ");
$effectiveDate = date('Y-m-d', strtotime("+1 months"));
$queryInserHostPremuim = mysqli_query($syncminatorcon,"INSERT INTO HOST_PREMUIM (HOST_ID, TILL_DATE) VALUES ('$hostID', '$effectiveDate')");
header("Location: /appointsync.com/public/login.php");
}
 ?>
