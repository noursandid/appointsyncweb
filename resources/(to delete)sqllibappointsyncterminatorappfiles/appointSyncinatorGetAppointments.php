<?php

  require 'userwitedbappointsyncinator.php';
   include ("appointsyncinatorResponseClass.php");
   include ("Appointment.php");
   include ("Note.php");
   include ("Attachment.php");
   $userID = $_POST["userID"];

       // $response = new Response();
       // $array = array(['AppointmentID'=>'2', "AppointmentClientName"=>'Charbel Sawma']);
       // $response->withData(['Appointments'=> $array]);
       // // $response->withError('500','ErrorDescription');
       // // $response->Response1();
       // echo json_encode($response->getResponse());

       $getType = mysqli_query($syncminatorcon,"SELECT TYPE FROM USER WHERE  ID = '$userID';");
       while($getTypeRow = mysqli_fetch_assoc($getType)){
         $type = $getTypeRow['TYPE'];
  }
       if ($type == 3){
         $query = mysqli_query($syncminatorcon,"SELECT A.ID,A.HOST_ID,A.USER_ID,A.STATUS, A.DATE, A.START_TIME, A.END_TIME, A.HAS_ATTACHMENT, L.DSC, L.LATITUDE, L.LONGITUDE,U.USERNAME,U.ID AS USERID,U.FIRST_NAME,U.LAST_NAME,U.DOB,U.GENDER,U.PHONE_NUMBER,U.EMAIL,U.IMAGE_NAME,R.REASON FROM APPOINTMENT A INNER JOIN USER U ON A.HOST_ID = U.ID INNER JOIN HOST_LOCATION L ON L.ID = A.LOCATION LEFT JOIN APPOINTMENT_REJECT_REASON R ON A.ID = R.APPOINTMENT_ID WHERE  A.USER_ID = '$userID'");
         $array = array();

         while($getAppointmentRow = mysqli_fetch_assoc($query)){
           	$appointment = new Appointment($getAppointmentRow);
           	$hasAttachment = $getAppointmentRow["HAS_ATTACHMENT"];
		$notesQuery = mysqli_query($syncminatorcon,"SELECT N.ID,N.APPOINTMENT_ID,N.USER_ID,N.NOTE FROM APPOINTMENT_NOTE N WHERE APPOINTMENT_ID = '$appointment->appointmentID'");
		$notesArray = array();
	
		while ($getNotesRow = mysqli_fetch_assoc($notesQuery)){
			$note = new Note($getNotesRow);
			array_push($notesArray,$note);
		}
		$appointment->notes = $notesArray;
		
		if($hasAttachment == 1) {
			$attachmentsQuery = mysqli_query($syncminatorcon,"SELECT A.ID,A.APPOINTMENT_ID,A.USER_ID FROM APPOINTMENT_NOTE_ATTACHMENT A WHERE APPOINTMENT_ID = '$appointment->appointmentID'");
			$attachmentsArray = array();
	
			while ($getAttachmentsRow = mysqli_fetch_assoc($attachmentsQuery)){
				$attachment = new Attachment($getAttachmentsRow);
				array_push($attachmentsArray,$attachment);
			}
		
			$appointment->attachments = $attachmentsArray;
		}
		
//		if ($appointment->status != 3 && $appointment->status != 4){
           		array_push($array,$appointment);
//           	}
         }
//         $appointments = array("appointments"=>$array);
         $response = new Response();
         $response->withData($array);
         // $response->withError("500","error");
         echo json_encode($response->getResponse(), JSON_UNESCAPED_SLASHES);
       }
       else if ($type == 2){
         $query = mysqli_query($syncminatorcon,"SELECT A.ID,A.HOST_ID,A.USER_ID,A.STATUS, A.DATE, A.START_TIME, A.END_TIME, A.HAS_ATTACHMENT, L.DSC, L.LATITUDE, L.LONGITUDE,U.USERNAME,U.ID AS USERID,U.FIRST_NAME,U.LAST_NAME,U.DOB,U.GENDER,U.PHONE_NUMBER,U.EMAIL,I.IMAGE_NAME,R.REASON FROM APPOINTMENT A INNER JOIN USER U ON A.USER_ID = U.ID INNER JOIN HOST_LOCATION L ON L.ID = A.LOCATION LEFT JOIN APPOINTMENT_REJECT_REASON R ON A.ID = R.APPOINTMENT_ID LEFT JOIN USER_IMAGE I ON A.USER_ID = I.USER_ID WHERE  A.HOST_ID = '$userID'");
         $array = array();

         while($getAppointmentRow = mysqli_fetch_assoc($query)){
           	$appointment = new Appointment($getAppointmentRow);
           	$hasAttachment = $getAppointmentRow["HAS_ATTACHMENT"];
	
		$notesQuery = mysqli_query($syncminatorcon,"SELECT N.ID,N.APPOINTMENT_ID,N.USER_ID,N.NOTE FROM APPOINTMENT_NOTE N WHERE APPOINTMENT_ID = '$appointment->appointmentID'");
		$notesArray = array();
	
		while ($getNotesRow = mysqli_fetch_assoc($notesQuery)){
			$note = new Note($getNotesRow);
			array_push($notesArray,$note);
		}
		$appointment->notes = $notesArray;
		if($hasAttachment == 1) {
			$attachmentsQuery = mysqli_query($syncminatorcon,"SELECT A.ID,A.APPOINTMENT_ID,A.USER_ID,A.ATTACHMENT FROM APPOINTMENT_NOTE_ATTACHMENT A WHERE APPOINTMENT_ID = '$appointment->appointmentID'");
			$attachmentsArray = array();
	
			while ($getAttachmentsRow = mysqli_fetch_assoc($attachmentsQuery)){
				$attachment = new Attachment($getAttachmentsRow);
				array_push($attachmentsArray,$attachment);
		
			}
		
			$appointment->attachments = $attachmentsArray;
		}
		
//		if ($appointment->status != 3 && $appointment->status != 4){
           		array_push($array,$appointment);
//           	}
         }
         
//         $appointments = array("appointments"=>$array);

         $response = new Response();
         $response->withData($array);
         // $response->withError("500","error");
         echo json_encode($response->getResponse(), JSON_UNESCAPED_SLASHES);
       }
       else{
       $response = new Response();
       $response->withError(1,"User Not Found");
	echo json_encode($response->getResponse(), JSON_UNESCAPED_SLASHES);
       }

?>