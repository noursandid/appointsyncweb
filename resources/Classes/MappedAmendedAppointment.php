<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/7/18
 * Time: 10:39 PM
 */

class MappedAmendedAppointment
{
    public $appointmentID = '';
    public $appointmentHostID = '';
    public $appointmentClientID = '';
    public $requesterID = '';
    public $startDate = '';
    public $endDate = '';
    public $locationID = '';
    public $status = '';
    public $amendingUserID = '';

    function __construct($array)
    {
        $this->appointmentID = $array['appointmentID'];
        $this->appointmentHostID = $array['appointmentHostID'];
        $this->appointmentClientID = $array['appointmentClientID'];
        $this->requesterID = $array["requesterID"];
        $this->startDate = $array['startDate'];
        $this->endDate = $array['endDate'];
        $this->locationID = $array['locationID'];
        $this->status = $array['status'];
        $this->amendingUserID = $array['amendingUserID'];

    }
}