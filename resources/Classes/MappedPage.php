<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/12/18
 * Time: 10:01 PM
 */

require_once 'MappedElement.php';
require_once 'MappedFileAttachment.php';

class MappedPage
{

    public $id;
    public $number;
    public $elements;
    public $name;
    public $date;
    public $dob;
    public $lastAppointmentDate;
    public $gender;
    public $phoneNumber;
    public $webID;
    public $note;
    public $attachments;

    function __construct($array){
        $this->id = isset($array['id'])?$array['id']:'';
        $this->number = isset($array['number'])?$array['number']:'';
        $this->name = isset($array['name'])?$array['name']:'';
        $this->date = isset($array['date'])?$array['date']:'';
        $this->dob = isset($array['dob'])?$array['dob']:'';
        $this->lastAppointmentDate = isset($array['lastAppointmentDate'])?$array['lastAppointmentDate']:'';
        $this->gender = isset($array['gender'])?$array['gender']:'';
        $this->phoneNumber = isset($array['phoneNumber'])?$array['phoneNumber']:'';
        $this->webID = isset($array['webID'])?$array['webID']:'';
        $this->note = isset($array['note'])?$array['note']:'';
        $elementsArray = array();
        if (isset($array['elements'])){
            foreach($array['elements'] as $elem){
                $element = new MappedElement($elem);
                array_push($elementsArray,$element);
            }
        }
        $this->elements = $elementsArray;

        $attachmentsArray = array();
        if (isset($array['attachments'])){
            foreach($array['attachments'] as $fileAttachment){
                $attachment = new MappedFileAttachment($fileAttachment);
                array_push($attachmentsArray,$attachment);
            }

        }
        $this->attachments = $attachmentsArray;

    }

}