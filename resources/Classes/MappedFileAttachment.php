<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/15/18
 * Time: 12:14 AM
 */

class MappedFileAttachment
{


    public $id;
    public $webID;
    public $attachment;
    public $date;
    public $index;
    function __construct($array)
    {
        $this->id = isset($array['id'])?$array['id']:'';
        $this->webID = isset($array['webID'])?$array['webID']:'';
        $this->attachment = isset($array['attachment'])?$array['attachment']:'';
        $this->date = isset($array['date'])?$array['date']:'';
        $this->index = isset($array['index'])?$array['index']:'';

    }

}