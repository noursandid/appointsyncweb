<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/31/18
 * Time: 7:15 PM
 */

require_once 'MappedUser.php';
class UserProfile extends MappedUser
{
    public $workingHourFrom = '';
    public $workingHourTo = '';
    public $profession = '';
    public $subscriptionDate = '';
    public $tillDate = '';
    public $hiddenFiles = '';
    public $loggedInAs = '';
    public $locations;

    function __construct($array)
    {
        $this->workingHourFrom = isset($array['workingHourFrom'])?$array['workingHourFrom']:'';
        $this->workingHourTo = isset($array['workingHourTo'])?$array['workingHourTo']:'';
        $this->profession = isset($array['profession'])?$array['profession']:'';
        $this->subscriptionDate = isset($array['subscriptionDate'])?$array['subscriptionDate']:'';
        $this->tillDate = isset($array['tillDate'])?$array['tillDate']:'';
        $this->hiddenFiles = isset($array['hiddenFiles'])?$array['hiddenFiles']:'';
        $this->loggedInAs = isset($array['loggedInAs'])?$array['loggedInAs']:'';
        $this->buildUser($array);
    }
}
