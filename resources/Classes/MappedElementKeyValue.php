<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/12/18
 * Time: 10:00 PM
 */

class MappedElementKeyValue
{

    public $id;
    public $webID;
    public $key;
    public $value;

    function __construct($array)
    {
        $this->id = isset($array['id'])?$array['id']:'';
        $this->webID = isset($array['webID'])?$array['webID']:'';
        $this->key = isset($array['key'])?$array['key']:'';
        $this->value = isset($array['value'])?$array['value']:'';
    }

}