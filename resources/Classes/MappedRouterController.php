<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/20/18
 * Time: 8:02 PM
 */

class MappedRouterController
{



    public $submitValue = 'submit';

    //API Flags

    public $loginFlag = 'login';
    public $createAppointmentFlag = 'createAppointment';
    public $acceptAppointment = 'acceptAppointment';
    public $amendAppointment ='amendAppointment';
    public $cancelAmendedAppointment ='cancelAppointmentAmendment';
    public $deleteAppointment = 'deleteAppointment';
    public $rejectAppointment = 'rejectAppointment';
    public $addAppointmentNote = 'addAppointmentNote';
    public $addClient = 'addClient';
    public $linkClient = 'linkClient';
    public $removeClient = 'removeClient';
    public $addOfflineClient = 'addOfflineClient';
    public $addNewLocation = 'addNewLocation';
    public $deleteLocation = 'deleteLocation';
    public $addAttachmentNote = 'addAttachmentNote';
    public $readNotifications = 'readNotifications';
    public $getFileTemplate = 'getFileTemplate';
    public $updateValue = 'updateValue';
    public $updateFileNote = 'updateFileNote';
    public $addFilePage = 'addFilePage';
    public $addFileAttachment = 'addFileAttachment';
    public $saveCurrency = 'saveCurrency';
    public $addPaymentReceived = 'addPaymentReceived';
    public $addPaymentDue = 'addPaymentDue';
    public $changePassword = 'changePassword';
    public $changePermission = 'changePermission';
    public $changeUserPermission = 'changeUserPermission';
    public $hostChangeView = 'hostChangeView';
    public $editProfile = 'editProfile';
    public $editProfilePicture = 'editProfilePicture';
    public $changeSecureFileFlag = 'changeSecureFileFlag';
    public $verifyPin = 'verifyPin';
    public $changeTemplatePin = 'changeTemplatePin';
    public $registerHost = 'registerHost';
    public $registerHostPremium = 'registerHostPremium';
    public $getThreads = 'getThreads';
    public $getMessages = 'getMessages';
    public $postContactMessage = 'postContactMessage';
    public $renewPremiumSubscription = 'renewPremiumSubscription';
    public $resetPassword = 'resetPassword';
    public $setResetPassword = 'setResetPassword';

    //Clients

    public $registerClient = 'registerClient';
    public $editClientProfile = 'editClientProfile';
    public $editClientProfilePicture = 'editClientProfilePicture';
    public $addHost = 'addHost';

    //Login Elements

    public $username = 'username';
    public $password = 'password';
    public $newPassword = 'newPassword';
    public $oldPassword = 'oldPassword';


    //Appointments

    public $selectClient = 'selectClient';
    public $dateTimeFromUTC = 'dateTimeFromUTC';
    public $dateTimeToUTC = 'dateTimeToUTC';
    public $selectLocation = 'selectLocation';
    public $newAppointmentComment = 'newAppointmentComment';

    public $appointmentWebID = 'appointmentWebID';

    public $rejectReason = 'rejectReason';


    //Host Profile

    public $firstName = 'firstName';
    public $lastName = 'lastName';
    public $profession = 'profession';
    public $workingHourFrom = 'workingHourFrom';
    public $workingHourTo = 'workingHourTo';
    public $secureFileValue = 'secureFile';
    public $pinValue = 'pinCode';
    public $renewPremiumPeriodFlag = 'premium';
    public $changeClientPassword = 'changeClientPassword';





    //Clients

    public $clientWebID = 'clientWebID';

    public $clientUsername = 'clientUsername';

    public $clientFirstName = 'clientFirstName';
    public $clientMiddleName = 'clientMiddleName';
    public $clientLastName = 'clientLastName';
    public $genderSelect = 'genderSelect';
    public $offlineClientDOB = 'offlineClientDOB';
    public $phoneNumber = 'phoneNumber';


    //Profile


    public $locationWebID = 'locationWebID';
    public $profilePicture = 'file';


    public $locationTitle = 'locationTitle';
    public $locationLongitude = 'locationLongitude';
    public $locationLatitude = 'locationLatitude';


    public $resetPasswordToken = 'token';


    //Permissions


    public $permissionValue = 'permission';
    public $permissionSentValue = 'permissionValue';

    public $permissionToRequest = '1';
    public $permissionToAccept = '2';
    public $permissionToAmend = '3';
    public $permissionToCancel = '4';


    //Change View

    public $loggedInAs = 'loggedInAs';
    public $loggedInAsValueHost = '0';
    public $loggedInAsValueClient = '1';


    //Registration


    public $rUsername = 'username';
    public $rPassword = 'password';
    public $rConfirmPassword = 'confirmPassword';
    public $rFirstName = 'firstName';
    public $rLastName = 'lastName';
    public $rProfession = 'profession';
    public $rClientFileTemplate = 'rClientFileTemplate';
    public $rEmail = 'email';
    public $rGender = 'gender';
    public $rPhoneNumber = 'phoneNumber';
    public $rWorkingHourFrom = 'workingHourFrom';
    public $rWorkingHourTo = 'workingHourTo';
    public $rDateOfBirth = 'dob';
    public $countryCode = 'countryCode';


    //Threads

    public $threadWebID = 'threadWebID';
    public $threadTitle = 'threadTitle';
    public $threadMessage = 'message';
    public $threadToWebID = 'toWebID';





}