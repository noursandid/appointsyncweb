<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/31/18
 * Time: 7:15 PM
 */

class MappedLocation
{
    public $locationID;
    public $locationWebID;
    public $locationTitle;
    public $locationLongitude;
    public $locationLatitude;
    function __construct($array)
    {
        $this->locationID = isset($array['locationID'])?$array['locationID']:'';
        $this->locationWebID = isset($array['locationWebID'])?$array['locationWebID']:'';
        $this->locationTitle = isset($array['locationTitle'])?$array['locationTitle']:'';
        $this->locationLongitude = isset($array['locationLongitude'])?$array['locationLongitude']:'';
        $this->locationLatitude = isset($array['locationLatitude'])?$array['locationLatitude']:'';
        
    }
}
