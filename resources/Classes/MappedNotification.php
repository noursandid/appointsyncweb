<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/12/18
 * Time: 3:00 AM
 */

class MappedNotification
{


    public $id = '';
    public $fromWebID = '';
    public $toID = '';
    public $appointmentWebID;
    public $description = '';
    public $type = '';
    public $status = '';
    public $post = '';
    public $date = '';
    function __construct($array)
    {
        $this->id = isset($array['id']) ? $array['id'] : '';
        $this->fromWebID = isset($array['fromWebID']) ? $array['fromWebID'] : '';
        $this->toID = isset($array['toID']) ? $array['toID'] : '';
        $this->appointmentWebID = isset($array['appointmentWebID']) ? $array['appointmentWebID'] : '';
        $this->description = isset($array['description']) ? $array['description'] : '';
        $this->type = isset($array['type']) ? $array['type'] : '';
        $this->status = isset($array['status']) ? $array['status'] : '';
        $this->post = isset($array['post']) ? $array['post'] : '';
        $this->date = isset($array['date']) ? $array['date'] : '';

    }


}