<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/29/18
 * Time: 6:14 PM
 */

class MappedTemplates
{
    public $id;
    public $webID;
    public $name;

    function __construct($array)
    {
        $this->id = isset($array['id']) ? $array['id'] : '';
        $this->webID = isset($array['webID']) ? $array['webID'] : '';
        $this->name = isset($array['name']) ? $array['name'] : '';
    }

}