<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/12/18
 * Time: 9:53 PM
 */


require_once 'MappedElementKeyValue.php';

class MappedElement
{

    public $id;
    public $webID;
    public $index;
    public $name;
    public $typeWebID;
    public $dataKeyValueList;
    function __construct($array)
    {
        $this->id = isset($array['id'])?$array['id']:'';
        $this->webID = isset($array['webID'])?$array['webID']:'';
        $this->index = isset($array['index'])?$array['index']:'';
        $this->name = isset($array['name'])?$array['name']:'';
        $this->typeWebID = isset($array['typeWebID'])?$array['typeWebID']:'';
        $dataKeyValueListArray = array();
        if (isset($array['dataKeyValueList'])){
            foreach($array['dataKeyValueList'] as $dataKey){
                $dataKeyValue = new MappedElementKeyValue($dataKey);
                array_push($dataKeyValueListArray,$dataKeyValue);
            }
        }
        $this->dataKeyValueList = $dataKeyValueListArray;

    }





}