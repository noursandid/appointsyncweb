<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/1/18
 * Time: 12:47 AM
 */
require_once 'MappedClient.php';
require_once 'UserProfile.php';
require_once 'MappedAmendedAppointment.php';
class MappedAppointment
{
    public $appointmentID 		= '';
    public $webID			= '';
    public $appointmentHostID 		= '';
    public $requesterID			= '';
    public $startDate	 		= '';
    public $endDate			= '';
    public $locationID			= '';
    public $status 			= '';
    public $requestComment       = '';
    public $reasonOfRejection		= '';
    public $rejecterID			= '';
    public $isOffline           = '';
    public $hasAttachment           = '';
    public $amendedAppointment ;
    public $client		;
    public $host		;
    function __construct($array)
    {
        $this->appointmentID = $array['appointmentID'];
        $this->appointmentHostID = $array['appointmentHostID'];
        $this->webID = $array['webID'];
        $this->status = $array['status'];
        $this->requestComment = $array['requestComment'];
        $this->startDate = $array['startDate'];
        $this->endDate = $array['endDate'];
        $this->locationID = $array['locationID'];
        $this->requesterID = $array["requesterID"];
        $this->reasonOfRejection = $array['reasonOfRejection'];
        $this->rejecterID = $array['rejecterID'];
        $this->hasAttachment = $array['hasAttachment'];
        $this->isOffline = $array['isOffline'];
        $this->client = new MappedClient($array['client']);
        $this->host = new UserProfile($array['host']);
        $this->amendedAppointment = isset($array['amendedAppointment'])?new MappedAmendedAppointment($array['amendedAppointment']):null;
    }
}