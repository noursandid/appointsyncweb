<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/15/18
 * Time: 2:33 PM
 */

class MappedPayments
{


    public $id;

    public $date;

    public $paymentDue;

    public $paymentReceived;

    public $currency;

    public $remainingAmount;

    public $note;

    public $isTotal = false;
    function __construct($array)
    {
        $this->id = isset($array['id']) ? $array['id'] : '';
        $this->date = isset($array['date']) ? $array['date'] : '';
        $this->paymentDue = isset($array['paymentDue']) ? $array['paymentDue'] : '';
        $this->paymentReceived = isset($array['paymentReceived']) ? $array['paymentReceived'] : '';
        $this->currency = isset($array['currency']) ? $array['currency'] : '';
        $this->remainingAmount = isset($array['remainingAmount']) ? $array['remainingAmount'] : '';
        $this->note = isset($array['note']) ? $array['note'] : '';
        $this->isTotal = isset($array['isTotal']) ? $array['isTotal'] : '';
    }


}