<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/10/18
 * Time: 12:28 AM
 */

class MappedAttachment{

    public $attachmentID = '';
    public $attachmentAppointmentID= '';
    public $attachmentUserID = '';
    public $attachment='';
    public $isDeleted = '';
    function __construct($array)
    {
        $this->attachmentID = $array['attachmentID'];
        $this->attachmentAppointmentID = $array['attachmentAppointmentID'];
        $this->attachmentUserID = $array['attachmentUserID'];
        $this->attachment = $array['attachment'];
        $this->isDeleted = isset($array['isDeleted'])?$array['isDeleted']:'';
    }


}