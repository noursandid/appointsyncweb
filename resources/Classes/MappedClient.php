<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/31/18
 * Time: 7:15 PM
 */

require_once 'MappedUser.php';
class MappedClient extends MappedUser
{
    public $middleName;
    public $clientNumber;
    function __construct($array)
    {
       $this->middleName = $array['middleName'];
       $this->clientNumber = $array['clientNumber'];
        $this->buildUser($array);
        $this->identifier = $this->type == 3 ? $this->username : ($this->middleName == ''? $this->firstName.' '.$this->lastName:$this->firstName.' '.$this->middleName.' '.$this->lastName);
    }
}
