<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/31/18
 * Time: 11:42 PM
 */
include_once 'MappedAppointment.php';
include_once 'MappedLocation.php';
include_once 'MappedPage.php';
include_once 'MappedClientPermission.php';
include_once 'MappedTemplates.php';
include_once 'MappedThread.php';
include_once 'MappedMessage.php';
class MappedResponse{

    public $errorCode;
    public $errorDescription;
    public $data;
    public $success;

    public static function isAssoc($arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public function withData($response,$className)
    {
        $this->errorCode = $response['errorCode'];
        $this->errorDescription = $response['errorDescription'];
        $this->success = $response['errorCode'] == 0;

        if ($this->success) {
            if (isset($className) && $className != ''){
                if (!$this::isAssoc($response['data'])) {

                    $array = array();
                    if ($this->success) {
                        foreach ($response['data'] as $d) {
                            $instance = new $className($d);
                            array_push($array, $instance);
                        }
                        $this->data = $array;
                    }
                } else {
                    $this->data = new $className($response['data']);
                }
        }
        }

        return $this;
    }

    public function withError($errorCode,$errorDescription){
        $this->errorCode = $errorCode;
        $this->errorDescription = $errorDescription;
        $this->success = false;
        return $this;
    }

}