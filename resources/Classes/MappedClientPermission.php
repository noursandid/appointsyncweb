<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/27/18
 * Time: 8:47 PM
 */

class MappedClientPermission
{


    public $canCreateAppointment = '';
    public $canAcceptAppointment= '';
    public $canAmendAppointment = '';
    public $canCancelAppointment='';
    function __construct($array)
    {
        $this->canCreateAppointment = $array['canCreateAppointment'];
        $this->canAcceptAppointment = $array['canAcceptAppointment'];
        $this->canAmendAppointment = $array['canAmendAppointment'];
        $this->canCancelAppointment = $array['canCancelAppointment'];
    }



}