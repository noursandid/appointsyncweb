<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/9/18
 * Time: 11:33 PM
 */

class MappedAppointmentNote
{

    public $noteID = '';
    public $noteAppointmentID= '';
    public $date= '';
    public $noteUserID = '';
    public $note = '';
    public $isDeleted = '';
    function __construct($array)
    {
        $this->noteID = $array['noteID'];
        $this->noteAppointmentID = $array['noteAppointmentID'];
        $this->date = $array['date'];
        $this->noteUserID = $array['noteUserID'];
        $this->note = isset($array['note'])?$array['note']:'';
        $this->isDeleted = isset($array['isDeleted'])?$array['isDeleted']:'';
    }
}