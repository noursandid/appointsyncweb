<?php

class MappedUser{
    
    public $id = '';
    public $webID = '';
    public $username = '';
    public $firstName = '';
    public $lastName = '';
    public $dob = '';
    public $phoneNumber = '';
    public $gender = '';
    public $email = '';
    public $type = '';
    public $profilePic;
    public $isOffline;
    public $identifier;

    function buildUser($array)
    {
        $this->id = isset($array['id'])?$array['id']:'';
        $this->webID = isset($array['webID'])?$array['webID']:'';
        $this->username = isset($array['username'])?$array['username']:'';
        $this->firstName = isset($array['firstName'])?$array['firstName']:'';
        $this->lastName = isset($array['lastName'])?$array['lastName']:'';
        $this->dob = isset($array['dob'])?$array['dob']:'';
        $this->gender = isset($array['gender'])?$array['gender']:'';
        $this->phoneNumber = isset($array['phoneNumber'])?$array['phoneNumber']:'';
        $this->email = isset($array['email'])?$array['email']:'';
        $this->type = isset($array['type'])?$array['type']:'';
        $this->profilePic = isset($array["profilePic"])?$array["profilePic"]:'';
        if($this->profilePic == ''){
            $this->profilePic =   "<img id='userProfile' src='http://appointsync.com/dev/webroot/userProfile/default.jpeg' alt='Default'>";
        }else{
            $this->profilePic = "<img id='userProfile' src='" . $this->profilePic . "' alt='Profile'>";
        }
        $this->isOffline = isset($array['isOffline'])?$array['isOffline']:'';
        
    }


}
