<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/30/18
 * Time: 7:04 PM
 */

class MappedMessage
{

    public $id;

    public $threadID;

    public $fromWebID;

    public $toWebID;

    public $message;

    public $date;

    public $isDeleted;

    function __construct($array)
    {
        $this->id = isset($array['id']) ? $array['id'] : '';
        $this->threadID = isset($array['threadID']) ? $array['threadID'] : '';
        $this->fromWebID = isset($array['fromWebID']) ? $array['fromWebID'] : '';
        $this->toWebID = isset($array['toWebID']) ? $array['toWebID'] : '';
        $this->message = isset($array['message']) ? $array['message'] : '';
        $this->date = isset($array['date']) ? $array['date'] : '';
        $this->isDeleted = isset($array['isDeleted']) ? $array['isDeleted'] : '';
    }

}