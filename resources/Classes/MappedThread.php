<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/30/18
 * Time: 7:03 PM
 */

class MappedThread
{

    public $id;

    public $webID;

    public $title;

    public $fromWebID;

    public $fromUsername;

    public $toWebID;

    public $toUsername;

    public $lastMessage;

    public $lastUpdatedDate;

    function __construct($array)
    {
        $this->id = isset($array['id']) ? $array['id'] : '';
        $this->webID = isset($array['webID']) ? $array['webID'] : '';
        $this->title = isset($array['title']) ? $array['title'] : '';
        $this->fromWebID = isset($array['fromWebID']) ? $array['fromWebID'] : '';
        $this->fromUsername = isset($array['fromUsername']) ? $array['fromUsername'] : '';
        $this->toWebID = isset($array['toWebID']) ? $array['toWebID'] : '';
        $this->toUsername = isset($array['toUsername']) ? $array['toUsername'] : '';
        $this->lastMessage = isset($array['lastMessage']) ? $array['lastMessage'] : '';
        $this->lastUpdatedDate = isset($array['lastUpdatedDate']) ? $array['lastUpdatedDate'] : '';
    }

}