<?php

session_start();
if(isset($_SESSION['loggedUser']) && $_SESSION['loggedUser']['terminatorUserType'] == 2) {
}else{header("Location: /appointsync.com/");}
require 'userwitedbappointsyncinator.php';
$userID =  $_SESSION['loggedUser']['terminatorID'];
$webID = $_SESSION['loggedUser']['terminatorwebID'];
$loggedInUSerType = $_SESSION['loggedUser']['terminatorUserType'];
$submitFlag = mysqli_real_escape_string($syncminatorcon, $_POST["controllerTypeFlag"]);
$dateUpdated = gmdate("d-m-Y H:i:s");
// flag 1: Delete Location
// flag 2: New Location
// flag 3: Change Profile
// flag 4: Working hours
// flag 5: Premium account
// flag 6: Change password

if($submitFlag == 1){


    $locationLatitude = stripslashes($_POST["LATITUDE"]);
    $locationLatitude = mysqli_real_escape_string($syncminatorcon, $_POST["LATITUDE"]);

    $locationLongitude = stripslashes($_POST["LONGITUDE"]);
    $locationLongitude = mysqli_real_escape_string($syncminatorcon, $_POST["LONGITUDE"]);

    $queryNewLocation = mysqli_query($syncminatorcon,"DELETE FROM HOST_LOCATION WHERE LATITUDE = '$locationLatitude' AND LONGITUDE = '$locationLongitude' AND USER_ID = '$userID'");

    $_SESSION["ProfileController"] = 1;
    header("Location: /appointsync.com/public/profile.php");

}elseif($submitFlag == 2){

    $newLocationTitle = stripslashes($_POST["locationTitle"]);
    $newLocationTitle = mysqli_real_escape_string($syncminatorcon, $_POST["locationTitle"]);

    $locationLatitude = stripslashes($_POST["locationLatitude"]);
    $locationLatitude = mysqli_real_escape_string($syncminatorcon, $_POST["locationLatitude"]);

    $locationLongitude = stripslashes($_POST["locationLongitude"]);
    $locationLongitude = mysqli_real_escape_string($syncminatorcon, $_POST["locationLongitude"]);

    $queryNewLocation = mysqli_query($syncminatorcon,"INSERT INTO HOST_LOCATION (LONGITUDE, LATITUDE, DSC, USER_ID, LAST_UPDATED_DATE) VALUES
('$locationLongitude', '$locationLatitude', '$newLocationTitle', '$userID','$dateUpdated')");

    $_SESSION["ProfileController"] = 2;
    header("Location: /appointsync.com/public/profile.php");

}elseif($submitFlag == 3) {

    $fileName = $_FILES["file"]["name"]; // The file name
    $fileTmpLoc = $_FILES["file"]["tmp_name"]; // File in the PHP tmp folder
    $fileType = $_FILES["file"]["type"]; // The type of file it is
    $fileSize = $_FILES["file"]["size"]; // File size in bytes
    $fileErrorMsg = $_FILES["file"]["error"]; // 0 for false... and 1 for true
    $fileName = $userID . rand(0, 99999999) . preg_replace('#[^a-z.0-9]#i', '', $fileName);
    $kaboom = explode(".", $fileName); // Split file name into an array using the dot
    $fileExt = end($kaboom);

    if (!empty($fileTmpLoc)) { // if file not chosen


        if ($fileSize > 5242880) {
            $_SESSION["imageUploadStatus"] = 1;
            unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
            header("Location: /appointsync.com/public/profile.php");
            exit();
        } else if (!preg_match("/.(gif|jpg|png)$/i", $fileName)) {
            $_SESSION["imageUploadStatus"] = 2;
            unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
            header("Location: /appointsync.com/public/profile.php");
            exit();
        } else if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
            $_SESSION["imageUploadStatus"] = 3;
            header("Location: /appointsync.com/public/profile.php");
            exit();
        }

        $moveResult = move_uploaded_file($fileTmpLoc, "../../webroot/userProfile/$fileName");


        $queryGetProfiles = mysqli_query($syncminatorcon, "SELECT * FROM USER_IMAGE WHERE USER_ID = '$userID'");
        $num_rows = mysqli_num_rows($queryGetProfiles);
        if($num_rows == 0){
        $queryInsertProfile = mysqli_query($syncminatorcon, "INSERT INTO USER_IMAGE (IMAGE_NAME, USER_ID) VALUES
          ('$fileName', '$userID')");
        }else {
        $queryUpdateProfile = mysqli_query($syncminatorcon, "UPDATE USER_IMAGE SET IMAGE_NAME = '$fileName' WHERE USER_ID = '$userID'");
        $queryUpdateUserDateChange = mysqli_query($syncminatorcon, "UPDATE USER SET LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$userID'");

        }
        $_SESSION["imageUploadStatus"] = 4;
        header("Location: /appointsync.com/public/profile.php");

// Check to make sure the move result is true before continuing
        if ($moveResult != true) {
            $_SESSION["imageUploadStatus"] = 5;
            header("Location: /appointsync.com/public/profile.php");
            exit();
        }

    } else if (!$fileTmpLoc) {


        $_SESSION["imageUploadStatus"] = 6;
        header("Location: /appointsync.com/public/profile.php");
        exit();

    }

}elseif($submitFlag == 4){


    $firstName = stripslashes($_POST["firstName"]);
    $firstName = mysqli_real_escape_string($syncminatorcon, $_POST["firstName"]);
    $lastName = stripslashes($_POST["lastName"]);
    $lastName = mysqli_real_escape_string($syncminatorcon, $_POST["lastName"]);
    $profession = stripslashes($_POST["profession"]);
    $profession = mysqli_real_escape_string($syncminatorcon, $_POST["profession"]);
    $fromTime = stripslashes($_POST["fromTime"]);
    $fromTime = mysqli_real_escape_string($syncminatorcon, $_POST["fromTime"]);
    $fromTime  = date("H:i", strtotime($fromTime));
    $toTime = stripslashes($_POST["toTime"]);
    $toTime = mysqli_real_escape_string($syncminatorcon, $_POST["toTime"]);
    $toTime  = date("H:i", strtotime($toTime));

    $queryUpdateWorkingHours = mysqli_query($syncminatorcon,"UPDATE HOST_WORKING_HOURS SET FROM_TIME = '$fromTime', TO_TIME = '$toTime' WHERE HOST_ID = '$userID'");
    $queryUpdateName = mysqli_query($syncminatorcon,"UPDATE USER SET FIRST_NAME = '$firstName', LAST_NAME = '$lastName' WHERE ID = '$userID'");
    $queryUpdateProfession = mysqli_query($syncminatorcon,"UPDATE HOST SET PROFESSION = '$profession' WHERE USER_ID = '$userID'");
    $queryUpdateUserDateChange = mysqli_query($syncminatorcon, "UPDATE USER SET LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$userID'");

    $_SESSION["ProfileController"] = 3;
    header("Location: /appointsync.com/public/profile.php");

}elseif($submitFlag == 5){


    $dt1 = new DateTime();
    $today = $dt1->format("d-m-Y");

    $dt2 = new DateTime("+1 month");
    $date = $dt2->format("d-m-Y");

    $queryHostPremium = mysqli_query($syncminatorcon,"INSERT INTO HOST_PREMIUM (HOST_ID, TILL_DATE) VALUES ('$userID', '$date') ON DUPLICATE KEY UPDATE HOST_ID='$userID', TILL_DATE='$date';");
    $queryUpdateUserDateChange = mysqli_query($syncminatorcon, "UPDATE USER SET LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$userID'");

    $_SESSION["ProfileController"] = 4;
    header("Location: /appointsync.com/public/profile.php");

}elseif($submitFlag == 6){

    $submittedHostCurrentPassword = stripslashes($_POST["currentPassword"]);
    $submittedHostCurrentPassword = mysqli_real_escape_string($syncminatorcon, $_POST["currentPassword"]);
    $submittedHostCurrentPasswordHash = hash('sha512',$submittedHostCurrentPassword);

    $submittedHostNewPassword = stripslashes($_POST["newPassword"]);
    $submittedHostNewPassword = mysqli_real_escape_string($syncminatorcon, $_POST["newPassword"]);
    $submittedHostNewPasswordHash = hash('sha512',$submittedHostNewPassword);

    $submittedHostConfirmPassword = stripslashes($_POST["confirmPassword"]);
    $submittedHostConfirmPassword = mysqli_real_escape_string($syncminatorcon, $_POST["confirmPassword"]);
    $submittedHostConfirmPasswordHash = hash('sha512',$submittedHostConfirmPassword);

    $getHostCurrentPassword = mysqli_query($syncminatorcon, "SELECT PASSWORD FROM ONLINE_USER WHERE USER_ID = '$userID'");
    while ($rowGetHostCurrentPassword = mysqli_fetch_assoc($getHostCurrentPassword)) {
        $hostCurrentPassword = $rowGetHostCurrentPassword['PASSWORD'];

        if($submittedHostCurrentPasswordHash != $hostCurrentPassword){

            $_SESSION["passwordController"] = 1;
            header("Location: /appointsync.com/public/profile.php");

        }elseif($submittedHostCurrentPasswordHash == $hostCurrentPassword){

           if($submittedHostNewPasswordHash != $submittedHostConfirmPasswordHash){

               $_SESSION["passwordController"] = 2;
               header("Location: /appointsync.com/public/profile.php");

           }elseif($submittedHostNewPasswordHash == $submittedHostConfirmPasswordHash){

               $_SESSION["passwordController"] = 3;
               $queryUpdatePassword = mysqli_query($syncminatorcon,"UPDATE ONLINE_USER SET PASSWORD = '$submittedHostConfirmPasswordHash' WHERE USER_ID = '$userID'");
               $queryUpdateUserDateChange = mysqli_query($syncminatorcon, "UPDATE USER SET LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$userID'");
               header("Location: /appointsync.com/public/profile.php");
           }

        }
    }




}

?>