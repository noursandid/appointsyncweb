<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/24/18
 * Time: 6:30 PM
 */


require 'userwitedbappointsyncinator.php';

$date = date("d-m-Y");
$queryGetPassedAppointments = mysqli_query($syncminatorcon,"SELECT A.ID,A.DATE,A.HAS_AMENDMENT FROM APPOINTMENT A WHERE STR_TO_DATE(A.DATE,'%d-%m-%Y') < STR_TO_DATE('$date','%d-%m-%Y')");


while($rowGetPassedAppointments = mysqli_fetch_assoc($queryGetPassedAppointments)) {

    $archivedAppointmentId = $rowGetPassedAppointments['ID'];
    $archivedAppointmentHasAmendment = $rowGetPassedAppointments['HAS_AMENDMENT'];
    if($archivedAppointmentHasAmendment == 1){
        $queryDeleteAmendment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_AMENDMENT WHERE MAIN_APPOINTMENT_ID = '$archivedAppointmentId'");
    }
    $queryCheckRequestComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REQUEST_COMMENT WHERE APPOINTMENT_ID = '$archivedAppointmentId'");
    if($queryCheckRequestComment->num_rows > 0 ) {
        while ($rowGetRequestComment = mysqli_fetch_assoc($queryCheckRequestComment)) {
            $requestCommentId = $rowGetRequestComment['ID'];
            $queryDeleteRequestComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REQUEST_COMMENT WHERE ID = '$requestCommentId'");
        }
    }
    $queryCheckRejectComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REJECT_REASON WHERE APPOINTMENT_ID = '$archivedAppointmentId'");
    if($queryCheckRejectComment->num_rows > 0 ) {
        while ($rowGetRejectComment = mysqli_fetch_assoc($queryCheckRejectComment)) {
            $rejectCommentId = $rowGetRejectComment['ID'];
            $queryDeleteRejectReason = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_REASON WHERE ID = '$rejectCommentId'");
            $queryDeleteRejectComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_COMMENT WHERE ID = '$rejectCommentId'");
        }
    }
    $queryAmendmentFlagAppointment = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET HAS_AMENDMENT = '0' WHERE ID = '$archivedAppointmentId' ");
    $queryMoveAppointments = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT_ARCHIVE SELECT* FROM APPOINTMENT WHERE ID='$archivedAppointmentId' ");
    $queryDeleteAppointmentFromTable = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT WHERE ID = '$archivedAppointmentId'");


}