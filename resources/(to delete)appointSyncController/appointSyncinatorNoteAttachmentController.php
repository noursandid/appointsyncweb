<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/22/18
 * Time: 6:28 PM
 */

session_start();
if(isset($_SESSION['loggedUser']) && $_SESSION['loggedUser']['terminatorUserType'] == 2) {
}else{header("Location: /appointsync.com/");}
$webID = $_SESSION['StoreWebIDFromURI'];
require 'userwitedbappointsyncinator.php';
$userId =  $_SESSION['loggedUser']['terminatorID'];
$submitFlag = stripslashes($_POST["controllerTypeFlag"]);
$submitFlag = mysqli_real_escape_string($syncminatorcon, $_POST["controllerTypeFlag"]);
$dateUpdated = gmdate("d-m-Y H:i:s");
//If New
if($submitFlag == 0){

    $userTextNote = stripslashes($_POST["userTextNote"]);
    $userTextNote = mysqli_real_escape_string($syncminatorcon, $_POST["userTextNote"]);


    $queryGetMainAppointmentID = mysqli_query($syncminatorcon,"SELECT ID, USER_ID, HOST_ID FROM APPOINTMENT WHERE WEB_ID = '$webID'");
    while($rowGetMainAppointmentID = mysqli_fetch_assoc($queryGetMainAppointmentID)) {

        $getAppointmentID = $rowGetMainAppointmentID['ID'];
        $getAppointmentHostId = $rowGetMainAppointmentID['HOST_ID'];
        $getAppointmentUserId = $rowGetMainAppointmentID['USER_ID'];
        if($userId != $getAppointmentHostId){
            $notificationToId = $getAppointmentHostId;
            $notificationFromId = $getAppointmentUserId;
        }else{
            $notificationToId = $getAppointmentUserId;
            $notificationFromId = $getAppointmentHostId;
        }


        $queryAndRejectTable = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_NOTE (APPOINTMENT_ID, USER_ID, NOTE, LAST_UPDATED_DATE) VALUES
							('$getAppointmentID', '$userId','$userTextNote','$dateUpdated')");

        $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$notificationFromId', '$notificationToId', '$getAppointmentID','5','0','$dateUpdated')");



    }


    header("Location: /appointsync.com/public/appointmentnotes.php/".$webID."");

}elseif($submitFlag == 1){

    $fileName = $_FILES["file"]["name"]; // The file name
    $fileTmpLoc = $_FILES["file"]["tmp_name"]; // File in the PHP tmp folder
    $fileType = $_FILES["file"]["type"]; // The type of file it is
    $fileSize = $_FILES["file"]["size"]; // File size in bytes
    $fileErrorMsg = $_FILES["file"]["error"]; // 0 for false... and 1 for true
    $fileName = $userId . rand(0, 99999999) . preg_replace('#[^a-z.0-9]#i', '', $fileName);
    $kaboom = explode(".", $fileName); // Split file name into an array using the dot
    $fileExt = end($kaboom);

    if (!empty($fileTmpLoc)) { // if file not chosen


        if ($fileSize > 5242880) {
            $_SESSION["imageUploadStatus"] = 1;
            unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
            header("Location: /appointsync.com/public/appointmentnotes.php/".$webID."");
            exit();
        } else if (!preg_match("/.(gif|jpg|png)$/i", $fileName)) {
            $_SESSION["imageUploadStatus"] = 2;
            unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
            header("Location: /appointsync.com/public/appointmentnotes.php/".$webID."");
            exit();
        } else if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
            $_SESSION["imageUploadStatus"] = 3;
            header("Location: /appointsync.com/public/appointmentnotes.php/".$webID."");
            exit();
        }

        $moveResult = move_uploaded_file($fileTmpLoc, "../../webroot/attachment/$fileName");

        $queryGetMainAppointmentID = mysqli_query($syncminatorcon,"SELECT ID, USER_ID, HOST_ID FROM APPOINTMENT WHERE WEB_ID = '$webID'");
        while($rowGetMainAppointmentID = mysqli_fetch_assoc($queryGetMainAppointmentID)) {


            $getAppointmentID = $rowGetMainAppointmentID['ID'];
            $getAppointmentHostId = $rowGetMainAppointmentID['HOST_ID'];
            $getAppointmentUserId = $rowGetMainAppointmentID['USER_ID'];
            if($userId != $getAppointmentHostId){
                $notificationToId = $getAppointmentHostId;
                $notificationFromId = $getAppointmentUserId;
            }else{
                $notificationToId = $getAppointmentUserId;
                $notificationFromId = $getAppointmentHostId;
            }


            $queryUpdateAppointmentAttachment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT SET HAS_ATTACHMENT = '1' WHERE WEB_ID = '$webID'");
            $queryInsertPicture = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_NOTE_ATTACHMENT (APPOINTMENT_ID, USER_ID, ATTACHMENT, LAST_UPDATED_DATE) VALUES
          ('$getAppointmentID','$userId','$fileName', '$dateUpdated')");


            $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$notificationFromId', '$notificationToId', '$getAppointmentID','5','0','$dateUpdated')");


        }
        $_SESSION["imageUploadStatus"] = 4;
        header("Location: /appointsync.com/public/appointmentnotes.php/".$webID."");

// Check to make sure the move result is true before continuing
        if ($moveResult != true) {
            $_SESSION["imageUploadStatus"] = 5;
            header("Location: /appointsync.com/public/appointmentnotes.php/".$webID."");
            exit();
        }

    } else if (!$fileTmpLoc) {


        $_SESSION["imageUploadStatus"] = 6;
        header("Location: /appointsync.com/public/appointmentnotes.php/".$webID."");
        exit();

    }

}