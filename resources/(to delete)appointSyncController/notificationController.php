<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/25/18
 * Time: 7:47 PM
 */
session_start();
//error_reporting(E_PARSE);
if(isset($_SESSION['loggedUser']) && $_SESSION['loggedUser']['terminatorUserType'] == 2) {
}else{header("Location: /appointsync.com/");}
require 'userwitedbappointsyncinator.php';
include '../appointSyncClasses/Notification.php';
include '../appointSyncClasses/Response.php';
$userId =  $_SESSION['loggedUser']['terminatorID'];
$dateUpdated = gmdate("d-m-Y H:i:s");

if(isset($_POST["notificationFlag"]) && $_POST["notificationFlag"] == '1'){

    $getUserNotifications = mysqli_query($syncminatorcon, "SELECT N.ID AS NOTIFICATION_ID, N.FROM_ID AS NOTIFICATION_FROM_ID, N.TO_ID AS NOTIFICATION_TO_ID, N.DSC AS NOTIFICATION_DESCRIPTION, N.TYPE AS NOTIFICATION_TYPE , N.STATUS AS NOTIFICATION_STATUS, O.USERNAME, NT.NOTIFICATION_DES,  U.WEB_ID AS USER_WEB_ID FROM NOTIFICATION N INNER JOIN USER U ON U.ID = N.FROM_ID INNER JOIN ONLINE_USER O ON O.USER_ID = N.FROM_ID INNER JOIN NOTIFICATION_TYPE NT ON NT.ID = N.TYPE WHERE N.TO_ID = '$userId' AND N.STATUS = '0' ORDER BY N.ID DESC");
    while ($rowGetUserNotifications = mysqli_fetch_assoc($getUserNotifications)) {
        $notification = New Notification($rowGetUserNotifications);
        $notificationId = $notification->id;
        $notificationFromId = $notification->fromId;
        $notificationToId = $notification->toId;
        $notificationFromUserWebId = $rowGetUserNotifications['USER_WEB_ID'];
        $notificationDescription = $rowGetUserNotifications['NOTIFICATION_DES'];
        $notificationDescriptionId = $notification->description;
        $notificationType = $notification->type;
        $username = $rowGetUserNotifications['USERNAME'];

        $queryMoveNotificationsToArchive = mysqli_query($syncminatorcon, "INSERT INTO NOTIFICATION_HISTORY (ID, FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE)
                                    VALUES ('$notificationId', '$notificationFromId', '$notificationToId','$notificationDescriptionId','$notificationType','1','$dateUpdated')");

        $queryDeleteFromNotificationTable = mysqli_query($syncminatorcon, "DELETE FROM NOTIFICATION WHERE ID = '$notificationId'");
    }
    unset($_POST["notificationFlag"]);
}

$getUserNotifications = mysqli_query($syncminatorcon, "SELECT N.ID FROM NOTIFICATION N WHERE N.TO_ID = '$userId' AND N.STATUS = '0'");
$num_rows = mysqli_num_rows($getUserNotifications);

 $numberOfNotifications = $num_rows;

 if($numberOfNotifications == 0){

 }else{
     $numberOfNotificationsText = '<span class="redDot">'.$numberOfNotifications.'</span>';

 }

