<?php

include_once dirname(__FILE__) . '/../../Classes/UserProfile.php';
include_once 'APISettings.php';
include_once 'APIRequester.php';

class APIController
{

    public static function getProfile($userID){
        $body = array('userID'=>$userID);
        return APIRequester::sendRequest(APISettings::getProfileURL(),"",$body,"UserProfile");
    }

    public static function login($username,$password){
        $body = array('username'=>$username,'password'=>$password);
        return APIRequester::sendRequest(APISettings::hostLoginURL(),"",$body,"UserProfile");
    }
    public static function getClients(){
        return APIRequester::sendRequest(APISettings::hostGetClientURL(),"",'',"MappedClient");
    }
    public static function getLocations(){
        return APIRequester::sendRequest(APISettings::hostGetLocationsURL(), "", '', "MappedLocation");
    }
    
    public static function getAppointments(){
        return APIRequester::sendRequest(APISettings::hostGetAppointmentsURL(),"",'',"MappedAppointment");
    }


}

