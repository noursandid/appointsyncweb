<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/31/18
 * Time: 8:26 PM
 */
include_once dirname(__FILE__) .'/../../Classes/MappedResponse.php';
class APIRequester
{
    public static function sendRequest($url,$headerData,$bodyData,$className)
    {
        $content = json_encode($bodyData);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $userID = isset($_SESSION['host'])? unserialize($_SESSION['host'])->id:null;
        if (!isset($userID)){
            $hashedContent = hash("sha512", $content);
        }
        else{
            $hashedContent = hash("sha512", $content.$userID);
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","USERID: $userID","TOKEN: $hashedContent",$headerData));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response = json_decode($json_response, true);
        $mappedResponse = new MappedResponse();
        if ($status == 200) {
            $instance = $mappedResponse->withData($response,$className);

        }
        else{
            $instance = $mappedResponse->withError($status,"Request Failed");
        }
        return $instance;
    }
}