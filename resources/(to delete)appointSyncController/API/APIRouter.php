<?php
require 'apicontroller.php';

if (isset($_POST['submit'])) {
    $submit = $_POST['submit'];
    switch ($submit){
        case 'login':
            login();
            break;
        default:
            break;
    }
}

function login()
{
    $username = $_POST['username'];
    $pass = $_POST['password'];
    $password = hash("sha512", $pass);
    $loginResponse = APIController::login($username, $password);
    if ($loginResponse->success) {
        $host = $loginResponse->data;
        $_SESSION['host'] = serialize($host);
        header("Location: /appointsync.com/public/dashboard.php");
    } else {
        $_SESSION['loginCheck'] = $loginResponse->errorDescription;
        header("Location: /appointsync.com/public/login.php");
    }
}

function getClients(){
    $clientsResponse = APIController::getClients();
    if ($clientsResponse->success) {
        $clients = $clientsResponse->data;
        $_SESSION['clients'] = serialize($clients);
        header("Location: /appointsync.com/public/dashboard.php");
    } else {
        $_SESSION['GetClientsError'] = $clientsResponse->errorDescription;
        header("Location: /appointsync.com/public/dashboard.php");
    }
}