<?php
/**
 * Created by Charbel.
 * User: cgsawma
 * Date: 7/31/18
 * Time: 7:23 PM
 */






class APISettings
{

    //Main URLs

    private static function getAppointSyncHostURL(){
        return "localhost/apiDev/web/host/";
    }

    private static function getAppointSyncClientURL(){
        return "http://appointsync.com/api/client/";
    }


    //For host

    //Appointments APIs


    public static function hostAcceptAppointment()
    {
        return APISettings::getAppointSyncHostURL() .  "acceptAppointment.php";
    }

    public static function hostGetAppointmentsURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/getAppointments.php";
    }

    public static function hostAddAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "addAppointment.php";
    }

    public static function hostAmendAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "amendAppointment.php";
    }

    public static function hostCancelAmendmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "cancelAmendment.php";
    }

    public static function hostDeleteAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "deleteAppointment.php";
    }

    public static function hostRejectAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "rejectAppointment.php";
    }

    public static function hostShiftAppointmentsURL()
    {
        return APISettings::getAppointSyncHostURL() . "shiftAppointments.php";
    }


    //Clients

    public static function hostGetClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/getClients.php";
    }


    public static function hostAddClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "addClient.php";
    }

    public static function hostAddClientWithQRCodeURL()
    {
        return APISettings::getAppointSyncHostURL() . "addClientWithQRCode.php";
    }

    public static function hostGetClientWithQRCodeURL()
    {
        return APISettings::getAppointSyncHostURL() . "getClientWithQRCode.php";
    }

    public static function hostRemoveClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "removeClient.php";
    }

    //Profile

    public static function hostGetProfileURL()
    {
        return APISettings::getAppointSyncHostURL() . "getProfile.php";
    }

    public static function hostLoginURL()
    {
        return APISettings::getAppointSyncHostURL() . "handler/login.php";
    }

    public static function hostRegisterHostURL()
    {
        return APISettings::getAppointSyncHostURL() . "registerHost.php";
    }

    public static function hostRegisterHostPremiumURL()
    {
        return APISettings::getAppointSyncHostURL() . "registerHostPremium.php";
    }

    public static function hostUploadImageURL()
    {
        return APISettings::getAppointSyncHostURL() . "uploadImage.php";
    }


    //Offline clients

    public static function hostAddOfflineClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "addOfflineClient.php";
    }

    public static function hostGetOfflineClientsURL()
    {
        return APISettings::getAppointSyncHostURL() . "getOfflineClients.php";
    }

    public static function hostLinkClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "linkClient.php";
    }


    //Notes

    public static function hostAddNoteURL()
    {
        return APISettings::getAppointSyncHostURL() . "addNote.php";
    }

    public static function hostAttachmentsForAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "attachmentsForAppointment.php";
    }

    public static function hostNotesForAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "notesForAppointment.php";
    }


    //Location

    public static function hostAddLocationURL()
    {
        return APISettings::getAppointSyncHostURL() . "addLocation.php";
    }

    public static function hostDeleteLocationURL()
    {
        return APISettings::getAppointSyncHostURL() . "deleteLocation.php";
    }

    public static function hostGetLocationsURL()
    {
        return APISettings::getAppointSyncHostURL() . "location/getLocations.php";
    }

    public static function hostUpdateLocationURL()
    {
        return APISettings::getAppointSyncHostURL() . "updateLocation.php";
    }


    //Notification

    public static function hostGetNotificationURL()
    {
        return APISettings::getAppointSyncHostURL() . "notification.php";
    }



}
