<?php

session_start();
if(isset($_SESSION['loggedUser']) && $_SESSION['loggedUser']['terminatorUserType'] == 2) {
}else{header("Location: /appointsync.com/");}
$webID = $_SESSION['StoreWebIDFromURI'];
require 'userwitedbappointsyncinator.php';
$userId =  $_SESSION['loggedUser']['terminatorID'];
$submitFlag = stripslashes($_POST["controllerTypeFlag"]);
$submitFlag = mysqli_real_escape_string($syncminatorcon, $_POST["controllerTypeFlag"]);
$dateUpdated = gmdate("d-m-Y H:i:s");
//If New
if($submitFlag == 0){
function randomString($length = 25) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;

}

$clientSelect = stripslashes($_POST["clientSelect"]);
$clientSelect = mysqli_real_escape_string($syncminatorcon, $_POST["clientSelect"]);

$datePicked = stripslashes($_POST["datePicked"]);
$datePicked = mysqli_real_escape_string($syncminatorcon, $_POST["datePicked"]);

$fromHour = stripslashes($_POST["timeFrom"]);
$fromHour = mysqli_real_escape_string($syncminatorcon, $_POST["timeFrom"]);

$time_in_24_hour_format  = date("H:i", strtotime($fromHour));
$time_in_24_hour_format;

$toHour = stripslashes($_POST["timeTo"]);
$toHour = mysqli_real_escape_string($syncminatorcon, $_POST["timeTo"]);


$time_in_24_hour_formatTo  = date("H:i", strtotime($toHour));
$time_in_24_hour_formatTo;

$locationSelect = stripslashes($_POST["locationSelect"]);
$locationSelect = mysqli_real_escape_string($syncminatorcon, $_POST["locationSelect"]);

$newAppointmentComment = stripslashes($_POST["newAppointmentComment"]);
$newAppointmentComment = mysqli_real_escape_string($syncminatorcon, $_POST["newAppointmentComment"]);


if($newAppointmentComment == ""){
	$newAppointmentCommentFlag = 0;
}else{

$newAppointmentCommentFlag = 1;

}

    $queryGetAppointedUserType = mysqli_query($syncminatorcon,"SELECT TYPE FROM USER WHERE ID = '".$clientSelect."'");
    while($rowGetAppointedUserType = mysqli_fetch_assoc($queryGetAppointedUserType)) {
        $AppointedUserType = $rowGetAppointedUserType['TYPE'];
        if($AppointedUserType == 4){

            $queryNewAppointment = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT (USER_ID, HOST_ID, LOCATION, DATE, START_TIME, END_TIME, STATUS, REQUESTER_ID, HAS_ATTACHMENT,HAS_AMENDMENT,IS_OFFLINE,LAST_UPDATED_DATE) VALUES
              ('$clientSelect', '$userId', '$locationSelect', '$datePicked', '$time_in_24_hour_format', '$time_in_24_hour_formatTo', '3', '$userId', '0', '0','1','$dateUpdated')");

            $queryID = mysqli_insert_id($syncminatorcon);
            $webKey = randomString().$queryID;
            $queryInsertWebId = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET WEB_ID = '$webKey' WHERE ID = '$queryID' ");
            if($newAppointmentCommentFlag == 1){
                $queryNewAppointmentComment = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT_REQUEST_COMMENT (APPOINTMENT_ID, COMMENT, USER_ID) VALUES
              ('$queryID', '$newAppointmentComment', '$userId')");
            }


            $_SESSION["appointmentController"] = 0;
            header("Location: /appointsync.com/public/dashboard.php");
            exit();

        }else{}
    }

$queryNewAppointment = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT (USER_ID, HOST_ID, LOCATION, DATE, START_TIME, END_TIME, STATUS, REQUESTER_ID, HAS_ATTACHMENT,HAS_AMENDMENT,IS_OFFLINE,LAST_UPDATED_DATE) VALUES
('$clientSelect', '$userId', '$locationSelect', '$datePicked', '$time_in_24_hour_format', '$time_in_24_hour_formatTo', '1', '$userId', '0', '0','0','$dateUpdated')");

$queryID = mysqli_insert_id($syncminatorcon);
$webKey = randomString().$queryID;
$queryInsertWebId = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET WEB_ID = '$webKey' WHERE ID = '$queryID' ");
if($newAppointmentCommentFlag == 1){
$queryNewAppointmentComment = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT_REQUEST_COMMENT (APPOINTMENT_ID, COMMENT, USER_ID) VALUES
('$queryID', '$newAppointmentComment', '$userId')");
}

    $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$userId', '$clientSelect', '$queryID','0','0','$dateUpdated')");

$_SESSION["appointmentController"] = 0;
header("Location: /appointsync.com/public/dashboard.php");


}//If Accept
elseif($submitFlag == 1){

    $queryGetAppointmentId = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT WHERE WEB_ID = '$webID'");
    while($rowGetAppointmentId = mysqli_fetch_assoc($queryGetAppointmentId)){

        $appointmentAmendedId = $rowGetAppointmentId['ID'];


	$queryGetAmendmentFlag = mysqli_query($syncminatorcon,"SELECT HAS_AMENDMENT FROM APPOINTMENT WHERE WEB_ID = '$webID'");
	while($rowGetAmendmentFlag = mysqli_fetch_assoc($queryGetAmendmentFlag)){

		$getAmendmentFlag = $rowGetAmendmentFlag['HAS_AMENDMENT'];
		if ($getAmendmentFlag == 1){

				$queryGetAmendmendedValues = mysqli_query($syncminatorcon,"SELECT LOCATION, DATE, START_TIME, END_TIME FROM APPOINTMENT_AMENDMENT WHERE WEB_ID = '".$webID."'");
				while($rowGetAmendmendedValues = mysqli_fetch_assoc($queryGetAmendmendedValues)){

						$updatedLocation = $rowGetAmendmendedValues['LOCATION'];
						$updatedDate = $rowGetAmendmendedValues['DATE'];
						$updatedStartTime = $rowGetAmendmendedValues['START_TIME'];
						$updatedEndTime = $rowGetAmendmendedValues['END_TIME'];

						$queryUpdateAcceptAppointment = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET
							LOCATION = '$updatedLocation', DATE = '$updatedDate', START_TIME = '$updatedStartTime', END_TIME = '$updatedEndTime', STATUS = '3', HAS_AMENDMENT = '0', LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");


					$queryUpdateAcceptAppointment = mysqli_query($syncminatorcon,"DELETE FROM APPOINTMENT_AMENDMENT WHERE WEB_ID = '$webID'");

			}

            $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$userId', '$clientSelect', '$appointmentAmendedId','4','0','$dateUpdated')");

			header("Location: /appointsync.com/public/appointment.php/".$webID."");

		}elseif($getAmendmentFlag == 0){

            $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$userId', '$clientSelect', '$appointmentAmendedId','4','0','$dateUpdated')");

			$queryGetAppointmentDetails = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET  STATUS = '3', LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '".$webID."'");
			header("Location: /appointsync.com/public/appointment.php/".$webID."");
		}


// Check if there's request comment or reject and remove
        $queryCheckRequestComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REQUEST_COMMENT WHERE APPOINTMENT_ID = '$getAppointmentId'");
        if($queryCheckRequestComment->num_rows > 0 ) {
            while ($rowGetRequestComment = mysqli_fetch_assoc($queryCheckRequestComment)) {
                $requestCommentId = $rowGetRequestComment['ID'];
                $queryDeleteRequestComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REQUEST_COMMENT WHERE ID = '$requestCommentId'");
            }
        }
        $queryCheckRejectComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REJECT_REASON WHERE APPOINTMENT_ID = '$getAppointmentId'");
        if($queryCheckRejectComment->num_rows > 0 ) {
            while ($rowGetRejectComment = mysqli_fetch_assoc($queryCheckRejectComment)) {
                $rejectCommentId = $rowGetRejectComment['ID'];
                $queryDeleteRejectComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_REASON WHERE ID = '$rejectCommentId'");
            }
        }

	}

    }
}//if Amend
elseif($submitFlag == 2){

	$timeFromInHours = stripslashes($_POST["timeFrom"]);
	$timeFromInHours = mysqli_real_escape_string($syncminatorcon, $_POST["timeFrom"]);
    $timeFromInHours  = date("H:i", strtotime($timeFromInHours));


	$timeToInHours = stripslashes($_POST["timeTo"]);
	$timeToInHours = mysqli_real_escape_string($syncminatorcon, $_POST["timeTo"]);
    $timeToInHours  = date("H:i", strtotime($timeToInHours));

	$dateAppointment = stripslashes($_POST["datePicked"]);
	$dateAppointment = mysqli_real_escape_string($syncminatorcon, $_POST["datePicked"]);

	$locationSelect = stripslashes($_POST["locationSelect"]);
	$locationSelect = mysqli_real_escape_string($syncminatorcon, $_POST["locationSelect"]);

	$queryGetAppointmentDetails = mysqli_query($syncminatorcon,"SELECT * FROM APPOINTMENT WHERE WEB_ID = '$webID'");
	while($rowGetAppointmentDetails = mysqli_fetch_assoc($queryGetAppointmentDetails)){

			 $appointmentID = $rowGetAppointmentDetails['ID'];
			 $appointmentWebID = $rowGetAppointmentDetails['WEB_ID'];
			 $appointmentUserID = $rowGetAppointmentDetails['USER_ID'];
			 $appointmentHostID = $rowGetAppointmentDetails['HOST_ID'];
			 if($locationSelect == 0){
			 $appointmentLocation = $rowGetAppointmentDetails['LOCATION'];
		 }else{
			 $appointmentLocation = $locationSelect;
		 }
			 $appointmentDate = $rowGetAppointmentDetails['DATE'];
			 $appointmentStartTime = $rowGetAppointmentDetails['START_TIME'];
			 $appointmentEndTime = $rowGetAppointmentDetails['END_TIME'];
			 $appointmentStatus = $rowGetAppointmentDetails['STATUS'];
			 $appointmentRequesterID = $rowGetAppointmentDetails['REQUESTER_ID'];
			 $appointmentHasAttachment = $rowGetAppointmentDetails['HAS_ATTACHMENT'];
			 $appointmentHasAmendment = $rowGetAppointmentDetails['HAS_AMENDMENT'];
			 $appointmentLastUpdatedDate = $rowGetAppointmentDetails['LAST_UPDATED_DATE'];

			 if($userId == $appointmentHostID){
			     $notificationToId = $appointmentUserID;
             }else{
                 $notificationToId = $appointmentHostID;
             }

        $queryGetUserType = mysqli_query($syncminatorcon,"SELECT TYPE FROM USER WHERE ID = '$appointmentUserID'");
        while($rowGetUserType = mysqli_fetch_assoc($queryGetUserType)) {

            $amendedUserAppointmentType = $rowGetUserType['TYPE'];

            if ($amendedUserAppointmentType != 4) {

                if ($appointmentHasAmendment == 0) {

                    $querySetAmendAppointment = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_AMENDMENT (WEB_ID, USER_ID, HOST_ID, LOCATION, DATE, START_TIME, END_TIME, STATUS, REQUESTER_ID, MAIN_APPOINTMENT_ID, HAS_ATTACHMENT, AMENDING_USER_ID) VALUES
		            ('$appointmentWebID', '$appointmentUserID', '$appointmentHostID', '$appointmentLocation', '$dateAppointment', '$timeFromInHours', '$timeToInHours', '$appointmentStatus', '$appointmentRequesterID', '$appointmentID', '$appointmentHasAttachment', '$userId')");

                    $queryUpdateAppointment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT SET STATUS = '2', HAS_AMENDMENT = '1', LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$appointmentID' ");
                    header("Location: /appointsync.com/public/appointment.php/" . $webID . "");

                    $queryAddNotification = mysqli_query($syncminatorcon, "INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$userId', '$notificationToId', '$appointmentID','1','0','$dateUpdated')");


                } elseif ($appointmentHasAmendment == 1) {

                    $queryRejectAppointment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT_AMENDMENT SET LOCATION = '$appointmentLocation', DATE = '$dateAppointment', START_TIME = '$timeFromInHours', END_TIME = '$timeToInHours', AMENDING_USER_ID = '$userId'  WHERE MAIN_APPOINTMENT_ID = '$appointmentID' ");
                    header("Location: /appointsync.com/public/appointment.php/" . $webID . "");

                    $queryAddNotification = mysqli_query($syncminatorcon, "INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$userId', '$notificationToId', '$appointmentID','1','0','$dateUpdated')");
                }

            } elseif ($amendedUserAppointmentType == 4) {

                $queryUpdateAppointment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT SET START_TIME = '$timeFromInHours', END_TIME = '$timeToInHours', DATE = '$dateAppointment', LOCATION = '$appointmentLocation', LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$appointmentID' ");
                header("Location: /appointsync.com/public/appointment.php/" . $webID . "");


            }

        }
}

}//if Cancel Amend
elseif($submitFlag == 3){

	$queryGetMainAppointmentID = mysqli_query($syncminatorcon,"SELECT ID, USER_ID, HOST_ID FROM APPOINTMENT WHERE WEB_ID = '$webID'");
	while($rowGetMainAppointmentID = mysqli_fetch_assoc($queryGetMainAppointmentID)){

	$getAppointmentId = $rowGetMainAppointmentID['ID'];
        $getAppointmentHostId = $rowGetMainAppointmentID['HOST_ID'];
        $getAppointmentUserId = $rowGetMainAppointmentID['USER_ID'];
        if($userId != $getAppointmentHostId){
            $notificationToId = $getAppointmentHostId;
            $notificationFromId = $getAppointmentUserId;
        }else{
            $notificationToId = $getAppointmentUserId;
            $notificationFromId = $getAppointmentHostId;
        }

	$queryGetMainAppointmentStatus = mysqli_query($syncminatorcon,"SELECT STATUS FROM APPOINTMENT_AMENDMENT WHERE MAIN_APPOINTMENT_ID = '$getAppointmentId'");
	while($rowGetMainAppointmentStatus = mysqli_fetch_assoc($queryGetMainAppointmentStatus)){

		$mainAppointmentStatus = $rowGetMainAppointmentStatus['STATUS'];
		$queryRejectAppointment = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET STATUS = '$mainAppointmentStatus', HAS_AMENDMENT = '0', LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$getAppointmentId' ");

}

$queryCancelAmmendAppointment = mysqli_query($syncminatorcon,"DELETE FROM APPOINTMENT_AMENDMENT WHERE MAIN_APPOINTMENT_ID = '$getAppointmentId'");



        $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$notificationFromId', '$notificationToId', '$getAppointmentId','6','0','$dateUpdated')");


    }

header("Location: /appointsync.com/public/appointment.php/".$webID."");

}//if Rejected
elseif($submitFlag == 4){

$rejectAppointmentComment = stripslashes($_POST["rejectMessage"]);
$rejectAppointmentComment = mysqli_real_escape_string($syncminatorcon, $_POST["rejectMessage"]);

$queryGetAmendmentFlag = mysqli_query($syncminatorcon,"SELECT ID, HAS_AMENDMENT, USER_ID, HOST_ID FROM APPOINTMENT WHERE WEB_ID = '$webID'");
while($rowGetAmendmentFlag = mysqli_fetch_assoc($queryGetAmendmentFlag)){

	$getAppointmentId = $rowGetAmendmentFlag['ID'];
	$getAmendmentFlag = $rowGetAmendmentFlag['HAS_AMENDMENT'];
    $getAppointmentHostId = $rowGetAmendmentFlag['HOST_ID'];
    $getAppointmentUserId = $rowGetAmendmentFlag['USER_ID'];
    if($userId != $getAppointmentHostId){
        $notificationToId = $getAppointmentHostId;
        $notificationFromId = $getAppointmentUserId;
    }else{
        $notificationToId = $getAppointmentUserId;
        $notificationFromId = $getAppointmentHostId;
    }

	if ($getAmendmentFlag == 1){

					$queryRejectAmendAppointment = mysqli_query($syncminatorcon,"DELETE FROM APPOINTMENT_AMENDMENT WHERE WEB_ID = '$webID'");
					$queryRejectAppointment = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET STATUS = '4', HAS_AMENDMENT = '0', LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");


					$queryAndRejectTable = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT_REJECT_REASON (APPOINTMENT_ID, USER_ID) VALUES
					('$getAppointmentId', '$userId')");

        $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$notificationFromId', '$notificationToId', '$getAppointmentId','2','0','$dateUpdated')");

					if($rejectAppointmentComment != ""){

							$queryID = mysqli_insert_id($syncminatorcon);
							$queryAndRejectTable = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT_REJECT_COMMENT (ID, COMMENT) VALUES
							('$queryID', '$rejectAppointmentComment')");

						header("Location: /appointsync.com/public/appointment.php/".$webID."");
					}else{

						header("Location: /appointsync.com/public/appointment.php/".$webID."");

					}


		}elseif($getAmendmentFlag == 0){


			$queryRejectAppointment = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET STATUS = '4', HAS_AMENDMENT = '0' WHERE WEB_ID = '$webID' ");


			$queryAndRejectTable = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT_REJECT_REASON (APPOINTMENT_ID, USER_ID) VALUES
			('$getAppointmentId', '$userId')");

        $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$notificationFromId', '$notificationToId', '$getAppointmentId','2','0','$dateUpdated')");


        if($rejectAppointmentComment != ""){

					$queryID = mysqli_insert_id($syncminatorcon);
					$queryAndRejectTable = mysqli_query($syncminatorcon,"INSERT INTO APPOINTMENT_REJECT_COMMENT (ID, COMMENT) VALUES
					('$queryID', '$rejectAppointmentComment')");

				header("Location: /appointsync.com/public/appointment.php/".$webID."");
			}else{

				header("Location: /appointsync.com/public/appointment.php/".$webID."");

			}

	}


// Check if there's request comment or reject and remove
    $queryCheckRequestComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REQUEST_COMMENT WHERE APPOINTMENT_ID = '$getAppointmentId'");
    if($queryCheckRequestComment->num_rows > 0 ) {
        while ($rowGetRequestComment = mysqli_fetch_assoc($queryCheckRequestComment)) {
            $requestCommentId = $rowGetRequestComment['ID'];
            $queryDeleteRequestComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REQUEST_COMMENT WHERE ID = '$requestCommentId'");
        }
    }


}


}//if Delete
elseif($submitFlag == 5){

	$queryGetAmendmentFlag = mysqli_query($syncminatorcon,"SELECT ID, HAS_AMENDMENT, USER_ID, HOST_ID FROM APPOINTMENT WHERE WEB_ID = '$webID'");
	while($rowGetAmendmentFlag = mysqli_fetch_assoc($queryGetAmendmentFlag)){

		$getAppointmentId = $rowGetAmendmentFlag['ID'];
		$getAmendmentFlag = $rowGetAmendmentFlag['HAS_AMENDMENT'];
        $getAppointmentHostId = $rowGetAmendmentFlag['HOST_ID'];
        $getAppointmentUserId = $rowGetAmendmentFlag['USER_ID'];
        if($userId != $getAppointmentHostId){
            $notificationToId = $getAppointmentHostId;
            $notificationFromId = $getAppointmentUserId;
        }else{
            $notificationToId = $getAppointmentUserId;
            $notificationFromId = $getAppointmentHostId;
        }

        // Check if there's request comment or reject and remove
        $queryCheckRequestComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REQUEST_COMMENT WHERE APPOINTMENT_ID = '$getAppointmentId'");
        if($queryCheckRequestComment->num_rows > 0 ) {
            while ($rowGetRequestComment = mysqli_fetch_assoc($queryCheckRequestComment)) {
                $requestCommentId = $rowGetRequestComment['ID'];
                $queryDeleteRequestComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REQUEST_COMMENT WHERE ID = '$requestCommentId'");
            }
        }
        $queryCheckRejectComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REJECT_REASON WHERE APPOINTMENT_ID = '$getAppointmentId'");
        if($queryCheckRejectComment->num_rows > 0 ) {
            while ($rowGetRejectComment = mysqli_fetch_assoc($queryCheckRejectComment)) {
                $rejectCommentId = $rowGetRejectComment['ID'];
                $queryDeleteRejectReason = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_REASON WHERE ID = '$rejectCommentId'");
                $queryDeleteRejectComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_COMMENT WHERE ID = '$rejectCommentId'");
            }
        }


        $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$notificationFromId', '$notificationToId', '$getAppointmentId','3','0','$dateUpdated')");



		if ($getAmendmentFlag == 1){

						$queryDeleteAmendAppointment = mysqli_query($syncminatorcon,"DELETE FROM APPOINTMENT_AMENDMENT WHERE WEB_ID = '$webID'");
						$queryRejectAppointment = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET STATUS = '5', HAS_AMENDMENT = '0', LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");
					    $queryMoveDeletedAppointment = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_ARCHIVE SELECT* FROM APPOINTMENT WHERE WEB_ID='$webID' ");
                        $queryUpdateLastDate = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT_ARCHIVE SET LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");
                        $queryDeleteAppointment = mysqli_query($syncminatorcon,"DELETE FROM APPOINTMENT WHERE WEB_ID = '$webID'");


							header("Location: /appointsync.com/public/dashboard.php/");



			}elseif($getAmendmentFlag == 0){


				$queryRejectAppointment = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET STATUS = '5', LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");
                $queryMoveDeletedAppointment = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_ARCHIVE SELECT* FROM APPOINTMENT WHERE WEB_ID='$webID' ");
                $queryUpdateLastDate = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT_ARCHIVE SET LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");
                $queryDeleteAppointment = mysqli_query($syncminatorcon,"DELETE FROM APPOINTMENT WHERE WEB_ID = '$webID'");


					header("Location: /appointsync.com/public/dashboard.php/");

}

    }
}//if removed
elseif($submitFlag == 6){



    $queryGetAmendmentFlag = mysqli_query($syncminatorcon,"SELECT ID, HAS_AMENDMENT, IS_OFFLINE FROM APPOINTMENT WHERE WEB_ID = '$webID'");
    while($rowGetAmendmentFlag = mysqli_fetch_assoc($queryGetAmendmentFlag)) {

        $getAppointmentId = $rowGetAmendmentFlag['ID'];


        if (isset($getAppointmentId)) {
            $appointmentIsArchived = 'false';

            $getAppointmentIsOffline = $rowGetAmendmentFlag['IS_OFFLINE'];
            $getAmendmentFlag = $rowGetAmendmentFlag['HAS_AMENDMENT'];

            if($getAppointmentIsOffline == '1'){

                $queryDeleteAppointment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT SET STATUS = '5', LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");
                $queryMoveDeletedAppointment = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_ARCHIVE SELECT* FROM APPOINTMENT WHERE WEB_ID='$webID' ");
                header("Location: /appointsync.com/public/dashboard.php/");

            }else {

                if ($getAmendmentFlag == 1) {
                    $queryDeleteAmendAppointment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_AMENDMENT WHERE WEB_ID = '$webID'");
                    $queryRejectAppointment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT SET HAS_AMENDMENT = '0', LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");
                }


                $queryMoveDeletedAppointment = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_ARCHIVE SELECT* FROM APPOINTMENT WHERE WEB_ID='$webID' ");
                $queryUpdateLastDate = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT_ARCHIVE SET LAST_UPDATED_DATE = '$dateUpdated' WHERE WEB_ID = '$webID' ");
                $queryDeleteAppointment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT WHERE WEB_ID = '$webID'");
                $queryInsertDeletedAppointmentFlag = mysqli_query($syncminatorcon, "INSERT INTO DELETED_APPOINTMENT (APPOINTMENT_ID, USER_ID) VALUES ('$getAppointmentId', '$userId')");

// Check if there's request comment or reject and remove
                $queryCheckRequestComment = mysqli_query($syncminatorcon, "SELECT ID FROM APPOINTMENT_REQUEST_COMMENT WHERE APPOINTMENT_ID = '$getAppointmentId'");
                if ($queryCheckRequestComment->num_rows > 0) {
                    while ($rowGetRequestComment = mysqli_fetch_assoc($queryCheckRequestComment)) {
                        $requestCommentId = $rowGetRequestComment['ID'];
                        $queryDeleteRequestComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REQUEST_COMMENT WHERE ID = '$requestCommentId'");
                    }
                }
                $queryCheckRejectComment = mysqli_query($syncminatorcon, "SELECT ID FROM APPOINTMENT_REJECT_REASON WHERE APPOINTMENT_ID = '$getAppointmentId'");
                if ($queryCheckRejectComment->num_rows > 0) {
                    while ($rowGetRejectComment = mysqli_fetch_assoc($queryCheckRejectComment)) {
                        $rejectCommentId = $rowGetRejectComment['ID'];
                        $queryDeleteRejectReason = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_REASON WHERE ID = '$rejectCommentId'");
                        $queryDeleteRejectComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_COMMENT WHERE ID = '$rejectCommentId'");
                    }
                }

                //check to delete Notes
                $queryCheckRemoveCount = mysqli_query($syncminatorcon, "SELECT ID FROM DELETED_APPOINTMENT WHERE APPOINTMENT_ID = '$getAppointmentId'");
                if ($queryCheckRemoveCount->num_rows > 1) {
                    while ($rowCheckRemoveCount = mysqli_fetch_assoc($queryCheckRemoveCount)) {
                        $deletedAppointmentCounterId = $rowCheckRemoveCount['APPOINTMENT_ID'];
                        $queryDeleteNotes = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE WHERE APPOINTMENT_ID = '$deletedAppointmentCounterId'");
                        $queryDeleteNotesAttachment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE_ATTACHMENT WHERE APPOINTMENT_ID = '$deletedAppointmentCounterId'");
                        $queryDeleteAppointment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT_ARCHIVE SET STATUS = '5', LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$deletedAppointmentCounterId' ");
                    }
                }

            }

            }

    header("Location: /appointsync.com/public/dashboard.php/");

    }if(!isset($getAppointmentId)){

        $queryGetArchiveId = mysqli_query($syncminatorcon,"SELECT ID, IS_OFFLINE FROM APPOINTMENT_ARCHIVE WHERE WEB_ID = '$webID'");
        while($rowGetArchiveId = mysqli_fetch_assoc($queryGetArchiveId)) {

            $getAppointmentId = $rowGetArchiveId['ID'];
            $getAppointmentIsOffline = $rowGetArchiveId['IS_OFFLINE'];

            if($getAppointmentIsOffline == '1'){

                $queryDeleteAppointment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT_ARCHIVE SET STATUS = '5', LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$getAppointmentId' ");
                header("Location: /appointsync.com/public/dashboard.php/");

            }else{

            $queryInsertDeletedAppointmentFlag = mysqli_query($syncminatorcon, "INSERT INTO DELETED_APPOINTMENT (APPOINTMENT_ID, USER_ID) VALUES ('$getAppointmentId', '$userId')");



            }

            $queryCheckRemoveCount = mysqli_query($syncminatorcon, "SELECT ID FROM DELETED_APPOINTMENT WHERE APPOINTMENT_ID = '$getAppointmentId'");
            if ($queryCheckRemoveCount->num_rows > 1) {
                while ($rowCheckRemoveCount = mysqli_fetch_assoc($queryCheckRemoveCount)) {
                    $deletedAppointmentCounterId = $rowCheckRemoveCount['APPOINTMENT_ID'];
                    $queryDeleteNotes = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE WHERE APPOINTMENT_ID = '$deletedAppointmentCounterId'");
                    $queryDeleteNotesAttachment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE_ATTACHMENT WHERE APPOINTMENT_ID = '$deletedAppointmentCounterId'");
                    $queryDeleteAppointment = mysqli_query($syncminatorcon, "UPDATE APPOINTMENT_ARCHIVE SET STATUS = '5', LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$deletedAppointmentCounterId' ");
                }
            }

        }

        header("Location: /appointsync.com/public/dashboard.php/");



    }
}
