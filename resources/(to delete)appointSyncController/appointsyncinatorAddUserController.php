<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 7/16/18
 * Time: 4:02 PM
 */


// Value 1 adding normal user
// Value 2 adding offline user
// Value 3 link offline user
// Value 4 Delete user

session_start();
if(isset($_SESSION['loggedUser']) && $_SESSION['loggedUser']['terminatorUserType'] == 2) {
}else{header("Location: /appointsync.com/");}
require 'userwitedbappointsyncinator.php';
$userId =  $_SESSION['loggedUser']['terminatorID'];
$submitFlag = stripslashes($_POST["controllerTypeFlag"]);
$submitFlag = mysqli_real_escape_string($syncminatorcon, $_POST["controllerTypeFlag"]);
$dateUpdated = gmdate("d-m-Y H:i:s");


if($submitFlag == 1){

    $usernameTab = stripslashes($_POST["usernameTab"]);
    $usernameTab = mysqli_real_escape_string($syncminatorcon, $_POST["usernameTab"]);

    $queryAddUserRelation = mysqli_query($syncminatorcon,"SELECT U.ID AS USERID, O.USERNAME FROM USER U INNER JOIN ONLINE_USER O ON O.USER_ID = U.ID WHERE O.USERNAME = '$usernameTab'");
    while($rowGetUserRelationID = mysqli_fetch_assoc($queryAddUserRelation)) {


        $addedUserID = $rowGetUserRelationID['USERID'];
        $addedUsername = $rowGetUserRelationID['USERNAME'];
        if($addedUsername == $usernameTab){

            $queryGetRelationList = mysqli_query($syncminatorcon,"SELECT * FROM RELATION WHERE HOST_ID='$userId'");
            while($rowGetRelationList = mysqli_fetch_assoc($queryGetRelationList)) {

                if($addedUserID == $rowGetRelationList['USER_ID']){

                    $_SESSION["addUserController"] = 4;
                    header("Location: /appointsync.com/public/clients.php");
                    exit();
                }

            }
        $queryRelationUser = mysqli_query($syncminatorcon,"INSERT INTO RELATION (USER_ID, HOST_ID, REQUESTER_ID, TYPE, LAST_UPDATED_DATE) VALUES
                                                      ('$addedUserID', '$userId', '$userId', '0', '$dateUpdated')");

            $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$userId', '$addedUserID', '$userId','10','0','$dateUpdated')");


            $_SESSION["addUserController"] = 2;
         header("Location: /appointsync.com/public/clients.php");
         exit();

        }else{

            $_SESSION["addUserController"] = 3;
            header("Location: /appointsync.com/public/clients.php");
            exit();

        }
    }if (!isset($addedUsername)) {


        $_SESSION["addUserController"] = 3;
        header("Location: /appointsync.com/public/clients.php");
        exit();

    }

}elseif($submitFlag == 2){


                $queryGetHostType = mysqli_query($syncminatorcon,"SELECT * FROM HOST_PREMIUM WHERE HOST_ID = '$userId'");
                while($rowGetGetHostType = mysqli_fetch_assoc($queryGetHostType)){

                    $hostAccountType = $rowGetGetHostType['HOST_ID'];
                    $hostTypeTillDate = $rowGetGetHostType['TILL_DATE'];

                }if (isset($hostAccountType)) {
                    $premiumTillDateUnix = 0;
                    $NowDateUnix = 0;
                    $premiumTillDateUnix = strtotime($hostTypeTillDate) . '<br/>';
                    $dateNow = date("d-m-Y");
                    $NowDateUnix = strtotime($dateNow) . '<br/>';

                if ($premiumTillDateUnix >= $NowDateUnix) {

                    function randomString($length = 25) {
                        $str = "";
                        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
                        $max = count($characters) - 1;
                        for ($i = 0; $i < $length; $i++) {
                            $rand = mt_rand(0, $max);
                            $str .= $characters[$rand];
                        }
                        return $str;

                    }

                    $clientFirstName = stripslashes($_POST["clientFirstName"]);
                    $clientFirstName = mysqli_real_escape_string($syncminatorcon, $_POST["clientFirstName"]);

                    $clientMiddleName = stripslashes($_POST["clientMiddleName"]);
                    $clientMiddleName = mysqli_real_escape_string($syncminatorcon, $_POST["clientMiddleName"]);

                    $clientLastName = stripslashes($_POST["clientLastName"]);
                    $clientLastName = mysqli_real_escape_string($syncminatorcon, $_POST["clientLastName"]);

                    $genderSelect = stripslashes($_POST["genderSelect"]);
                    $genderSelect = mysqli_real_escape_string($syncminatorcon, $_POST["genderSelect"]);

                    $clientPhoneNumber = stripslashes($_POST["clientPhoneNumber"]);
                    $clientPhoneNumber = mysqli_real_escape_string($syncminatorcon, $_POST["clientPhoneNumber"]);



                    echo $clientFirstName . '<br/>';
                    if ($clientMiddleName != ''){
                        $queryInsertOfflineUser = mysqli_query($syncminatorcon,"INSERT INTO USER (FIRST_NAME, LAST_NAME, GENDER, TYPE, PHONE_NUMBER, LAST_UPDATED_DATE) VALUES
                                                      ('$clientFirstName', '$clientLastName', '$genderSelect', 4, '$clientPhoneNumber', '$dateUpdated')");
                        $queryID = mysqli_insert_id($syncminatorcon);
                        $webKey = randomString().$queryID;
                        $queryInsertWebId = mysqli_query($syncminatorcon,"UPDATE USER SET WEB_ID = '$webKey' WHERE ID = '$queryID' ");
                        $queryAddHostLink = mysqli_query($syncminatorcon,"INSERT INTO OFFLINE_USER (ID, HOST_ID) VALUES ('$queryID', '$userId')");
                        $queryInsertOfflineUserMN = mysqli_query($syncminatorcon,"INSERT INTO OFFLINE_USER_MN (OFFLINE_USER_ID, MIDDLE_NAME) VALUES
                                                      ('$queryID', '$clientMiddleName')");

                        $_SESSION["addUserController"] = 0;
                        header("Location: /appointsync.com/public/clients.php");
                        exit();

                    }else{
                        $queryInsertOfflineUser = mysqli_query($syncminatorcon,"INSERT INTO USER (FIRST_NAME, LAST_NAME, GENDER, TYPE, PHONE_NUMBER, LAST_UPDATED_DATE) VALUES
                                                      ('$clientFirstName', '$clientLastName', '$genderSelect', 4, '$clientPhoneNumber', '$dateUpdated')");
                        $queryID = mysqli_insert_id($syncminatorcon);
                        $webKey = randomString().$queryID;
                        $queryInsertWebId = mysqli_query($syncminatorcon,"UPDATE USER SET WEB_ID = '$webKey' WHERE ID = '$queryID' ");
                        $queryAddHostLink = mysqli_query($syncminatorcon,"INSERT INTO OFFLINE_USER (ID, HOST_ID) VALUES ('$queryID', '$userId')");

                        $_SESSION["addUserController"] = 0;
                        header("Location: /appointsync.com/public/clients.php");
                        exit();
                    }


                } else {

                    $_SESSION["addUserController"] = 1;
                    header("Location: /appointsync.com/public/clients.php");
                    exit();

                }

                }else{

                    $_SESSION["addUserController"] = 1;
                    header("Location: /appointsync.com/public/clients.php");
                    exit();

                }




}elseif($submitFlag == 3) {

    $clientUsernameName = stripslashes($_POST["usernameTab"]);
    $clientUsernameName = mysqli_real_escape_string($syncminatorcon, $_POST["usernameTab"]);

    $offlineUserWebID = stripslashes($_POST["offlineUserWebID"]);
    $offlineUserWebID = mysqli_real_escape_string($syncminatorcon, $_POST["offlineUserWebID"]);


    $queryAddUserRelation = mysqli_query($syncminatorcon,"SELECT U.ID AS USERID, U.WEB_ID, O.USERNAME FROM USER U INNER JOIN ONLINE_USER O ON O.USER_ID = U.ID WHERE O.USERNAME = '$clientUsernameName'");
    while($rowGetUserRelationID = mysqli_fetch_assoc($queryAddUserRelation)) {


        $addedUserID = $rowGetUserRelationID['USERID'];
        $addedUsername = $rowGetUserRelationID['USERNAME'];
        $addedUserWebId = $rowGetUserRelationID['WEB_ID'];
        if($addedUsername == $clientUsernameName){

            $queryGetRelationList = mysqli_query($syncminatorcon,"SELECT * FROM RELATION WHERE HOST_ID='$userId'");
            while($rowGetRelationList = mysqli_fetch_assoc($queryGetRelationList)) {

                if($addedUserID == $rowGetRelationList['USER_ID']){

                    $_SESSION["addUserController"] = 4;
                    header("Location: /appointsync.com/public/clientprofile.php/".$offlineUserWebID."");
                    exit();
                }

            }
            $queryRelationUser = mysqli_query($syncminatorcon,"INSERT INTO RELATION (USER_ID, HOST_ID, REQUESTER_ID, TYPE, LAST_UPDATED_DATE) VALUES ('$addedUserID', '$userId','$userId', '0', '$dateUpdated')");
            $queryGetOfflineUserID = mysqli_query($syncminatorcon,"SELECT ID FROM USER WHERE WEB_ID='$offlineUserWebID'");
            while($rowGetOfflineUserID = mysqli_fetch_assoc($queryGetOfflineUserID)) {

                $offlineUserID = $rowGetOfflineUserID['ID'];
                $queryUpdateAppointments = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET USER_ID = '$addedUserID' WHERE USER_ID = '$offlineUserID' ");
                $queryOfflineUserUser = mysqli_query($syncminatorcon,"DELETE FROM USER WHERE ID = '$offlineUserID'");
                $queryOfflineUserCreation = mysqli_query($syncminatorcon,"DELETE FROM OFFLINE_USER WHERE ID = '$offlineUserID'");

                $queryGetOfflineMN = mysqli_query($syncminatorcon,"SELECT OFFLINE_USER_ID FROM OFFLINE_USER_MN WHERE OFFLINE_USER_ID='$offlineUserID'");
                while($rowGetOfflineUserMN = mysqli_fetch_assoc($queryGetOfflineMN)) {

                    $offlineUserMNId = $rowGetOfflineUserMN['OFFLINE_USER_ID'];

                }if(isset($offlineUserMNId)) {
                    $queryOfflineUserMN = mysqli_query($syncminatorcon, "DELETE FROM OFFLINE_USER_MN WHERE OFFLINE_USER_ID = '$offlineUserID'");
                }
            }


            $queryAddNotification = mysqli_query($syncminatorcon,"INSERT INTO NOTIFICATION (FROM_ID, TO_ID, DSC, TYPE, STATUS, LAST_UPDATED_DATE) VALUES
                                                      ('$userId', '$addedUserID', '$userId','10','0','$dateUpdated')");

            $_SESSION["addUserController"] = 2;
            header("Location: /appointsync.com/public/clientprofile.php/".$addedUserWebId."");
            exit();
        }else{

            $_SESSION["addUserController"] = 3;
            header("Location: /appointsync.com/public/clientprofile.php/".$offlineUserWebID."");
            exit();

        }
    }if (!isset($addedUsername)) {


        $_SESSION["addUserController"] = 3;
        header("Location: /appointsync.com/public/clientprofile.php/".$offlineUserWebID."");
        exit();

    }

}elseif($submitFlag == 4) {

    $UserWebID = stripslashes($_POST["UserWebID"]);
    $UserWebID = mysqli_real_escape_string($syncminatorcon, $_POST["UserWebID"]);
    //getUserType

    $queryGetUserInfo = mysqli_query($syncminatorcon,"SELECT ID, TYPE FROM USER WHERE WEB_ID='$UserWebID'");
    while($rowGetUserInfo = mysqli_fetch_assoc($queryGetUserInfo)) {

        $getUserId= $rowGetUserInfo['ID'];
        $getUserType= $rowGetUserInfo['TYPE'];

        //check if user is a relation
        if($getUserType == 3 || $getUserType == 2){
        $queryGetUserRelation = mysqli_query($syncminatorcon,"SELECT ID FROM RELATION WHERE USER_ID='$getUserId'");
        while($rowGetUserRelation = mysqli_fetch_assoc($queryGetUserRelation)) {

            $relationId = $rowGetUserRelation['ID'];

    }if(!isset($relationId)){

            header("Location: /appointsync.com/public/clients.php");
            exit();

        }else{

            //Relation Handling

            $queryMoveRelation = mysqli_query($syncminatorcon,"INSERT INTO RELATION_ARCHIVE SELECT* FROM RELATION WHERE ID='$relationId' ");
            $queryDeletedRelationStatus = mysqli_query($syncminatorcon,"UPDATE RELATION_ARCHIVE SET STATUS = '1' WHERE ID='$relationId' ");
            $queryDeleteRelation = mysqli_query($syncminatorcon, "DELETE FROM RELATION WHERE ID = '$relationId'");

            //Appointment Handling


            $queryUserAppointments = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT WHERE USER_ID='$getUserId'");
            while($rowGetUserAppointments = mysqli_fetch_assoc($queryUserAppointments)) {


                $selectedAppointmentId = $rowGetUserAppointments['ID'];
                $selectedAppointmentAmendmentFlag = $rowGetUserAppointments['HAS_AMENDMENT'];

                $queryUpdatedAppointmentBeforeMoving = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET HAS_ATTACHMENT = '0', HAS_AMENDMENT = '0', STATUS = '5' WHERE ID='$selectedAppointmentId' ");

                if($selectedAppointmentAmendmentFlag == '1') {
                    $queryDeleteIfAppointmentHasAmendment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_AMENDMENT WHERE MAIN_APPOINTMENT_ID = '$selectedAppointmentId'");
                }
                $queryMoveRelation = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_ARCHIVE SELECT* FROM APPOINTMENT WHERE ID='$selectedAppointmentId' ");
                $queryUpdateLastDate = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT_ARCHIVE SET LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$selectedAppointmentId' ");

                $queryCheckRequestComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REQUEST_COMMENT WHERE APPOINTMENT_ID = '$selectedAppointmentId'");
                if($queryCheckRequestComment->num_rows > 0 ) {
                    while ($rowGetRequestComment = mysqli_fetch_assoc($queryCheckRequestComment)) {
                        $requestCommentId = $rowGetRequestComment['ID'];
                        $queryDeleteRequestComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REQUEST_COMMENT WHERE ID = '$requestCommentId'");
                    }
                }
                $queryCheckRejectComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REJECT_REASON WHERE APPOINTMENT_ID = '$selectedAppointmentId'");
                if($queryCheckRejectComment->num_rows > 0 ) {
                    while ($rowGetRejectComment = mysqli_fetch_assoc($queryCheckRejectComment)) {
                        $rejectCommentId = $rowGetRejectComment['ID'];
                        $queryDeleteRejectReason = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_REASON WHERE ID = '$rejectCommentId'");
                        $queryDeleteRejectComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_COMMENT WHERE ID = '$rejectCommentId'");
                    }
                }

                $queryDeleteAppointmentFromTable = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT WHERE ID = '$selectedAppointmentId'");
                $queryDeleteAppointmentNotes = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE WHERE APPOINTMENT_ID = '$selectedAppointmentId'");
                $queryDeleteAppointmentNotesAttachment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE_ATTACHMENT WHERE APPOINTMENT_ID = '$selectedAppointmentId'");


            }


                $queryUserAppointments = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_ARCHIVE WHERE USER_ID='$getUserId' AND STATUS != '5'");
                while($rowGetUserAppointments = mysqli_fetch_assoc($queryUserAppointments)) {


                    $selectedAppointmentId = $rowGetUserAppointments['ID'];
                    $selectedAppointmentAmendmentFlag = $rowGetUserAppointments['HAS_AMENDMENT'];

                    $queryUpdatedAppointmentBeforeMoving = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT_ARCHIVE SET HAS_ATTACHMENT = '0', HAS_AMENDMENT = '0', STATUS = '5' WHERE ID='$selectedAppointmentId' ");

                    if($selectedAppointmentAmendmentFlag == '1') {
                        $queryDeleteIfAppointmentHasAmendment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_AMENDMENT WHERE MAIN_APPOINTMENT_ID = '$selectedAppointmentId'");
                    }


                    $queryCheckRequestComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REQUEST_COMMENT WHERE APPOINTMENT_ID = '$selectedAppointmentId'");
                    if($queryCheckRequestComment->num_rows > 0 ) {
                        while ($rowGetRequestComment = mysqli_fetch_assoc($queryCheckRequestComment)) {
                            $requestCommentId = $rowGetRequestComment['ID'];
                            $queryDeleteRequestComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REQUEST_COMMENT WHERE ID = '$requestCommentId'");
                        }
                    }
                    $queryCheckRejectComment = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_REJECT_REASON WHERE APPOINTMENT_ID = '$selectedAppointmentId'");
                    if($queryCheckRejectComment->num_rows > 0 ) {
                        while ($rowGetRejectComment = mysqli_fetch_assoc($queryCheckRejectComment)) {
                            $rejectCommentId = $rowGetRejectComment['ID'];
                            $queryDeleteRejectReason = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_REASON WHERE ID = '$rejectCommentId'");
                            $queryDeleteRejectComment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_REJECT_COMMENT WHERE ID = '$rejectCommentId'");
                        }
                    }

                    $queryDeleteAppointmentNotes = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE WHERE APPOINTMENT_ID = '$selectedAppointmentId'");
                    $queryDeleteAppointmentNotesAttachment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE_ATTACHMENT WHERE APPOINTMENT_ID = '$selectedAppointmentId'");


                }

        }

      }elseif($getUserType == 4){
            $queryGetUserRelation = mysqli_query($syncminatorcon,"SELECT ID FROM USER WHERE WEB_ID='$UserWebID'");
            while($rowGetUserRelation = mysqli_fetch_assoc($queryGetUserRelation)) {

                $relationOfflineId = $rowGetUserRelation['ID'];

                $queryGetOfflineUserRelation = mysqli_query($syncminatorcon,"SELECT ID FROM OFFLINE_USER WHERE ID='$relationOfflineId' AND HOST_ID = '$userId'");
                while($rowGetOfflineUserRelation = mysqli_fetch_assoc($queryGetOfflineUserRelation)) {

                    $relationId = $rowGetOfflineUserRelation['ID'];

                }
            }if(!isset($relationId)){

                header("Location: /appointsync.com/public/clients.php");
                exit();

            }else{

                //Relation Handling

                $queryDeleteRelation = mysqli_query($syncminatorcon, "DELETE FROM USER WHERE ID = '$relationId'");


                //Appointment Handling


                $queryUserAppointments = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT WHERE USER_ID='$getUserId'");
                while($rowGetUserAppointments = mysqli_fetch_assoc($queryUserAppointments)) {


                    $selectedAppointmentId = $rowGetUserAppointments['ID'];

                    $queryUpdatedAppointmentBeforeMoving = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT SET HAS_ATTACHMENT = '0', HAS_AMENDMENT = '0', STATUS = '5' WHERE ID='$selectedAppointmentId' ");

                    $queryMoveRelation = mysqli_query($syncminatorcon, "INSERT INTO APPOINTMENT_ARCHIVE SELECT* FROM APPOINTMENT WHERE ID='$selectedAppointmentId' ");
                    $queryUpdateLastDate = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT_ARCHIVE SET LAST_UPDATED_DATE = '$dateUpdated' WHERE ID = '$selectedAppointmentId' ");


                    $queryDeleteAppointmentFromTable = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT WHERE ID = '$selectedAppointmentId'");
                    $queryDeleteAppointmentNotes = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE WHERE APPOINTMENT_ID = '$selectedAppointmentId'");
                    $queryDeleteAppointmentNotesAttachment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE_ATTACHMENT WHERE APPOINTMENT_ID = '$selectedAppointmentId'");


                }


                $queryUserAppointments = mysqli_query($syncminatorcon,"SELECT ID FROM APPOINTMENT_ARCHIVE WHERE USER_ID='$getUserId' AND STATUS != '5'");
                while($rowGetUserAppointments = mysqli_fetch_assoc($queryUserAppointments)) {


                    $selectedAppointmentId = $rowGetUserAppointments['ID'];

                    $queryUpdatedAppointmentBeforeMoving = mysqli_query($syncminatorcon,"UPDATE APPOINTMENT_ARCHIVE SET HAS_ATTACHMENT = '0', HAS_AMENDMENT = '0', STATUS = '5' WHERE ID='$selectedAppointmentId' ");


                    $queryDeleteAppointmentNotes = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE WHERE APPOINTMENT_ID = '$selectedAppointmentId'");
                    $queryDeleteAppointmentNotesAttachment = mysqli_query($syncminatorcon, "DELETE FROM APPOINTMENT_NOTE_ATTACHMENT WHERE APPOINTMENT_ID = '$selectedAppointmentId'");


                }

                $queryDeleteRelation = mysqli_query($syncminatorcon, "DELETE FROM OFFLINE_USER WHERE ID = '$relationId'");
                $queryDeleteRelation = mysqli_query($syncminatorcon, "DELETE FROM OFFLINE_USER_MN WHERE OFFLINE_USER_ID = '$relationId'");
            }

        }

        header("Location: /appointsync.com/public/clients.php");
        exit();
    }



    //

    $queryGetRelationList = mysqli_query($syncminatorcon,"SELECT * FROM RELATION WHERE HOST_ID='$userId'");
    while($rowGetRelationList = mysqli_fetch_assoc($queryGetRelationList)) {

    }


}


