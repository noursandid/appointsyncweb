<?php
include_once dirname(__FILE__) . '/../../Classes/MappedResponse.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class APIRequester
{
    public static function sendRequest($url, $headerData, $bodyData, $className)
    {
        $content = json_encode($bodyData);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $userID = isset($_SESSION['user']) ? unserialize($_SESSION['user'])->id : null;
        if (!isset($userID)) {
            $hashedContent = hash("sha512", $content);
        } else {
            $hashedContent = hash("sha512", $content . $userID);
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json", "USERID: $userID", "TOKEN: $hashedContent", $headerData));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response = json_decode($json_response, true);
        $mappedResponse = new MappedResponse();
        if ($status == 200) {
            $instance = $mappedResponse->withData($response, $className);

        } else {
            $instance = $mappedResponse->withError($status, "Request Failed");
        }
        return $instance;
    }

    private static function getHttpCode($http_response_header)
    {
        if(is_array($http_response_header))
        {
            $parts=explode(' ',$http_response_header[0]);
            if(count($parts)>1) //HTTP/1.0 <code> <text>
                return intval($parts[1]); //Get code
        }
        return 0;
    }

    public static function sendFile($url, $headerData, $body, $className)
    {

        $userID = isset($_SESSION['user']) ? unserialize($_SESSION['user'])->id : null;
        $appointmentWebID = $body['appointmentWebID'];
        $fileName = $body['file'];
        $tempFileName = $body['temp'];
//        $fileName = $_FILES["file"]["name"]; // The file name
        $hashedContent = hash("sha512", $appointmentWebID . $userID);
        $eol = "\r\n";
        $data = '';

        $mime_boundary = md5(time());
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $contentType = finfo_file($finfo, $tempFileName);
        finfo_close($finfo);

        $data .= '--' . $mime_boundary . $eol;
        $data .= 'Content-Disposition: form-data; name="file"; filename="'.$fileName.'"' . $eol;
        $data .= 'Content-Type: '.$contentType . $eol.$eol;
        $data .=  file_get_contents($tempFileName). $eol;
        $data .= '--' . $mime_boundary . $eol;
        $data .= 'Content-Disposition: form-data; name="appointmentWebID"' . $eol . $eol;
        $data .= $appointmentWebID . $eol;

        $data .= "--" . $mime_boundary . "--" . $eol . $eol; // finish with two eol's!!
//echo $data;
        $params = array('http' => array(
            'method' => 'POST',
            'header' => 'Content-Type: multipart/form-data; boundary=' . $mime_boundary . $eol.'USERID: '.$userID.$eol.'TOKEN: '. $hashedContent.$eol,
            'content' => $data
        ));
//        echo json_encode($data);
        $ctx = stream_context_create($params);
        $response = @file_get_contents($url, FILE_TEXT, $ctx);
        $response = json_decode($response, true);
        $mappedResponse = new MappedResponse();
        if (APIRequester::getHttpCode($http_response_header) == 200) {
            $instance = $mappedResponse->withData($response,'');

        }
        else{
            $instance = $mappedResponse->withError(APIRequester::getHttpCode($http_response_header),"Request Failed");
        }
        return $instance;
    }





public static function addFileAttachment($url, $headerData, $body, $className)
{

    $userID = isset($_SESSION['user']) ? unserialize($_SESSION['user'])->id : null;
    $clientWebID = $body['clientWebID'];
    $pageWebID = $body['pageWebID'];
    $fileName = $body['file'];
    $tempFileName = $body['temp'];
//        $fileName = $_FILES["file"]["name"]; // The file name
    $hashedContent = hash("sha512", $clientWebID . $pageWebID . $userID);
    $eol = "\r\n";
    $data = '';

    $mime_boundary = md5(time());
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $contentType = finfo_file($finfo, $tempFileName);
    finfo_close($finfo);

    $data .= '--' . $mime_boundary . $eol;
    $data .= 'Content-Disposition: form-data; name="file"; filename="'.$fileName.'"' . $eol;
    $data .= 'Content-Type: '.$contentType . $eol.$eol;
    $data .=  file_get_contents($tempFileName). $eol;
    $data .= '--' . $mime_boundary . $eol;
    $data .= 'Content-Disposition: form-data; name="clientWebID"' . $eol . $eol;
    $data .= $clientWebID . $eol;
    $data .= '--' . $mime_boundary . $eol;
    $data .= 'Content-Disposition: form-data; name="pageWebID"' . $eol . $eol;
    $data .= $pageWebID . $eol;

    $data .= "--" . $mime_boundary . "--" . $eol . $eol; // finish with two eol's!!
//echo $data;
    $params = array('http' => array(
        'method' => 'POST',
        'header' => 'Content-Type: multipart/form-data; boundary=' . $mime_boundary . $eol.'USERID: '.$userID.$eol.'TOKEN: '. $hashedContent.$eol,
        'content' => $data
    ));
        echo json_encode($data);
    $ctx = stream_context_create($params);
    $response = @file_get_contents($url, FILE_TEXT, $ctx);
    $response = json_decode($response, true);
    $mappedResponse = new MappedResponse();
    if (APIRequester::getHttpCode($http_response_header) == 200) {
        $instance = $mappedResponse->withData($response,'');

    }
    else{
        $instance = $mappedResponse->withError(APIRequester::getHttpCode($http_response_header),"Request Failed");
    }
    return $instance;
}



    public static function uploadProfilePicture($url, $headerData, $body, $className)
    {

        $userID = isset($_SESSION['user']) ? unserialize($_SESSION['user'])->id : null;
//        $clientWebID = $body['clientWebID'];
//        $pageWebID = $body['pageWebID'];
        $fileName = $body['file'];
        $tempFileName = $body['temp'];
//        $fileName = $_FILES["file"]["name"]; // The file name
        $hashedContent = hash("sha512", $userID);
        $eol = "\r\n";
        $data = '';

        $mime_boundary = md5(time());
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $contentType = finfo_file($finfo, $tempFileName);
        finfo_close($finfo);

        $data .= '--' . $mime_boundary . $eol;
        $data .= 'Content-Disposition: form-data; name="file"; filename="'.$fileName.'"' . $eol;
        $data .= 'Content-Type: '.$contentType . $eol.$eol;
        $data .=  file_get_contents($tempFileName). $eol;
        $data .= '--' . $mime_boundary . $eol;

        $data .= "--" . $mime_boundary . "--" . $eol . $eol; // finish with two eol's!!
//echo $data;
        $params = array('http' => array(
            'method' => 'POST',
            'header' => 'Content-Type: multipart/form-data; boundary=' . $mime_boundary . $eol.'USERID: '.$userID.$eol.'TOKEN: '. $hashedContent.$eol,
            'content' => $data
        ));
        echo json_encode($data);
        $ctx = stream_context_create($params);
        $response = @file_get_contents($url, FILE_TEXT, $ctx);
        $response = json_decode($response, true);
        $mappedResponse = new MappedResponse();
        if (APIRequester::getHttpCode($http_response_header) == 200) {
            $instance = $mappedResponse->withData($response,'');

        }
        else{
            $instance = $mappedResponse->withError(APIRequester::getHttpCode($http_response_header),"Request Failed");
        }
        return $instance;
    }



}


