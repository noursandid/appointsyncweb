<?php
include_once dirname(__FILE__) . '/ApiController.php';

if (isset($_POST['submit'])) {
    $submit = $_POST['submit'];
    switch ($submit){
        case 'login':
            login();
            break;
        case 'createAppointment':
            createAppointment();
            break;
        case 'amendAppointment':
            amendAppointment();
            break;
        case 'deleteAppointment':
            deleteAppointment();
            break;
        case 'addAppointmentNote':
            addAppointmentNote();
            break;
        case 'addClient':
            addClient();
            break;
        case 'linkClient':
            linkClient();
            break;
        case 'removeClient':
            removeClient();
            break;
        case 'addOfflineClient':
            addOfflineClient();
            break;
        case 'addNewLocation':
            addNewLocation();
            break;
        case 'deleteLocation':
            deleteLocation();
            break;
        case 'addAttachmentNote':
            addAttachmentNote();
            break;
        case 'readNotifications':
            readNotifications();
            break;
        case 'getFileTemplate':
            getFileTemplate();
            break;
        case 'updateValue':
            updateValue();
            break;
        case 'updateFileNote':
            updateFileNote();
            break;
        case 'addFilePage':
            addFilePage();
            break;
        case 'addFileAttachment':
            addFileAttachment();
            break;
        case 'saveCurrency':
            saveCurrency();
            break;
        case 'addPaymentReceived':
            addPaymentReceived();
            break;
        case 'addPaymentDue':
            addPaymentDue();
            break;
        case 'changePassword':
            changePassword();
            break;
        case 'cancelAppointmentAmendment':
            cancelAppointmentAmendment();
            break;
        case 'acceptAppointment':
            acceptAppointment();
            break;
        case 'rejectAppointment':
            rejectAppointment();
            break;
        case 'getFilePage':
            getFilePage();
            break;
        case 'changePermission':
            changePermission();
            break;
        case 'hostChangeView':
            hostChangeView();
            break;
        case 'editProfile':
            editProfile();
            break;
        case 'editProfilePicture':
            editProfilePicture();
            break;
        case 'changeUserPermission':
            changeUserPermission();
            break;
        case 'changeSecureFileFlag':
            changeSecureFileFlag();
            break;
        case 'verifyPin':
            verifyPin();
            break;
        case 'changeTemplatePin':
            changeTemplatePin();
            break;
        case 'registerHost':
            registerHost();
            break;
        case 'getThreads':
            getThreads();
            break;
        case 'getMessages':
            getMessages();
            break;
        case 'postContactMessage':
            postContactMessage();
            break;
        case 'renewPremiumSubscription':
            renewPremiumSubscription();
            break;
        case 'resetPassword':
            resetPassword();
            break;
        case 'setResetPassword':
            setResetPassword();
            break;

            //

//        Clients

        case 'registerClient':
            registerClient();
            break;
        case 'editClientProfile':
            editClientProfile();
            break;
        case 'editClientProfilePicture':
            editClientProfilePicture();
            break;
        case 'changeClientPassword':
            changeClientPassword();
            break;
        case 'addHost':
            addHost();
            break;

        default:
            break;
    }
}

function login()
{
    $username = $_POST['username'];
    $pass = $_POST['password'];
    $password = hash("sha512", $pass);
    $loginResponse = APIController::login($username, $password);
    echo json_encode($loginResponse);
    if ($loginResponse->success) {
        $user = $loginResponse->data;
        $_SESSION['user'] = serialize($user);
//        echo json_encode($loginResponse);
//        header("Location: /appointsync.com/public/dashboard.php");
    } else {
        $_SESSION['loginCheck'] = $loginResponse->errorDescription;
//        echo json_encode($loginResponse);
//        header("Location: /appointsync.com/public/login.php");
    }
}


function createAppointment(){

    $clientSelect = stripslashes($_POST["selectClient"]);
    $timeFrom = stripslashes($_POST["dateTimeFromUTC"]);
    $timeTo = stripslashes($_POST["dateTimeToUTC"]);
    $locationWebID = stripslashes($_POST["selectLocation"]);
    $newAppointmentComment = stripslashes($_POST["newAppointmentComment"]);

    $createAppointmentResponse = APIController::createAppointment($clientSelect, $timeFrom, $timeTo, $locationWebID, $newAppointmentComment);
    if ($createAppointmentResponse->success) {

        $appointmentWebID = $createAppointmentResponse->data->webID;
        echo json_encode($createAppointmentResponse);
//        header("Location: /appointsync.com/public/appointment.php/".$appointmentWebID."");
        exit();
    } else {
        $_SESSION['appointmentController'] = 1;
        echo json_encode($createAppointmentResponse);
//        header("Location: /appointsync.com/public/dashboard.php");
    }

}


function amendAppointment(){

    $amendedAppointmentWebID = stripslashes($_POST["appointmentWebID"]);
    $timeFrom = stripslashes($_POST["dateTimeFromUTC"]);
    $timeTo = stripslashes($_POST["dateTimeToUTC"]);
    $locationWebID = stripslashes($_POST["selectLocation"]);

    $amendAppointmentResponse = APIController::amendAppointment($amendedAppointmentWebID, $timeFrom, $timeTo, $locationWebID);
    if ($amendAppointmentResponse->success) {

        echo json_encode($amendAppointmentResponse);
    } else {
        echo json_encode($amendAppointmentResponse);
    }

}


function acceptAppointment(){

    $appointmentWebID = stripslashes($_POST["appointmentWebID"]);

    $acceptAppointmentResponse = APIController::acceptAppointment($appointmentWebID);
    if ($acceptAppointmentResponse->success) {

        echo json_encode($acceptAppointmentResponse);
    } else {
        echo json_encode($acceptAppointmentResponse);
    }

}


function cancelAppointmentAmendment(){

    $appointmentWebID = stripslashes($_POST["appointmentWebID"]);

    $cancelAmendedAppointmentResponse = APIController::cancelAppointmentAmendment($appointmentWebID);
    if ($cancelAmendedAppointmentResponse->success)
 {
        echo json_encode($cancelAmendedAppointmentResponse);
    } else {
        echo json_encode($cancelAmendedAppointmentResponse);
    }

}


function rejectAppointment(){

    $appointmentWebID = stripslashes($_POST["appointmentWebID"]);
    $rejectReason = stripslashes($_POST["rejectReason"]);

    $rejectAppointmentResponse = APIController::rejectAppointment($appointmentWebID, $rejectReason);
    if ($rejectAppointmentResponse->success)
 {
        echo json_encode($rejectAppointmentResponse);
    } else {
        echo json_encode($rejectAppointmentResponse);
    }

}


function deleteAppointment(){

    $appointmentWebID = stripslashes($_POST["appointmentWebID"]);

    $deletedAppointmentResponse = APIController::deleteAppointment($appointmentWebID);
    if ($deletedAppointmentResponse->success) {

        echo json_encode($deletedAppointmentResponse);
    } else {
        echo json_encode($deletedAppointmentResponse);
    }

}


function addAppointmentNote(){

    $addAppointmentNoteWebID = stripslashes($_POST["addAppointmentNote"]);
    $addAppointmentNote = stripslashes($_POST["userTextNote"]);

    $addedAppointmentResponse = APIController::addAppointmentNote($addAppointmentNoteWebID, $addAppointmentNote);
    if ($addedAppointmentResponse->success) {

        header("Location: /appointsync.com/public/appointmentnotes.php/".$addAppointmentNoteWebID."");
        exit();
    } else {
        $_SESSION['appointmentController'] = 4;
        header("Location: /appointsync.com/public/appointmentnotes.php/".$addAppointmentNoteWebID."");
    }

}


function addClient(){

    $clientUsername = stripslashes($_POST["clientUsername"]);

    $addedAppointmentResponse = APIController::addClient($clientUsername);
    if ($addedAppointmentResponse->success) {

        echo json_encode($addedAppointmentResponse);
        exit();
    } else {
        echo json_encode($addedAppointmentResponse);
        $_SESSION['addUserController'] = 5;
    }

}


function linkClient(){

    $clientUsername = stripslashes($_POST["clientUsername"]);
    $offlineClientWebID = stripslashes($_POST["offlineClientWebID"]);
echo $clientUsername."             ".$offlineClientWebID.'<br/>';
    $linkedClientResponse = APIController::linkClient($clientUsername,$offlineClientWebID);
    echo json_encode($linkedClientResponse);
    if ($linkedClientResponse->success) {

        $linkedClientWebID = $linkedClientResponse->data->webID;

        header("Location: /appointsync.com/public/clientprofile.php/".$linkedClientWebID."");
        exit();
    } else {


        $_SESSION['addUserController'] = 5;
        header("Location: /appointsync.com/public/clients.php/");
    }

}


function removeClient(){

    $clientWebID = stripslashes($_POST["selectClient"]);

    $removeClientResponse = APIController::removeClient($clientWebID);

    if ($removeClientResponse->success) {

        echo json_encode($removeClientResponse);
        exit();
    } else {

        $_SESSION['addUserController'] = 6;
        echo json_encode($removeClientResponse);
        exit();
    }

}


function addOfflineClient()
{

    $firstName = stripslashes($_POST["clientFirstName"]);
    $lastName = stripslashes($_POST["clientLastName"]);
    $middleName = stripslashes($_POST["clientMiddleName"]);
    $dob = $_POST["offlineClientDOB"];
    $gender = stripslashes($_POST["genderSelect"]);
    $phoneNumber = stripslashes($_POST["phoneNumber"]);
    $addOfflineClientResponse = APIController::addOfflineClient($firstName, $lastName, $middleName, $dob, $gender, $phoneNumber);
    if ($addOfflineClientResponse->success) {

        echo json_encode($addOfflineClientResponse);
    } else {

        echo json_encode($addOfflineClientResponse);
    }

}


function addNewLocation()
{

    $locationTitle = stripslashes($_POST["locationTitle"]);
    $locationLongitude = stripslashes($_POST["locationLongitude"]);
    $locationLatitude = stripslashes($_POST["locationLatitude"]);

    $addNewLocationResponse = APIController::addNewLocation($locationTitle, $locationLongitude, $locationLatitude);
    if ($addNewLocationResponse->success) {

        header("Location: /appointsync.com/public/profile.php/");
        exit();
    } else {


        $_SESSION['ProfileController'] = 5;
        header("Location: /appointsync.com/public/profile.php/");
    }

}


function deleteLocation()
{

    $locationWebID = stripslashes($_POST["locationWebID"]);

    $DeleteLocationResponse = APIController::deleteLocation($locationWebID);
    if ($DeleteLocationResponse->success) {

        header("Location: /appointsync.com/public/profile.php/");
        exit();
    } else {

        echo $DeleteLocationResponse->errorDescription;
        $_SESSION['ProfileController'] = 7;
        header("Location: /appointsync.com/public/profile.php/");
    }

}


function changePassword()
{

    $oldPasswordWithoutHash = stripslashes($_POST["oldPassword"]);
    $oldPassword = hash('sha512', $oldPasswordWithoutHash);
    $newPasswordWithoutHash = stripslashes($_POST["newPassword"]);
    $newPassword = hash('sha512', $newPasswordWithoutHash);

    $changePasswordResponse = APIController::changePassword($oldPassword, $newPassword);
    if ($changePasswordResponse->success) {

        $_SESSION['passwordController'] = 3;
        exit();
    }elseif($changePasswordResponse->errorCode ==  445900){

        $_SESSION['passwordController'] = 1;
        exit();

    } else {

        $changePasswordResponse->errorDescription;
        $_SESSION['profileRequestError'] = 1;
        exit();
    }

}


function readNotifications()
{
    $DeleteLocationResponse = APIController::readNotifications();
    if ($DeleteLocationResponse->success) {} else {}
}


function addAttachmentNote()
{

    $appointmentWebID = stripslashes($_POST["addAppointmentAttachment"]);


    if (isset($_FILES['file']) && $_FILES['file'] != '' ) {
        $fileData = $_FILES['file'];
        $addAttachmentResponse = APIController::addAttachment($appointmentWebID, $fileData);
        if ($addAttachmentResponse->success) {

            header("Location: /appointsync.com/public/appointmentnotes.php/" . $appointmentWebID . "");
            exit();
        } else {

            $addAttachmentResponse->errorDescription;
            $_SESSION['imageUploadStatus'] = 6;
            header("Location: /appointsync.com/public/appointmentnotes.php/" . $appointmentWebID . "");
        }
    }

}


function updateFileNote()
{

    $clientWebID = stripslashes($_POST["clientWebID"]);
    $pageWebID = stripslashes($_POST["pageWebID"]);
    $clientFileNote = $_POST["clientFileNote"];


    if (isset($clientWebID) && $clientWebID != '' && isset($pageWebID) && $pageWebID != '') {

        $addAttachmentResponse = APIController::updateFileNote($clientWebID, $pageWebID,$clientFileNote);
        echo json_encode($addAttachmentResponse);
        if ($addAttachmentResponse->success) {
        } else {
        }
    }

}


function getFileTemplate()
{

    $clientWebID = stripslashes($_POST["clientWebID"]);

    if (isset($clientWebID) && $clientWebID != '' ) {


    $getFileTemplate = APIController::getFileTemplate($clientWebID);
    if ($getFileTemplate->success) {} else {}
    }
}


function getFilePage()
{

    $webID = stripslashes($_POST["webID"]);
    if(isset($_POST["pageWebID"])){
        $pageWebID = stripslashes($_POST["pageWebID"]);
    }
    $pinCode = stripslashes($_POST["pinCode"]);
    $hashedPin = hash('sha512', $pinCode);

    if (isset($pageWebID) && $pageWebID != '') {
        $getFileTemplatePin = APIController::getFilePage($webID, $pageWebID, $hashedPin);
        echo json_encode($getFileTemplatePin);
    } else {
        $getFileTemplatePin = APIController::getFilePage($webID, null, $hashedPin);
        echo json_encode($getFileTemplatePin);

    }
}


function addFilePage()
{

    $clientWebID = stripslashes($_POST["clientWebID"]);

    if (isset($clientWebID) && $clientWebID != '' ) {


    $getFileTemplate = APIController::addFilePage($clientWebID);
        if ($getFileTemplate->success) {
            echo json_encode($getFileTemplate->data);
//            header("Location: google.com");
//            exit();
        } else {
//            header("Location: /appointsync.com/public/profile.php");
        }
    }
}


function addFileAttachment()
{
    $clientWebID = stripslashes($_POST["clientWebID"]);
    $pageWebID = stripslashes($_POST["pageWebID"]);


    if (isset($_FILES['file']) && $_FILES['file'] != '' ) {
        $fileData = $_FILES['file'];
        $addFileAttachmentResponse = APIController::addFileAttachment($clientWebID, $pageWebID, $fileData);
        echo json_encode($addFileAttachmentResponse);
        if ($addFileAttachmentResponse->success) {

            exit();
        } else {

            $addFileAttachmentResponse->errorDescription;
        }
    }

}


function getPayments()
{

    $clientWebID = stripslashes($_POST["clientWebID"]);


    if (isset($clientWebID) && $clientWebID != '' ) {
        $getPaymentsResponse = APIController::getPayments($clientWebID);
        echo json_encode($getPaymentsResponse);
        if ($getPaymentsResponse->success) {

            exit();
        } else {

            $getPaymentsResponse->errorDescription;
        }
    }

}


function addPaymentDue()
{

    $clientWebID = stripslashes($_POST["clientWebID"]);
    $paymentDue = stripslashes($_POST["paymentDue"]);
    $note = stripslashes($_POST["note"]);
    $date = stripslashes($_POST["date"]);


    if (isset($_POST["clientWebID"]) && $_POST["clientWebID"] != '' && isset($_POST["paymentDue"]) && $_POST["paymentDue"] != '' && isset($_POST["date"]) && $_POST["date"] != '') {
        $addPaymentDueResponse = APIController::addPaymentDue($clientWebID,$paymentDue,$note,$date);
        echo json_encode($addPaymentDueResponse);
        if ($addPaymentDueResponse->success) {

            exit();
        } else {

            $addPaymentDueResponse->errorDescription;
        }
    }

}


function addPaymentReceived()
{

    $clientWebID = stripslashes($_POST["clientWebID"]);
    $paymentReceived = stripslashes($_POST["paymentReceived"]);
    $note = stripslashes($_POST["note"]);
    $date = stripslashes($_POST["date"]);


    if (isset($_POST["clientWebID"]) && $_POST["clientWebID"] != '' && isset($_POST["paymentReceived"]) && $_POST["paymentReceived"] != '' && isset($_POST["date"]) && $_POST["date"] != '') {
        $addPaymentReceivedResponse = APIController::addPaymentReceived($clientWebID,$paymentReceived,$note,$date);
        echo json_encode($addPaymentReceivedResponse);
        if ($addPaymentReceivedResponse->success) {

            exit();
        } else {

            $addPaymentReceivedResponse->errorDescription;
        }
    }

}


function updateValue()
{

    $clientWebID = stripslashes($_POST["clientWebID"]);
    $pageWebID = stripslashes($_POST['pageWebID']);
    $elementWebID = stripslashes($_POST['elementWebID']);
    $keyWebID = stripslashes($_POST['keyWebID']);
    $value = stripslashes($_POST['value']);
    if (isset($clientWebID) && $clientWebID != '' && isset($pageWebID) && $pageWebID != '' && isset($keyWebID) && $keyWebID != '' && isset($elementWebID) && $elementWebID != '') {
        $getFileTemplate = APIController::updateFileValue($clientWebID,$pageWebID,$elementWebID,$keyWebID,$value);
        if ($getFileTemplate->success) {} else {}
    }
    else{
    }
}


function saveCurrency()
{

    $currency = stripslashes($_POST["currency"]);
    if (isset($currency) && $currency != '' ) {
        $postCurrency = APIController::saveCurrency($currency);
        if ($postCurrency->success) {} else {}
    }
    else{
    }
}


function changePermission()
{

    $permission = stripslashes($_POST["permission"]);
    $permissionValue = stripslashes($_POST["permissionValue"]);


    if (isset($permission) && $permission != '' && isset($permissionValue) && $permissionValue != '') {
        $changePermission = APIController::changePermission($permission, $permissionValue);
        echo json_encode($changePermission);
        if ($changePermission->success) {

        } else {

        }
    }
    else{
    }
}


function changeUserPermission()
{

    $clientWebID = stripslashes($_POST["clientWebID"]);
    $permission = stripslashes($_POST["permission"]);
    $permissionValue = stripslashes($_POST["permissionValue"]);


    if (isset($permission) && $permission != '' && isset($permissionValue) && $permissionValue != '' && isset($clientWebID) && $clientWebID != '') {
        $changeUserPermission = APIController::changeUserPermission($permission, $permissionValue, $clientWebID);
        echo json_encode($changeUserPermission);
        if ($changeUserPermission->success) {

        } else {

        }
    }
    else{
    }
}


function hostChangeView()
{

    $loggedInAs = stripslashes($_POST["loggedInAs"]);


    if (isset($loggedInAs) && $loggedInAs != '') {
        $loggedInAs = APIController::changeLoggedInAsFlag($loggedInAs);
        echo json_encode($loggedInAs);
        if ($loggedInAs->success) {

        } else {

        }
    }
    else{
    }
}


function editProfile()
{

    $firstName = stripslashes($_POST["firstName"]);
    $lastName = stripslashes($_POST["lastName"]);
    $profession = stripslashes($_POST["profession"]);
    $workingHourFrom = stripslashes($_POST["workingHourFrom"]);
    $workingHourTo = stripslashes($_POST["workingHourTo"]);


    if (isset($firstName) && $firstName != '' && isset($lastName) && $lastName != '' && isset($profession) && $profession != '' && isset($workingHourFrom) && $workingHourFrom != ''
            && isset($workingHourTo) && $workingHourTo != '') {
        $editProfile = APIController::editProfile($firstName,$lastName,$profession,$workingHourFrom,$workingHourTo);
        echo json_encode($editProfile);
        if ($editProfile->success) {

        } else {

        }
    }
    else{
    }
}


function editProfilePicture()
{


    if (isset($_FILES['file']) && $_FILES['file'] != '' ) {
        $fileData = $_FILES['file'];
        $uploadProfilePicture = APIController::uploadProfilePicture($fileData);
        echo json_encode($uploadProfilePicture);
        if ($uploadProfilePicture->success) {


        } else {}
    }

}


function changeSecureFileFlag()
{

    $secureFlagValue = stripslashes($_POST["secureFile"]);


    if (isset($secureFlagValue) && $secureFlagValue != '') {
        $changeSecureFlag = APIController::changeSecureFileFlag($secureFlagValue);
        echo json_encode($changeSecureFlag);
        if ($changeSecureFlag->success) {

        } else {

        }

    }

}


function resetPassword()
{

    $email = stripslashes($_POST["email"]);


    if (isset($email) && $email != '') {
        $resetPassword = APIController::resetPassword($email);
        echo json_encode($resetPassword);
        if ($resetPassword->success) {

        } else {

        }

    }

}


function setResetPassword()
{

    $token = stripslashes($_POST["token"]);
    $email = stripslashes($_POST["email"]);
    $password = stripslashes($_POST["password"]);
    $rPasswordHash = hash('sha512', $password);


    if (isset($token) && $token != '' && isset($email) && $email != '' && isset($password) && $password != '') {
        $setResetPassword = APIController::setResetPassword($token, $email, $rPasswordHash);
        echo json_encode($setResetPassword);
        if ($setResetPassword->success) {

        } else {

        }

    }

}


function verifyPin()
{

    $pinCodeReal = stripslashes($_POST["pinCode"]);
    $pinCode = hash('sha512', $pinCodeReal);

    if (isset($pinCode) && $pinCode != '') {
        $verifyPin = APIController::verifyPin($pinCode);
        echo json_encode($verifyPin);
        if ($verifyPin->success) {

        } else {

        }

    }

}


function changeTemplatePin()
{

    $pinCodeReal = stripslashes($_POST["pinCode"]);
    $pinCode = hash('sha512', $pinCodeReal);

    if (isset($pinCode) && $pinCode != '') {
        $verifyPin = APIController::changeTemplatePin($pinCode);
        echo json_encode($verifyPin);
        if ($verifyPin->success) {

        } else {

        }

    }

}


function registerHost()
{

     $rUsername = stripslashes($_POST["username"]);
     $rPassword = stripslashes($_POST["password"]);
     $rPasswordHash = hash('sha512', $rPassword);
     $rFirstName = stripslashes($_POST["firstName"]);
     $rLastName = stripslashes($_POST["lastName"]);
     $rProfession = stripslashes($_POST["profession"]);
     $rClientFileTemplate = stripslashes($_POST["rClientFileTemplate"]);
     $rEmail = stripslashes($_POST["email"]);
     $rGender = stripslashes($_POST["gender"]);
     $rPhoneNumber = stripslashes($_POST["phoneNumber"]);
     $rCountryCode = stripslashes($_POST["countryCode"]);
     $rWorkingHourFrom = stripslashes($_POST["workingHourFrom"]);
     $rWorkingHourTo = stripslashes($_POST["workingHourTo"]);
     $rDateOfBirth = stripslashes($_POST["dob"]);



    if (isset($rUsername) && $rUsername != '' && isset($rPassword) && $rPassword != '' && isset($rFirstName) && $rFirstName != '' && isset($rLastName) && $rLastName != ''
        && isset($rProfession) && $rProfession != '' && isset($rClientFileTemplate) && $rClientFileTemplate != '' && isset($rEmail) && $rEmail != '' && isset($rGender) && $rGender != ''
        && isset($rPhoneNumber) && $rPhoneNumber != '' && isset($rWorkingHourFrom) && $rWorkingHourFrom != '' && isset($rWorkingHourTo) && $rWorkingHourTo != '' && isset($rDateOfBirth) && $rDateOfBirth != '') {
        $registerUser = APIController::registerHost($rUsername,$rPasswordHash,$rFirstName,$rLastName,$rProfession,$rClientFileTemplate,$rEmail,$rGender,$rCountryCode,$rPhoneNumber,$rWorkingHourFrom,
            $rWorkingHourTo,$rDateOfBirth);
        echo json_encode($registerUser);
        if ($registerUser->success) {

        } else {

        }

    }

}


function getThreads()
{

  $getThreads = APIController::getThreads();
        echo json_encode($getThreads);
        if ($getThreads->success) {

        } else {

        }


}


function getMessages()
{

    $threadWebID = stripslashes($_POST['threadWebID']);

    $getMessages = APIController::getMessages($threadWebID);
    echo json_encode($getMessages);
    if ($getMessages->success) {

    } else {

    }

}


function postContactMessage()
{

    if(isset($_POST['threadTitle'])){
    $threadTitle = stripslashes($_POST['threadTitle']);
    $threadValue = 'threadTitle';
    }else{
    $threadTitle = stripslashes($_POST['threadWebID']);
    $threadValue = 'threadWebID';
    }
    $message = stripslashes($_POST['message']);
    $toWebID = stripslashes($_POST['toWebID']);

    $postContactMessage = APIController::postContactMessage($threadValue,$threadTitle,$message,$toWebID);
    echo json_encode($postContactMessage);
    if ($postContactMessage->success) {

    } else {

    }

}


function renewPremiumSubscription()
{

    $premium = stripslashes($_POST['premium']);

    $renewPremiumSubscription = APIController::renewPremiumSubscription($premium);
    echo json_encode($renewPremiumSubscription);
    if ($renewPremiumSubscription->success) {

    } else {

    }

}


//Clients


function registerClient()
{

    $rUsername = stripslashes($_POST["username"]);
    $rPassword = stripslashes($_POST["password"]);
    $rPasswordHash = hash('sha512', $rPassword);
    $rFirstName = stripslashes($_POST["firstName"]);
    $rLastName = stripslashes($_POST["lastName"]);
    $rEmail = stripslashes($_POST["email"]);
    $rGender = stripslashes($_POST["gender"]);
    $rPhoneNumber = stripslashes($_POST["phoneNumber"]);
    $rCountryCode = stripslashes($_POST["countryCode"]);
    $rDateOfBirth = stripslashes($_POST["dob"]);



    if (isset($rUsername) && $rUsername != '' && isset($rPassword) && $rPassword != '' && isset($rFirstName) && $rFirstName != '' && isset($rLastName) && $rLastName != ''
        && isset($rEmail) && $rEmail != '' && isset($rGender) && $rGender != '' && isset($rPhoneNumber) && $rPhoneNumber != '' && isset($rDateOfBirth) && $rDateOfBirth != '') {
        $registerClient = APIController::registerClient($rUsername,$rPasswordHash,$rFirstName,$rLastName,$rEmail,$rGender,$rCountryCode,$rPhoneNumber,$rDateOfBirth);
        echo json_encode($registerClient);
        if ($registerClient->success) {

        } else {

        }

    }

}

function editClientProfile()
{

    $firstName = stripslashes($_POST["firstName"]);
    $lastName = stripslashes($_POST["lastName"]);


    if (isset($firstName) && $firstName != '' && isset($lastName) && $lastName != '') {
        $editProfile = APIController::editClientProfile($firstName, $lastName);
        echo json_encode($editProfile);
        if ($editProfile->success) {

        } else {

        }
    } else {
    }

}

    function editClientProfilePicture()
    {


        if (isset($_FILES['file']) && $_FILES['file'] != '' ) {
            $fileData = $_FILES['file'];
            $uploadProfilePicture = APIController::uploadClientProfilePicture($fileData);
            echo json_encode($uploadProfilePicture);
            if ($uploadProfilePicture->success) {


            } else {}
        }

    }

    function changeClientPassword()
    {



        $oldPasswordWithoutHash = stripslashes($_POST["oldPassword"]);
        $oldPassword = hash('sha512', $oldPasswordWithoutHash);
        $newPasswordWithoutHash = stripslashes($_POST["newPassword"]);
        $newPassword = hash('sha512', $newPasswordWithoutHash);

        $changePasswordResponse = APIController::changeClientPassword($oldPassword, $newPassword);
        if ($changePasswordResponse->success) {

            $_SESSION['passwordController'] = 3;
            exit();
        }elseif($changePasswordResponse->errorCode ==  445900){

            $_SESSION['passwordController'] = 1;
            exit();

        } else {

            $changePasswordResponse->errorDescription;
            $_SESSION['profileRequestError'] = 1;
            exit();
        }
    }

    function addHost()
    {

        $hostUsername = stripslashes($_POST["username"]);

        $addHostResponse = APIController::addHost($hostUsername);
        if ($addHostResponse->success) {

            echo json_encode($addHostResponse);
            exit();
        } else {
            echo json_encode($addHostResponse);
        }
    }



