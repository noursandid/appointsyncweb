<?php
include_once dirname(__FILE__) . '/../../Classes/UserProfile.php';
include_once dirname(__FILE__) . '/APISettings.php';
include_once dirname(__FILE__) . '/APIRequester.php';

class APIController
{


    //Register

    public static function registerHost($rUsername,$rPasswordHash,$rFirstName,$rLastName,$rProfession,$rClientFileTemplate,$rEmail,$rGender,$rCountryCode,$rPhoneNumber,$rWorkingHourFrom,
                                        $rWorkingHourTo,$rDateOfBirth){
        $body = array('firstName'=>$rFirstName,'lastName'=>$rLastName,'profession'=>$rProfession,'email'=>$rEmail,'gender'=>$rGender,'countryCode'=>$rCountryCode,'phoneNumber'=>$rPhoneNumber,
            'fileTemplateWebID'=>$rClientFileTemplate,'workingHourFrom'=>$rWorkingHourFrom,'workingHourTo'=>$rWorkingHourTo,'dob'=>$rDateOfBirth,'username'=>$rUsername,'password'=>$rPasswordHash,);
        return APIRequester::sendRequest(APISettings::hostRegisterHostURL(),"",$body,"");
    }


    public static function setResetPassword($token, $email, $rPasswordHash){
        $body = array('token'=>$token,'email'=>$email,'password'=>$rPasswordHash);
        return APIRequester::sendRequest(APISettings::hostSetResetPasswordURL(),"",$body,"");
    }




    //Profile
    
    public static function getProfile(){
//        $body = array('userID'=>$userID);
        return APIRequester::sendRequest(APISettings::getProfileURL(),"",'',"UserProfile");
    }

    public static function changePassword($oldPassword, $newPassword){
        $body = array('oldPassword'=>$oldPassword,'newPassword'=>$newPassword);
        return APIRequester::sendRequest(APISettings::hostChangePasswordURL(),"",$body,"");
    }

    public static function editProfile($firstName, $lastName, $profession, $workingHourFrom, $workingHourTo){
        $body = array('firstName'=>$firstName,'lastName'=>$lastName, 'profession'=>$profession, 'workingHourFrom'=>$workingHourFrom, 'workingHourTo'=>$workingHourTo);
        return APIRequester::sendRequest(APISettings::hostEditProfileURL(),"",$body,"UserProfile");
    }

    public static function resetPassword($email){
        $body = array('email'=>$email);
        return APIRequester::sendRequest(APISettings::hostResetPasswordURL(),"",$body,"");
    }



    public static function uploadProfilePicture($fileData){
        $body = array('file'=>$fileData['name'],'temp'=>$fileData['tmp_name']);
        return APIRequester::uploadProfilePicture(APISettings::hostChangeProfilePictureURL(),"",$body,"UserProfile");
    }


    public static function changeSecureFileFlag($secureFlagValue){
        $body = array('secureFile'=>$secureFlagValue);
        return APIRequester::sendRequest(APISettings::hostChangeSecureFileFlagURL(),"",$body,"");
    }



    public static function verifyPin($pinCode){
        $body = array('pinCode'=>$pinCode);
        return APIRequester::sendRequest(APISettings::hostVerifyPinURL(),"",$body,"");
    }



    public static function changeTemplatePin($pinCode){
        $body = array('pinCode'=>$pinCode);
        return APIRequester::sendRequest(APISettings::hostChangeTemplatePinURL(),"",$body,"");
    }

    
    //Appointment
    public static function getAppointments(){
        return APIRequester::sendRequest(APISettings::hostGetAppointmentsURL(),"",'',"MappedAppointment");
    }

    public static function getAppointment($sendAppointmentWebID){
        $body = array('appointmentWebID'=>$sendAppointmentWebID);
        return APIRequester::sendRequest(APISettings::hostGetAppointmentURL(),"",$body,"MappedAppointment");
    }

    public static function amendAppointment($amendedAppointmentWebID,$timeFrom,$timeTo,$locationWebID){
        $body = array('appointmentWebID'=>$amendedAppointmentWebID,'appointmentStartDate'=>$timeFrom,'appointmentEndDate'=>$timeTo,'appointmentLocationWebID'=>$locationWebID);
        return APIRequester::sendRequest(APISettings::hostAmendAppointmentURL(),"",$body,null);
    }

    public static function cancelAppointmentAmendment($appointmentWebID){
        $body = array('appointmentWebID'=>$appointmentWebID);
        return APIRequester::sendRequest(APISettings::hostCancelAmendmentURL(),"",$body,"");
    }

    public static function acceptAppointment($appointmentWebID){
        $body = array('appointmentWebID'=>$appointmentWebID);
        return APIRequester::sendRequest(APISettings::hostAcceptAppointment(),"",$body,"");
    }

    public static function rejectAppointment($appointmentWebID, $rejectReason){
        $body = array('appointmentWebID'=>$appointmentWebID, 'rejectReason'=>$rejectReason);
        return APIRequester::sendRequest(APISettings::hostRejectAppointmentURL(),"",$body,"");
    }

    public static function deleteAppointment($deletedAppointmentWebID){
        $body = array('appointmentWebID'=>$deletedAppointmentWebID);
        return APIRequester::sendRequest(APISettings::hostDeleteAppointmentURL(),"",$body,null);
    }

    //Add Appointment
    public static function createAppointment($clientSelect,$timeFrom,$timeTo,$locationWebID,$newAppointmentComment){
        $body = array('appointmentClientWebID'=>$clientSelect,'appointmentStartDate'=>$timeFrom,'appointmentEndDate'=>$timeTo,'appointmentLocationWebID'=>$locationWebID,'appointmentComment'=>$newAppointmentComment);
        return APIRequester::sendRequest(APISettings::hostAddAppointmentURL(),"",$body,"MappedAppointment");
    }

    // Notes

    public static function getAppointmentNotes($sendAppointmentWebID){
        $body = array('appointmentWebID'=>$sendAppointmentWebID);
        return APIRequester::sendRequest(APISettings::hostGetAppointmentNotesURL(),"",$body,"MappedAppointmentNote");
    }

    public static function addAppointmentNote($addAppointmentNoteWebID, $addAppointmentNote){
        $body = array('appointmentWebID'=>$addAppointmentNoteWebID, 'note'=>$addAppointmentNote,);
        return APIRequester::sendRequest(APISettings::hostAddNoteURL(),"",$body,null);
    }

    //Get Attachments

    public static function getAppointmentAttachments($sendAppointmentWebID){
        $body = array('appointmentWebID'=>$sendAppointmentWebID);
        return APIRequester::sendRequest(APISettings::hostGetAppointmentAttachmentsURL(),"",$body,"MappedAttachment");
    }

    public static function addAttachment($sendAppointmentWebID,$filedata){
        $body = array('appointmentWebID'=>$sendAppointmentWebID,'file'=>$filedata['name'],'temp'=>$filedata['tmp_name']);
        return APIRequester::sendFile(APISettings::hostAddAppointmentAttachmentsURL(),"",$body,"MappedAttachment");
    }

    //Location
    public static function getLocations(){
        return APIRequester::sendRequest(APISettings::hostGetLocationsURL(), "", '', "MappedLocation");
    }


    public static function addNewLocation($locationTitle, $locationLongitude, $locationLatitude){
        $body = array('locationName'=>$locationTitle, 'locationLongitude'=>$locationLongitude,'locationLatitude'=>$locationLatitude);
        return APIRequester::sendRequest(APISettings::hostAddLocationURL(),"",$body,"MappedLocation");
    }

    public static function deleteLocation($locationWebID){
        $body = array('locationWebID'=>$locationWebID);
        return APIRequester::sendRequest(APISettings::hostDeleteLocationURL(),"",$body,null);
    }
    
    //Handler
    public static function login($username,$password){
        $body = array('username'=>$username,'password'=>$password);
        return APIRequester::sendRequest(APISettings::hostLoginURL(),"",$body,"UserProfile");
    }

    public static function getNotification(){
        return APIRequester::sendRequest(APISettings::hostGetNotificationURL(),"",'',"MappedNotification");
    }

        public static function readNotifications(){
        return APIRequester::sendRequest(APISettings::hostReadNotificationURL(),"",'',null);
    }

        public static function changeLoggedInAsFlag($loggedInAs){
            $body = array('loggedInAs'=>$loggedInAs);
        return APIRequester::sendRequest(APISettings::hostChangeLoggedInAsFlagURL(),"",$body,null);
    }

    //Client
    public static function getClients(){
        return APIRequester::sendRequest(APISettings::hostGetClientsURL(),"",'',"MappedClient");
    }

    public static function getClient($sendUserWebID){
        $body = array('clientWebID'=>$sendUserWebID);
        return APIRequester::sendRequest(APISettings::hostGetClientURL(),"",$body,"MappedClient");
    }

    public static function addClient($clientUsername){
        $body = array('username'=>$clientUsername);
        return APIRequester::sendRequest(APISettings::hostAddClientURL(),"",$body,"MappedClient");
    }


    public static function linkClient($clientUsername, $offlineClientWebID){
        $body = array('username'=>$clientUsername,'offlineWebID'=>$offlineClientWebID);
        return APIRequester::sendRequest(APISettings::hostLinkClientURL(),"",$body,"MappedClient");
    }

    public static function removeClient($clientWebID){
        $body = array('clientWebID'=>$clientWebID);
        return APIRequester::sendRequest(APISettings::hostRemoveClientURL(),"",$body,null);
    }

    public static function addOfflineClient($firstName, $lastName, $middleName, $dob, $gender, $phoneNumber){
        $body = array('firstName'=>$firstName, 'lastName'=>$lastName,'middleName'=>$middleName,'dob'=>$dob,'gender'=>$gender ,'phoneNumber'=>$phoneNumber);
        return APIRequester::sendRequest(APISettings::hostAddOfflineClientURL(),"",$body,"MappedClient");
    }

    public static function getClientsPermissions(){
        return APIRequester::sendRequest(APISettings::hostGetClientsPermissions(),"",'',"MappedClientPermission");
    }

    public static function getClientPermissions($sendUserWebID){
        $body = array('clientWebID'=>$sendUserWebID);
        return APIRequester::sendRequest(APISettings::hostGetClientPermissions(),"",$body,"MappedClientPermission");
    }


    public static function changePermission($permission, $permissionValue){
        $body = array('permission'=>$permission,'permissionValue'=>$permissionValue);
        return APIRequester::sendRequest(APISettings::hostChangePermissionURL(),"",$body,"");
    }



    public static function changeUserPermission($permission, $permissionValue, $clientWebID){
        $body = array('permission'=>$permission,'permissionValue'=>$permissionValue,'clientWebID'=>$clientWebID);
        return APIRequester::sendRequest(APISettings::hostChangeUserPermissionURL(),"",$body,"");
    }

//Files

    public static function getFileTemplate($clientWebID,$pinCode){
        $body = array('clientWebID'=>$clientWebID,'pinCode'=>$pinCode);
        return APIRequester::sendRequest(APISettings::hostGetFileTemplateURL(),"",$body,"MappedPage");
    }


    public static function getFilePage($clientWebID, $pageWebID, $hashedPin){
    $body = array('clientWebID'=>$clientWebID,'pageWebID'=>$pageWebID,'pinCode'=>$hashedPin);
    return APIRequester::sendRequest(APISettings::hostGetFilePageURL(),"",$body,"MappedPage");
}

    public static function addFilePage($clientWebID){
        $body = array('clientWebID'=>$clientWebID);
        return APIRequester::sendRequest(APISettings::hostAddFilePageURL(),"",$body,"MappedPage");
    }

    public static function updateFileValue($clientWebID,$pageWebID,$elementWebID,$keyWebID,$value){
        $body = array('clientWebID'=>$clientWebID,'pageWebID'=>$pageWebID,'elementWebID'=>$elementWebID,'keyWebID'=>$keyWebID,'value'=>$value);
        return APIRequester::sendRequest(APISettings::hostUpdateFileValueURL(),"",$body,"");
    }

    public static function updateFileNote($clientWebID,$pageWebID,$clientFileNote){
        $body = array('clientWebID'=>$clientWebID,'pageWebID'=>$pageWebID,'note'=>$clientFileNote);
        return APIRequester::sendRequest(APISettings::hostUpdateFileNoteURL(),"",$body,"");
    }

    public static function addFileAttachment($clientWebID, $pageWebID, $fileData){
        $body = array('clientWebID'=>$clientWebID,'pageWebID'=>$pageWebID,'file'=>$fileData['name'],'temp'=>$fileData['tmp_name']);
        return APIRequester::addFileAttachment(APISettings::hostAddFileAttachmentURL(),"",$body,"");
    }

    public static function getPayments($clientWebID,$pinCode){
        $body = array('clientWebID'=>$clientWebID,'pinCode'=>$pinCode);
        return APIRequester::sendRequest(APISettings::hostGetPaymentsURL(),"",$body,"MappedPayments");
    }

    public static function saveCurrency($currency){
        $body = array('currency'=>$currency);
        return APIRequester::sendRequest(APISettings::hostSaveCurrencyURL(),"",$body,"");
    }

    public static function addPaymentDue($clientWebID,$paymentDue,$note,$date){
        $body = array('clientWebID'=>$clientWebID,'paymentDue'=>$paymentDue,'note'=>$note,'date'=>$date);
        return APIRequester::sendRequest(APISettings::hostAddPaymentDueURL(),"",$body,"");
    }

    public static function addPaymentReceived($clientWebID,$paymentReceived,$note,$date){
        $body = array('clientWebID'=>$clientWebID,'paymentReceived'=>$paymentReceived,'note'=>$note,'date'=>$date);
        return APIRequester::sendRequest(APISettings::hostAddPaymentReceivedURL(),"",$body,"");
    }


    public static function getAllTemplates(){
        return APIRequester::sendRequest(APISettings::hostGetAllTemplatesURL(),"","","MappedTemplates");
    }


    // Messages


    public static function getThreads(){
        return APIRequester::sendRequest(APISettings::hostGetThreadsURL(),"",'',"MappedThread");
    }


    public static function getMessages($threadWebID){
        $body = array('threadWebID'=>$threadWebID);
        return APIRequester::sendRequest(APISettings::hostGetMessagesURL(),"",$body,"MappedMessage");
    }


    public static function postContactMessage($threadValue,$threadTitle,$message,$toWebID){
        $body = array($threadValue=>$threadTitle,'message'=>$message,'toWebID'=>$toWebID);
        return APIRequester::sendRequest(APISettings::hostPostContactMessageURL(),"",$body,"");
    }


    public static function renewPremiumSubscription($premium){
        $body = array('premium'=>$premium);
        return APIRequester::sendRequest(APISettings::HostRenewPremiumSubscription(),"",$body,"");
    }



    //Clients



    public static function registerClient($rUsername,$rPasswordHash,$rFirstName,$rLastName,$rEmail,$rGender,$rCountryCode,$rPhoneNumber,$rDateOfBirth){
        $body = array('firstName'=>$rFirstName,'lastName'=>$rLastName,'email'=>$rEmail,'gender'=>$rGender,'countryCode'=>$rCountryCode,'phoneNumber'=>$rPhoneNumber,'dob'=>$rDateOfBirth,'username'=>$rUsername,'password'=>$rPasswordHash,);
        return APIRequester::sendRequest(APISettings::clientRegisterClientURL(),"",$body,"");
    }

    public static function getClientProfile(){
        $body = array();
        return APIRequester::sendRequest(APISettings::clientGetProfileURL(),"","",'MappedClient');
    }


    public static function editClientProfile($firstName,$lastName){
        $body = array('firstName'=>$firstName,'lastName'=>$lastName);
        return APIRequester::sendRequest(APISettings::clientEditProfileURL(),"",$body,'MappedClient');
    }

    public static function uploadClientProfilePicture($fileData){
        $body = array('file'=>$fileData['name'],'temp'=>$fileData['tmp_name']);
        return APIRequester::uploadProfilePicture(APISettings::clientChangeProfilePictureURL(),"",$body,"MappedClient");
    }

    public static function changeClientPassword($oldPassword, $newPassword){
        $body = array('oldPassword'=>$oldPassword,'newPassword'=>$newPassword);
        return APIRequester::sendRequest(APISettings::clientChangePasswordURL(),"",$body,"");
    }


    public static function getHosts(){
        $body = array();
        return APIRequester::sendRequest(APISettings::clientGetHostsURL(),"","","UserProfile");
    }

    public static function addHost($hostUsername){
        $body = array('username'=>$hostUsername);
        return APIRequester::sendRequest(APISettings::clientAddHostsURL(),"",$body,"UserProfile");
    }

    public static function getClientAppointments(){
        $body = array();
        return APIRequester::sendRequest(APISettings::clientGetClientAppointmentsURL(),"","","MappedAppointment");
    }

    public static function getHost($sendHostWebID){
        $body = array('hostWebID'=>$sendHostWebID);
        return APIRequester::sendRequest(APISettings::clientGetHostURL(),"",$body,"UserProfile");
    }

    public static function getAccountPermissions($sendHostWebID){
        $body = array('hostWebID'=>$sendHostWebID);
        return APIRequester::sendRequest(APISettings::clientGetAccountPermissionsURL(),"",$body,"MappedClientPermission");
    }


}

