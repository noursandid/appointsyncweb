<?php

class APISettings
{

    //Main URLs

    private static function getAppointSyncHostURL(){
        return "http://nourhost:8080/apiDev/web/host/";
//        return "http://appointsync.com/api/host";
//        return "http://192.168.1.13/apiDev/web/host/";

    }

    private static function getAppointSyncClientURL(){
        return "http://nourhost:8080/apiDev/web/client/";
    }


    //For host


    public static function hostRegisterHostURL()
    {
        return APISettings::getAppointSyncHostURL() .  "handler/registerHost.php";
    }

    public static function hostSetResetPasswordURL()
    {
        return APISettings::getAppointSyncHostURL() .  "handler/resetPassword.php";
    }
    //Appointments APIs


    public static function hostAcceptAppointment()
    {
        return APISettings::getAppointSyncHostURL() .  "appointment/acceptAppointment.php";
    }

    public static function hostGetAppointmentsURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/getAppointments.php";
    }

    public static function hostResetPasswordURL()
    {
        return APISettings::getAppointSyncHostURL() . "handler/forgotPassword.php";
    }

    public static function hostGetAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/getAppointment.php";
    }

    public static function hostAddAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/addAppointment.php";
    }

    public static function hostAmendAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/amendAppointment.php";
    }

    public static function hostCancelAmendmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/cancelAmendment.php";
    }

    public static function hostDeleteAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/deleteAppointment.php";
    }

    public static function hostRejectAppointmentURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/rejectAppointment.php";
    }

    public static function hostShiftAppointmentsURL()
    {
        return APISettings::getAppointSyncHostURL() . "shiftAppointments.php";
    }


    public static function hostChangeSecureFileFlagURL()
    {
        return APISettings::getAppointSyncHostURL() . "/file/changeSecureFile.php";
    }


    //Clients

    public static function hostGetClientsURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/getClients.php";
    }


    public static function hostGetClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/getClient.php";
    }


    public static function hostAddClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/addClient.php";
    }

    public static function linkClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/linkClient.php";
    }
    public static function hostAddClientWithQRCodeURL()
    {
        return APISettings::getAppointSyncHostURL() . "addClientWithQRCode.php";
    }

    public static function hostGetClientWithQRCodeURL()
    {
        return APISettings::getAppointSyncHostURL() . "getClientWithQRCode.php";
    }

    public static function hostRemoveClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/removeClient.php";
    }
    public static function hostGetClientsPermissions()
    {
        return APISettings::getAppointSyncHostURL() . "client/getClientsPermissions.php";
    }

    public static function hostGetClientPermissions()
    {
        return APISettings::getAppointSyncHostURL() . "client/getClientPermissions.php";
    }


    public static function hostChangeUserPermissionURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/updateClientPermissions.php";
    }

    //Offline clients

    public static function hostAddOfflineClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/addOfflineClient.php";
    }

    public static function hostGetOfflineClientsURL()
    {
        return APISettings::getAppointSyncHostURL() . "getOfflineClients.php";
    }

    public static function hostLinkClientURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/linkClient.php";
    }



    //Profile

    public static function getProfileURL()
    {
        return APISettings::getAppointSyncHostURL() . "profile/getProfile.php";
    }


    public static function HostRenewPremiumSubscription()
    {
        return APISettings::getAppointSyncHostURL() .  "profile/updatePremium.php";
    }


    public static function hostLoginURL()
    {
        return APISettings::getAppointSyncHostURL() . "handler/login.php";
    }


    public static function hostRegisterHostPremiumURL()
    {
        return APISettings::getAppointSyncHostURL() . "registerHostPremium.php";
    }


    public static function hostChangeProfilePictureURL()
    {
        return APISettings::getAppointSyncHostURL() . "profile/editProfilePic.php";
    }


  public static function hostChangePasswordURL()
    {
        return APISettings::getAppointSyncHostURL() . "handler/changePassword.php";
    }


  public static function hostChangePermissionURL()
    {
        return APISettings::getAppointSyncHostURL() . "client/updateClientsPermissions.php";
    }


  public static function hostChangeLoggedInAsFlagURL()
    {
        return APISettings::getAppointSyncHostURL() . "handler/switchViews.php";
    }



  public static function hostEditProfileURL()
    {
        return APISettings::getAppointSyncHostURL() . "profile/editProfile.php";
    }




  public static function hostVerifyPinURL()
    {
        return APISettings::getAppointSyncHostURL() . "file/verifyPin.php";
    }



  public static function hostChangeTemplatePinURL()
    {
        return APISettings::getAppointSyncHostURL() . "file/changeTemplatePin.php";
    }




    //Notes

    public static function hostAddNoteURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/addNote.php";
    }

    public static function hostGetAppointmentAttachmentsURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/getAttachments.php";
    }

    public static function hostAddAppointmentAttachmentsURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/addAttachment.php";
    }


    public static function hostGetAppointmentNotesURL()
    {
        return APISettings::getAppointSyncHostURL() . "appointment/getNotes.php";
    }


    //Location

    public static function hostAddLocationURL()
    {
        return APISettings::getAppointSyncHostURL() . "location/addLocation.php";
    }

    public static function hostDeleteLocationURL()
    {
        return APISettings::getAppointSyncHostURL() . "location/deleteLocation.php";
    }

    public static function hostGetLocationsURL()
    {
        return APISettings::getAppointSyncHostURL() . "location/getLocations.php";
    }

    public static function hostUpdateLocationURL()
    {
        return APISettings::getAppointSyncHostURL() . "updateLocation.php";
    }


    //Notification

    public static function hostGetNotificationURL()
    {
        return APISettings::getAppointSyncHostURL() . "notification/getNotifications.php";
    }

    public static function hostReadNotificationURL()
    {
        return APISettings::getAppointSyncHostURL() . "notification/readNotifications.php";
    }


//Files

    public static function hostGetFileTemplateURL()
    {
        return APISettings::getAppointSyncHostURL() . "file/getFilesPages.php";
    }

    public static function hostGetFilePageURL()
    {
        return APISettings::getAppointSyncHostURL() . "file/getFilePage.php";
    }

    public static function hostAddFilePageURL()
    {
        return APISettings::getAppointSyncHostURL() . "file/addFile.php";
    }

    public static function hostUpdateFileValueURL(){
        return APISettings::getAppointSyncHostURL() . "file/updateFileValue.php";
    }

   public static function hostUpdateFileNoteURL(){
        return APISettings::getAppointSyncHostURL() . "file/updateFileNote.php";
    }

   public static function hostAddFileAttachmentURL(){
        return APISettings::getAppointSyncHostURL() . "file/addAttachment.php";
    }

   public static function hostGetPaymentsURL(){
        return APISettings::getAppointSyncHostURL() . "file/getPayments.php";
    }


   public static function hostSaveCurrencyURL(){
        return APISettings::getAppointSyncHostURL() . "file/updateCurrency.php";
    }


   public static function hostAddPaymentDueURL(){
        return APISettings::getAppointSyncHostURL() . "file/addPaymentDue.php";
    }


   public static function hostAddPaymentReceivedURL(){
        return APISettings::getAppointSyncHostURL() . "file/addPaymentReceived.php";
    }


   public static function hostGetAllTemplatesURL(){
        return APISettings::getAppointSyncHostURL() . "file/getTemplates.php";
    }


    //Messages


   public static function hostGetThreadsURL(){
        return APISettings::getAppointSyncHostURL() . "message/getThreads.php";
    }

   public static function hostGetMessagesURL(){
        return APISettings::getAppointSyncHostURL() . "message/getMessages.php";
    }

   public static function hostPostContactMessageURL(){
        return APISettings::getAppointSyncHostURL() . "message/postMessage.php";
    }


    //Clients

    public static function clientRegisterClientURL(){
        return APISettings::getAppointSyncClientURL() . "handler/registerClient.php";
    }

    public static function clientGetProfileURL(){
        return APISettings::getAppointSyncClientURL() . "profile/getProfile.php";
    }

    public static function clientEditProfileURL(){
        return APISettings::getAppointSyncClientURL() . "profile/editProfile.php";
    }

    public static function clientChangeProfilePictureURL(){
        return APISettings::getAppointSyncClientURL() . "profile/editProfilePic.php";
    }

    public static function clientChangePasswordURL(){
        return APISettings::getAppointSyncClientURL() . "handler/changePassword.php";
    }

    public static function clientGetHostsURL(){
        return APISettings::getAppointSyncClientURL() . "host/getHosts.php";
    }

    public static function clientGetHostURL(){
        return APISettings::getAppointSyncClientURL() . "host/getHost.php";
    }

    public static function clientAddHostsURL(){
        return APISettings::getAppointSyncClientURL() . "host/addHost.php";
    }


    public static function clientGetClientAppointmentsURL(){
        return APISettings::getAppointSyncClientURL() . "appointment/getAppointments.php";
    }


    public static function clientGetAccountPermissionsURL(){
        return APISettings::getAppointSyncClientURL() . "host/getClientPermissions.php";
    }



}
