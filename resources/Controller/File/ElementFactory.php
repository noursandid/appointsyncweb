<?php
/**
 * Created by PhpStorm.
 * User: cgsawma
 * Date: 8/13/18
 * Time: 7:21 PM
 */
include_once dirname(__FILE__) . '/../../Classes/MappedElement.php';
include_once dirname(__FILE__) . '/../../Classes/MappedElementKeyValue.php';
?>
<html>
<head/>
<body>

<style>


    #ElementCheckBox{

        border:rgba(0,0,0,0.2) solid 1px;
        border-radius: 5px;
        height: 100px;
        margin:3% auto 0 2%;
        width: 48%;
        overflow: hidden;
        display: inline-block;
        float:left;
    }

    /*#ElementTeeth{*/

        /*border:rgba(0,0,0,0.2) solid 1px;*/
        /*border-radius: 5px;*/
        /*margin:3% auto 0 2%;*/
        /*width: 48%;*/
        /*height: 35%;*/
        /*overflow: hidden;*/
        /*display: inline-block;*/
        /*float:left;*/
        /*max-height: 200px;*/
        /*max-width: 600px;*/
        /*min-height: 200px;*/
        /*min-width: 500px;*/
    /*}*/


    #ElementTeeth{

        border:rgba(0,0,0,0.2) solid 1px;
        border-radius: 5px;
        margin:3% auto 0 2%;
        width: 48%;
        height: 35%;
        overflow: hidden;
        display: inline-block;
        float:left;
        max-height: 200px;
        max-width: 600px;
        min-height: 100px;
        min-width: 250px;
    }


    #ElementCheckBox .elementValue {
        padding: 7% 0 0 8%;
    }

    #ElementCheckBox .elementName {
        padding: 5% 5% 0 5%;
        min-width: 120px;
        overflow: hidden;
        font-size: 90%;

    }



    .elementName{

        width:35%;
        /*min-width: 250px;*/
        float:left;
        height: 100%;
        border-right: rgba(0,0,0,0.2) solid 1px;
        text-align: center;
        font-weight: bold ;

    }

    .elementValue{

        width:32%;
        /*min-width: 150px;*/
        float:left;

    }

    .elementNameTeeth{

        width:30%;
        min-width: 140px;
        float:left;
        padding-top:15%;
        height: 100%;
        border-right: rgba(0,0,0,0.2) solid 1px;
        text-align: center;
        font-weight: bold ;
    }

    .elementValueTeeth{

        margin:0 0 0 0.21%;
        width:6%;
        float:left;
        text-align:center;
        z-index: 1;

    }
    .elementValueTeethRed{

        margin:0 0 0 0.21%;
        width:6%;
        float:left;
        text-align:center;
        z-index: 1;
        background: red;
    }
    .elementValueTeethGreen{

        margin:0 0 0 0.21%;
        width:6%;
        float:left;
        text-align:center;
        z-index: 1;
        background: green;
    }
    .elementValueTeethPurple{

        margin:0 0 0 0.21%;
        width:6%;
        float:left;
        text-align:center;
        z-index: 1;
        background: purple;
    }

   .upperTooth{

       border-bottom:black solid 1px;

   }

   .upperCenterTooth
   {
       /*border-bottom:black solid 1px;*/
       padding-bottom: 3px;
       border-right: black solid 1px;
   }
    .elementValueTeethRed img, .elementValueTeethGreen img, .elementValueTeethPurple img{

        opacity:0.6;
    }

    .elementNameTeethContainer{


        height: 210px;
        z-index: 5;
        min-width: 150px;
        margin-top:5%;
        /*background-image: url("../img/teethZabre.png");*/
        /*background-repeat: no-repeat;*/
        /*background-size: contain;*/
        overflow: auto;
        max-height: 210px;
        max-width: 450px;

    }

    .elementValueTeeth:hover{
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);
    }

    .elementGeneralBoxTitle{
    }

    .elementGeneralBoxValue span{
        width:23%;
        float:left;
        font-size: 0.8vw;
    }

    .elementGeneralBoxValue input{
        width:74%;
        float:left;
    }


    .elementGeneralBoxValue{
        float:left;
        margin-top:2%;
        width:50%;
        min-width: 300px !important;
    }



</style>

<div>
    <?php
    class ElementFactory
    {



        private static $boxPost = '</div>';

        public static function createFactory($clientWebID,$pageWebID){
            echo '<script>var clientWebID =\''.$clientWebID.'\' </script>';
            echo '<script>var pageWebID =\''.$pageWebID.'\' </script>';
        }

        public static function getElement($element){
//Kwudnc5Q0h8RkRd550fd61eb695

            switch ($element->typeWebID){
                case "hBsHxxMsun0GbtZe08083a192e7":
                    $boxPre = '<div id="elementGeneralBox">';
                    $body = $boxPre;
                    $body .= '<div class="elementGeneralBoxTitle"><u><strong>'.$element->name.'</strong></u></div>';
                    $body .= '<div class="elementGeneralBoxValue"><span>'.$element->dataKeyValueList[0]->key.'</span><input type="text"  id="'.$element->dataKeyValueList[0]->webID.$element->webID.'" value="'.$element->dataKeyValueList[0]->value.'" onkeyup="onKeyUp'.$element->dataKeyValueList[0]->webID.$element->webID.'()"></div>';
                    $body .= '<div class="elementGeneralBoxValue"><span>'.$element->dataKeyValueList[1]->key.'</span><input type="text"  id="'.$element->dataKeyValueList[1]->webID.$element->webID.'" value="'.$element->dataKeyValueList[1]->value.'" onkeyup="onKeyUp'.$element->dataKeyValueList[1]->webID.$element->webID.'()"></div>';
                    $body .= '<div class="elementGeneralBoxValue"><span>'.$element->dataKeyValueList[2]->key.'</span><input type="text"  id="'.$element->dataKeyValueList[2]->webID.$element->webID.'" value="'.$element->dataKeyValueList[2]->value.'" onkeyup="onKeyUp'.$element->dataKeyValueList[2]->webID.$element->webID.'()"></div>';
                    $body .= '<div class="elementGeneralBoxValue"><span>'.$element->dataKeyValueList[3]->key.'</span><input type="text"  id="'.$element->dataKeyValueList[3]->webID.$element->webID.'" value="'.$element->dataKeyValueList[3]->value.'" onkeyup="onKeyUp'.$element->dataKeyValueList[3]->webID.$element->webID.'()"></div>';



                    $body .= ElementFactory::$boxPost.'<br/>';


//                    foreach ($element->dataKeyValueList as $list){
//                        echo '<script>';
//                        echo 'function ayre' . $list->webID . $element->webID . '(key){';
//                        echo 'console.log(document.getElementById(key).value);';
//                        echo '}';
//                        echo '</script>';
//                    }


                    echo '<script>';
                foreach ($element->dataKeyValueList as $list) {



                    echo    '$( document ).ready(function() {
                            window.x' . $list->webID . $element->webID . ' = document.getElementById("' . $list->webID . $element->webID . '").value;
                            });';

                    echo '  var typingTimer' . $list->webID . $element->webID . ';
                            var doneTypingInterval' . $list->webID . $element->webID . ' = 5000;
                            var $input' . $list->webID . $element->webID . ' = $(\'#' . $list->webID . $element->webID . '\');
                            function onKeyUp' . $list->webID . $element->webID . '() {
                                clearTimeout(typingTimer' . $list->webID . $element->webID . ');
                                typingTimer' . $list->webID . $element->webID . ' = setTimeout(doneTyping' . $list->webID . $element->webID . ', doneTypingInterval' . $list->webID . $element->webID . ');
                            };
                            $input' . $list->webID . $element->webID . '.on(\'keydown\', function () {
                                clearTimeout(typingTimer' . $list->webID . $element->webID . ');
                            });
                            function doneTyping' . $list->webID . $element->webID . ' () {
                                var keyWebID = \''.$list->webID.'\';
                                var elementWebID = \''.$element->webID.'\';
                                var y = document.getElementById("' . $list->webID . $element->webID . '").value;
                                if(x != y){
                                    window.x = document.getElementById("' . $list->webID . $element->webID . '").value;
                                    $.ajax({
                                        type: "POST",
                                        url:  "/appointsync.com/resources/Controller/API/APIRouter.php",
                                        data: {\'submit\':\'updateValue\',\'clientWebID\':clientWebID,\'pageWebID\':pageWebID,\'keyWebID\':keyWebID,\'elementWebID\':elementWebID,\'value\':document.getElementById("' . $list->webID . $element->webID . '").value},
                                    });
                                }
                            }';


                }
                    echo 'window.addEventListener("beforeunload", function() {';
                    foreach ($element->dataKeyValueList as $list) {
                        echo '  var y' . $list->webID . $element->webID . ' = document.getElementById("' . $list->webID . $element->webID . '").value;
                                var keyWebID = \''.$list->webID.'\';
                                var elementWebID = \''.$element->webID.'\';
                                if (x' . $list->webID . $element->webID . ' != y' . $list->webID . $element->webID . ') {
                                    $.ajax({
                                        type: "POST",
                                        url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                                        data: {
                                            \'submit\': \'updateValue\',
                                            \'clientWebID\': clientWebID,
                                            \'pageWebID\': pageWebID,
                                            \'keyWebID\': keyWebID,
                                            \'elementWebID\': elementWebID,
                                            \'value\': document.getElementById("' . $list->webID . $element->webID . '").value
                                        },
                                        async: false,
                                    });
                                }';

                    }
                    echo '});';

                    echo '</script>';



                    return $body;
                    break;

                case "Kwudnc5Q0h8RkRd550fd61eb695":
                    $boxPre = '<div id="ElementTeeth">';
                    $body = $boxPre;
                    $body .= '<div class="elementNameTeeth">'.$element->name.'</div>';
                    $body .= '<div class="elementNameTeethContainer">';
                    $body .= '<div id="'.$element->dataKeyValueList[15]->webID.$element->webID. '" class="'.($element->dataKeyValueList[15]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[15]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[15]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[15]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[15]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[15]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth16.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[14]->webID.$element->webID.'" class="'.($element->dataKeyValueList[14]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[14]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[14]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[14]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[14]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[14]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth15.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[13]->webID.$element->webID.'" class="'.($element->dataKeyValueList[13]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[13]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[13]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[13]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[13]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[13]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth14.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[12]->webID.$element->webID.'" class="'.($element->dataKeyValueList[12]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[12]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[12]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[12]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[12]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[12]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth13.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[11]->webID.$element->webID.'" class="'.($element->dataKeyValueList[11]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[11]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[11]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[11]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[11]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[11]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth12.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[10]->webID.$element->webID.'" class="'.($element->dataKeyValueList[10]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[10]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[10]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[10]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[10]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[10]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth11.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[9]->webID.$element->webID.'" class="'.($element->dataKeyValueList[9]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[9]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[9]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[9]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[9]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[9]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth10.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[8]->webID.$element->webID.'" class="'.($element->dataKeyValueList[8]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[8]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[8]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[8]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[8]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[8]->value.'\')"><img class="upperTooth upperCenterTooth" src="/appointsync.com/public/img/teeth/tooth9.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[7]->webID.$element->webID.'" class="'.($element->dataKeyValueList[7]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[7]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[7]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[7]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[7]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[7]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth8.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[6]->webID.$element->webID.'" class="'.($element->dataKeyValueList[6]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[6]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[6]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[6]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[6]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[6]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth7.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[5]->webID.$element->webID.'" class="'.($element->dataKeyValueList[5]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[5]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[5]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[5]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[5]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[5]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth6.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[4]->webID.$element->webID.'" class="'.($element->dataKeyValueList[4]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[4]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[4]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[4]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[4]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[4]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth5.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[3]->webID.$element->webID.'" class="'.($element->dataKeyValueList[3]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[3]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[3]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[3]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[3]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[3]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth4.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[2]->webID.$element->webID.'" class="'.($element->dataKeyValueList[2]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[2]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[2]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[2]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[2]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[2]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth3.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[1]->webID.$element->webID.'" class="'.($element->dataKeyValueList[1]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[1]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[1]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[1]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[1]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[1]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth2.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[0]->webID.$element->webID.'" class="'.($element->dataKeyValueList[0]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[0]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[0]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[0]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[0]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[0]->value.'\')"><img class="upperTooth" src="/appointsync.com/public/img/teeth/tooth1.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[16]->webID.$element->webID.'" class="'.($element->dataKeyValueList[16]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[16]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[16]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[16]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[16]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[16]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth17.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[17]->webID.$element->webID.'" class="'.($element->dataKeyValueList[17]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[17]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[17]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[17]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[17]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[17]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth18.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[18]->webID.$element->webID.'" class="'.($element->dataKeyValueList[18]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[18]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[18]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[18]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[18]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[18]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth19.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[19]->webID.$element->webID.'" class="'.($element->dataKeyValueList[19]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[19]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[19]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[19]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[19]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[19]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth20.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[20]->webID.$element->webID.'" class="'.($element->dataKeyValueList[20]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[20]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[20]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[20]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[20]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[20]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth21.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[21]->webID.$element->webID.'" class="'.($element->dataKeyValueList[21]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[21]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[21]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[21]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[21]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[21]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth22.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[22]->webID.$element->webID.'" class="'.($element->dataKeyValueList[22]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[22]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[22]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[22]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[22]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[22]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth23.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[23]->webID.$element->webID.'" class="'.($element->dataKeyValueList[23]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[23]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[23]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[23]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[23]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[23]->value.'\')"><img class="upperCenterTooth" src="/appointsync.com/public/img/teeth/tooth24.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[24]->webID.$element->webID.'" class="'.($element->dataKeyValueList[24]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[24]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[24]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[24]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[24]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[24]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth25.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[25]->webID.$element->webID.'" class="'.($element->dataKeyValueList[25]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[25]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[25]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[25]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[25]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[25]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth26.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[26]->webID.$element->webID.'" class="'.($element->dataKeyValueList[26]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[26]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[26]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[26]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[26]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[26]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth27.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[27]->webID.$element->webID.'" class="'.($element->dataKeyValueList[27]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[27]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[27]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[27]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[27]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[27]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth28.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[28]->webID.$element->webID.'" class="'.($element->dataKeyValueList[28]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[28]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[28]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[28]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[28]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[28]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth29.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[29]->webID.$element->webID.'" class="'.($element->dataKeyValueList[29]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[29]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[29]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[29]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[29]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[29]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth30.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[30]->webID.$element->webID.'" class="'.($element->dataKeyValueList[30]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[30]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[30]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[30]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[30]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[30]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth31.png"/></a></div>';
                    $body .= '<div id="'.$element->dataKeyValueList[31]->webID.$element->webID.'" class="'.($element->dataKeyValueList[31]->value == '1' ? 'elementValueTeethRed' : ($element->dataKeyValueList[31]->value == '2' ? 'elementValueTeethGreen' : ($element->dataKeyValueList[31]->value == '3' ? 'elementValueTeethPurple' : 'elementValueTeeth'))).'"><a href="#/" id="'.$element->dataKeyValueList[31]->webID.'"onclick="updateToothValue(\''.$element->dataKeyValueList[31]->webID.'\',\''.$element->webID.'\',\''.$element->dataKeyValueList[31]->value.'\')"><img src="/appointsync.com/public/img/teeth/tooth32.png"/></a></div>';
                    $body .= '</div>'.ElementFactory::$boxPost;
                    return $body;
                    break;
                case "q93ieXxnyI5e92E5dadf9bry5ie":
                    $boxPre = '<div id="ElementCheckBox">';
                    $body = $boxPre;
                    $body .= '<div class="elementName">'.$element->name.'</div>';
                    if ($element->dataKeyValueList[0]->value == '1'){
                        $body .= '<div class="elementValue">'.$element->dataKeyValueList[0]->key.'  <input type="checkbox"  id="'.$element->dataKeyValueList[0]->webID.$element->webID.'" onclick="updateCheckBoxValue(\''.$element->dataKeyValueList[0]->webID.'\',\''.$element->webID.'\')" checked></div>';
                    }
                    else{
                        $body .= '<div class="elementValue">'.$element->dataKeyValueList[0]->key.'  <input type="checkbox" id="'.$element->dataKeyValueList[0]->webID.$element->webID.'" onclick="updateCheckBoxValue(\''.$element->dataKeyValueList[0]->webID.'\',\''.$element->webID.'\')"></div>';
                    }

                    if ($element->dataKeyValueList[1]->value == '1'){
                        $body .= '<div class="elementValue">'.$element->dataKeyValueList[1]->key.'  <input type="checkbox" id="'.$element->dataKeyValueList[1]->webID.$element->webID.'" onclick="updateCheckBoxValue(\''.$element->dataKeyValueList[1]->webID.'\',\''.$element->webID.'\')" checked></div>';
                    }
                    else{
                        $body .= '<div class="elementValue">'.$element->dataKeyValueList[1]->key.'  <input type="checkbox" id="'.$element->dataKeyValueList[1]->webID.$element->webID.'" onclick="updateCheckBoxValue(\''.$element->dataKeyValueList[1]->webID.'\',\''.$element->webID.'\')"></div>';
                    }
                    $body .= ElementFactory::$boxPost;
                    return $body;
                    break;
                default:
                    return '';
                    break;

            }

        }

    }

    ?>

</div>
<script>

    function updateToothValue(keyWebID,elementWebID,keyValue){
        if(keyValue == '' || keyValue == 0){
           var keyValueSet = 1;
           document.getElementById(keyWebID+elementWebID).className = "elementValueTeethRed";
        }else if(keyValue == 1){
            var keyValueSet = 2;
            document.getElementById(keyWebID+elementWebID).className = "elementValueTeethGreen";
        }else if(keyValue == 2){
            var keyValueSet = 3;
            document.getElementById(keyWebID+elementWebID).className = "elementValueTeethPurple";
        }else if(keyValue == 3){
            var keyValueSet = 2;
            document.getElementById(keyWebID+elementWebID).className = "elementValueTeethGreen";
        }
        // $("body").load('body');
        document.getElementById(keyWebID+elementWebID).outerHTML = document.getElementById(keyWebID+elementWebID).outerHTML.replace("('"+keyWebID+"','"+elementWebID+"','"+keyValue+"')","('"+keyWebID+"','"+elementWebID+"','"+keyValueSet+"')");
        //'updateToothValue(\''+keyWebID+'\',\''+elementWebID+'\',\''+keyValueSet+'\')'
        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {'submit':'updateValue','clientWebID':clientWebID,'pageWebID':pageWebID,'keyWebID':keyWebID,'elementWebID':elementWebID,'value':keyValueSet},
            success: function (response) {

                // alert(response);

            }
        });
    }


    window.addEventListener("beforeunload", function() {
        var y = document.getElementById("clientFileNotes").value;

        if (x != y) {
            $.ajax({
                type: "POST",
                url: "/appointsync.com/resources/Controller/API/APIRouter.php",
                data: {
                    'submit': 'updateValue',
                    'clientWebID': clientWebID,
                    'pageWebID': pageWebID,
                    'keyWebID': keyWebID,
                    'elementWebID': elementWebID,
                    'value': document.getElementById("' . $list->webID . $element->webID . '").value
                },
                async: false,
            });
        }
    });

    function updateCheckBoxValue(keyWebID,elementWebID){

        // console.log('ClientWebID: ' + clientWebID + ' pageWebID: ' + pageWebID + ' KeyWebID:' + keyWebID + 'ElementWebID: ' + elementWebID);

        $.ajax({
            type: "POST",
            url: "/appointsync.com/resources/Controller/API/APIRouter.php",
            data: {'submit':'updateValue','clientWebID':clientWebID,'pageWebID':pageWebID,'keyWebID':keyWebID,'elementWebID':elementWebID,'value':document.getElementById(keyWebID+elementWebID).checked?'1':'0'},
            success: function (response) {

                // alert(response);

            }
        });


    }


</script>

</body>
</html>